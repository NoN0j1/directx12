#include <cassert>
#include "DirectX12/ResourceManager.h"
#include "DirectX12/Input.h"
#include "DirectX12/Dx12Wrapper.h"
#include "DirectX12/Texture/Texture2D.h"
#include "DirectX12/Mesh/PrimitiveMesh.h"
#include "DirectX12/Object/Actor.h"
#include "Application.h"
#include "DirectX12/Object/Player.h"
#include "DirectX12/Object/Ground/Ground.h"
#include "DirectX12/Material/Material.h"
#include "imgui/imgui.h"


using namespace std;

constexpr int window_width = 1280;
constexpr int window_height = 720;

std::shared_ptr<Actor> testesT;

namespace
{
    auto& resource = ResourceManager::Instance();
}

Application& Application::Instance()
{
    static Application instance_;
    return instance_;
}

extern IMGUI_IMPL_API LRESULT ImGui_ImplWin32_WndProcHandler(HWND hwnd, UINT msg, WPARAM wparam, LPARAM lparam);

LRESULT WindowProcedure(HWND hwnd, UINT msg, WPARAM wparam, LPARAM lparam)
{
    if (msg == WM_DESTROY)
    {
        PostQuitMessage(0);
        return 0;
    }
    ImGui_ImplWin32_WndProcHandler(hwnd, msg, wparam, lparam);
    return DefWindowProcW(hwnd, msg, wparam, lparam);
}

int Application::WindowsOrder(MSG& msg)
{
    if (PeekMessage(&msg, nullptr, 0, 0, PM_REMOVE))
    {
        TranslateMessage(&msg);
        DispatchMessage(&msg);
        if (msg.message == WM_QUIT)
        {
            return -1;
        }
    }
    return 1;
}

bool Application::Init()
{
    HRESULT result = CoInitializeEx(nullptr, COINIT_MULTITHREADED);
    if (FAILED(result))
    {
        assert(false);
        return false;
    }
    
    wclass_.hInstance = GetModuleHandle(nullptr);
    wclass_.cbSize = sizeof(WNDCLASSEX);
    wclass_.lpfnWndProc = (WNDPROC)WindowProcedure;
    wclass_.lpszClassName = L"DirectX12Study2020";
    RegisterClassEx(&wclass_);

    RECT rc = {};
    rc.left = 0;
    rc.top = 0;
    rc.right = window_width;
    rc.bottom = window_height;
    AdjustWindowRect(&rc, WS_OVERLAPPEDWINDOW, false);

    windowSize_.cx = window_width;
    windowSize_.cy = window_height;

    windowHundle_ = CreateWindow(
        wclass_.lpszClassName,
        L"1816009_川野颯英",
        WS_OVERLAPPEDWINDOW & ~WS_MAXIMIZEBOX & ~WS_THICKFRAME,
        CW_USEDEFAULT, CW_USEDEFAULT,
        rc.right - rc.left,
        rc.bottom - rc.top,
        nullptr,
        nullptr,
        wclass_.hInstance,
        nullptr
    );
    if (windowHundle_ == nullptr)
    {
        return false;
    }
    Input::Instance().Init(windowHundle_ ,wclass_.hInstance);
    dx12wrapper_ = make_unique<Dx12Wrapper>(windowHundle_);
    if (!dx12wrapper_->Init())
    {
        return false;
    }
    resource.LoadModel(L"Resource/Model/PMD/model/初音ミク.pmd", L"みく");
    resource.LoadModel(L"Resource/Model/PMD/model/初音ミクmetal.pmd", L"みくめたる");
    resource.LoadModel(L"Resource/Model/PMD/桜ミク/mikuXS雪ミク.pmd", L"ゆきみく");
    resource.LoadModel(L"Resource/Model/PMD/桜ミク/mikuXS桜ミク.pmd", L"さくらみく");
    resource.LoadModel(L"Resource/Model/PMD/柳生/柳生Ver1.12SW.pmd", L"やぎゅう");
    resource.LoadModel(L"Resource/Model/PMD/古明地さとり_abs_152/古明地さとり152Normal.pmd", L"さとり");
    //resource.LoadModel(L"Resource/Model/PMX/kazii式煉獄杏寿郎ver2.0/煉獄杏寿郎.pmx", L"れんごく");
    //resource.LoadModel(L"Resource/Model/PMX/ときのそら公式mmd_ver1.0/ときのそら.pmx", L"そら");
    resource.LoadAnimation(L"Resource/Model/Animation/first.vmd", L"ポーズ1");
    resource.LoadAnimation(L"Resource/Model/Animation/pose.vmd", L"ポーズ2");
    resource.LoadAnimation(L"Resource/Model/Animation/charge.vmd", L"ポーズ3");
    resource.LoadAnimation(L"Resource/Model/Animation/ヤゴコロダンス.vmd", L"ポーズ4");
    resource.LoadAnimation(L"Resource/Model/Animation/swing.vmd", L"ポーズ5");
    resource.LoadAnimation(L"Resource/Model/Animation/swing2.vmd", L"ポーズ6");
    LoadPrimitiveShader();
    LoadPMDShader();
    //LoadPMXShader();
    LoadTextureShader();
    LoadPostProcessShader();
    LoadShrinkShader();
    LoadSSAOShader();
    LoadComputeShader();
    testesT = dx12wrapper_->SpawnActor(Player(), Transform({ 0, 0, 0 }), 0);
    testesT->SetMesh(resource.GetModel(L"みく"), resource.GetShader(L"PmdCullNone"), resource.GetShader(L"PmdShadow"));

    auto gr = dx12wrapper_->SpawnGround(Transform({ 0,0,0 }, {0,0,0}, { 1.0f,1.0f, 1.0f }));
    auto wall = dx12wrapper_->SpawnGround(Transform({ 0,0,50 }, { 90,180,0 }, { 1.0f,1.0f,1.0f }));
    auto tes = &wall->GetMesh();
    auto tes2 = &gr->GetMesh();
    auto& temtem = resource.GetModel(PrimitiveMeshType::Plane);
    dx12wrapper_->CreateInstanceMaterial(wall->GetMesh()->GetMaterialManager());
    dx12wrapper_->CreateInstanceMaterial(gr->GetMesh()->GetMaterialManager());

    wall->GetMesh()->SetMaterialParameter(Material({ 1, 0, 0, 1 }, {}, {}, 10, 3), 0);
    gr->GetMesh()->SetMaterialParameter(Material({ 0, 0, 1, 1 }, {}, {}, 10, 3), 0);
    dx12wrapper_->LoadEffect(u"Resource/Effect/Laser01.efk", L"レーザー");

    return true;
}

void Application::Run()
{
    ShowWindow(windowHundle_, SW_SHOW);
    MSG msg = {};
    //メインループ
    dx12wrapper_->MakeScreen(L"ModelBase");
    dx12wrapper_->MakeScreen(L"ModelNormal");
    dx12wrapper_->MakeScreen(L"ModelHDR");
    dx12wrapper_->MakeScreen(L"ModelShadowMap");
    dx12wrapper_->MakeDepthScreen(L"SubDepth");
    dx12wrapper_->MakeShrinkScreen(L"Bloom");
    dx12wrapper_->MakeShrinkScreen(L"Dof");

    while (WindowsOrder(msg) != -1 && !Input::Instance().GetKey(KeyCode::Escape))
    {
        Input::Instance().UpdateKeyState();
        if (Input::Instance().GetKeyDown(KeyCode::BackSpace))
        {
            dx12wrapper_->PlayEffect(L"レーザー", { -10, 10,-10 });
        }
        ChangeModel();
        //dx12wrapper_->DispathComputeShader(L"演算用");
        dx12wrapper_->UpdateActor();
        dx12wrapper_->DrawCastShadow();
        dx12wrapper_->SetDrawScreen({ L"ModelBase", L"ModelNormal", L"ModelHDR", L"ModelShadowMap" }, { L"MainDepth" });
        dx12wrapper_->DrawAllActor();
        dx12wrapper_->DrawGround();
        dx12wrapper_->DrawEffect();
        dx12wrapper_->DrawSSAO();
        dx12wrapper_->DrawToShrinkBuffer({ L"Bloom", L"Dof" });
        dx12wrapper_->DrawAddScreenToBackBuffer();
        dx12wrapper_->ScreenFlip();
    }
}

void Application::Terminate()
{
    CoUninitialize();
    UnregisterClass(wclass_.lpszClassName, wclass_.hInstance);
}

Application::Application()
{
}

void Application::LoadPrimitiveShader()
{
    vector<D3D12_INPUT_ELEMENT_DESC> inputDesc;
    inputDesc.push_back(D3D12_INPUT_ELEMENT_DESC({ "POSITION", 0, DXGI_FORMAT_R32G32B32A32_FLOAT, 0, D3D12_APPEND_ALIGNED_ELEMENT,D3D12_INPUT_CLASSIFICATION_PER_VERTEX_DATA, 0 }));
    inputDesc.push_back(D3D12_INPUT_ELEMENT_DESC({ "TEXCOORD", 0, DXGI_FORMAT_R32G32_FLOAT, 0, D3D12_APPEND_ALIGNED_ELEMENT,D3D12_INPUT_CLASSIFICATION_PER_VERTEX_DATA, 0 }));
    inputDesc.push_back(D3D12_INPUT_ELEMENT_DESC({ "NORMAL", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, D3D12_APPEND_ALIGNED_ELEMENT,D3D12_INPUT_CLASSIFICATION_PER_VERTEX_DATA, 0 }));

    ShaderDetailed vertexShader(L"Shader/VS_Primitive.hlsl", "main", "vs_5_1");
    ShaderDetailed pixelShader(L"Shader/PS_Primitive.hlsl", "main", "ps_5_1");
    PipelineDescSetting pSetting(RenderTargetBlend::Blend, RasterizerCullingState::None, true, inputDesc);
    if (resource.LoadShader(&ShaderParameter(L"Primitive", &pSetting, &vertexShader, &pixelShader)) == false)
    {
        assert(0);
    }

    inputDesc.clear();//一旦初期化
    inputDesc.push_back(D3D12_INPUT_ELEMENT_DESC({ "POSITION", 0, DXGI_FORMAT_R32G32B32A32_FLOAT, 0, D3D12_APPEND_ALIGNED_ELEMENT, D3D12_INPUT_CLASSIFICATION_PER_VERTEX_DATA, 0 }));

    ShaderDetailed shadowVS(L"Shader/VS_PrimitiveShadowShader.hlsl", "main", "vs_5_1");
    PipelineDescSetting pShadowSetting(RenderTargetBlend::None, RasterizerCullingState::None, true, inputDesc);
    if (resource.LoadShader(&ShaderParameter(L"PrimitiveShadow", &pShadowSetting, &shadowVS)) == false)
    {
        assert(0);
    }
}

void Application::LoadPMDShader()
{
    vector<D3D12_INPUT_ELEMENT_DESC> inputDesc;
    inputDesc.push_back(D3D12_INPUT_ELEMENT_DESC({ "POSITION", 0, DXGI_FORMAT_R32G32B32A32_FLOAT, 0, D3D12_APPEND_ALIGNED_ELEMENT, D3D12_INPUT_CLASSIFICATION_PER_VERTEX_DATA, 0 }));
    inputDesc.push_back(D3D12_INPUT_ELEMENT_DESC({ "TEXCOORD", 0, DXGI_FORMAT_R32G32_FLOAT, 0, D3D12_APPEND_ALIGNED_ELEMENT,D3D12_INPUT_CLASSIFICATION_PER_VERTEX_DATA, 0 }));
    inputDesc.push_back(D3D12_INPUT_ELEMENT_DESC({ "NORMAL", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, D3D12_APPEND_ALIGNED_ELEMENT, D3D12_INPUT_CLASSIFICATION_PER_VERTEX_DATA, 0 }));
    inputDesc.push_back(D3D12_INPUT_ELEMENT_DESC({ "BONENO", 0, DXGI_FORMAT_R16G16_UINT, 0, D3D12_APPEND_ALIGNED_ELEMENT,D3D12_INPUT_CLASSIFICATION_PER_VERTEX_DATA, 0 }));
    inputDesc.push_back(D3D12_INPUT_ELEMENT_DESC({ "WEIGHT", 0, DXGI_FORMAT_R32_FLOAT, 0, D3D12_APPEND_ALIGNED_ELEMENT,D3D12_INPUT_CLASSIFICATION_PER_VERTEX_DATA, 0 }));

    ShaderDetailed vertexShader(L"Shader/VS_PmdShader.hlsl", "main", "vs_5_1");
    ShaderDetailed pixelShader(L"Shader/PS_PmdShader.hlsl", "main", "ps_5_1");
    PipelineDescSetting pSetting(RenderTargetBlend::AlphaBlend, RasterizerCullingState::Back, true, inputDesc);
    if (resource.LoadShader(&ShaderParameter(L"PmdCullBack",&pSetting, &vertexShader, &pixelShader)) == false)
    {
        assert(0);
    }
    pSetting.cullState = RasterizerCullingState::None;
    if (resource.LoadShader(&ShaderParameter(L"PmdCullNone", &pSetting, &vertexShader, &pixelShader)) == false)
    {
        assert(0);
    }

    ShaderDetailed shadowVS(L"Shader/VS_PmdShadowShader.hlsl", "main", "vs_5_1");
    PipelineDescSetting pShadowSetting(RenderTargetBlend::None, RasterizerCullingState::Back, true, inputDesc);
    if (resource.LoadShader(&ShaderParameter(L"PmdShadow",&pShadowSetting, &shadowVS)) == false)
    {
        assert(0);
    }
}

void Application::LoadPMXShader()
{
    vector<D3D12_INPUT_ELEMENT_DESC> inputDesc;
    inputDesc.push_back(D3D12_INPUT_ELEMENT_DESC({ "POSITION", 0, DXGI_FORMAT_R32G32B32A32_FLOAT, 0, D3D12_APPEND_ALIGNED_ELEMENT, D3D12_INPUT_CLASSIFICATION_PER_VERTEX_DATA, 0 }));
    inputDesc.push_back(D3D12_INPUT_ELEMENT_DESC({ "TEXCOORD", 0, DXGI_FORMAT_R32G32_FLOAT, 0, D3D12_APPEND_ALIGNED_ELEMENT,D3D12_INPUT_CLASSIFICATION_PER_VERTEX_DATA, 0 }));
    inputDesc.push_back(D3D12_INPUT_ELEMENT_DESC({ "NORMAL", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, D3D12_APPEND_ALIGNED_ELEMENT, D3D12_INPUT_CLASSIFICATION_PER_VERTEX_DATA, 0 }));
    inputDesc.push_back(D3D12_INPUT_ELEMENT_DESC({ "BONENO", 0, DXGI_FORMAT_R32G32B32A32_UINT, 0, D3D12_APPEND_ALIGNED_ELEMENT,D3D12_INPUT_CLASSIFICATION_PER_VERTEX_DATA, 0 }));
    inputDesc.push_back(D3D12_INPUT_ELEMENT_DESC({ "WEIGHT", 0, DXGI_FORMAT_R32G32B32A32_FLOAT, 0, D3D12_APPEND_ALIGNED_ELEMENT,D3D12_INPUT_CLASSIFICATION_PER_VERTEX_DATA, 0 }));
    inputDesc.push_back(D3D12_INPUT_ELEMENT_DESC({ "SDEF_C", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, D3D12_APPEND_ALIGNED_ELEMENT,D3D12_INPUT_CLASSIFICATION_PER_VERTEX_DATA, 0 }));
    inputDesc.push_back(D3D12_INPUT_ELEMENT_DESC({ "SDEF_Rzero", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, D3D12_APPEND_ALIGNED_ELEMENT,D3D12_INPUT_CLASSIFICATION_PER_VERTEX_DATA, 0 }));
    inputDesc.push_back(D3D12_INPUT_ELEMENT_DESC({ "SDEF_Rone", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, D3D12_APPEND_ALIGNED_ELEMENT,D3D12_INPUT_CLASSIFICATION_PER_VERTEX_DATA, 0 }));
    inputDesc.push_back(D3D12_INPUT_ELEMENT_DESC({ "BONETYPE", 0, DXGI_FORMAT_R32_UINT, 0, D3D12_APPEND_ALIGNED_ELEMENT,D3D12_INPUT_CLASSIFICATION_PER_VERTEX_DATA, 0 }));

    ShaderDetailed vertexShader(L"Shader/VS_PmxShader.hlsl", "main", "vs_5_1");
    ShaderDetailed pixelShader(L"Shader/PS_PmxShader.hlsl", "main", "ps_5_1");
    PipelineDescSetting pSetting(RenderTargetBlend::AlphaBlend, RasterizerCullingState::Back, true, inputDesc);
    if (resource.LoadShader(&ShaderParameter(L"PmxCullBack", &pSetting, &vertexShader, &pixelShader)) == false)
    {
        assert(0);
    }
    pSetting.cullState = RasterizerCullingState::None;
    if (resource.LoadShader(&ShaderParameter(L"PmxCullNone", &pSetting, &vertexShader, &pixelShader)) == false)
    {
        assert(0);
    }

    ShaderDetailed shadowVS(L"Shader/VS_PmdShadowShader.hlsl", "main", "vs_5_1");
    PipelineDescSetting pShadowSetting(RenderTargetBlend::None, RasterizerCullingState::Back, true, inputDesc);
    if (resource.LoadShader(&ShaderParameter(L"PmxShadow", &pShadowSetting, &shadowVS)) == false)
    {
        assert(0);
    }
}

void Application::LoadTextureShader()
{
    vector<D3D12_INPUT_ELEMENT_DESC> inputDesc;
    inputDesc.push_back(D3D12_INPUT_ELEMENT_DESC({ "POSITION", 0, DXGI_FORMAT_R32G32B32A32_FLOAT, 0, D3D12_APPEND_ALIGNED_ELEMENT,D3D12_INPUT_CLASSIFICATION_PER_VERTEX_DATA, 0 }));
    inputDesc.push_back(D3D12_INPUT_ELEMENT_DESC({ "TEXCOORD", 0, DXGI_FORMAT_R32G32_FLOAT, 0, D3D12_APPEND_ALIGNED_ELEMENT,D3D12_INPUT_CLASSIFICATION_PER_VERTEX_DATA, 0 }));

    ShaderDetailed pixelShader(L"Shader/PS_Texture.hlsl", "main", "ps_5_1");
    ShaderDetailed vertexShader(L"Shader/VS_Texture.hlsl", "main", "vs_5_1");
    PipelineDescSetting pSetting(RenderTargetBlend::AlphaBlend, RasterizerCullingState::Back, false, inputDesc);
    if (resource.LoadShader(&ShaderParameter(L"Texture", &pSetting, &vertexShader, &pixelShader)) == false)
    {
        assert(0);
    }
}

void Application::LoadPostProcessShader()
{
    vector<D3D12_INPUT_ELEMENT_DESC> inputDesc;
    inputDesc.push_back(D3D12_INPUT_ELEMENT_DESC({ "POSITION", 0, DXGI_FORMAT_R32G32B32A32_FLOAT, 0, D3D12_APPEND_ALIGNED_ELEMENT,D3D12_INPUT_CLASSIFICATION_PER_VERTEX_DATA, 0 }));
    inputDesc.push_back(D3D12_INPUT_ELEMENT_DESC({ "TEXCOORD", 0, DXGI_FORMAT_R32G32_FLOAT, 0, D3D12_APPEND_ALIGNED_ELEMENT,D3D12_INPUT_CLASSIFICATION_PER_VERTEX_DATA, 0 }));
    ShaderDetailed pixelShader(L"Shader/PS_PostProcess.hlsl", "main", "ps_5_1");
    ShaderDetailed vertexShader(L"Shader/VS_PostProcess.hlsl", "main", "vs_5_1");
    PipelineDescSetting pSetting(RenderTargetBlend::AlphaBlend, RasterizerCullingState::Back, false, inputDesc);
    if (resource.LoadShader(&ShaderParameter(L"PostProcess", &pSetting, &vertexShader, &pixelShader)) == false)
    {
        assert(0);
    }
}

void Application::LoadShrinkShader()
{
    vector<D3D12_INPUT_ELEMENT_DESC> inputDesc;
    inputDesc.push_back(D3D12_INPUT_ELEMENT_DESC({ "POSITION", 0, DXGI_FORMAT_R32G32B32A32_FLOAT, 0, D3D12_APPEND_ALIGNED_ELEMENT,D3D12_INPUT_CLASSIFICATION_PER_VERTEX_DATA, 0 }));
    inputDesc.push_back(D3D12_INPUT_ELEMENT_DESC({ "TEXCOORD", 0, DXGI_FORMAT_R32G32_FLOAT, 0, D3D12_APPEND_ALIGNED_ELEMENT,D3D12_INPUT_CLASSIFICATION_PER_VERTEX_DATA, 0 }));
    ShaderDetailed pixelShader(L"Shader/PS_Shrink.hlsl", "main", "ps_5_1");
    ShaderDetailed vertexShader(L"Shader/VS_Shrink.hlsl", "main", "vs_5_1");
    PipelineDescSetting pSetting(RenderTargetBlend::AlphaBlend, RasterizerCullingState::Back, false, inputDesc);
    if (resource.LoadShader(&ShaderParameter(L"Shrink", &pSetting, &vertexShader, &pixelShader)) == false)
    {
        assert(0);
    }
}

void Application::LoadSSAOShader()
{
    vector<D3D12_INPUT_ELEMENT_DESC> inputDesc;
    inputDesc.push_back(D3D12_INPUT_ELEMENT_DESC({ "POSITION", 0, DXGI_FORMAT_R32G32B32A32_FLOAT, 0, D3D12_APPEND_ALIGNED_ELEMENT,D3D12_INPUT_CLASSIFICATION_PER_VERTEX_DATA, 0 }));
    inputDesc.push_back(D3D12_INPUT_ELEMENT_DESC({ "TEXCOORD", 0, DXGI_FORMAT_R32G32_FLOAT, 0, D3D12_APPEND_ALIGNED_ELEMENT,D3D12_INPUT_CLASSIFICATION_PER_VERTEX_DATA, 0 }));
    ShaderDetailed pixelShader(L"Shader/PS_SSAO.hlsl", "main", "ps_5_1");
    ShaderDetailed vertexShader(L"Shader/VS_SSAO.hlsl", "main", "vs_5_1");
    PipelineDescSetting pSetting(RenderTargetBlend::None, RasterizerCullingState::Back, false, inputDesc, 8, DXGI_FORMAT_R32_FLOAT);
    if (resource.LoadShader(&ShaderParameter(L"SSAO", &pSetting, &vertexShader, &pixelShader)) == false)
    {
        assert(0);
    }
}

void Application::LoadComputeShader()
{
    ShaderDetailed computeShader(L"Shader/ComputeShader.hlsl", "main", "cs_5_1");
    if (resource.LoadComputeShader(computeShader, L"演算用") == false)
    {
        assert(0);
    }
}

Application::~Application()
{
}

const SIZE Application::GetWindowSize() const
{
    return windowSize_;
}

void Application::ChangeModel()
{
    if (Input::Instance().GetKeyDown(KeyCode::KEY_1, DoublePush::Ctr))
    {
        testesT->SetMesh(resource.GetModel(L"みく"), resource.GetShader(L"PmdCullNone"), resource.GetShader(L"PmdShadow"));
    }
    if (Input::Instance().GetKeyDown(KeyCode::KEY_2, DoublePush::Ctr))
    {
        testesT->SetMesh(resource.GetModel(L"ゆきみく"), resource.GetShader(L"PmdCullNone"), resource.GetShader(L"PmdShadow"));
    }
    if (Input::Instance().GetKeyDown(KeyCode::KEY_3, DoublePush::Ctr))
    {
        testesT->SetMesh(resource.GetModel(L"さくらみく"), resource.GetShader(L"PmdCullNone"), resource.GetShader(L"PmdShadow"));
    }
    if (Input::Instance().GetKeyDown(KeyCode::KEY_4, DoublePush::Ctr))
    {
        testesT->SetMesh(resource.GetModel(L"さとり"), resource.GetShader(L"PmdCullNone"), resource.GetShader(L"PmdShadow"));
    }
    if (Input::Instance().GetKeyDown(KeyCode::KEY_5, DoublePush::Ctr))
    {
        testesT->SetMesh(resource.GetModel(L"みくめたる"), resource.GetShader(L"PmdCullNone"), resource.GetShader(L"PmdShadow"));
    }
    if (Input::Instance().GetKeyDown(KeyCode::KEY_6, DoublePush::Ctr))
    {
        testesT->SetMesh(resource.GetModel(L"やぎゅう"), resource.GetShader(L"PmdCullNone"), resource.GetShader(L"PmdShadow"));
    }
}
#include "Shrink.hlsli"

[RootSignature(My)]
EditData main(float4 pos : POSITION, float2 uv : TEXCOORD)
{
    EditData output;
    output.svPos = pos;
    output.pos = pos;
    output.uv = uv;
    return output;
}
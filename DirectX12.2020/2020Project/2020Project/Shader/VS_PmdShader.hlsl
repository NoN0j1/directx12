#include "Pmd.hlsli"
#include "MMDBufferData.hlsli"

[RootSignature(MMD)]
EditData main(float4 pos : POSITION, float2 uv : TEXCOORD, float3 normal : NORMAL,
min16uint2 boneNo : BONENO, float weight : WEIGHT)
{
    matrix m = bone.transMatrces[boneNo.x] * weight + bone.transMatrces[boneNo.y] * (1 - weight);
    EditData output;
    pos = mul(m, pos);
    output.pos = mul(ActorTransform.world, pos); //3Dの座標
    output.svPos = mul(camera.viewproj, output.pos); //2Dに潰された座標
    output.uv = uv;
    output.normal.xyz = mul(ActorTransform.world, mul(m, float4(normal, 0))).xyz;
    output.tpos = mul(shadowCamera.viewproj, output.pos); //ライトビューで0〜1に潰した座標(シャドウマップ用)
    return output;
}
float GetBrightness(in float3 light, in float3 normal)
{
    return saturate(dot(-light, normal));
}

float4 GetToonColor(in Texture2D toonTex, in SamplerState samplerState, in float brightness)
{
    float2 toonUV = float2(0.0f, 0.5f * (1.0f - brightness));
    return toonTex.Sample(samplerState, toonUV);
}

float4 GetSphereColor(in Texture2D tex, in SamplerState samplerState, in float3 normal)
{
    //UV変換値 本来は視線の反射ベクトルが正しいがMMDの仕様で法線を使う
    float2 sphereMapUV = (1.0f + float2(1.0f, -1.0f) * normal.xy) * 0.5f;
    return tex.Sample(samplerState, sphereMapUV);
}

float GetMMDSpecularity(in float3 light, in float3 normal, in float power)
{
    float3 reflectRay = reflect(-light, normal);
    return pow( max( 0.0001f, dot(reflectRay, normal) ), power);
}

float GetHalfVecSpecularity(in float3 halfVec, in float3 normal, in float power)
{
    return pow(saturate(dot(halfVec, normal)), power);
}

float GetSpecularity(in float3 light, in float3 eyeRay, in float3 normal, in float power)
{
    float3 halfAngle = normalize(-light + eyeRay);
    return pow(saturate(dot(eyeRay, halfAngle)), power);
}
struct EditData
{
    float4 svPos : SV_Position;
    float4 pos : POSITION;
    float4 tpos : TPOS;
    float4 normal : NORMAL;
    float2 uv : TEXCOORD;
};

struct PixelOutput
{
    float4 color : SV_TARGET0;
    float4 normal : SV_TARGET1;
    float4 hdr : SV_TARGET2;
    float4 shadowMap : SV_TARGET3;
};
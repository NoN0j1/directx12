#ifdef __cplusplus
#pragma once
using float3 = DirectX::XMFLOAT3;
using float4 = DirectX::XMFLOAT4;
using matrix = DirectX::XMMATRIX;
#endif


struct TransformMatrix
{
    matrix world;
};

struct CameraCommons
{
    matrix viewproj;
    matrix proj;
    matrix invProj;
    float3 eyeRay;
};

struct LightCommons
{
    float3 direction;
    float3 attenuation;
    float3 color;
};

struct BoneMatrix
{
    matrix transMatrces[512];
};

struct MaterialParameter
{
    float4 diffuse;
    float4 ambient;
    float4 spcular;
    float spcularPower;
    int texIndex;
};

struct PostEffectParameter
{
    float Time;
    float ShakeX;
    float ShakeY;
};

struct EditorParameter
{
    float3 CameraEye;
    float3 CameraTarget;
    float3 CameraRotate;
    float3 LightDirection;
    float3 LightAttenuation;
    float3 LightColor;
};
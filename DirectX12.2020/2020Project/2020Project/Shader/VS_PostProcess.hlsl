#include "PostProcess.hlsli"

[RootSignature(PostProcess)]
EditData main(float4 pos : POSITION, float2 uv : TEXCOORD)
{
    EditData output;
    pos = mul(WindowScale, pos);
    output.pos = pos;
    output.svPos = pos;
    output.uv = uv;
    return output;
}
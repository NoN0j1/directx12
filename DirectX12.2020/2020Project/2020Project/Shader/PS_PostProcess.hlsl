#include "PostProcess.hlsli"

[RootSignature(PostProcess)]
float4 main(EditData input) : SV_TARGET
{
    float w, h, level;
    Texture2D screen1 = RenderTexture[0];
    Texture2D scr1Depth = ShadowTexture[0];
    Texture2D lightDepth = ShadowTexture[1];
    Texture2D hdrTex = RenderTexture[2];
    Texture2D shadowMapTex = RenderTexture[3];
    Texture2D shrinkTex = RenderTexture[4];
    Texture2D shrinkDofTex = RenderTexture[5];
    
    screen1.GetDimensions(0, w, h, level);
    float2 dt = float2(1.0f / w, 1.0f / h); //画面サイズ分の1ピクセル 
    float2 aspect = float2(w / h, 1);
    float2 shrinkdt = float2(1.0f / (w / 2.0f), 1.0f / h); //縮小バッファ画面サイズ分の1ピクセル 
    float2 UV = input.uv + float2(EffectParameter.ShakeX, EffectParameter.ShakeY);
    float2 centerUV = float2(0.5f/2.0f, 0.5f) + float2(EffectParameter.ShakeX/2.0f, EffectParameter.ShakeY);
    float2 shrinkUV = float2(input.uv.x/2.0f, input.uv.y) + float2(EffectParameter.ShakeX/2.0f, EffectParameter.ShakeY);
    
    if (distance(input.uv * aspect, float2(0.5, 0.5) * aspect) < 0.01)
    {
        return float4(1, 1, 0, 1);
    }
    
    if (UV.x < 0.2f && UV.y < 0.2f)
    {
        //深度出力(ライトから見た深度)
        float depth = lightDepth.Sample(samplerState, (UV * 5.0f)).r;
        depth = 1.0f - depth;
        return float4(depth, depth, depth, 1);
    }
    if (UV.x < 0.2f && UV.y < 0.4f)
    {
        //深度出力(メインカメラの深度)
        float depth = scr1Depth.Sample(samplerState, (UV - float2(0, 0.2f)) * 5.0f).r;
        depth = 1.0f - pow(depth, 10);
        return float4(depth, depth, depth, 1);
    }
    if (UV.x < 0.2f && UV.y < 0.6f)
    {
        //アウトライン出力
        return GetDepthOutline(scr1Depth, (UV - float2(0, 0.4f)) * 5.0f, dt, 2.0f);
    }
    if (UV.x < 0.2f && UV.y < 0.8f)
    {
        //マルチパスの法線を出力
        return RenderTexture[1].Sample(samplerState, (UV - float2(0, 0.6f)) * 5.0f);
    }
    if (UV.x < 0.2f && UV.y < 1.0f)
    {
        //高輝度を出力
        return RenderTexture[2].Sample(samplerState, (UV - float2(0, 0.8f)) * 5.0f);
    }
    if (UV.x > 0.2 && UV.x < 0.4f && UV.y < 0.2f)
    {
        //シャドウマップを出力
        return RenderTexture[3].Sample(samplerState, (UV - float2(0.2, 0.0f)) * 5.0f);
    }
    if (UV.x > 0.4 && UV.x < 0.6f && UV.y < 0.2f)
    {
        //SSAOを出力
        return SSAO.Sample(samplerState, (UV - float2(0.4, 0.0f)) * 5.0f);
    }
    if (UV.x > 0.6 && UV.x < 0.7f && UV.y < 0.2f)
    {
        //縮小バッファを出力
        return RenderTexture[4].Sample(samplerState, (UV - float2(0.6, 0.0f)) * 5.0f);
    }
    if (UV.x > 0.7 && UV.x < 0.8f && UV.y < 0.2f)
    {
        //縮小バッファを出力
        return RenderTexture[5].Sample(samplerState, (UV - float2(0.7, 0.0f)) * 5.0f);
    }
    
    float2 nmUV = UV - 0.5;
    nmUV /= EffectParameter.Time / 100000;
    nmUV += 0.5;//座標
    float4 nmCol = tex[3].Sample(samplerState, nmUV);
    
   
    float4 color = screen1.Sample(samplerState, UV);
    float4 shadow = GetAverageBlur(shadowMapTex, UV, dt);
    float4 depth = scr1Depth.Sample(samplerState, UV);
    depth = float4(depth.rrr, 1);
    
    //この辺をツールからオンオフさせたい
    float4 mono = GetMonochromeColor(color.rgb);
    float4 gba = GetGBAColor(mono.rgb);
    float4 sharp = GetSharpness(screen1, UV, dt);
    float4 gauss = GetGaussianFilteredColor5x5(screen1, UV, dt);
    float4 emboss = GetEmboss(screen1, UV, dt);
    float4 postarize = GetPosterization(color.rgb, 0.25f);
    float4 outLine = GetDepthOutline(scr1Depth, UV, dt, 1.0f);
    float4 bloom = GetBloom(hdrTex, shrinkTex, UV, shrinkUV, dt, float3(1, 1, 1));
    float4 Dof = GetDepthOfField(scr1Depth, shrinkDofTex, input.uv, centerUV, dt, color);
    float4 dof0 = GetGaussianFilteredColor5x5(shrinkDofTex, input.uv * 0.5,dt);
    float4 dof1 = GetGaussianFilteredColor5x5(shrinkDofTex, input.uv * 0.25f + float2(0, 0.5f),dt);
    float4 dof2 = GetGaussianFilteredColor5x5(shrinkDofTex, input.uv * 0.125f + float2(0, 0.75f),dt);
    float4 dof3 = GetGaussianFilteredColor5x5(shrinkDofTex, input.uv * 0.06125f + float2(0, 0.875f),dt);
    float speed = 500.0f;
    float2 moveUV = UV + float2(1.0f / speed, 1.0f / speed) * fmod(EffectParameter.Time, speed);
    float2 distUV = GetDistortionUV(tex[3], loopSamplerState, moveUV);
    float4 distColor = screen1.Sample(samplerState, UV + distUV * 0.02f);
    float ssao = pow(SSAO.Sample(samplerState, UV).r, 1);
    float4 ssaoColor = float4(ssao, ssao, ssao, 1);
    
    return saturate(color) * shadow * outLine;
}
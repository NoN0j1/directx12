#include "Shrink.hlsli"

struct Output
{
    float4 highLumen : SV_Target0;
    float4 DepthOfField : SV_Target1;
};

[RootSignature(My)]
Output main(EditData input)
{   
    Output o;
    o.highLumen = RenderTexture[2].Sample(samplerState, input.uv);
    o.DepthOfField = RenderTexture[0].Sample(samplerState, input.uv);
    return o;
}
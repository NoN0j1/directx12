Texture2D<float4> tex[512] : register(t0, space1);
Texture2D<float4> ShadowTexture[16] : register(t1, space2);
Texture2D<float4> RenderTexture[16] : register(t2, space3);
Texture2D<float>  SSAO: register(t3);
SamplerState samplerState : register(s0);
SamplerState loopSamplerState : register(s1);
SamplerState gaussianSampler : register(s2);

cbuffer ConstantParameter : register(b0)
{
    matrix WindowScale;
}

#include "MultiPurpose.hlsli"
ConstantBuffer<PostEffectParameter> EffectParameter : register(b1);
ConstantBuffer<EditorParameter> DebugEditor : register(b2);

struct EditData
{
    float4 svPos : SV_POSITION;
    float4 pos : POSITION;
    float2 uv : TEXCOORD;
};

#define PostProcess\
"RootFlags( ALLOW_INPUT_ASSEMBLER_INPUT_LAYOUT), " \
"DescriptorTable(SRV(t0, numDescriptors = unbounded, space = 1, flags = DESCRIPTORS_VOLATILE)),"\
"DescriptorTable(SRV(t1, numDescriptors = unbounded, space = 2, flags = DESCRIPTORS_VOLATILE)),"\
"DescriptorTable(SRV(t2, numDescriptors = unbounded, space = 3, flags = DESCRIPTORS_VOLATILE)),"\
"DescriptorTable(SRV(t3)),"\
"DescriptorTable(CBV(b0, numDescriptors = 1)),"\
"DescriptorTable(CBV(b1, numDescriptors = 1)),"\
"DescriptorTable(CBV(b2, numDescriptors = 1)),"\
"StaticSampler(s0, filter = FILTER_MIN_MAG_MIP_POINT,"\
"addressU = TEXTURE_ADDRESS_CLAMP,"\
"addressV = TEXTURE_ADDRESS_CLAMP,"\
"addressW = TEXTURE_ADDRESS_CLAMP),"\
"StaticSampler(s1),"\
"StaticSampler(s2, filter = FILTER_MIN_MAG_MIP_LINEAR,"\
"addressU = TEXTURE_ADDRESS_CLAMP,"\
"addressV = TEXTURE_ADDRESS_CLAMP,"\
"addressW = TEXTURE_ADDRESS_CLAMP,"\
"comparisonFunc = COMPARISON_LESS_EQUAL,"\
"borderColor = STATIC_BORDER_COLOR_OPAQUE_BLACK)"\

float4 GetMonochromeColor(in float3 baseRGB)
{
    float b = dot(float3(0.298912f, 0.586611f, 0.114478f), baseRGB);
    return float4(b, b, b, 1.0f);
}

float4 GetGBAColor(in float3 monochromeRGB)
{
    return float4(monochromeRGB.r * 0.7f, monochromeRGB.g * 0.7f, monochromeRGB.b * 0, 1);
}

float4 GetGaussianFilteredColor5x5(in Texture2D tex, in float2 uv, in float2 dt)
{
    float4 ret = 0;
//今のピクセルに周囲24マスのピクセル値を加算
    ret += tex.Sample(gaussianSampler, uv + float2(-2 * dt.x, 2 * dt.y)) * 1 / 256;
    ret += tex.Sample(gaussianSampler, uv + float2(-1 * dt.x, 2 * dt.y)) * 4 / 256;
    ret += tex.Sample(gaussianSampler, uv + float2(0 * dt.x, 2 * dt.y)) * 6 / 256;
    ret += tex.Sample(gaussianSampler, uv + float2(1 * dt.x, 2 * dt.y)) * 4 / 256;
    ret += tex.Sample(gaussianSampler, uv + float2(2 * dt.x, 2 * dt.y)) * 1 / 256;
    ret += tex.Sample(gaussianSampler, uv + float2(-2 * dt.x, 1 * dt.y)) * 4 / 256;
    ret += tex.Sample(gaussianSampler, uv + float2(-1 * dt.x, 1 * dt.y)) * 16 / 256;
    ret += tex.Sample(gaussianSampler, uv + float2(0 * dt.x, 1 * dt.y)) * 24 / 256;
    ret += tex.Sample(gaussianSampler, uv + float2(1 * dt.x, 1 * dt.y)) * 16 / 256;
    ret += tex.Sample(gaussianSampler, uv + float2(2 * dt.x, 1 * dt.y)) * 4 / 256;
    ret += tex.Sample(gaussianSampler, uv + float2(-2 * dt.x, 0 * dt.y)) * 6 / 256;
    ret += tex.Sample(gaussianSampler, uv + float2(-1 * dt.x, 0 * dt.y)) * 24 / 256;
    ret += tex.Sample(gaussianSampler, uv) * 36/256;
    ret += tex.Sample(gaussianSampler, uv + float2(1 * dt.x, 0 * dt.y)) * 24 / 256;
    ret += tex.Sample(gaussianSampler, uv + float2(2 * dt.x, 0 * dt.y)) * 6 / 256;
    ret += tex.Sample(gaussianSampler, uv + float2(-2 * dt.x, -1 * dt.y)) * 4 / 256;
    ret += tex.Sample(gaussianSampler, uv + float2(-1 * dt.x, -1 * dt.y)) * 16 / 256;
    ret += tex.Sample(gaussianSampler, uv + float2(0 * dt.x, -1 * dt.y)) * 24 / 256;
    ret += tex.Sample(gaussianSampler, uv + float2(1 * dt.x, -1 * dt.y)) * 16 / 256;
    ret += tex.Sample(gaussianSampler, uv + float2(2 * dt.x, -1 * dt.y)) * 4 / 256;
    ret += tex.Sample(gaussianSampler, uv + float2(-2 * dt.x, -2 * dt.y)) * 1 / 256;
    ret += tex.Sample(gaussianSampler, uv + float2(-1 * dt.x, -2 * dt.y)) * 4 / 256;
    ret += tex.Sample(gaussianSampler, uv + float2(0 * dt.x, -2 * dt.y)) * 6 / 256;
    ret += tex.Sample(gaussianSampler, uv + float2(1 * dt.x, -2 * dt.y)) * 4 / 256;
    ret += tex.Sample(gaussianSampler, uv + float2(2 * dt.x, -2 * dt.y)) * 1 / 256;
    return ret;
}

float4 GetAverageBlur(in Texture2D tex, in float2 uv, in float2 dt)
{
    float4 blur = 0;
        //上段
    blur += tex.Sample(samplerState, uv + float2(-dt.x, dt.y)) / 9.0f;
    blur += tex.Sample(samplerState, uv + float2(0, dt.y)) / 9.0f;
    blur += tex.Sample(samplerState, uv + float2(dt.x, dt.y)) / 9.0f;
        //中段
    blur += tex.Sample(samplerState, uv + float2(-dt.x, 0)) / 9.0f;
    blur += tex.Sample(samplerState, uv) / 9.0f;
    blur += tex.Sample(samplerState, uv + float2(dt.x, 0)) / 9.0f;
        //下段
    blur += tex.Sample(samplerState, uv + float2(-dt.x, -dt.y)) / 9.0f;
    blur += tex.Sample(samplerState, uv + float2(0, -dt.y)) / 9.0f;
    blur += tex.Sample(samplerState, uv + float2(dt.x, -dt.y)) / 9.0f;
    return blur;
}

float4 GetSharpness(in Texture2D color, in float2 uv, in float2 dt)
{
    float4 sharp = 0;
    sharp += color.Sample(samplerState, uv);
    sharp += color.Sample(samplerState, uv + float2(-dt.x, 0)) * -1; // 中央の1つ左
    sharp += color.Sample(samplerState, uv + float2(dt.x, 0)) * -1; // 中央の1つ右
    sharp += color.Sample(samplerState, uv + float2(0, -dt.y)) * -1; // 中央の1つ上
    sharp += color.Sample(samplerState, uv + float2(0, dt.y)) * -1; // 中央の1つ下
    return float4(sharp.rgb, 1);
}

float4 GetLineView(float4 sharp)
{
    float4 liner = 0;
    liner = float4(1.0f - sharp.rgb, sharp.a);
    float Y = dot(liner.rgb, float3(0.299f, 0.587f, 0.114f)); //YUV変換
    Y = step(0.2, pow(Y, 20)); //左側が右側より大きければ1を返す
    return float4(Y, Y, Y, 1);
}

//左上ずらしのエンボスを取得する
float4 GetEmboss(in Texture2D color, in float2 uv, in float2 dt)
{
    float4 emboss = 0;
    emboss += color.Sample(samplerState, uv);
    emboss += color.Sample(samplerState, uv + float2(-dt.x, -dt.y)) * 2; //左上
    emboss += color.Sample(samplerState, uv + float2(dt.x, -dt.y)) * 1; //↑の1つ右
    emboss += color.Sample(samplerState, uv + float2(-dt.x, 0)) * 1; //↑↑の1つ下

    emboss += color.Sample(samplerState, uv + float2(0, dt.y)) * -1; //中央から1つ下
    emboss += color.Sample(samplerState, uv + float2(dt.x, 0)) * -1; //中央から1つ右
    emboss += color.Sample(samplerState, uv + float2(dt.x, dt.y)) * -2; //右下
    return emboss;
}

//ポスタリゼーションをかけた色を取得する
float4 GetPosterization(in float3 color, in float power)
{
    return float4(color - fmod(color, power), 1.0f);
}

//メインカメラから撮影した深度を使ってアウトラインを抽出する
float4 GetDepthOutline(in Texture2D depthTex, in float2 uv, in float2 dt, in float lineWidth)
{
    float retdepth = depthTex.Sample(samplerState, uv) * 4 -
                         depthTex.Sample(samplerState, uv + float2(-lineWidth * dt.x, 0)) -
                         depthTex.Sample(samplerState, uv + float2(lineWidth * dt.x, 0)) -
                         depthTex.Sample(samplerState, uv + float2(0, -lineWidth * dt.y)) -
                         depthTex.Sample(samplerState, uv + float2(0, lineWidth * dt.y));
    retdepth = 1 - step(0.001, retdepth);
    return float4(retdepth, retdepth, retdepth, 1);
}

float4 GetBloom(in Texture2D highLumenTex, in Texture2D bloomTex, in float2 uv,in float2 shrinkUV, in float2 dt, in float3 color)
{
    float4 accumBloom = GetGaussianFilteredColor5x5(highLumenTex, uv, dt);
    float2 dsize = float2(1.0f, 0.5f);
    float2 UVofset = 0;
    for (int i = 0; i < 8; i++)
    {
        accumBloom += GetGaussianFilteredColor5x5(bloomTex, shrinkUV * dsize + UVofset, dt);
        UVofset.y += dsize.y;
        dsize *= 0.5f;
    }
    return float4(accumBloom.rgb * color, 1) * 0.1f;
}

//なんかおかしい。
float4 GetDepthOfField(in Texture2D screenDepth, in Texture2D shrinkColorTex, in float2 uv, in float2 targetUV, in float2 dt, in float4 retColor)
{
    float targetDepth = screenDepth.Sample(samplerState, targetUV);
    float inputDepth = screenDepth.Sample(samplerState, uv);
    float4 targetdepthPack = (0.0f, 0.0f, 256.0f, 256.0f);
    targetdepthPack.g = modf(targetDepth * 256.0f, targetdepthPack.r);
    targetdepthPack.b *= modf(targetdepthPack.g * 256.0f, targetdepthPack.g);
    targetdepthPack /= 256.0f;
    
    float4 inputdepthPack = (0.0f, 0.0f, 256.0f, 256.0f);
    inputdepthPack.g = modf(inputDepth * 256.0f, inputdepthPack.r);
    inputdepthPack.b *= modf(inputdepthPack.g * 256.0f, inputdepthPack.g);
    inputdepthPack /= 256.0f;
    
    float depthDistance = pow(distance(targetdepthPack, inputdepthPack), 0.5f);
    depthDistance *= 8.0f;
    float4 colors[2];
    float no;
    depthDistance = modf(depthDistance, no);
    if (no = 0.0f)
    {
        colors[0] = retColor;
        colors[1] = GetGaussianFilteredColor5x5(shrinkColorTex, uv * 0.5f, dt);
    }
    else
    {
        colors[0] = GetGaussianFilteredColor5x5(shrinkColorTex, uv * 0.5f, dt);
        colors[1] = GetGaussianFilteredColor5x5(shrinkColorTex, uv * 0.25f + float2(0, 0.5f), dt);
    }
    return lerp(colors[0], colors[1], depthDistance);
}

float2 GetDistortionUV(in Texture2D distTex, in SamplerState st, in float2 uv)
{
    float2 nmMex = distTex.Sample(st, uv).xy;
    return nmMex * 2.0f - 1.0f;
}
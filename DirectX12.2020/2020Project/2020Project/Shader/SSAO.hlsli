Texture2D<float4> RenderTexture[16] : register(t0, space1);
Texture2D<float4> ShadowTexture[16] : register(t1, space2);
SamplerState samplerState : register(s0);

#include "MultiPurpose.hlsli"
ConstantBuffer<CameraCommons> camera : register(b0);

/*テクスチャ*/
/*テクスチャ上でスクリーンを指すIndex*/
/*影テクスチャ*/
/*アクター*/
/*カメラ*/
/*影カメラ*/
/*マテリアル*/
#define SSAO\
"RootFlags(ALLOW_INPUT_ASSEMBLER_INPUT_LAYOUT),"\
"DescriptorTable(SRV(t0, numDescriptors = unbounded, space = 1, flags = DESCRIPTORS_VOLATILE)),"\
"DescriptorTable(SRV(t1, numDescriptors = unbounded, space = 2,  flags = DESCRIPTORS_VOLATILE)),"\
"DescriptorTable(CBV(b0, numDescriptors = 1,space = 0)),"\
"StaticSampler(s0, filter = FILTER_MIN_MAG_MIP_LINEAR,"\
"addressU = TEXTURE_ADDRESS_CLAMP,"\
"addressV = TEXTURE_ADDRESS_CLAMP,"\
"addressW = TEXTURE_ADDRESS_CLAMP,"\
"comparisonFunc = COMPARISON_NEVER,"\
"borderColor = STATIC_BORDER_COLOR_OPAQUE_BLACK)"

struct EditData
{
    float4 svPos : SV_Position;
    float4 pos : POSITION;
    float2 uv : TEXCOORD;
};

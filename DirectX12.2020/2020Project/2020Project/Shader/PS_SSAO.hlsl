#include "SSAO.hlsli"

float Random(float2 uv)
{
    return frac(sin(dot(uv, float2(12.9898f, 78.233f))) * 43758.5453f);
}

[RootSignature(SSAO)]
float main(EditData input) : SV_TARGET
{
    float w, h, miplevel;
    ShadowTexture[0].GetDimensions(0, w, h, miplevel);
    float2 duv = float2(1.0f / w, 1.0f / h);
    Texture2D DepthTex = ShadowTexture[0];
    Texture2D NormalTex = RenderTexture[1];
    
    
    float depth = DepthTex.Sample(samplerState, input.uv);
    //元の座標を復元
    float4 pos = mul(camera.invProj, float4(input.uv * float2(2, -2) + float2(-1, 1), depth, 1));
    pos.xyz /= pos.w; //この座標がXpにあたる
    float3 N = NormalTex.Sample(samplerState, input.uv);
    N = normalize((N * 2.0f) - 1.0f);
    const float rad = 0.2f;
    float div = 0.0f;
    float ao = 0.0f;
    if(depth < 1.0f)
    {
        for (int i = 0; i < 128; i++)
        {
            float3 omega;
            omega.x = Random(i * duv) * 2 - 1;
            omega.y = Random(float2(omega.x, i * duv.y)) * 2 - 1;
            omega.z = Random(omega.yx) * 2 - 1;
            omega = normalize(omega);
            float dt = dot(omega, N.rgb);
            omega *= sign(dt);
            dt *= sign(dt);
            float4 rpos = float4(pos.xyz + omega * rad, 1);//再度射影変換
            rpos = mul(camera.proj, rpos);
            rpos.xyz /= rpos.w;
            float2 rdUV = (rpos.xy + float2(1, -1)) * float2(0.5f, -0.5f);
            float rdDepth = DepthTex.Sample(samplerState, rdUV);
            if (rpos.z > rdDepth) //現在の深度より奥なら遮蔽されている
            {
                ao += dt; //遮蔽されていた場合の結果
            }
            div += dt; //遮蔽を考えない結果
        }
        ao /= div; //遮蔽率を出す
    }
    
    return 1.0f - ao;
}
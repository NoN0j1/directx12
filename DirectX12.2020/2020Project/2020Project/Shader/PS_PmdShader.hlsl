#include "Common.hlsli"
#include "Pmd.hlsli"
#include "MMDBufferData.hlsli"

[RootSignature(MMD)]
PixelOutput main(EditData input, uint inst : SV_PrimitiveID) : SV_TARGET
{
    MaterialParameter useMat = Material[MatNum[inst]];
    float3 baseColor = 1;
    float4 mainTex = tex[useMat.texIndex].Sample(samplerState, input.uv);
    Texture2D sph = tex[AddTex[MatNum[inst] * 3]];
    Texture2D spa = tex[AddTex[MatNum[inst] * 3 + 1]];
    Texture2D toon = tex[AddTex[MatNum[inst] * 3 + 2]];

    float brightness = GetBrightness(Light.direction, input.normal.rgb);
    float specularWeight = GetSpecularity(Light.direction, camera.eyeRay, input.normal.rgb, useMat.spcularPower);
    float3 toonDiffuse = GetToonColor(toon, samplerState, brightness).rgb;
    float3 sphColor = GetSphereColor(sph, samplerState, input.normal.rgb).rgb;
    float3 spaColor = GetSphereColor(spa, samplerState, input.normal.rgb).rgb;
    float rim = 1.0 - saturate(dot(normalize(input.normal.xyz), camera.eyeRay));
    rim = pow(rim, 5);
    float3 specular = useMat.spcular.rgb * specularWeight;
     //シャドウマップ
    float3 posFromLight = input.tpos.xyz;
    float2 shadowUV = ((float2(1, -1) + input.tpos.xy / input.tpos.w) * float2(0.5, -0.5));
    float epsilon = 0.001f;
    float shadowZ = ShadowTexture[1].SampleCmpLevelZero(smpCompShadow, shadowUV, input.tpos.z - epsilon);
    float shadowWeight = lerp(0.5f, 1.0f, shadowZ);
    //if文の代わりにlerpで
    
    float3 ret = baseColor * mainTex.rgb * sphColor + spaColor; //baseColor
    ret *= 1.0f * (useMat.diffuse.rgb + specular) + useMat.ambient.rgb * brightness;
    ret = saturate(ret);
    PixelOutput output;
    output.hdr = step(0.95f, float4(ret * shadowWeight, 1));
    ret *= toonDiffuse;
    //ret = saturate(ret.rgb + float3(rim, rim, rim)); 簡易rim

    output.color = float4(ret, mainTex.a);
    output.normal = float4(input.normal.rgb, 1);
    output.shadowMap = float4(shadowWeight, shadowWeight, shadowWeight, 1);
   
    return output;
}
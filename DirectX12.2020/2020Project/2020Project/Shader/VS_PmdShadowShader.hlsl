#include "MultiPurpose.hlsli"
ConstantBuffer<TransformMatrix> ActorTransform : register(b0);
ConstantBuffer<CameraCommons> camera : register(b1);
ConstantBuffer<LightCommons> Light : register(b2);
ConstantBuffer<BoneMatrix> bone : register(b3);

#define PMDShadow\
"RootFlags(ALLOW_INPUT_ASSEMBLER_INPUT_LAYOUT), " \
"DescriptorTable(CBV(b0, numDescriptors = 1,space = 0)),"\
"DescriptorTable(CBV(b1, numDescriptors = 1,space = 0)),"\
"DescriptorTable(CBV(b2, numDescriptors = 1,space = 0)),"\
"DescriptorTable(CBV(b3, numDescriptors = 1,space = 0))"

[RootSignature(PMDShadow)]
float4 main(float4 pos : POSITION, float2 uv : TEXCOORD, float3 normal : NORMAL,
min16uint2 boneNo : BONENO, float weight : WEIGHT) :SV_Position
{
    matrix m = bone.transMatrces[boneNo.x] * weight + bone.transMatrces[boneNo.y] * (1 - weight);
    pos = mul(m, pos);
    pos = mul(ActorTransform.world, pos);
    return mul(camera.viewproj, pos);
}
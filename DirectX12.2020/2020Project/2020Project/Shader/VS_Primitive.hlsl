#include "Primitive.hlsli"


[RootSignature(Primitive)]
EditData main(float4 pos : POSITION, float3 normal : NORMAL, float2 uv : TEXCOORD)
{
    EditData output;
    pos = mul(ActorTransform.world, pos);
    output.pos = pos;
    output.svPos = mul(camera.viewproj, output.pos);
    output.uv = uv;
    output.normal.xyz = mul(ActorTransform.world, float4(normal, 0)).xyz;
    output.tpos = mul(shadowCamera.viewproj, output.pos);
    return output;
}
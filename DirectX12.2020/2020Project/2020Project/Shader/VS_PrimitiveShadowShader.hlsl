#include "MultiPurpose.hlsli"
ConstantBuffer<TransformMatrix> ActorTransform : register(b0);
ConstantBuffer<CameraCommons> camera : register(b1);
ConstantBuffer<LightCommons> Light : register(b2);

#define PrimitiveShadow\
"RootFlags(ALLOW_INPUT_ASSEMBLER_INPUT_LAYOUT),"\
"DescriptorTable(CBV(b0, numDescriptors = 1,space = 0)),"\
"DescriptorTable(CBV(b1, numDescriptors = 1,space = 0)),"\
"DescriptorTable(CBV(b2, numDescriptors = 1,space = 0))"

[RootSignature(PrimitiveShadow)]
float4 main(float4 pos : POSITION) : SV_Position
{
    pos = mul(ActorTransform.world, pos);
    return mul(camera.viewproj, pos);
}
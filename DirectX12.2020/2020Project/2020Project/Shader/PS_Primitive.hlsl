#include "Primitive.hlsli"
#include "Common.hlsli"

[RootSignature(Primitive)]
PixelOutput main(EditData input) : SV_TARGET
{
    float brightness = GetBrightness(Light.direction, input.normal.xyz);
    float specularWeight = GetSpecularity(Light.direction, camera.eyeRay, input.normal.rgb, 10.0f);
    //シャドウマップ
    float3 posFromLight = input.pos.xyz;
    float2 shadowUV = ((float2(1, -1) + input.tpos.xy / input.tpos.w) * float2(0.5, -0.5));
    float epsilon = 0.001f;
    float shadowZ = ShadowTexture[1].SampleCmpLevelZero(smpCompShadow, shadowUV, input.tpos.z - epsilon);
    float shadowWeight = lerp(0.5f, 1.0f, shadowZ) * brightness;
    PixelOutput output;
    output.color = float4(Material[0].diffuse.rgb, 1.0f);
    output.color.rgb += Material[0].diffuse.rgb * specularWeight;

    float4 texColor = tex[Material[0].texIndex].Sample(smp, input.uv * float2(1, 1));
    if(Material[0].texIndex != 0)
    {
        output.color.rgb *= texColor.rgb;
        output.color.a = texColor.a;
        saturate(output.color.rgb);
    }
    output.color.rgb *= brightness;
    output.normal = float4(input.normal.xyz, 1);
    output.hdr = step(0.95f, output.color);
    output.shadowMap = float4(shadowWeight, shadowWeight, shadowWeight, 1);
    return output;
}
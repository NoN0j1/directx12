Texture2D<float4> tex[512] : register(t0, space1);
Texture2D<float4> ShadowTexture[16] : register(t1, space2);

#include "MultiPurpose.hlsli"
ConstantBuffer<TransformMatrix> ActorTransform : register(b0);
ConstantBuffer<CameraCommons> camera : register(b1);
ConstantBuffer<CameraCommons> shadowCamera : register(b2);
ConstantBuffer<LightCommons> Light : register(b3);
ConstantBuffer<BoneMatrix> bone : register(b4);

StructuredBuffer<MaterialParameter> Material : register(t2);
StructuredBuffer<uint> MatNum : register(t3);
StructuredBuffer<int> AddTex : register(t4);
SamplerState samplerState : register(s0);
SamplerComparisonState smpCompShadow : register(s1);

/*テクスチャ*/
/*深度テクスチャIndex*/
/*アクター*/
/*カメラ*/
/*シャドウカメラ*/
/*ライト*/
/*マテリアル*/
/*ボーン*/
#define MMD\
"RootFlags( ALLOW_INPUT_ASSEMBLER_INPUT_LAYOUT), " \
"DescriptorTable(SRV(t0, numDescriptors = unbounded, space = 1, flags = DESCRIPTORS_VOLATILE)),"\
"DescriptorTable(SRV(t1, numDescriptors = unbounded, space = 2, flags = DESCRIPTORS_VOLATILE)),"\
"DescriptorTable(CBV(b0, numDescriptors = 1,space = 0)),"\
"DescriptorTable(CBV(b1, numDescriptors = 1,space = 0)),"\
"DescriptorTable(CBV(b2, numDescriptors = 1,space = 0)),"\
"DescriptorTable(CBV(b3, numDescriptors = 1,space = 0)),"\
"DescriptorTable(SRV(t2, numDescriptors = 1,space = 0),"\
                "SRV(t3, numDescriptors = 1,space = 0),"\
                "SRV(t4, numDescriptors = 1,space = 0)),"\
"DescriptorTable(CBV(b4, numDescriptors = 1,space = 0)),"\
"StaticSampler(s0, filter = FILTER_MIN_MAG_MIP_POINT,"\
"addressU = TEXTURE_ADDRESS_CLAMP,"\
"addressV = TEXTURE_ADDRESS_CLAMP,"\
"addressW = TEXTURE_ADDRESS_CLAMP),"\
"StaticSampler(s1, filter = FILTER_COMPARISON_MIN_MAG_MIP_LINEAR,"\
"addressU = TEXTURE_ADDRESS_CLAMP,"\
"addressV = TEXTURE_ADDRESS_CLAMP,"\
"addressW = TEXTURE_ADDRESS_CLAMP,"\
"borderColor = STATIC_BORDER_COLOR_OPAQUE_BLACK)"
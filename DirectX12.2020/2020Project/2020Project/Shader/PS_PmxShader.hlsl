#include "Common.hlsli"
#include "Pmx.hlsli"
#include "MMDBufferData.hlsli"

[RootSignature(MMD)]
float4 main(EditData input, uint inst : SV_PrimitiveID) : SV_TARGET
{
    MaterialParameter useMat = Material[MatNum[inst]];
    float4 mainTex = tex[useMat.texIndex].Sample(samplerState, input.uv);
    Texture2D sph = tex[AddTex[MatNum[inst] * 3]];
    Texture2D spa = tex[AddTex[MatNum[inst] * 3 + 1]];
    Texture2D toon = tex[AddTex[MatNum[inst] * 3 + 2]];

    float3 eye = float3(0.0f, 10.5f, 30.0f);
    float3 eray = normalize(eye - input.pos.xyz);
    
    float spec = GetMMDSpecularity(Light.direction, input.normal, useMat.spcularPower);
    
    float brightness = GetBrightness(Light.direction, input.normal);
    float4 toonDiffuse = GetToonColor(toon, samplerState, brightness);
    float4 sphColor = GetSphereColor(sph, samplerState, input.normal);
    float4 spaColor = GetSphereColor(spa, samplerState, input.normal);
    
    float4 ret = saturate(toonDiffuse * useMat.diffuse * mainTex * sphColor + spaColor);
    ret += useMat.spcular * spec;
    ret.rgb += (mainTex.rgb * useMat.ambient.rgb * brightness) / 3;
    ret = saturate(ret);
    ret.a = mainTex.a;
    return ret;
}
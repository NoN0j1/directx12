struct EditData
{
    float4 svPos : SV_Position;
    float4 pos : POSITION;
    float3 normal : NORMAL;
    float2 uv : TEXCOORD;
    uint4 boneNo : BONENO;
    float4 weight : WEIGHT;
    float3 SDEF_C : SDEF_C;
    float3 SDEF_R0 : SDEF_Rzero;
    float3 SDEF_R1 : SDEF_Rone;
    min16uint1 boneType : BONETYPE;
};
Texture2D<float4> tex[512] : register(t0, space1);
SamplerState samplerState : register(s0);

cbuffer ConstantParameter : register(b0)
{
    matrix WindowScale;
    matrix Position;
    uint texIndex;
}

struct EditData
{
    float4 svPos : SV_POSITION;
    float4 pos : POSITION;
    float2 uv : TEXCOORD;
};

#define My\
"RootFlags( ALLOW_INPUT_ASSEMBLER_INPUT_LAYOUT), " \
"DescriptorTable(SRV(t0, numDescriptors = unbounded, space = 1, flags = DESCRIPTORS_VOLATILE)),"\
"DescriptorTable(CBV(b0, numDescriptors = 1,space = 0), visibility = SHADER_VISIBILITY_ALL)," \
"StaticSampler(s0)"
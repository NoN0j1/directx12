Texture2D<float4> RenderTexture[16] : register(t0, space1);
SamplerState samplerState : register(s0);

struct EditData
{
    float4 svPos : SV_POSITION;
    float4 pos : POSITION;
    float2 uv : TEXCOORD;
};

#define My\
"RootFlags( ALLOW_INPUT_ASSEMBLER_INPUT_LAYOUT), " \
"DescriptorTable(SRV(t0, numDescriptors = unbounded, space = 1, flags = DESCRIPTORS_VOLATILE)),"\
"StaticSampler(s0, filter = FILTER_MIN_MAG_MIP_POINT,"\
"addressU = TEXTURE_ADDRESS_CLAMP,"\
"addressV = TEXTURE_ADDRESS_CLAMP,"\
"addressW = TEXTURE_ADDRESS_CLAMP)"
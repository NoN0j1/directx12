#include "pmx.hlsli"
#include "MMDBufferData.hlsli"


//挑戦中
[RootSignature(MMD)]
EditData main(float4 pos : POSITION, float2 uv : TEXCOORD, float3 normal : NORMAL,
uint4 boneNo : BONENO, float4 weight : WEIGHT,
float3 sdef_c : SDEF_C, float3 sdef_R0 : SDEF_Rzero, float3 sdef_R1 : SDEF_Rone, uint type : BONETYPE)
{
    matrix m;
    float w1 = weight.x;
    float w2 = weight.y;
    m = bone.transMatrces[boneNo.x] * w1 + bone.transMatrces[boneNo.y] * w2;
    //if (type == 0)
    //{
    //    float w;
    //    w = weight.x;
    //    m = boneMats[boneNo.x] * w;
    //}
    //else if (type == 1)
    //{
    //    float w1 = weight.x;
    //    float w2 = weight.y;
    //    m = boneMats[boneNo.x] * w1 + boneMats[boneNo.y] * w2;
    //}
    //else if (type == 2)
    //{
    //    float w1 = weight.x;
    //    float w2 = weight.y;
    //    float w3 = weight.z;
    //    float w4 = weight.w;
    //    m = saturate(boneMats[boneNo.x] * w1 + boneMats[boneNo.y] * w2 + boneMats[boneNo.z] * w3 + boneMats[boneNo.w] * w4);
    //}
    //else
    //{
    //    float w1 = weight.x;
    //    float w2 = weight.y;
    //    m = boneMats[boneNo.x] * w1 + boneMats[boneNo.y] * w2;
    //}
    EditData output;
    pos = mul(m, pos);
    output.pos = mul(ActorTransform.world, pos); //3Dの座標
    output.svPos = mul(camera.viewproj, output.pos); //2Dに潰された座標
    output.uv = uv;
    output.normal = mul(ActorTransform.world, float4(normal, 0)).xyz;
    //output.boneNo = boneNo;
    //output.weight = weight;
    //output.SDEF_C = sdef_c;
    //output.SDEF_C = sdef_R0;
    //output.SDEF_C = sdef_R1;
    //output.boneType = type;
    return output;
}
#include "ComputeShader.hlsli"
RWStructuredBuffer<Buffer_t> outBuffer : register(u0);

#define Compute\
"RootFlags(ALLOW_STREAM_OUTPUT),"\
"DescriptorTable(UAV(u0, numDescriptors = unbounded, space = 0, flags = DESCRIPTORS_VOLATILE))"\


[RootSignature(Compute)]
[numthreads(4, 4, 4)]
void main( uint3 DTid : SV_DispatchThreadID )
{
    outBuffer[DTid.x * 4 * 4 + DTid.y * 4 + DTid.z].id = DTid.x * 4 * 4 + DTid.y * 4 + DTid.z;
}
#include "Texture.hlsli"

[RootSignature(My)]
float4 main(EditData inData) : SV_TARGET
{
    return tex[texIndex].Sample(samplerState, inData.uv);
}
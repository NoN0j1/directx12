#include <Windows.h>
#include "Application.h"

int WINAPI WinMain(HINSTANCE, HINSTANCE, LPSTR, int)
{
	Application& app = Application::Instance();
	if (app.Init() == false)
	{
		return -1; //初期化に失敗
	}
	app.Run();
	app.Terminate();
	return 0;
}
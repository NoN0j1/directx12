#include "DebugEditor.h"
#include "imgui/imgui.h"
#include "imgui/imgui_impl_dx12.h"
#include "imgui/imgui_impl_win32.h"
#include "DirectX12/Dx12Wrapper.h"
#include "DirectX12/Dx12Commons.h"

using namespace std;

DebugEditor::DebugEditor(HWND hwnd, Microsoft::WRL::ComPtr<ID3D12Device6> dev)
{
	D3D12_DESCRIPTOR_HEAP_DESC heapDesc = {};
	heapDesc.Flags = D3D12_DESCRIPTOR_HEAP_FLAG_SHADER_VISIBLE;
	heapDesc.NodeMask = 0;
	heapDesc.NumDescriptors = 1;
	heapDesc.Type = D3D12_DESCRIPTOR_HEAP_TYPE_CBV_SRV_UAV;
	auto result = dev->CreateDescriptorHeap(&heapDesc, IID_PPV_ARGS(guiHeap_.ReleaseAndGetAddressOf()));
	if (FAILED(result))
	{
		assert(0);
	}

	ImGui::CreateContext();
	bool imgResult = ImGui_ImplWin32_Init(hwnd);
	if (!imgResult)
	{
		assert(0);
	}
	imgResult = ImGui_ImplDX12_Init(
		dev.Get(),
		3,
		DXGI_FORMAT_R8G8B8A8_UNORM,
		guiHeap_.Get(),
		guiHeap_->GetCPUDescriptorHandleForHeapStart(),
		guiHeap_->GetGPUDescriptorHandleForHeapStart()
	);
	if (!imgResult)
	{
		assert(0);
	}
	ImGuiIO& io = ImGui::GetIO();
	io.Fonts->AddFontFromFileTTF("C:\\Windows\\Fonts\\meiryo.ttc", 18.0f, nullptr, io.Fonts->GetGlyphRangesJapanese());
}

void DebugEditor::InitBuffer(Dx12Wrapper& dx12Wrapper)
{
	auto& commons = dx12Wrapper.GetCommons();
	commons.CreateConstantBuffer(*editorParamBuffer_.GetAddressOf(), sizeof(mappedEditorParamter_));
	commons.CreateCBVDescriptorAndPlaceView(
		*editorParamDescriptor_.GetAddressOf(),
		editorParamBuffer_.Get(), mappedEditorParamter_);
}

void DebugEditor::Draw(ID3D12GraphicsCommandList5*& cmdList)
{
	ImGuiCond ImGuiCondOnce = ImGuiCond_Once;
	ImGui_ImplDX12_NewFrame();
	ImGui_ImplWin32_NewFrame();
	ImGui::NewFrame();
	ImGui::Begin("DebugWindow");
	ImGui::SetWindowSize(ImVec2(400, 500), ImGuiCond_::ImGuiCond_FirstUseEver);
	ImGui::Text("FPS:%.3f", ImGui::GetIO().Framerate);
	//個別にマップする必要あり
	
	ImGui::SetNextTreeNodeOpen(true, ImGuiCondOnce);
	if (ImGui::TreeNode("Camera")) {

		if (mappedEditorParamter_->CameraEye)
		{
			auto& ceye = mappedEditorParamter_->CameraEye;
			auto oldeye = *ceye;
			auto& ctar = mappedEditorParamter_->CameraTarget;
			float camera[3] = {};
			float target[3] = {};
			ImGui::DragFloat3("Position", camera, 0.1f, -100, 100);
			ceye->x += camera[0];
			ceye->y += camera[1];
			ceye->z += camera[2];
			ctar->x += target[0] + camera[0];
			ctar->y += target[1] + camera[1];
		}
		ImGui::TreePop();
	}

	ImGui::SetNextTreeNodeOpen(true, ImGuiCondOnce);
	if (ImGui::TreeNode("LightDirection")) {

		if (mappedEditorParamter_->LightDirection)
		{
			ImGui::SliderFloat("X_Direction", &mappedEditorParamter_->LightDirection->x, -1, 1);
			ImGui::SliderFloat("Y_Direction", &mappedEditorParamter_->LightDirection->y, -1, 1);
			ImGui::SliderFloat("Z_Direction", &mappedEditorParamter_->LightDirection->z, -1, 1);
		}
		ImGui::TreePop();
	}
	ImGui::End();
	ImGui::Render();

	cmdList->SetDescriptorHeaps(1, guiHeap_.GetAddressOf());
	ImGui_ImplDX12_RenderDrawData(ImGui::GetDrawData(), cmdList);
}

void DebugEditor::SetDebugDescriptorTable(ID3D12GraphicsCommandList5*& cmdList, UINT tableNum)
{
	cmdList->SetDescriptorHeaps(1, editorParamDescriptor_.GetAddressOf());
	cmdList->SetGraphicsRootDescriptorTable(tableNum, editorParamDescriptor_->GetGPUDescriptorHandleForHeapStart());
}

void DebugEditor::Update()
{

}

DebugEditorParameter*& DebugEditor::GetEditorParameter()
{
	return mappedEditorParamter_;
}

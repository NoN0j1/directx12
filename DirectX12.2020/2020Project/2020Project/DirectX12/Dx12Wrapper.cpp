#include <cassert>
#include <algorithm>
#include <string>
#include "d3dx12.h"
#include "../Application.h"
#include "Dx12Wrapper.h"
#include "Dx12CommandList.h"
#include "Object/Actor.h"
#include "Mesh/Mesh.h"
#include "Mesh/ModelManager.h"
#include "Texture/Texture.h"
#include "ShaderManager.h"
#include "DirectX12ToolFunction.h"
#include "DirectX12Geometory.h"
#include "Material/Material.h"
#include "MaterialManager.h"
#include "ResourceManager.h"
#include "EffectManager.h"
#include "Object/Player.h"
#include "Object/Ground/GroundFactory.h"
#include "Object/Ground/Ground.h"
#include "Camera.h"
#include "Mesh/CharacterMesh.h"
#include "Mesh/PrimitiveMesh.h"
#include "BoneManager.h"
#include "DirectionalLight.h"
#include "../DebugEditor.h"

#pragma comment(lib, "d3d12.lib")
#pragma comment(lib, "dxgi.lib")

using namespace std;
constexpr float clearColor[4] = { 1.0f, 1.0f, 1.0f, 1.0f };
constexpr UINT renderTargetNum = 16;
constexpr UINT postEffectTexNum = 256;
constexpr const wchar_t* mainDepthName = L"MainDepth";

Dx12Wrapper::Dx12Wrapper(HWND hwnd):hwnd_(hwnd)
{
	view_ = {};
	usingCameraNum_ = {};
}

Dx12Wrapper::~Dx12Wrapper()
{
	for (auto& c : commandLists_)
	{
		c.Get()->Close();
	}
	for (int i = 0; i < commandLists_.size(); ++i)
	{
		ExcuteCommand(i);
	}
	WaitWithFence();
}

bool Dx12Wrapper::Init()
{
	HRESULT result = S_OK;
#ifdef _DEBUG
	EnableDirectXDebugLayer();
#endif
	if (!CreateDxgiFactory())
	{
		return false;
	}
	if (!CreateDevice())
	{
		return false;
	}
	if (!CreateCommandQueue())
	{
		return false;
	}
	if (!CreateSwapChain())
	{
		return false;
	}
	if (!CreateFence())
	{
		return false;
	}
	commandLists_.push_back(Dx12CommandList(*device_.GetAddressOf()));
	commandLists_.push_back(Dx12CommandList(*device_.GetAddressOf()));
	commons_ = make_unique<Dx12Commons>(device_.Get());
	ManagerAndFactoryInit();
	CreateBaseRenderTarget();
	if (!LinkScreenAndView())
	{
		return false;
	}
	view_ = {};
	SetViewSize(view_, Application::Instance().GetWindowSize());
	CreateBaseVertexBuffer();
	PostProcessInit();
	CreateSSAOBuffer();
	ComputeShadeingBufferInit();
	SpawnDirectionalLight(DirectX::XMFLOAT3(-0.5f, -1.0f, 0.5f));
	DebugEditorInit();
	return true;
}

void Dx12Wrapper::ScreenFlip()
{
	auto cmdList = commandLists_[0].Get();
	debugEditor_->Draw(cmdList);
	mappedPostEffect_->Time += 1.0f;
	if (shakeParam_.shakeTime > 0)
	{
		auto& wsize = Application::Instance().GetWindowSize();
		float wave = powf(cosf(shakeParam_.shakeTime), static_cast<float>(shakeParam_.interval)) / shakeParam_.cycle;
		auto strengthOverTime = (shakeParam_.shakeTime / shakeParam_.shakeTimeMAX) * shakeParam_.strength;
		mappedPostEffect_->ShakeX = (wave * shakeParam_.vector.x * strengthOverTime) / wsize.cx;
		mappedPostEffect_->ShakeY = (wave * shakeParam_.vector.y * strengthOverTime) / wsize.cy;
		shakeParam_.shakeTime--;
	}

	for (auto& s : drawScreen_)
	{
		s->Barrier(D3D12_RESOURCE_STATE_PRESENT, commandLists_[0].Get());
	}
	for (auto c : commandLists_)
	{
		c.Get()->Close();
	}
	for (int i = 0; i < commandLists_.size(); ++i)
	{
		ExcuteCommand(i);
	}
	WaitWithFence();

	//コマンド実行後の後処理
	for (auto c : commandLists_)
	{
		c.Reset();
	}
	HRESULT result = swapChain_->Present(1, 0);
	if (!CheckResult(result))
	{
		OutputDebugString(L"SwapChain::Presentエラー\n");
	}
	device_->GetDeviceRemovedReason(); //swapchainがremoveされた時のエラーを表示
}

void Dx12Wrapper::DrawGraph(float x, float y, int handle)
{
	auto cmdList = commandLists_[0].Get();
	cmdList->IASetPrimitiveTopology(D3D_PRIMITIVE_TOPOLOGY_TRIANGLELIST);

	auto& resource = ResourceManager::Instance();
	auto shader = resource.GetShader(L"Texture");
	cmdList->SetGraphicsRootSignature(shader->rootSignature.Get());
	cmdList->SetPipelineState(shader->pipelineState.Get());

	auto tex = ResourceManager::Instance().GetTexture(handle);
	tex.SetReadTextureIndex(handle);
	tex.SetDrawPosition(x, y);

	//テクスチャのリストをセット
	SetTextureBuffer(cmdList, static_cast<UINT>(GraphTable::LoadTextures));

	//テクスチャ定数バッファ(表示座標を変える用)をセット
	auto descHeap = tex.GetDescriptorHeap();
	if (descHeap != nullptr)
	{
		cmdList->SetDescriptorHeaps(1, &descHeap);
		cmdList->SetGraphicsRootDescriptorTable(static_cast<UINT>(GraphTable::TextureConstant), descHeap->GetGPUDescriptorHandleForHeapStart());
	}
	
	auto& v = tex.GetVertexBuffer();
	if (v.resource == nullptr)
	{
		cmdList->IASetVertexBuffers(0, 1, &wsizeVertexBuffer_->view);
		cmdList->IASetIndexBuffer(&wsizeVertexBuffer_->indexView);
		cmdList->DrawIndexedInstanced(static_cast<UINT>(wsizeVertexBuffer_->indices.size()), 1, 0, 0, 0);
	}
	else
	{
		cmdList->IASetVertexBuffers(0, 1, &v.view);
		cmdList->IASetIndexBuffer(&v.indexView);
		cmdList->DrawIndexedInstanced(static_cast<UINT>(v.indices.size()), 1, 0, 0, 0);
	}
}

void Dx12Wrapper::DispathComputeShader(const wstring& shaderName)
{
	auto cmdList = commandLists_[1].Get();
	auto shader = ResourceManager::Instance().GetShader(shaderName);
	cmdList->SetPipelineState(shader->pipelineState.Get());
	cmdList->SetComputeRootSignature(shader->rootSignature.Get());
	cmdList->SetDescriptorHeaps(1, computeDescriptor_.GetAddressOf());
	cmdList->SetComputeRootDescriptorTable(0, computeDescriptor_->GetGPUDescriptorHandleForHeapStart());
	cmdList->Dispatch(4, 4, 4);

	Barrier(computeBuffer_.Get(), D3D12_RESOURCE_STATE_UNORDERED_ACCESS, D3D12_RESOURCE_STATE_COPY_SOURCE, cmdList);
	cmdList->CopyResource(computeCopyBuffer_.Get(), computeBuffer_.Get());
	cmdList->Close();
	ExcuteCommand(1);
	WaitWithFence();
	commandLists_[1].Reset();
	D3D12_RANGE range = {};
	Buffer_t* mappedBuffer;
	if (!CheckResult(computeCopyBuffer_->Map(0, &range, (void**)&mappedBuffer)))
	{
		return;
	}
	std::copy_n(mappedBuffer, computeData_.size(), computeData_.data());
	computeCopyBuffer_->Unmap(0, nullptr);
	Barrier(computeBuffer_.Get(), D3D12_RESOURCE_STATE_COPY_SOURCE, D3D12_RESOURCE_STATE_UNORDERED_ACCESS, cmdList);
}

void Dx12Wrapper::ShakeScreen(const ShakeParameter& param)
{
	if (shakeParam_.shakeTime == 0)
	{
		//初回の揺れの時
		shakeParam_ = param;
	}
	else
	{
		//振動中
		shakeParam_.strength = shakeParam_.strength * (shakeParam_.shakeTime / shakeParam_.shakeTimeMAX) + param.strength;
		shakeParam_.shakeTime = param.shakeTime;
		shakeParam_.shakeTimeMAX = param.shakeTime;
		shakeParam_.vector = param.vector;
	}
}

const std::shared_ptr<ParticleEffect>& Dx12Wrapper::PlayEffect(const std::wstring& name, const DirectX::XMFLOAT3& pos, float scale)
{
	auto& temp = effectManager_->PlayEffect(name, pos, scale);
	if (temp == nullptr)
	{
		return temp;
	}
	ShakeScreen(ShakeParameter(90.0f, { 1,0 }, 5.0f, 1, 5));
	return temp;
}

bool Dx12Wrapper::LoadEffect(const char16_t* path, const std::wstring& name)
{
	return effectManager_->LoadEffect(path, name);
}

void Dx12Wrapper::DrawEffect()
{
	effectManager_->Update();
	effectManager_->Draw(commandLists_[0].Get());
}

void Dx12Wrapper::DrawAddScreenToBackBuffer()
{
	SetCurrentScreen(false);

	auto cmdList = commandLists_[0].Get();
	cmdList->IASetPrimitiveTopology(D3D_PRIMITIVE_TOPOLOGY_TRIANGLELIST);

	auto& resource = ResourceManager::Instance();
	auto shader = resource.GetShader(L"PostProcess");
	cmdList->SetGraphicsRootSignature(shader->rootSignature.Get());
	cmdList->SetPipelineState(shader->pipelineState.Get());

	//書き込み先のバッファを画像として使用するのでバリアを張る
	for (auto& screen : depthScreen_)
	{
		screen.second->Barrier(D3D12_RESOURCE_STATE_PIXEL_SHADER_RESOURCE, cmdList);
	}
	for (auto& screen : addScreen_)
	{
		screen.second->Barrier(D3D12_RESOURCE_STATE_PIXEL_SHADER_RESOURCE, cmdList);
	}

	//テクスチャのリストをセット
	SetTextureBuffer(cmdList, static_cast<UINT>(PostEffectTable::LoadTextures));
	SetShadowTextureBuffer(cmdList, static_cast<UINT>(PostEffectTable::ShadowTextures));
	SetRenderTextureBuffer(cmdList, static_cast<UINT>(PostEffectTable::RenderTextures));

	//SSAOテクスチャを送る
	ssaoScreen_->Barrier(D3D12_RESOURCE_STATE_PIXEL_SHADER_RESOURCE | D3D12_RESOURCE_STATE_NON_PIXEL_SHADER_RESOURCE, cmdList);
	cmdList->SetDescriptorHeaps(1, ssaoSRVDescriptor_.GetAddressOf());
	cmdList->SetGraphicsRootDescriptorTable(static_cast<UINT>(PostEffectTable::SSAO), ssaoSRVDescriptor_->GetGPUDescriptorHandleForHeapStart());

	//画面サイズを送る
	cmdList->SetDescriptorHeaps(1, wsizeShrinkDescriptor_.GetAddressOf());
	cmdList->SetGraphicsRootDescriptorTable(static_cast<UINT>(PostEffectTable::WindowShrink), wsizeShrinkDescriptor_->GetGPUDescriptorHandleForHeapStart());

	//ポストエフェクト用パラメータを送る
	cmdList->SetDescriptorHeaps(1, postEffectDescriptor_.GetAddressOf());
	cmdList->SetGraphicsRootDescriptorTable(static_cast<UINT>(PostEffectTable::Parameter), postEffectDescriptor_->GetGPUDescriptorHandleForHeapStart());

	//エディタ用パラメータをセット
	debugEditor_->SetDebugDescriptorTable(cmdList, static_cast<UINT>(PostEffectTable::Debug));

	cmdList->IASetVertexBuffers(0, 1, &wsizeVertexBuffer_->view);
	cmdList->IASetIndexBuffer(&wsizeVertexBuffer_->indexView);
	cmdList->DrawIndexedInstanced(static_cast<UINT>(wsizeVertexBuffer_->indices.size()), 1, 0, 0, 0);
}

void Dx12Wrapper::SetCamera(UINT num)
{
	if (cameraActors_.size() < num)
	{
		return;
	}
	usingCameraNum_ = num;
	usingCamera_ = cameraActors_[usingCameraNum_];
	auto& editparam = debugEditor_->GetEditorParameter();
	commons_->PermanentMap(usingCamera_->GetConstantBuffer(), editparam->CameraEye);
	editparam->CameraEye = const_cast<DirectX::XMFLOAT3*>(&usingCamera_->GetTransform().position);
	commons_->PermanentMap(usingCamera_->GetConstantBuffer(), editparam->CameraTarget);
	editparam->CameraTarget = const_cast<DirectX::XMFLOAT3*>(&usingCamera_->GetTarget());
}

std::shared_ptr<Camera> Dx12Wrapper::SpawnCameraActor(const Transform& transform, const int controllNum)
{
	auto& camera = actorFactory_->SpawnCamera(transform);
	cameraActors_.push_back(camera);
	CameraBufferInit(*camera);
	camera->SetController(controllNum);
	camera->CameraInit(true);
	return camera;
}

std::shared_ptr<Ground> Dx12Wrapper::SpawnGround(const Transform& transform)
{
	auto& spawn = groundFactory_->spawnGround(transform);
	auto& cb = spawn->GetConstantBuffer();
	commons_->CreateConstantBuffer(cb, sizeof(DirectX::XMMATRIX));
	cb->SetName(L"ActorConstantBuffer");
	auto p = ResourceManager::Instance().GetModel(PrimitiveMeshType::Plane);
	auto& matManager = p->GetMaterialManager();
	auto& res = ResourceManager::Instance();
	spawn->SetMesh(p, res.GetShader(L"Primitive"), res.GetShader(L"PrimitiveShadow"));
	commons_->CreateCBVDescriptorAndPlaceView(spawn->GetDescriptorHeap(), cb, spawn->GetConstantParameter());
	spawn->InitConstantParameter();
	
	return spawn;
}

void Dx12Wrapper::SpawnDirectionalLight(const DirectX::XMFLOAT3& direction)
{
	//生成→カメラの定数バッファを作成→初期化→影resouceを作成→DSV作成→テクスチャに登録
	light_ = make_shared<DirectionalLight>(direction);
	auto& lightcam = light_->GetShadowCamera();
	CameraBufferInit(*lightcam);
	MakeLightShadowScreen(L"LightDepth",light_->GetShadowScreen(),SIZE({ 4096 ,4096 }));
	auto& mapptr = light_->GetMappingConstant();
	commons_->CreateConstantBuffer(light_->GetBuffer(), sizeof(*mapptr));
	commons_->CreateCBVDescriptorAndPlaceView(light_->GetDescriptorHeap(), light_->GetBuffer(), mapptr);
	light_->LightInit(false);
}

void Dx12Wrapper::UpdateActor()
{
	auto& cameras = actorFactory_->GetManageCameras();
	for (auto& c : cameras)
	{
		c->Update();
	}
	auto& acts = actorFactory_->GetManageActors();
	for (auto& a : acts)
	{
		a->Update();
	}
	light_->Update();
}

void Dx12Wrapper::DrawCastShadow()
{
	auto cmdList = commandLists_[0].Get();
	auto& depthScreen = light_->GetShadowScreen();
	depthScreen->Barrier(D3D12_RESOURCE_STATE_DEPTH_WRITE, cmdList);

	cmdList->OMSetRenderTargets(0, nullptr, false, &depthScreen->handle);
	cmdList->ClearDepthStencilView(depthScreen->handle, D3D12_CLEAR_FLAG_DEPTH, 1.0f, 0, 0, nullptr);
	
	const auto& desc = depthScreen->buffer->GetDesc();
	auto width = static_cast<LONG>(desc.Width);
	auto height = static_cast<LONG>(desc.Height);
	D3D12_VIEWPORT tempView = {};
	SetViewSize(tempView, SIZE({ width, height }));
	cmdList->RSSetViewports(1, &tempView);
	cmdList->RSSetScissorRects(1, &CD3DX12_RECT(0, 0, static_cast<LONG>(tempView.Width), static_cast<LONG>(tempView.Height)));

	//描画
	auto& grounds = groundFactory_->GetManageGrounds();
	auto& cameraDescriptor = light_->GetShadowCamera()->GetDescriptorHeap();
	for (auto& g : grounds)
	{
		auto& mesh = g->GetMesh();
		if (!mesh->SetCastShadowShader(cmdList))
		{
			continue;
		}
		SetCastShadowGroundDrawCommands(*g, cmdList, cameraDescriptor);
	}

	auto& acts = actorFactory_->GetManageActors();
	for (auto& a : acts)
	{
		auto& mesh = a->GetMesh();
		if (!mesh->SetCastShadowShader(cmdList))
		{
			continue;
		}
		SetCastShadowActorDrawCommands(*a, cmdList, cameraDescriptor);
	}
	depthScreen->Barrier(D3D12_RESOURCE_STATE_PIXEL_SHADER_RESOURCE, cmdList);
}

void Dx12Wrapper::DrawAllActor()
{
	auto& grounds = groundFactory_->GetManageGrounds();
	auto cmdList = commandLists_[0].Get();
	auto& cameraDescriptor = usingCamera_->GetDescriptorHeap();
	auto& acts = actorFactory_->GetManageActors();
	for (auto& a : acts)
	{
		DrawingActorProcess(*a, cmdList, cameraDescriptor);
	}
}

void Dx12Wrapper::DrawActor(Actor& act)
{
	auto& cameraDescriptor = usingCamera_->GetDescriptorHeap();
	auto cmdList = commandLists_[0].Get();
	DrawingActorProcess(act, cmdList, cameraDescriptor);
}

void Dx12Wrapper::DrawGround()
{
	auto& grounds = groundFactory_->GetManageGrounds();
	auto cmdList = commandLists_[0].Get();
	auto& cameraDescriptor = usingCamera_->GetDescriptorHeap();
	for (auto& g : grounds)
	{
		g->Update();
		auto& mesh = g->GetMesh();
		if (!mesh->SettingMeshDrawCommands(cmdList, static_cast<UINT>(PrimitiveTable::Materials)))
		{
			continue;
		}
		SetGroundDrawCommands(*g, cmdList, cameraDescriptor);
	}
}

void Dx12Wrapper::SetCurrentScreen(bool depth)
{
	auto cmdList = commandLists_[0].Get();
	auto idx = swapChain_->GetCurrentBackBufferIndex();
	auto& screen = drawScreen_[idx];
	screen->Barrier(D3D12_RESOURCE_STATE_RENDER_TARGET, cmdList);
	if (depth)
	{
		auto& depth = depthScreen_[wstring(mainDepthName)];
		depth->Barrier(D3D12_RESOURCE_STATE_DEPTH_WRITE, cmdList);
		cmdList->OMSetRenderTargets(1, &screen->handle, false, &depth->handle);
		cmdList->ClearDepthStencilView(depth->handle, D3D12_CLEAR_FLAG_DEPTH, 1.0f, 0, 0, nullptr);
	}
	else
	{
		cmdList->OMSetRenderTargets(1, &screen->handle, false, nullptr);
	}

	cmdList->ClearRenderTargetView(screen->handle, clearColor, 0, nullptr);
	cmdList->RSSetViewports(1, &view_);
	cmdList->RSSetScissorRects(1, &CD3DX12_RECT(0, 0, static_cast<LONG>(view_.Width), static_cast<LONG>(view_.Height)));
}

void Dx12Wrapper::SetDrawScreen(vector<wstring> scrName, vector<wstring> depscrName)
{
	auto cmdList = commandLists_[0].Get();

	//ハンドルを取得する対象を選定する(マップに登録済みのバッファ名だけ取得)
	vector<D3D12_CPU_DESCRIPTOR_HANDLE> screenHandle;
	for (auto& name : scrName)
	{
		if (!addScreen_.contains(name))
		{
			continue;
		}
		auto& scr = addScreen_[name];
		scr->Barrier(D3D12_RESOURCE_STATE_RENDER_TARGET, cmdList);
		screenHandle.push_back(scr->handle);
	}

	if (depscrName.empty())
	{
		//深度を書き込まない
		cmdList->OMSetRenderTargets(static_cast<UINT>(screenHandle.size()), screenHandle.data(), false, nullptr);
	}
	else
	{
		vector<D3D12_CPU_DESCRIPTOR_HANDLE> depHandle;
		for (auto& name : depscrName)
		{
			if (!depthScreen_.contains(name))
			{
				continue;
			}
			auto& scr = depthScreen_[name];
			scr->Barrier(D3D12_RESOURCE_STATE_DEPTH_WRITE, cmdList);
			depHandle.push_back(scr->handle);
		}

		cmdList->OMSetRenderTargets(static_cast<UINT>(screenHandle.size()), screenHandle.data(), false, depHandle.data());
		for (auto& h : depHandle)
		{
			cmdList->ClearDepthStencilView(h, D3D12_CLEAR_FLAG_DEPTH, 1.0f, 0, 0, nullptr);
		}
	}

	for (auto& scr : screenHandle)
	{
		cmdList->ClearRenderTargetView(scr, clearColor, 0, nullptr);
	}
	cmdList->RSSetViewports(1, &view_);
	cmdList->RSSetScissorRects(1, &CD3DX12_RECT(0, 0, static_cast<LONG>(view_.Width), static_cast<LONG>(view_.Height)));
}

void Dx12Wrapper::DrawToShrinkBuffer(vector<wstring> shrinkScrName)
{
	auto cmdList = commandLists_[0].Get();
	vector<D3D12_CPU_DESCRIPTOR_HANDLE> screenHandle;
	for (auto& name : shrinkScrName)
	{
		if (!shrinkScreen_.contains(name))
		{
			continue;
		}
		auto& scr = shrinkScreen_[name];
		scr->Barrier(D3D12_RESOURCE_STATE_RENDER_TARGET, cmdList);
		screenHandle.push_back(scr->handle);
	}

	cmdList->OMSetRenderTargets(static_cast<UINT>(screenHandle.size()), screenHandle.data(), false, nullptr);
	float clsColor[4] = { 1.0f, 0.0f, 1.0f, 1.0f };
	for (auto& scr : screenHandle)
	{
		cmdList->ClearRenderTargetView(scr, clearColor, 0, nullptr);
	}
	//描画準備
	auto& resource = ResourceManager::Instance();
	auto shader = resource.GetShader(L"Shrink");
	cmdList->SetGraphicsRootSignature(shader->rootSignature.Get());
	cmdList->SetPipelineState(shader->pipelineState.Get());
	cmdList->IASetPrimitiveTopology(D3D_PRIMITIVE_TOPOLOGY_TRIANGLESTRIP);
	cmdList->IASetVertexBuffers(0, 1, &baseVertexBuffer_->view);
	//描画スクリーンを画像としてセット
	for (auto& screen : addScreen_)
	{
		screen.second->Barrier(D3D12_RESOURCE_STATE_PIXEL_SHADER_RESOURCE, cmdList);
	}
	SetRenderTextureBuffer(cmdList, 0);
	auto desc = shrinkScreen_[shrinkScrName[0]]->buffer->GetDesc();
	float width = static_cast<float>(desc.Width);
	float height = static_cast<float>(desc.Height);
	D3D12_VIEWPORT vp = {};
	vp.TopLeftX = 0;
	vp.TopLeftY = 0;
	vp.Width = width / 2;
	vp.Height = height / 2;
	vp.MinDepth = 0.0f;
	vp.MaxDepth = 1.0f;
	D3D12_RECT rc = {};
	rc.top = 0;
	rc.left = 0;
	rc.right = static_cast<LONG>(vp.Width);
	rc.bottom = static_cast<LONG>(vp.Height);
	for (int i = 0; i < 8; ++i)
	{
		cmdList->RSSetViewports(1, &vp);
		cmdList->RSSetScissorRects(1, &rc);
		cmdList->DrawInstanced(4, 1, 0, 0);
		//書いたら下にずらして次を書く準備
		vp.TopLeftY += vp.Height;
		rc.top = static_cast<LONG>(vp.TopLeftY);
		vp.Height /= 2;
		vp.Width /= 2;
		rc.bottom = rc.top + static_cast<LONG>(vp.Height);
	}
}

void Dx12Wrapper::DrawSSAO()
{
	//スクリーンのセット
	auto cmdList = commandLists_[0].Get();
	ssaoScreen_->Barrier(D3D12_RESOURCE_STATE_RENDER_TARGET, cmdList);
	cmdList->OMSetRenderTargets(1, &ssaoScreen_->handle, false, nullptr);
	float clscreenColor[4] = { 1.0f, 1.0f, 1.0f, 1.0f };
	cmdList->ClearRenderTargetView(ssaoScreen_->handle, clscreenColor, 0, nullptr);
	cmdList->RSSetViewports(1, &view_);
	cmdList->RSSetScissorRects(1, &CD3DX12_RECT(0, 0, static_cast<LONG>(view_.Width), static_cast<LONG>(view_.Height)));

	auto shader = ResourceManager::Instance().GetShader(L"SSAO");
	cmdList->SetGraphicsRootSignature(shader->rootSignature.Get());
	cmdList->SetPipelineState(shader->pipelineState.Get());
	
	SetShadowTextureBuffer(cmdList, static_cast<UINT>(SSAOTable::ShadowTexture));
	SetRenderTextureBuffer(cmdList, static_cast<UINT>(SSAOTable::RenderTexture));
	auto& camera = usingCamera_->GetDescriptorHeap();
	auto& testes = usingCamera_->GetConstantParameter();
	cmdList->SetDescriptorHeaps(1, &camera);
	cmdList->SetGraphicsRootDescriptorTable(
		static_cast<UINT>(SSAOTable::Camera), 
		camera->GetGPUDescriptorHandleForHeapStart());

	cmdList->IASetPrimitiveTopology(D3D_PRIMITIVE_TOPOLOGY_TRIANGLESTRIP);
	cmdList->IASetVertexBuffers(0, 1, &baseVertexBuffer_->view);
	cmdList->DrawInstanced(4, 1, 0, 0);
}

void Dx12Wrapper::MakeScreen(const std::wstring& name)
{
	//バッファ作成
	auto temp = make_shared<Screen>();
	if (!commons_->CreateRenderTargetBuffer(*temp->buffer.GetAddressOf(), drawScreen_[0]->buffer->GetDesc(), clearColor))
	{
		OutputDebugString(L"Screenの作成に失敗しました\n");
	}
	temp->buffer->SetName(name.c_str());

	//ビュー作成
	UINT offset = static_cast<UINT>(addScreen_.size());
	auto handle = addScreenDescriptor_->GetCPUDescriptorHandleForHeapStart();
	handle.ptr += static_cast<size_t>(device_->GetDescriptorHandleIncrementSize(D3D12_DESCRIPTOR_HEAP_TYPE_RTV)) * offset;
	if (!commons_->CreateRTV(temp->buffer.Get(), handle))
	{
		return;
	}
	temp->handle = handle;
	temp->resourceState = D3D12_RESOURCE_STATE_PIXEL_SHADER_RESOURCE;

	//登録
	addScreen_[name] = temp;
	ResourceManager::Instance().AddRenderGraph(addScreen_[name]->buffer);
}

void Dx12Wrapper::MakeDepthScreen(const std::wstring& name,const SIZE& size)
{
	//バッファ作成
	auto temp = make_shared<Screen>();
	CreateDepthScreenProcess(name, temp, size);

	//画像を登録
	ResourceManager::Instance().AddShadowGraph(*temp->buffer.GetAddressOf());
}

void Dx12Wrapper::MakeShrinkScreen(const std::wstring& name, const SIZE& size)
{
	//バッファ作成
	auto temp = make_shared<Screen>();
	auto scrDesc = drawScreen_[0]->buffer->GetDesc();
	scrDesc.Width /= 2;
	if (!commons_->CreateRenderTargetBuffer(*temp->buffer.GetAddressOf(), scrDesc, clearColor))
	{
		OutputDebugString(L"ShrinkScreenの作成に失敗しました\n");
	}
	temp->buffer->SetName(name.c_str());

	//ビュー作成
	UINT offset = static_cast<UINT>(shrinkScreen_.size());
	auto handle = shrinkScreenDescriptor_->GetCPUDescriptorHandleForHeapStart();
	handle.ptr += static_cast<size_t>(device_->GetDescriptorHandleIncrementSize(D3D12_DESCRIPTOR_HEAP_TYPE_RTV)) * offset;
	if (!commons_->CreateRTV(temp->buffer.Get(), handle))
	{
		return;
	}
	temp->handle = handle;
	temp->resourceState = D3D12_RESOURCE_STATE_PIXEL_SHADER_RESOURCE;

	//登録
	shrinkScreen_[name] = temp;
	ResourceManager::Instance().AddRenderGraph(shrinkScreen_[name]->buffer);
}

void Dx12Wrapper::MakeLightShadowScreen(const std::wstring& name, std::shared_ptr<Screen>& screen, const SIZE& size)
{
	CreateDepthScreenProcess(name, screen, size);
	//画像を登録
	ResourceManager::Instance().AddShadowGraph(*screen->buffer.GetAddressOf());
}

bool Dx12Wrapper::CreateDepthScreenProcess(const std::wstring& name, std::shared_ptr<Screen>& screen, const SIZE& size)
{
	//バッファ作成
	if (!commons_->CreateDepthBuffer(*screen->buffer.GetAddressOf(), size))
	{
		OutputDebugString(L"DepthScreenの作成に失敗しました\n");
		return false;
	}
	screen->buffer->SetName(name.c_str());

	//ビュー作成
	UINT placeNum = static_cast<UINT>(depthScreen_.size());
	auto& descriptor = *dsvDescriptor_.GetAddressOf();
	auto handle = descriptor->GetCPUDescriptorHandleForHeapStart();
	handle.ptr += static_cast<size_t>(device_->GetDescriptorHandleIncrementSize(D3D12_DESCRIPTOR_HEAP_TYPE_DSV)) * placeNum;
	if (!commons_->CreateDSV(screen->buffer.Get(), handle))
	{
		OutputDebugString(L"ShadowScreenViewの作成に失敗しました\n");
		return false;
	}
	screen->handle = handle;
	screen->resourceState = D3D12_RESOURCE_STATE_PIXEL_SHADER_RESOURCE;
	//登録
	depthScreen_[name] = screen;
	return true;
}

void Dx12Wrapper::SetViewSize(D3D12_VIEWPORT& view, const SIZE& size, float depthMax)
{
	view.Width = static_cast<FLOAT>(size.cx);
	view.Height = static_cast<FLOAT>(size.cy);
	view.MaxDepth = depthMax;
}

void Dx12Wrapper::Barrier(ID3D12Resource* p, D3D12_RESOURCE_STATES befor, D3D12_RESOURCE_STATES aftor, ID3D12GraphicsCommandList5* cmdlist)
{
	D3D12_RESOURCE_BARRIER barrierDesc = {};
	barrierDesc.Type = D3D12_RESOURCE_BARRIER_TYPE_TRANSITION;
	barrierDesc.Flags = D3D12_RESOURCE_BARRIER_FLAG_NONE;
	barrierDesc.Transition.pResource = p;
	barrierDesc.Transition.Subresource = D3D12_RESOURCE_BARRIER_ALL_SUBRESOURCES;
	barrierDesc.Transition.StateBefore = befor;
	barrierDesc.Transition.StateAfter = aftor;
	cmdlist->ResourceBarrier(1, &barrierDesc);
}

void Dx12Wrapper::ExcuteCommand()
{
	ID3D12CommandList* cmdLists[] = { commandLists_[0].Get() };

	cmdQueue_->ExecuteCommandLists(_countof(cmdLists), cmdLists);
	cmdQueue_->Signal(fence_.Get(), ++fenceVal_);
}

void Dx12Wrapper::ExcuteCommand(char idx)
{
	ID3D12CommandList* cmdLists[] = { commandLists_[idx].Get() };

	cmdQueue_->ExecuteCommandLists(_countof(cmdLists), cmdLists);
	cmdQueue_->Signal(fence_.Get(), ++fenceVal_);
}

void Dx12Wrapper::CopyTexture(const D3D12_TEXTURE_COPY_LOCATION& source, D3D12_TEXTURE_COPY_LOCATION& dest)
{
	auto cmdList = commandLists_[1].Get();
	cmdList->CopyTextureRegion(&dest, 0, 0, 0, &source, nullptr);
	Barrier(dest.pResource, D3D12_RESOURCE_STATE_COPY_DEST, D3D12_RESOURCE_STATE_PIXEL_SHADER_RESOURCE | D3D12_RESOURCE_STATE_NON_PIXEL_SHADER_RESOURCE, cmdList);
	cmdList->Close();
	ExcuteCommand(1);
	WaitWithFence();
	commandLists_[1].Reset();
}

bool Dx12Wrapper::CreateInstanceMaterial(MaterialManager& matManager)
{
	auto tempMats = matManager.GetMaterials();
	auto tempMatNums = matManager.GetMatNums();
	matManager = MaterialManager();
	auto& materials = matManager.GetMaterials();
	matManager.SetMaterials(tempMats);
	matManager.SetMatNums(tempMatNums);
	auto& mDescriptor = matManager.GetDescriptor();
	auto& matBuff = matManager.GetMaterialBuffer();
	auto& mNumBuff = matManager.GetMaterialNumBuffer();

	commons_->CreateStructureBufferAndPlaceView(matBuff, materials, mDescriptor, 2);
	matBuff->SetName(L"MaterialsBuffer");
	commons_->CreateStructureBufferAndPlaceView(mNumBuff, matManager.GetMatNums(), mDescriptor, 1);
	mNumBuff->SetName(L"MaterialSettingNumBuffer");
	return commons_->PermanentMap(matManager.GetMaterialBuffer(), matManager.GetMaterials(), matManager.GetMaterialParameter());
}

void Dx12Wrapper::DrawEditor()
{
}

void Dx12Wrapper::WaitWithFence()
{
	if (fence_->GetCompletedValue() < fenceVal_)
	{
		//処理終了待ち(イベント通知を使用)
		auto event = CreateEvent(nullptr, false, false, nullptr);
		fence_->SetEventOnCompletion(fenceVal_, event);
		WaitForSingleObject(event, INFINITE);
		CloseHandle(event);
	}
}

bool Dx12Wrapper::CreateDevice()
{
	HRESULT result = S_OK;

	vector<Microsoft::WRL::ComPtr<IDXGIAdapter>> adapters;
	Microsoft::WRL::ComPtr<IDXGIAdapter> adapter = nullptr;
	for (int i = 0; dxgiFactory_->EnumAdapters(i, &adapter) != DXGI_ERROR_NOT_FOUND; ++i)
	{
		adapters.push_back(adapter);
	}
	for (auto& adpt : adapters)
	{
		DXGI_ADAPTER_DESC adDesc = {};
		adpt->GetDesc(&adDesc);
		std::wstring strDesc = adDesc.Description;
		if (strDesc.find(L"NVIDIA") != std::string::npos)
		{
			adapter = adpt;
			break;
		}
	}

	D3D_FEATURE_LEVEL levels[] = {
		D3D_FEATURE_LEVEL_12_1,
		D3D_FEATURE_LEVEL_12_0,
		D3D_FEATURE_LEVEL_11_1,
		D3D_FEATURE_LEVEL_11_0,
	};
	for (auto lev : levels)
	{
		result = D3D12CreateDevice(adapter.Get(), lev, IID_PPV_ARGS(&device_));
		if (SUCCEEDED(result))
		{
			break;
		}
	}

	if (device_ == nullptr)
	{
		return false;
	}
	device_->SetName(L"Device6-Object");
	return true;
}

ID3D12Device6* Dx12Wrapper::GetDevice()
{
	return device_.Get();
}

Dx12Commons& Dx12Wrapper::GetCommons()
{
	return *commons_.get();
}

bool Dx12Wrapper::CreateDxgiFactory()
{
#ifdef _DEBUG
	HRESULT result = CreateDXGIFactory2(DXGI_CREATE_FACTORY_DEBUG, IID_PPV_ARGS(dxgiFactory_.ReleaseAndGetAddressOf()));
#else
	HRESULT result = CreateDXGIFactory2(0, IID_PPV_ARGS(dxgiFactory_.ReleaseAndGetAddressOf()));
#endif
	if (!CheckResult(result))
	{
		assert(false);
		return false;
	}
	return true;
}

bool Dx12Wrapper::CreateCommandQueue()
{
	D3D12_COMMAND_QUEUE_DESC queDesc = {};
	queDesc.Flags = D3D12_COMMAND_QUEUE_FLAG_NONE;
	queDesc.NodeMask = 0;
	queDesc.Priority = D3D12_COMMAND_QUEUE_PRIORITY_NORMAL;
	queDesc.Type = D3D12_COMMAND_LIST_TYPE_DIRECT;
	HRESULT result = device_->CreateCommandQueue(&queDesc, IID_PPV_ARGS(cmdQueue_.ReleaseAndGetAddressOf()));
	if (FAILED(result))
	{
		assert(false);
		return false;
	}
	cmdQueue_->SetName(L"BaseCommandQue");
	return true;
}

bool Dx12Wrapper::CreateSwapChain()
{
	auto wsize = Application::Instance().GetWindowSize();

	DXGI_SWAP_CHAIN_DESC1 desc = {};
	desc.Width = wsize.cx;
	desc.Height = wsize.cy;
	desc.Format = DXGI_FORMAT_R8G8B8A8_UNORM;
	desc.Stereo = false;
	desc.SampleDesc.Count = 1;
	desc.SampleDesc.Quality = 0;
	desc.BufferCount = 2;
	desc.BufferUsage = DXGI_USAGE_RENDER_TARGET_OUTPUT;
	desc.Scaling = DXGI_SCALING_STRETCH;
	desc.SwapEffect = DXGI_SWAP_EFFECT_FLIP_DISCARD;
	desc.AlphaMode = DXGI_ALPHA_MODE_UNSPECIFIED;
	desc.Flags = 0;

	Microsoft::WRL::ComPtr<IDXGISwapChain1> swap;

	HRESULT result = dxgiFactory_->CreateSwapChainForHwnd(
		cmdQueue_.Get(),
		hwnd_,
		&desc,
		nullptr, nullptr, swap.ReleaseAndGetAddressOf());
	if (!CheckResult(result))
	{
		assert(false);
		return false;
	}

	result = swap.As(&swapChain_);
	if (!CheckResult(result))
	{
		assert(false);
		return false;
	}
	return true;
}

void Dx12Wrapper::CreateSSAOBuffer()
{
	//SSAOバッファ作成
	ssaoScreen_ = make_shared<Screen>();
	auto resDesc = drawScreen_[0]->buffer->GetDesc();
	resDesc.Format = DXGI_FORMAT_R32_FLOAT;
	float clcolor[4] = { 1.0f, 1.0f ,1.0f ,1.0f };
	if (!commons_->CreateRenderTargetBuffer(*ssaoScreen_->buffer.GetAddressOf(),resDesc, clcolor))
	{
		OutputDebugString(L"Screenの作成に失敗しました\n");
	}
	ssaoScreen_->buffer->SetName(L"SSAObuffer");

	//SSAOビュー作成
	UINT offset = static_cast<UINT>(addScreen_.size());
	auto handle = ssaoRTVDescriptor_->GetCPUDescriptorHandleForHeapStart();
	if (!commons_->CreateRTV(ssaoScreen_->buffer.Get(), handle, DXGI_FORMAT_R32_FLOAT))
	{
		return;
	}
	ssaoScreen_->handle = handle;
	ssaoScreen_->resourceState = D3D12_RESOURCE_STATE_PIXEL_SHADER_RESOURCE;
	if (!commons_->CreateSRV(*ssaoSRVDescriptor_.Get(), *ssaoScreen_->buffer.Get(), 0))
	{
		OutputDebugString(L"SSAOsrvの作成に失敗しました\n");
	}
}

void Dx12Wrapper::ComputeShadeingBufferInit()
{
	computeData_.resize(4 * 4 * 4);
	auto s = sizeof(computeData_.data());
	auto dataSize = s * computeData_.size();
	commons_->CreateUnorderedAccessBuffer(*computeBuffer_.GetAddressOf(), static_cast<UINT>(dataSize), *computeCopyBuffer_.GetAddressOf());
	commons_->CreateCB_SR_UAViewDescriptorHeap(*computeDescriptor_.GetAddressOf(), 1);
	commons_->CreateUAV(computeBuffer_.Get(), computeDescriptor_->GetCPUDescriptorHandleForHeapStart(), computeData_);
}

void Dx12Wrapper::PostProcessInit()
{
	auto& resource = ResourceManager::Instance();
	auto loadH = resource.LoadGraph(L"Resource/Image/n_water.png");
	commons_->CreateConstantBuffer(*postEffectBuffer_.GetAddressOf(), sizeof(*mappedPostEffect_));
	commons_->CreateCBVDescriptorAndPlaceView(*postEffectDescriptor_.GetAddressOf(), postEffectBuffer_.Get(), mappedPostEffect_);
}

bool Dx12Wrapper::LinkScreenAndView()
{
	DXGI_SWAP_CHAIN_DESC1 temp;
	swapChain_->GetDesc1(&temp);
	auto handle = rtvDescHeap_->GetCPUDescriptorHandleForHeapStart();
	auto incSize = device_->GetDescriptorHandleIncrementSize(D3D12_DESCRIPTOR_HEAP_TYPE_RTV);

	for (UINT i = 0; i < temp.BufferCount; ++i)
	{
		drawScreen_[i] = make_shared<Screen>();
		auto& screen = drawScreen_[i];
		//バックバッファ作成
		HRESULT result = swapChain_->GetBuffer(i, IID_PPV_ARGS(screen->buffer.ReleaseAndGetAddressOf()));
		if (!CheckResult(result))
		{
			return false;
		}
		screen->buffer->SetName(L"SpawScreen_RenderTargetBuffer");

		//ビュー作成
		device_->CreateRenderTargetView(screen->buffer.Get(), nullptr, handle);
		screen->handle = handle;
		screen->resourceState = D3D12_RESOURCE_STATE_PRESENT;

		//進めておく
		handle.ptr += incSize;
	}
	return true;
}

bool Dx12Wrapper::CreateFence()
{
	HRESULT result = device_->CreateFence(fenceVal_, D3D12_FENCE_FLAG_NONE, IID_PPV_ARGS(fence_.ReleaseAndGetAddressOf()));
	if (!CheckResult(result))
	{
		assert(false);
		return false;
	}
	return true;
}

void Dx12Wrapper::CreateBaseVertexBuffer()
{
	auto& wsize = Application::Instance().GetWindowSize();
	wsizeVertexBuffer_ = make_shared<VertexBuffer>();
	wsizeVertexBuffer_->vertices.push_back(Vertex({ 0.0f, 0.0f, 0.0f }, { 0.0f, 0.0f }));
	wsizeVertexBuffer_->vertices.push_back(Vertex({ static_cast<float>(wsize.cx), 0.0f, 0.0f }, { 1.0f, 0.0f }));
	wsizeVertexBuffer_->vertices.push_back(Vertex({ 0.0f, static_cast<float>(wsize.cy), 0.0f }, { 0.0f, 1.0f }));
	wsizeVertexBuffer_->vertices.push_back(Vertex({ static_cast<float>(wsize.cx), static_cast<float>(wsize.cy), 0.0f }, { 1.0f, 1.0f }));
	commons_->CreateVertexBuffer(*wsizeVertexBuffer_);
	wsizeVertexBuffer_->resource->SetName(L"WinSizeVertexBuffer");
	commons_->CreateIndexBuffer(*wsizeVertexBuffer_);
	wsizeVertexBuffer_->index->SetName(L"WinSizeIndexBuffer");

	baseVertexBuffer_ = make_shared<VertexBuffer>();
	baseVertexBuffer_->vertices.push_back(Vertex({ -1.0f, 1.0f, 0.0f }, { 0.0f, 0.0f })); //左上
	baseVertexBuffer_->vertices.push_back(Vertex({ 1.0f, 1.0f, 0.0f }, { 1.0f, 0.0f })); //右上
	baseVertexBuffer_->vertices.push_back(Vertex({ -1.0f, -1.0f, 0.0f }, { 0.0f, 1.0f })); //左下
	baseVertexBuffer_->vertices.push_back(Vertex({ 1.0f, -1.0f, 0.0f },  { 1.0f, 1.0f })); //右下
	commons_->CreateVertexBuffer(*baseVertexBuffer_);
	baseVertexBuffer_->resource->SetName(L"baseVertexBuffer");
	commons_->CreateIndexBuffer(*baseVertexBuffer_);
	baseVertexBuffer_->index->SetName(L"baseIndexBuffer");

	commons_->CreateConstantBuffer(*wsizeShrinkConstantBuffer_.GetAddressOf(), sizeof(*mappedShrinkParam_));
	commons_->CreateCBVDescriptorAndPlaceView(*wsizeShrinkDescriptor_.GetAddressOf(), wsizeShrinkConstantBuffer_.Get(), mappedShrinkParam_);
	*mappedShrinkParam_ = DirectX::XMMatrixIdentity();
	mappedShrinkParam_->r[0].m128_f32[0] = 1.0f / (wsize.cx / 2);
	mappedShrinkParam_->r[1].m128_f32[1] = -1.0f / (wsize.cy / 2);
	mappedShrinkParam_->r[3].m128_f32[0] = -1.0f;
	mappedShrinkParam_->r[3].m128_f32[1] = 1.0f;
}

void Dx12Wrapper::EnableDirectXDebugLayer()
{
	ID3D12Debug* debug;
	HRESULT hr = D3D12GetDebugInterface(IID_PPV_ARGS(&debug));
	if (!CheckResult(hr))
	{
		OutputDebugString(L"Failed::GetDebugInterface");
	}
	debug->EnableDebugLayer();
	debug->Release();
}

void Dx12Wrapper::CreateBaseRenderTarget()
{
	//スワップチェインを格納するデスクリプタヒープを作成
	DXGI_SWAP_CHAIN_DESC1 temp;
	swapChain_->GetDesc1(&temp);
	commons_->CreateRTVDescriptorHeap(*rtvDescHeap_.GetAddressOf(), 2);

	//追加バッファ用のデスクリプタヒープを作成
	commons_->CreateRTVDescriptorHeap(*addScreenDescriptor_.GetAddressOf(), renderTargetNum);

	//縮小バッファ用のデスクリプタヒープを作成
	commons_->CreateRTVDescriptorHeap(*shrinkScreenDescriptor_.GetAddressOf(), renderTargetNum);

	//SSAOバッファ用のデスクリプタヒープを作成
	commons_->CreateRTVDescriptorHeap(*ssaoRTVDescriptor_.GetAddressOf(), 1);
	commons_->CreateCB_SR_UAViewDescriptorHeap(*ssaoSRVDescriptor_.GetAddressOf(), 1);
	
	//深度バッファ用のデスクリプタヒープを作成
	commons_->CreateDescriptorHeap(*dsvDescriptor_.GetAddressOf(), renderTargetNum, D3D12_DESCRIPTOR_HEAP_TYPE_DSV);

	//深度バッファを一枚作っておく
	MakeDepthScreen(wstring(mainDepthName));
}

void Dx12Wrapper::CameraBufferInit(Camera& camera)
{
	//バッファ作成
	auto& descriptor = camera.GetDescriptorHeap();

	auto& csvBuff = camera.GetConstantBuffer();
	auto& commonsParam = camera.GetConstantCommonsParameter();
	commons_->CreateConstantBuffer(csvBuff, sizeof(*commonsParam));
	csvBuff->SetName(L"CameraConstantBuffer");
	commons_->CreateCBVDescriptorAndPlaceView(descriptor, csvBuff, commonsParam);
}

void Dx12Wrapper::ManagerAndFactoryInit()
{
	actorFactory_ = make_unique<ActorFactory>(*this);
	groundFactory_ = make_unique<GroundFactory>();
	usingCamera_ = SpawnCameraActor(Transform({ 0, 10, -22 }), 0);
	ResourceManager::Instance().Init(*this);
	effectManager_ = make_unique<EffectManager>();
	effectManager_->Init(device_, cmdQueue_, usingCamera_);
	debugEditor_ = make_unique<DebugEditor>(hwnd_, device_);
	debugEditor_->InitBuffer(*this);
}

void Dx12Wrapper::DebugEditorInit()
{
	auto& editor = debugEditor_->GetEditorParameter();
	commons_->PermanentMap(light_->GetBuffer(), editor->LightDirection);
	editor->LightDirection = &light_->GetMappingConstant()->direction;
	SetCamera(0);
}

void Dx12Wrapper::SpawnActorInit(Actor& act, const int controllerNum)
{
	auto& csvBuff = act.GetConstantBuffer();
	commons_->CreateConstantBuffer(csvBuff, sizeof(DirectX::XMMATRIX));
	csvBuff->SetName(L"ActorConstantBuffer");
	commons_->CreateCBVDescriptorAndPlaceView(act.GetDescriptorHeap(), csvBuff, act.GetConstantParameter());
	act.InitConstantParameter();
	act.SetController(controllerNum);
}

void Dx12Wrapper::SetTextureBuffer(ID3D12GraphicsCommandList5* cmdList, UINT enumVal)
{
	auto& resManager = ResourceManager::Instance();
	auto& descHeap = resManager.GetTextureDescriptorHeap();
	cmdList->SetDescriptorHeaps(1, &descHeap);
	cmdList->SetGraphicsRootDescriptorTable(enumVal, descHeap->GetGPUDescriptorHandleForHeapStart());
}

void Dx12Wrapper::SetShadowTextureBuffer(ID3D12GraphicsCommandList5* cmdList, UINT enumVal)
{
	auto& resManager = ResourceManager::Instance();
	auto& descHeap = resManager.GetShadowDescriptorHeap();
	cmdList->SetDescriptorHeaps(1, &descHeap);
	cmdList->SetGraphicsRootDescriptorTable(enumVal, descHeap->GetGPUDescriptorHandleForHeapStart());
}

void Dx12Wrapper::SetRenderTextureBuffer(ID3D12GraphicsCommandList5* cmdList, UINT enumVal)
{
	auto& resManager = ResourceManager::Instance();
	auto& descHeap = resManager.GetRenderTextureDescriptorHeap();
	cmdList->SetDescriptorHeaps(1, &descHeap);
	cmdList->SetGraphicsRootDescriptorTable(enumVal, descHeap->GetGPUDescriptorHandleForHeapStart());
}

void Dx12Wrapper::DrawingActorProcess(Actor& act, ID3D12GraphicsCommandList5* cmdList, ID3D12DescriptorHeap* camera)
{
	auto& mesh = act.GetMesh();
	if (mesh == nullptr)
	{
		return;
	}
	if (!mesh->SettingMeshDrawCommands(cmdList, static_cast<UINT>(ActorTable::Materials)))
	{
		return;
	}
	SetActorDrawCommands(act, cmdList, camera);
}

void Dx12Wrapper::SetActorDrawCommands(Actor& act, ID3D12GraphicsCommandList5* cmdList, ID3D12DescriptorHeap* camera)
{
	auto descHeap = act.GetDescriptorHeap();
	auto handle = descHeap->GetGPUDescriptorHandleForHeapStart();
	//テクスチャのバッファをセット
	SetTextureBuffer(cmdList, static_cast<UINT>(ActorTable::LoadTextures));
	SetShadowTextureBuffer(cmdList, static_cast<UINT>(ActorTable::ShadowTextures));
	//Actorのworld
	cmdList->SetDescriptorHeaps(1, &descHeap);
	cmdList->SetGraphicsRootDescriptorTable(static_cast<UINT>(ActorTable::WorldConstant), handle);
	//カメラ定数バッファ
	cmdList->SetDescriptorHeaps(1, &camera);
	cmdList->SetGraphicsRootDescriptorTable(static_cast<UINT>(ActorTable::CameraView), camera->GetGPUDescriptorHandleForHeapStart());
	//光源カメラ定数バッファ
	auto& lightcam = light_->GetShadowCamera()->GetDescriptorHeap();
	cmdList->SetDescriptorHeaps(1, &lightcam);
	cmdList->SetGraphicsRootDescriptorTable(static_cast<UINT>(ActorTable::ShadowCameraView), lightcam->GetGPUDescriptorHandleForHeapStart());
	//ライト定数バッファ
	auto& lightConst = light_->GetDescriptorHeap();
	cmdList->SetDescriptorHeaps(1, &lightConst);
	cmdList->SetGraphicsRootDescriptorTable(static_cast<UINT>(ActorTable::Light), lightConst->GetGPUDescriptorHandleForHeapStart());

	auto& charaMesh = act.GetCharacterMesh();
	if (charaMesh != nullptr)
	{
		auto& bManager = charaMesh->GetBoneManager();
		descHeap = bManager->GetDescriptorHeap();
		cmdList->SetDescriptorHeaps(1, &descHeap);
		cmdList->SetGraphicsRootDescriptorTable(static_cast<UINT>(ActorTable::Bone), descHeap->GetGPUDescriptorHandleForHeapStart());
	}
	auto& vertexParam = act.GetMesh()->GetVertexBuffer();
	cmdList->IASetVertexBuffers(0, 1, &vertexParam->view);
	cmdList->IASetIndexBuffer(&vertexParam->indexView);
	cmdList->DrawIndexedInstanced(static_cast<UINT>(vertexParam->indices.size()), 1, 0, 0, 0);
}

void Dx12Wrapper::SetGroundDrawCommands(Ground& ground, ID3D12GraphicsCommandList5* cmdList, ID3D12DescriptorHeap* camera)
{
	auto descHeap = ground.GetDescriptorHeap();
	auto handle = descHeap->GetGPUDescriptorHandleForHeapStart();
	//テクスチャのバッファをセット
	SetTextureBuffer(cmdList, static_cast<UINT>(PrimitiveTable::LoadTextures));
	SetShadowTextureBuffer(cmdList, static_cast<UINT>(PrimitiveTable::ShadowTextures));
	//Actorのworld
	cmdList->SetDescriptorHeaps(1, &descHeap);
	cmdList->SetGraphicsRootDescriptorTable(static_cast<UINT>(PrimitiveTable::WorldConstant), handle);
	//カメラ定数バッファ
	cmdList->SetDescriptorHeaps(1, &camera);
	cmdList->SetGraphicsRootDescriptorTable(static_cast<UINT>(PrimitiveTable::CameraView), camera->GetGPUDescriptorHandleForHeapStart());
	//光源カメラ定数バッファ
	auto& lightcam = light_->GetShadowCamera()->GetDescriptorHeap();
	cmdList->SetDescriptorHeaps(1, &lightcam);
	cmdList->SetGraphicsRootDescriptorTable(static_cast<UINT>(PrimitiveTable::ShadowCameraView), lightcam->GetGPUDescriptorHandleForHeapStart());
	//ライト定数バッファ
	auto& lightConst = light_->GetDescriptorHeap();
	cmdList->SetDescriptorHeaps(1, &lightConst);
	cmdList->SetGraphicsRootDescriptorTable(static_cast<UINT>(PrimitiveTable::Light), lightConst->GetGPUDescriptorHandleForHeapStart());
	//マテリアルストラクチャバッファ
	auto& matManager = ground.GetMesh()->GetMaterialManager();
	cmdList->SetDescriptorHeaps(1, &matManager.GetDescriptor());
	cmdList->SetGraphicsRootDescriptorTable(static_cast<UINT>(PrimitiveTable::Materials), matManager.GetDescriptor()->GetGPUDescriptorHandleForHeapStart());

	auto& vertexParam = ground.GetMesh()->GetVertexBuffer();
	cmdList->IASetVertexBuffers(0, 1, &vertexParam->view);
	cmdList->IASetIndexBuffer(&vertexParam->indexView);
	cmdList->DrawIndexedInstanced(static_cast<UINT>(vertexParam->indices.size()), 1, 0, 0, 0);
}


void Dx12Wrapper::SetCastShadowActorDrawCommands(Actor& act, ID3D12GraphicsCommandList5* cmdList, ID3D12DescriptorHeap* camera)
{
	auto descHeap = act.GetDescriptorHeap();
	auto handle = descHeap->GetGPUDescriptorHandleForHeapStart();
	//Actorのworld
	cmdList->SetDescriptorHeaps(1, &descHeap);
	cmdList->SetGraphicsRootDescriptorTable(static_cast<UINT>(ActorShadowTable::WorldConstant), handle);
	//カメラ定数バッファ
	cmdList->SetDescriptorHeaps(1, &camera);
	cmdList->SetGraphicsRootDescriptorTable(static_cast<UINT>(ActorShadowTable::CameraView), camera->GetGPUDescriptorHandleForHeapStart());
	//ライト定数バッファ
	auto& lightConst = light_->GetDescriptorHeap();
	cmdList->SetDescriptorHeaps(1, &lightConst);
	cmdList->SetGraphicsRootDescriptorTable(static_cast<UINT>(ActorShadowTable::Light), lightConst->GetGPUDescriptorHandleForHeapStart());
	
	auto& charaMesh = act.GetCharacterMesh();
	if (charaMesh != nullptr)
	{
		auto& bManager = charaMesh->GetBoneManager();
		descHeap = bManager->GetDescriptorHeap();
		cmdList->SetDescriptorHeaps(1, &descHeap);
		cmdList->SetGraphicsRootDescriptorTable(static_cast<UINT>(ActorShadowTable::Bone), descHeap->GetGPUDescriptorHandleForHeapStart());
	}

	auto& vertexParam = act.GetMesh()->GetVertexBuffer();
	cmdList->IASetVertexBuffers(0, 1, &vertexParam->view);
	cmdList->IASetIndexBuffer(&vertexParam->indexView);
	cmdList->DrawIndexedInstanced(static_cast<UINT>(vertexParam->indices.size()), 1, 0, 0, 0);
}

void Dx12Wrapper::SetCastShadowGroundDrawCommands(Ground& ground, ID3D12GraphicsCommandList5* cmdList, ID3D12DescriptorHeap* camera)
{
	auto descHeap = ground.GetDescriptorHeap();
	auto handle = descHeap->GetGPUDescriptorHandleForHeapStart();
	//Actorのworld
	cmdList->SetDescriptorHeaps(1, &descHeap);
	cmdList->SetGraphicsRootDescriptorTable(static_cast<UINT>(PrimitiveShadowTable::WorldConstant), handle);
	//カメラ定数バッファ
	cmdList->SetDescriptorHeaps(1, &camera);
	cmdList->SetGraphicsRootDescriptorTable(static_cast<UINT>(PrimitiveShadowTable::CameraView), camera->GetGPUDescriptorHandleForHeapStart());
	//ライト定数バッファ
	auto& lightConst = light_->GetDescriptorHeap();
	cmdList->SetDescriptorHeaps(1, &lightConst);
	cmdList->SetGraphicsRootDescriptorTable(static_cast<UINT>(PrimitiveShadowTable::Light), lightConst->GetGPUDescriptorHandleForHeapStart());
	
	auto& vertexParam = ground.GetMesh()->GetVertexBuffer();
	cmdList->IASetVertexBuffers(0, 1, &vertexParam->view);
	cmdList->IASetIndexBuffer(&vertexParam->indexView);
	cmdList->DrawIndexedInstanced(static_cast<UINT>(vertexParam->indices.size()), 1, 0, 0, 0);
}

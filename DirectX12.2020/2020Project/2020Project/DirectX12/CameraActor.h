#pragma once
#include "Camera.h"
class CameraActor : public Camera
{
public:
	CameraActor();

	void CameraInit(bool perce)override final;

	/// <summary>
	/// 毎フレームの更新
	/// </summary>
	void Update()override final;
protected:
	CameraActor(const Transform& tr);
private:
	/// <summary>
	/// カメラを操作する
	/// </summary>
	/// <param name="input"></param>
	void Move(const Input& input);
};


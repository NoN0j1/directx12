#pragma once
#include <memory>
#include <wrl.h>
#include <vector>
#include <DirectXMath.h>
#include "../MaterialManager.h"

struct VertexBufferBase;
struct ID3D12GraphicsCommandList5;
struct ID3D12RootSignature;
struct ID3D12PipelineState;
struct Shader;

class MaterialManager;
struct Material;
class ModelManager;

class Mesh
{
	friend ModelManager;
public:
	Mesh();
	~Mesh() = default;
	std::shared_ptr<VertexBufferBase>& GetVertexBuffer();

	/// <summary>
	/// マテリアルデータを登録する
	/// </summary>
	/// <param name="inMat"></param>
	void SetMaterial(const std::vector<Material>& inMat);

	/// <summary>
	/// 指定番号のマテリアル情報を動的に変更する(範囲外の場合はエラーを表示)
	/// </summary>
	/// <param name="mat">マテリアルパラメータ</param>
	/// <param name="matNum">マテリアル番号</param>
	void SetMaterialParameter(const Material& mat, UINT matNum);

	/// <summary>
	/// マテリアルを管理しているマネージャーを取得する
	/// </summary>
	/// <returns></returns>
	MaterialManager& GetMaterialManager();

	const DirectX::XMFLOAT3& GetOffsetCenterPosition() const;

	/// <summary>
	/// メッシュの描画設定を行う。所持しているシェーダのパイプラインとルートシグネチャをセット
	/// </summary>
	/// <param name="cmdList">実行コマンドリスト</param>
	/// <param name="enumTableVal">DescriptorTableのマテリアルのセット場所</param>
	/// <returns></returns>
	virtual bool SettingMeshDrawCommands(ID3D12GraphicsCommandList5*& cmdList, int enumTableVal);

	/// <summary>
	/// 影描画用のパイプラインとルートシグネチャをセットする
	/// </summary>
	/// <param name="cmdList">実行コマンドリスト</param>
	/// <returns></returns>
	virtual bool SetCastShadowShader(ID3D12GraphicsCommandList5*& cmdList);
	virtual void SetShader(const std::shared_ptr<Shader>& shader, const std::shared_ptr<Shader> shadow = nullptr);
protected:
	std::shared_ptr<VertexBufferBase> vertexBuffer_;
	std::shared_ptr<Shader> shaderParam_;
	std::shared_ptr<Shader> shadowShader_;
	MaterialManager materialManager_;
	DirectX::XMFLOAT3* centerOffset_{};
};


#pragma once
#include <string>
#include <unordered_map>
#include <memory>

class Dx12Wrapper;
struct D3D12_INPUT_ELEMENT_DESC;
class CharacterMesh;
class PrimitiveMesh;
class MaterialManager;
enum class PrimitiveMeshType;

class ModelManager
{
public:
	ModelManager(Dx12Wrapper& wrapper);
	~ModelManager() = default;
	/// <summary>
	/// モデルデータを読み込む
	/// </summary>
	/// <param name="filePath">読み込みパス</param>
	/// <param name="name">登録名</param>
	/// <returns>true = 新規読み込み成功、 false = 失敗or新規読み込み無し</returns>
	bool LoadModel(const std::wstring& filePath, const std::wstring& name);

	/// <summary>
	/// ロード済みメッシュを取得する
	/// </summary>
	/// <param name="name">登録名</param>
	/// <returns>メッシュデータ</returns>
	const std::shared_ptr<Mesh> GetMesh(const std::wstring& name)const;

	/// <summary>
	/// プリミティブメッシュを取得する
	/// </summary>
	/// <param name="primType">プリミティブタイプ</param>
	/// <returns>メッシュデータ</returns>
	const std::shared_ptr<Mesh> GetMesh(const PrimitiveMeshType primType)const;

private:
	/// <summary>
	/// PMD形式のモデルデータを読み込む
	/// </summary>
	/// <param name="path">ファイルパス</param>
	/// <param name="name">マップ登録名</param>
	/// <returns>true = 未登録なので読み込んだ、false = 登録済み</returns>
	bool LoadPMD(const std::wstring& path, const std::wstring& name);
	
	/// <summary>
	/// PMD形式:ヘッダー読み込み
	/// </summary>
	/// <param name="fp">fopenデータ</param>
	void LoadPMDHeader(FILE* fp);
	
	/// <summary>
	/// PMD形式:頂点データと頂点配列読み込み
	/// </summary>
	/// <param name="fp">fopenデータ</param>
	/// <param name="vb">登録先</param>
	void LoadPMDVertex(FILE* fp, VertexBufferPMD& vb);
	
	/// <summary>
	/// PMD形式:マテリアルデータ読み込み
	/// </summary>
	/// <param name="fp">fopenデータ</param>
	/// <param name="mm">マテリアル登録先</param>
	/// <param name="toonNum">後に必要になる一時データ</param>
	/// <param name="loadTexPath">後に必要になる一時データ</param>
	void LoadPMDMaterial(FILE* fp, MaterialManager& mm, std::vector<uint8_t>& toonNum, std::vector<std::string>& loadTexPath);
	
	/// <summary>
	/// PMD形式:ボーンデータ読み込み
	/// </summary>
	/// <param name="fp">fopenデータ</param>
	/// <param name="cMesh">登録先のメッシュ</param>
	void LoadPMDBone(FILE* fp, uint16_t& boneNum, std::shared_ptr<CharacterMesh>& cMesh);

	/// <summary>
	/// PMD形式:IKデータ読み込み
	/// </summary>
	/// <param name="fp">fopenデータ</param>
	/// <param name="cMesh">登録先のメッシュ</param>
	void LoadPMDIK(FILE* fp, std::shared_ptr<CharacterMesh>& cMesh);

	/// <summary>
	/// PMD形式:表情モーフデータ読み込み
	/// </summary>
	/// <param name="fp">fopenデータ</param>
	/// <param name="cMesh">登録先のメッシュ</param>
	/// <param name="loadTexPath">後に必要になる一時データ</param>
	void LoadPMDFaceSkin(FILE* fp, std::shared_ptr<CharacterMesh>& cMesh, uint16_t& skinNum);

	/// <summary>
	/// PMD形式:エディタ描画用UIデータ
	/// </summary>
	/// <param name="fp">fopenデータ</param>
	/// <param name="boneNum">予め読み込んだボーン数</param>
	void LoadPMDEditorDisplayData(FILE* fp, uint16_t boneNum, uint16_t skinNum);

	/// <summary>
	/// PMD形式:トゥーン画像の割り当て読み込み
	/// </summary>
	/// <param name="fp">fopenデータ</param>
	/// <param name="mm">登録先マテリアルデータ</param>
	/// <param name="toonNum">予め読み込んだtoon割り当て配列</param>
	/// <param name="loadTexPath">予め読み込んだTextureFileパス</param>
	/// <param name="path">モデル自体のパス</param>
	void LoadPMDToonData(FILE* fp, MaterialManager& mm, const std::vector<uint8_t>& toonNum, const std::vector<std::string>& loadTexPath, const std::wstring& path);
	
	/// <summary>
	/// PMX形式のデータを読み込む
	/// </summary>
	/// <param name="path">ファイルパス</param>
	/// <param name="name">マップ登録名</param>
	/// <returns>true = 成功、false = 失敗</returns>
	bool LoadPMX(const std::wstring& path, const std::wstring& name);

	/// <summary>
	/// 読み込んだキャラクターメッシュの頂点バッファの作成
	/// </summary>
	/// <param name="readMesh">読み込んだメッシュ</param>
	void ReadModelVertexSetting(std::shared_ptr<CharacterMesh> & readMesh);

	/// <summary>
	/// 読み込んだキャラクターメッシュのマテリアルバッファの作成
	/// </summary>
	/// <param name="readMesh">読み込んだメッシュ</param>
	void ReadModelMaterialSetting(std::shared_ptr<CharacterMesh>& readMesh);

	/// <summary>
	/// 読み込んだキャラクターメッシュのボーンバッファの作成
	/// </summary>
	/// <param name="readMesh">読み込んだメッシュ</param>
	void ReadModelBoneSetting(std::shared_ptr<CharacterMesh>& readMesh);

	/// <summary>
	/// プリミティブの頂点バッファの作成
	/// </summary>
	/// <param name="mesh"></param>
	void PrimitiveVertexSetting(std::shared_ptr<PrimitiveMesh>& mesh);

	/// <summary>
	/// プリミティブのマテリアルバッファの作成
	/// </summary>
	/// <param name="mesh"></param>
	void PrimitiveMaterialSetting(std::shared_ptr<PrimitiveMesh>& mesh);

	/// <summary>
	/// 読み込んだCharacterMeshを格納する
	/// </summary>
	std::unordered_map<std::wstring, std::shared_ptr<CharacterMesh>> readModelData_;

	/// <summary>
	/// 固定メッシュデータを格納する
	/// </summary>
	std::unordered_map<PrimitiveMeshType, std::shared_ptr<PrimitiveMesh>> primitivModelDatas_;

	Dx12Wrapper& dx12_;
};

template<typename T>
struct TextBuff
{
	UINT textLen{};
	std::vector<T> text{};
};
template<typename T>
struct BDEF1
{
	T boneIndex; //ウェイト1.0の単一ボーン参照インデックス
};
#pragma pack(1)
template<typename T>
struct BDEF2
{
	std::array<T, 2> boneIndex{}; //ボーンの参照インデックス
	float boneWeight1{}; //ボーン1のウェイト値(0〜1.0), ボーン2のウェイト値は 1.0-ボーン1ウェイト
};
#pragma pack()
template<typename T>
struct BDEF4
{
	std::array<T, 4> boneIndex{}; //ボーンの参照インデックス
	std::array<float, 4> boneWeight{}; //ボーンウェイト値(計1.0の保証無し)
};
template<typename T>
struct SDEF
{
	std::array<T, 2> boneIndex{}; //ボーンの参照インデックス
	float boneWeight1{}; //ボーン1のウェイト値(0〜1.0), ボーン2のウェイト値は 1.0-ボーン1ウェイト
	DirectX::XMFLOAT3 SDEF_C{};
	DirectX::XMFLOAT3 SDEF_R0{};
	DirectX::XMFLOAT3 SDEF_R1{};
};

#pragma pack(1)
template<typename T>
struct PMXBone
{
	DirectX::XMFLOAT3 headPos;
	T parentBoneIndex;
	int transformHierarchy;
	uint16_t boneFlag;
};
#pragma pack()
#include "CharacterMesh.h"
#include "../BoneManager.h"
#include "../MorphingManager.h"
#include <d3d12.h>

CharacterMesh::CharacterMesh()
{
	boneManager_ = std::make_shared<BoneManager>();
	morpManager_ = std::make_shared<MorphingManager>();
}

CharacterMesh::~CharacterMesh()
{
}

void CharacterMesh::Update()
{
	boneManager_->Update();
	morpManager_->Update(vertexBuffer_);
}

void CharacterMesh::SetAnimation(const MotionData& set, bool loop, uint32_t interval)
{
	boneManager_->SetAnimation(set, loop, interval);
	morpManager_->SetAnimation(set, loop, interval);
}

std::shared_ptr<BoneManager>& CharacterMesh::GetBoneManager()
{
	return boneManager_;
}

std::shared_ptr<MorphingManager>& CharacterMesh::GetMorphingManager()
{
	return morpManager_;
}

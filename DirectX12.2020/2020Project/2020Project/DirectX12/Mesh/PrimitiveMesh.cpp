#include "PrimitiveMesh.h"
#include "../DirectX12Geometory.h"
#include "../../Application.h"
#include "../MaterialManager.h"
#include "../Material/Material.h"

using namespace std;

constexpr float PlaneXY = 100.0f;

PrimitiveMesh::PrimitiveMesh(const PrimitiveMeshType& type)
{
	auto& wsize = Application::Instance().GetWindowSize();
	auto temp = make_shared<VertexBuffer3D>();
	float planeHalfS = PlaneXY / 2.0f;
	switch (type)
	{
	case PrimitiveMeshType::Plane:
		temp->vertices.push_back(Vertex3D({ -planeHalfS, 0.0f, planeHalfS }, { 0.0f, 0.0f }, { 0.0f,1.0f,0.0f }));
		temp->vertices.push_back(Vertex3D({ planeHalfS, 0.0f, planeHalfS }, { 0.0f, 1.0f }, { 0.0f,1.0f,0.0f }));
		temp->vertices.push_back(Vertex3D({ -planeHalfS, 0.0f, -planeHalfS }, { 1.0f, 0.0f }, { 0.0f,1.0f,0.0f }));
		temp->vertices.push_back(Vertex3D({ planeHalfS, 0.0f, -planeHalfS }, { 1.0f, 1.0f }, { 0.0f,1.0f,0.0f }));
		centerOffset_ = new DirectX::XMFLOAT3(-planeHalfS, 0, -planeHalfS);
		temp->indices.clear();
		temp->indices.push_back(0);
		temp->indices.push_back(1);
		temp->indices.push_back(2);
		temp->indices.push_back(1);
		temp->indices.push_back(3);
		temp->indices.push_back(2);
			break;
	default:
		break;
	}
	vector<Material> mat;
	vector<uint32_t> matNums;
	mat.push_back(Material({ 1,0,0,1 }, { 0,0,0 }, {0,0,0},0));
	for (int i = 0; i < static_cast<int>(temp->indices.size()) / 3 ; ++i)
	{
		matNums.push_back(0);
	}
	vertexBuffer_ = temp;

	materialManager_.SetMaterials(mat);
	materialManager_.SetMatNums(matNums);
}
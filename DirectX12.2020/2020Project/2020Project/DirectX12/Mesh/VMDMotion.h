#pragma once
#include <vector>
#include <DirectXMath.h>
#include <unordered_map>
#include <map>
#include <string>


class VMDMotion
{
public:
	VMDMotion();
	~VMDMotion();
	const MotionData& GetAnimationData(const char* motionName);
private:
	
	bool LoadFromVMD(const char* filePath);
	
	std::vector<std::string> animationNames_; //モーション管理と参照に使う名前
};


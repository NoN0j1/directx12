#pragma once
#include "Mesh.h"
#include <DirectXMath.h>

enum class PrimitiveMeshType
{
	Plane,
	Cube,
	MAX
};

class PrimitiveMesh : public Mesh
{
public:
	PrimitiveMesh(const PrimitiveMeshType& type);
	~PrimitiveMesh() = default;
};


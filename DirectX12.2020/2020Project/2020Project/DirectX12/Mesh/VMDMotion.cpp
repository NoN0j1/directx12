#include "VMDMotion.h"
#include <memory>
#include <Windows.h>
#include <cassert>
#include <algorithm>

using namespace DirectX;
using namespace std;

namespace
{
	std::string GetFilename(const std::string & path)
	{
		int index1 = static_cast<int>(path.find_last_of('/'));
		int index2 = static_cast<int>(path.find_last_of('\\'));
		auto index = max(index1, index2);
		int ext = static_cast<int>(path.find_last_of('.'));

		int length = ext - index;
		auto file = path.substr(static_cast<size_t>(index) + 1, static_cast<size_t>(length) - 1);

		return file;
	}
}

VMDMotion::VMDMotion()
{
}

VMDMotion::~VMDMotion()
{
}


const MotionData& VMDMotion::GetAnimationData(const char* animName)
{
	auto it = animationList_.find(animName);
	if (it == animationList_.end())
	{
		return animationList_["null"] = {};
	}
	return animationList_[animName];
}

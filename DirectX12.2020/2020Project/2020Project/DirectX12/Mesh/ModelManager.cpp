#include <filesystem>
#include <cassert>
#include <d3d12.h>
#include <sstream>
#include <iostream>
#include <iomanip>
#include "CharacterMesh.h"
#include "PrimitiveMesh.h"
#include "../Dx12Wrapper.h"
#include "../Dx12Commons.h"
#include "../DirectX12Geometory.h"
#include "../DirectX12ToolFunction.h"
#include "ModelManager.h"
#include "../Material/Material.h"
#include "../MaterialManager.h"
#include "../ResourceManager.h"
#include "../BoneManager.h"
#include "../MorphingManager.h"

using namespace std;

constexpr const wchar_t* toonPathDir = L"Resource/Model/PMD/toon/";

ModelManager::ModelManager(Dx12Wrapper& wrapper):dx12_(wrapper)
{
	primitivModelDatas_[PrimitiveMeshType::Plane] = make_shared<PrimitiveMesh>(PrimitiveMeshType::Plane);
	auto& add = primitivModelDatas_[PrimitiveMeshType::Plane];
	PrimitiveVertexSetting(add);
	PrimitiveMaterialSetting(add);
}

bool ModelManager::LoadModel(const wstring& path, const std::wstring& name)
{
	if (readModelData_.contains(name))
	{
		return false;
	}
	filesystem::path filePath(path);
	if (!filesystem::exists(filePath))
	{
		OutputDebugString(L"モデルの読み込みパスが不正です。");
		assert(false);
		return false;
	}

	auto ext = filePath.extension();
	if (ext == ".pmd")
	{
		if (!LoadPMD(path, name))
		{
			return false;
		}
	}
	else if (ext == ".pmx")
	{
		if (!LoadPMX(path, name))
		{
			return false;
		}
	}

	auto& add = readModelData_[name];
	ReadModelVertexSetting(add);
	ReadModelMaterialSetting(add);
	ReadModelBoneSetting(add);
	return true;
}

const shared_ptr<Mesh> ModelManager::GetMesh(const std::wstring& name)const
{
	if (readModelData_.contains(name))
	{
		return readModelData_.at(name);
	}
	return nullptr;
}

const shared_ptr<Mesh> ModelManager::GetMesh(const PrimitiveMeshType primType)const
{
	return make_shared<Mesh>(*primitivModelDatas_.at(primType).get());
}

bool ModelManager::LoadPMD(const wstring& path, const wstring& name)
{
	FILE* fp = nullptr;;
	auto error = _wfopen_s(&fp, path.c_str(), L"rb");
	if (error != 0)
	{
		assert(error == 0);
		return false;
	}

	readModelData_[name] = make_shared<CharacterMesh>();
	auto& charaMesh = readModelData_[name];
	auto& materialManager = charaMesh->GetMaterialManager();
	vector<string> texPath;
	vector<uint8_t> attachToonNum;
	uint16_t boneNum = {};
	//順固定↓↓↓
	LoadPMDHeader(fp);
	auto temp = make_shared<VertexBufferPMD>();
	LoadPMDVertex(fp, *temp);
	charaMesh->GetVertexBuffer() = temp;
	LoadPMDMaterial(fp, materialManager, attachToonNum, texPath);
	LoadPMDBone(fp, boneNum, charaMesh);
	LoadPMDIK(fp, charaMesh);
	uint16_t skinNum;
	LoadPMDFaceSkin(fp, charaMesh, skinNum);
	LoadPMDEditorDisplayData(fp, boneNum, skinNum);
	LoadPMDToonData(fp, materialManager, attachToonNum, texPath, path);
	std::fclose(fp);
	return true;
}

void ModelManager::LoadPMDHeader(FILE* fp)
{
#pragma pack(1)
	struct Header
	{
		char signature[3]; // "Pmd"
		//ここにパディング1が入る
		float version; // 00 00 80 3F == 1.00
		char name[20];
		char comment[256];
	};//283バイト
#pragma pack()
	//ぶっちゃけこの情報はほぼ使わないのでfseekで飛ばす
	std::fseek(fp, sizeof(Header), SEEK_CUR);
}

void ModelManager::LoadPMDVertex(FILE* fp, VertexBufferPMD& vb)
{
#pragma pack(1)
	struct PMDVertex
	{
		DirectX::XMFLOAT3 pos; // x, y, z 座標
		DirectX::XMFLOAT3 normalVec; // nx, ny, nz 法線ベクトル
		DirectX::XMFLOAT2 uv; // u, v // UV座標 // MMDは頂点UV
		uint16_t boneNum[2]; // ボーン番号1、番号2 // モデル変形(頂点移動)時に影響
		uint8_t boneWeight; // ボーン1に与える影響度 // min:0 max:100 // ボーン2への影響度は、(100 - bone_weight)
		uint8_t edgeFlag; // 0:通常、1:エッジ無効 //エッジ(輪郭)が有効の場合
	};//合計38バイト
#pragma pack()
	uint32_t vertNum;
	std::fread(&vertNum, sizeof(vertNum), 1, fp); //頂点数読み込み
	//構造体を頂点数分読み込み
	vector<PMDVertex> vert(vertNum);
	std::fread(vert.data(), sizeof(PMDVertex)* vertNum, 1, fp);
	for (int i = 0; i < vert.size(); ++i)
	{
		vb.vertices.push_back(VertexPMD(vert[i].pos, vert[i].uv, vert[i].normalVec, vert[i].boneNum, (vert[i].boneWeight/100.0f)));
	}

	//頂点インデックス数読み込み
	uint32_t indexNum;
	std::fread(&indexNum, sizeof(indexNum), 1, fp);
	//頂点インデックスデータの読み込み
	vb.indices.clear();
	vb.indices.resize(indexNum);
	auto oneIndex = sizeof(uint16_t);
	for (auto& ind : vb.indices)
	{
		uint16_t temp;
		fread(&temp, oneIndex, 1, fp);
		ind = temp;
	}
}

void ModelManager::LoadPMDMaterial(FILE* fp, MaterialManager& mm, std::vector<uint8_t>& toonNum, std::vector<std::string>& loadTexPath)
{
#pragma pack(1)
	struct LoadMaterialParameter
	{
		DirectX::XMFLOAT4 diffuse; //ディフューズ色
		float pPower; //スペキュラ乗数
		DirectX::XMFLOAT3 spcular; //光沢色
		DirectX::XMFLOAT3 ambient; //環境色
		uint8_t toon; //トゥーン配色番号
		uint8_t edge; //輪郭、影
		uint32_t faceVertData; //色を適用するべき面頂点データ
		char texturePath[20]; //テクスチャパス(相対)
	};
#pragma pack()
	unsigned int materialCount;
	fread(&materialCount, sizeof(materialCount), 1, fp); //マテリアル数取得
	std::vector<LoadMaterialParameter> pmdMaterials(materialCount);
	fread(pmdMaterials.data(), sizeof(LoadMaterialParameter), materialCount, fp); //マテリアル情報読み込み

	vector<Material> loadMat = {};
	vector<UINT> matIndexData = {};
	auto& resManager = ResourceManager::Instance();
	for (auto& mat : pmdMaterials)
	{
		loadMat.push_back(Material(mat.diffuse, mat.ambient, mat.spcular, mat.pPower));
	}

	int offset = 0;
	for (int m = 0; m < pmdMaterials.size(); ++m)
	{
		for (uint32_t i = offset; i < (offset + pmdMaterials[m].faceVertData); i += 3)
		{
			matIndexData.push_back(m);
		}
		toonNum.push_back(pmdMaterials[m].toon);
		loadTexPath.push_back(pmdMaterials[m].texturePath);
		offset += pmdMaterials[m].faceVertData;
	}
	mm.SetMatNums(matIndexData);
	mm.SetMaterials(loadMat);
}

void ModelManager::LoadPMDBone(FILE* fp, uint16_t& boneNum, std::shared_ptr<CharacterMesh>& cMesh)
{
#pragma pack(1)
	struct PMDBone
	{
		char boneName[20]; //ボーン名
		uint16_t parentBoneIndex; //親ボーン番号
		uint16_t tailBoneIndex; //tailになるボーン番号
		uint8_t type; //ボーンの種類
		uint16_t ikParentIdx; //影響するIKボーン
		DirectX::XMFLOAT3 headPos; //ボーンのヘッド座標
	};
#pragma pack()
	//ボーン読み込み
	fread(&boneNum, sizeof(boneNum), 1, fp);

	std::vector<PMDBone> readPmdBones(static_cast<int>(boneNum));
	fread(readPmdBones.data(), sizeof(PMDBone), boneNum, fp);
	
	auto& boneManager = cMesh->GetBoneManager();
	auto& boneMatrix = boneManager->GetBoneMatrices();
	auto& boneNodeMap = boneManager->GetBoneMap();
	auto& boneTree = boneManager->GetBoneTree();
	boneMatrix.resize(readPmdBones.size());
	boneTree.resize(readPmdBones.size());
	fill(boneMatrix.begin(), boneMatrix.end(), DirectX::XMMatrixIdentity());

	for (int i = 0; i < readPmdBones.size(); ++i)
	{
		string name(readPmdBones[i].boneName);
		wstring wboneName = WstringFromString(name);
		boneNodeMap[wboneName].boneIndex = i;
		boneNodeMap[wboneName].headPos = readPmdBones[i].headPos;
		boneMatrix[i] = DirectX::XMMatrixIdentity();
		if (readPmdBones[i].parentBoneIndex == 0xffff)
		{
			continue;
		}
		boneTree[readPmdBones[i].parentBoneIndex].push_back(i);
	}

	for (int index = 0; index < readPmdBones.size(); ++index)
	{
		auto& bone = readPmdBones[index];
		string name(bone.boneName);
		wstring wboneName = WstringFromString(name);
		auto& boneNode = boneNodeMap[wboneName];
		boneNode.boneIndex = index;
		boneNode.headPos = bone.headPos;
		boneNode.tailPos = readPmdBones[bone.tailBoneIndex].headPos;
	}
	for (auto& node : boneNodeMap)
	{
		if (readPmdBones[node.second.boneIndex].parentBoneIndex >= readPmdBones.size())
		{
			continue; //親ボーン番号が読み込みサイズより大きいデータの場合はそいつが一番上の親
		}
		auto parentIndex = readPmdBones[node.second.boneIndex].parentBoneIndex;
		string parentName(readPmdBones[parentIndex].boneName);
		wstring wName = WstringFromString(parentName);
		boneNodeMap[wName].children.push_back(&node.second);
	}
}

void ModelManager::LoadPMDIK(FILE* fp, std::shared_ptr<CharacterMesh>& cMesh)
{
#pragma pack(1)
	struct IK
	{
		uint16_t IKBoneIndex{}; //IK対象ボーン番号
		uint16_t IKTargetBoneIndex{}; //IK対象ボーンが一番最初に目標にするボーン
		uint8_t IKChildLength{}; //IKチェーンの長さ(子の数)
		uint16_t iretations{}; //再帰演算回数 //IK値1
		float controlWeight{}; //再帰演算1回あたりの制限角度 //IK値2
		vector<uint16_t> ChildBoneIndices{}; //IKの影響を受けるボーン番号
	};
#pragma pack()
	uint16_t ikNum{};
	fread(&ikNum, sizeof(ikNum), 1, fp);
	vector<IK> pmdIK(ikNum);
	for (auto& ik : pmdIK)
	{
		fread(&ik, sizeof(IK) - sizeof(vector<uint16_t>), 1, fp);
		ik.ChildBoneIndices.resize(ik.IKChildLength);
		fread(ik.ChildBoneIndices.data(), sizeof(uint16_t), ik.IKChildLength, fp);
	}
}

void ModelManager::LoadPMDFaceSkin(FILE* fp, std::shared_ptr<CharacterMesh>& cMesh, uint16_t& skinNum)
{
#pragma pack(1)
	struct SkinVertexData
	{
		SkinVertexData() {}
		uint32_t skinVertIndex{}; //表情用の頂点番号(頂点リストの番号と連動) //type:Base以外はbaseの番号を指す
		DirectX::XMFLOAT3 skinVertPos{}; //表情用の頂点座標 //type:Base以外は座標のオフセットになる
	};

	//struct SkinData
	//{
	//	SkinData() {}
	//	char skinName[20]{}; //表情名
	//	uint32_t skinVertCount{}; //表情用の頂点数
	//	uint8_t skinType{}; //表情の種類 //0:base, 1:まゆ、 2:目、 3:リップ、4:その他
	//	vector<SkinVertexData> skinVertices{}; //表情用の頂点データ
	//};
#pragma pack()
	fread(&skinNum, sizeof(skinNum), 1, fp);
	//vector<SkinData> pmdSkins(skinNum);

	auto& morphManager = cMesh->GetMorphingManager();
	size_t readSize = 0;
	for (int i = 0; i < skinNum; ++i)
	{
		char skinName[20];
		fread_s(skinName, 20, 20, 1, fp);
		auto& skinData = morphManager->GetPMDSkinData(skinName).skindata;
		uint32_t skinVertCount = 0;
		readSize = fread_s(&skinVertCount, sizeof(skinVertCount), sizeof(skinVertCount), 1, fp);
		fseek(fp, 1, SEEK_CUR);//表情種別を飛ばす
		skinData.resize(skinVertCount);
		for (auto& data : skinData)
		{
			fread(&data.first, sizeof(data.first), 1, fp);
			fread(&data.second, sizeof(data.second), 1, fp);
		}
	}
	//morphManager->Update(cMesh->GetVertexBuffer());
	/*for (auto& skin : pmdSkins)
	{
		fread(&skin, sizeof(SkinData) - sizeof(vector<SkinVertexData>), 1, fp);
		skin.skinVertices.resize(skin.skinVertCount);
		fread(skin.skinVertices.data(), sizeof(SkinVertexData), skin.skinVertCount, fp);
	}*/
}

void ModelManager::LoadPMDEditorDisplayData(FILE* fp, uint16_t boneNum, uint16_t skinNum)
{
	uint8_t skinDisplayNum; //エディタ表示用
	fread(&skinDisplayNum, sizeof(skinDisplayNum), 1, fp);
	fseek(fp, sizeof(uint16_t) * skinDisplayNum, SEEK_CUR); //表情の表示用データは飛ばす(エディタ用？)

	uint8_t boneFrameDisplayNum;
	fread(&boneFrameDisplayNum, sizeof(boneFrameDisplayNum), 1, fp);
	fseek(fp, sizeof(char[50]) * boneFrameDisplayNum, SEEK_CUR); //ボーン枠用の枠名データ、不要なので(エディタ用？)

#pragma pack(1)
	struct BoneDisplayData
	{
		uint16_t index;
		uint8_t displayFrameIndex;
	};
#pragma pack()
	uint32_t boneDisplayNum; //表示名は不要なので飛ばす(エディタ用？)
	fread(&boneDisplayNum, sizeof(boneDisplayNum), 1, fp);
	fseek(fp, sizeof(BoneDisplayData) * boneDisplayNum, SEEK_CUR);


	bool EnglishCompatibility; //英名の互換性
	fread(&EnglishCompatibility, sizeof(EnglishCompatibility), 1, fp);
	if (EnglishCompatibility)
	{
		//英語対応データが存在するが全部不要なので飛ばす
		fseek(fp, sizeof(char[20]) + sizeof(char[256]), SEEK_CUR); //モデル名、コメント名
		fseek(fp, sizeof(char[20]) * boneNum, SEEK_CUR); //ボーン名
		fseek(fp, sizeof(char[20]) * (skinNum-1), SEEK_CUR); //表情の英語表示用
		fseek(fp, sizeof(char[50]) * boneFrameDisplayNum, SEEK_CUR); //ボーン枠用、枠名リスト
	}
}

void ModelManager::LoadPMDToonData(FILE* fp, MaterialManager& mm, const std::vector<uint8_t>& toonNum, const std::vector<std::string>& loadTexPath, const wstring& path)
{
	auto loadMat = mm.GetMaterials();
	auto& resManager = ResourceManager::Instance();
	std::array<char[100], 10> toonTextureFile;
	fread(toonTextureFile.data(), sizeof(toonTextureFile[0]), toonTextureFile.size(), fp);

	filesystem::path modelDir = GetDirectoryFromPath(path);
	for (int i = 0; i < loadMat.size(); ++i)
	{
		uint32_t texHandle = static_cast<int>(DummyTextureType::White);
		uint32_t sphHandle = static_cast<int>(DummyTextureType::White);
		uint32_t spaHandle = static_cast<int>(DummyTextureType::Black);
		uint32_t toonHandle = static_cast<int>(DummyTextureType::Gradation);
		vector<int> addTextureHandle;
		auto separeteStr = SeparateString(loadTexPath[i]);

		for (auto& str : separeteStr)
		{
			filesystem::path fpath(modelDir.c_str() + WstringFromString(str));

			auto ext = fpath.extension();
			if (ext == L".sph")
			{
				sphHandle = resManager.LoadGraph(fpath.c_str());
				continue;
			}
			if (ext == L".spa")
			{
				spaHandle = resManager.LoadGraph(fpath.c_str());
				continue;
			}
			if (str != "")
			{
				texHandle = resManager.LoadGraph(fpath.c_str());
				continue;
			}
		}
		auto tempStr = string(toonTextureFile.at(toonNum[i]));
		auto toonPath = filesystem::path(modelDir.c_str() + WstringFromString(tempStr));
		if (!filesystem::exists(toonPath))
		{
			toonPath = filesystem::path(toonPathDir + WstringFromString(tempStr));
		}

		toonHandle = resManager.LoadGraph(toonPath.c_str());
		loadMat[i].texHandle = texHandle;

		addTextureHandle.push_back(sphHandle);
		addTextureHandle.push_back(spaHandle);
		addTextureHandle.push_back(toonHandle);
		mm.AddIntParameter(addTextureHandle);
	}
	mm.SetMaterials(loadMat);
}

bool ModelManager::LoadPMX(const std::wstring& path, const std::wstring& name)
{
	FILE* fp = nullptr;;
	auto error = _wfopen_s(&fp, path.c_str(), L"rb");
	if (error != 0)
	{
		assert(error == 0);
		return false;
	}

	readModelData_[name] = make_shared<CharacterMesh>();
	auto& charaMesh = readModelData_[name];
#pragma pack(1)
	struct PMXHeader
	{
		uint8_t type[4];
		float version;
		uint8_t byteSize;
	};
#pragma pack()
	/*バイト列 - byte
		[0] - エンコード方式 | 0:UTF16 1 : UTF8
		[1] - 追加UV数 | 0〜4 詳細は頂点参照

		[2] - 頂点Indexサイズ | 1, 2, 4 のいずれか
		[3] - テクスチャIndexサイズ | 1, 2, 4 のいずれか
		[4] - 材質Indexサイズ | 1, 2, 4 のいずれか
		[5] - ボーンIndexサイズ | 1, 2, 4 のいずれか
		[6] - モーフIndexサイズ | 1, 2, 4 のいずれか
		[7] - 剛体Indexサイズ | 1, 2, 4 のいずれか*/
	vector<uint8_t> byteData{};
	PMXHeader header;
	fread(&header, sizeof(header), 1, fp);
	for (int i = 0; i < header.byteSize; ++i)
	{
		uint8_t temp;
		fread(&temp, sizeof(temp), 1, fp);
		byteData.push_back(temp);
	}
	for (int info = 0; info < 4; ++info)
	{
		if (byteData[0] == 0)
		{
			//2バイト文字
			TextBuff<wchar_t> modelInfo;
			fread(&modelInfo.textLen, sizeof(modelInfo.textLen), 1, fp);
			modelInfo.text.resize(modelInfo.textLen / 2);
			fread(modelInfo.text.data(), sizeof(wchar_t) * modelInfo.text.size(), 1, fp);
		}
		else
		{
			//1バイト文字
			TextBuff<char> modelInfo;
			fread(&modelInfo.textLen, sizeof(modelInfo.textLen), 1, fp);
			modelInfo.text.resize(modelInfo.textLen);
			fread(modelInfo.text.data(), sizeof(char) * modelInfo.text.size(), 1, fp);
		}
	}

#pragma pack(1)
	struct PMXVertexCommon
	{
		DirectX::XMFLOAT3 vpos;
		DirectX::XMFLOAT3 normal;
		DirectX::XMFLOAT2 uv;
	};
#pragma pack()
	uint32_t vertNum;
	fread(&vertNum, sizeof(vertNum), 1, fp); //頂点数読み込み

	vector<DirectX::XMFLOAT4> AddUV(byteData[1]);
	PMXVertexCommon vcommon;
	auto tempvb = make_shared<VertexBufferPMX>();
	uint8_t boneWeightSDEFType;
	for (uint32_t v = 0; v < vertNum; ++v)
	{
		//頂点、法線、UV読み込み
		fread(&vcommon, sizeof(vcommon), 1, fp);
		//追加UV読み込み
		if (AddUV.size() != 0)
		{
			fread(AddUV.data(), sizeof(AddUV[0]), sizeof(AddUV.size()), fp);
		}
		
		fread(&boneWeightSDEFType, sizeof(boneWeightSDEFType), 1, fp);
		float edgeScale{};
		tempvb->vertices.push_back(VertexPMX(vcommon.vpos, vcommon.uv, vcommon.normal, boneWeightSDEFType));
		
		auto& vb = tempvb->vertices.back();
		switch (boneWeightSDEFType)
		{
		case 0:
			if (byteData[5] == 1)
			{
				BDEF1<uint8_t> vertexWeight;
				fread(&vertexWeight, sizeof(vertexWeight), 1, fp);
				vb.boneWeight[0] = 1.0f;
				vb.boneNo[0] = vertexWeight.boneIndex;
			}
			else if(byteData[5] == 2)
			{
				BDEF1<uint16_t> vertexWeight;
				fread(&vertexWeight, sizeof(vertexWeight), 1, fp);
				vb.boneWeight[0] = 1.0f;
				vb.boneNo[0] = vertexWeight.boneIndex;
			}
			else
			{
				BDEF1<uint32_t> vertexWeight;
				fread(&vertexWeight, sizeof(vertexWeight), 1, fp);
				vb.boneWeight[0] = 1.0f;
				vb.boneNo[0] = vertexWeight.boneIndex;
			}
		break;

		case 1:
			if (byteData[5] == 1)
			{
				BDEF2<uint8_t> vertexWeight;
				fread(&vertexWeight, sizeof(vertexWeight), 1, fp);
				vb.boneWeight[0] = vertexWeight.boneWeight1 / 100.0f;
				vb.boneWeight[1] = 1.0f - vb.boneWeight[0];
				vb.boneNo[0] = vertexWeight.boneIndex[0];
				vb.boneNo[1] = vertexWeight.boneIndex[1];
			}
			else if (byteData[5] == 2)
			{
				BDEF2<uint16_t> vertexWeight;
				fread(&vertexWeight, sizeof(vertexWeight), 1, fp);
				vb.boneWeight[0] = vertexWeight.boneWeight1 / 100.0f;
				vb.boneWeight[1] = 1.0f - vb.boneWeight[0];
				vb.boneNo[0] = vertexWeight.boneIndex[0];
				vb.boneNo[1] = vertexWeight.boneIndex[1];
			}
			else
			{
				BDEF2<uint32_t> vertexWeight;
				fread(&vertexWeight, sizeof(vertexWeight), 1, fp);
				vb.boneWeight[0] = vertexWeight.boneWeight1 / 100.0f;
				vb.boneWeight[1] = 1.0f - vb.boneWeight[0];
				vb.boneNo[0] = vertexWeight.boneIndex[0];
				vb.boneNo[1] = vertexWeight.boneIndex[1];
			}
			break;

		case 2:
			if (byteData[5] == 1)
			{
				BDEF4<uint8_t> vertexWeight;
				fread(&vertexWeight, sizeof(vertexWeight), 1, fp);
				for (int i = 0; i < vertexWeight.boneWeight.size(); ++i)
				{
					vb.boneWeight[i] = vertexWeight.boneWeight[i] / 100.0f;
					vb.boneNo[i] = vertexWeight.boneIndex[i];
 				}
			}
			else if (byteData[5] == 2)
			{
				BDEF4<uint16_t> vertexWeight;
				fread(&vertexWeight, sizeof(vertexWeight), 1, fp);
				for (int i = 0; i < vertexWeight.boneWeight.size(); ++i)
				{
					vb.boneWeight[i] = vertexWeight.boneWeight[i] / 100.0f;
					vb.boneNo[i] = vertexWeight.boneIndex[i];
				}
			}
			else
			{
				BDEF4<uint32_t> vertexWeight;
				fread(&vertexWeight, sizeof(vertexWeight), 1, fp);
				for (int i = 0; i < vertexWeight.boneWeight.size(); ++i)
				{
					vb.boneWeight[i] = vertexWeight.boneWeight[i] / 100.0f;
					vb.boneNo[i] = vertexWeight.boneIndex[i];
				}
			}
			break;

		case 3:
			if (byteData[5] == 1)
			{
				SDEF<uint8_t> vertexWeight;
				fread(&vertexWeight, sizeof(vertexWeight), 1, fp);
				vb.boneWeight[0] = vertexWeight.boneWeight1 / 100.0f;
				vb.boneWeight[1] = 1.0f - vb.boneWeight[0];
				vb.boneNo[0] = vertexWeight.boneIndex[0];
				vb.boneNo[1] = vertexWeight.boneIndex[1];
				vb.SDEF_C = vertexWeight.SDEF_C;
				vb.SDEF_R0 = vertexWeight.SDEF_R0;
				vb.SDEF_R1 = vertexWeight.SDEF_R1;
			}
			else if (byteData[5] == 2)
			{
				SDEF<uint16_t> vertexWeight;
				fread(&vertexWeight, sizeof(vertexWeight), 1, fp);
				vb.boneWeight[0] = vertexWeight.boneWeight1 / 100.0f;
				vb.boneWeight[1] = 1.0f - vb.boneWeight[0];
				vb.boneNo[0] = vertexWeight.boneIndex[0];
				vb.boneNo[1] = vertexWeight.boneIndex[1];
				vb.SDEF_C = vertexWeight.SDEF_C;
				vb.SDEF_R0 = vertexWeight.SDEF_R0;
				vb.SDEF_R1 = vertexWeight.SDEF_R1;
			}
			else
			{
				SDEF<uint32_t> vertexWeight;
				fread(&vertexWeight, sizeof(vertexWeight), 1, fp);
				vb.boneWeight[0] = vertexWeight.boneWeight1 / 100.0f;
				vb.boneWeight[1] = 1.0f - vb.boneWeight[0];
				vb.boneNo[0] = vertexWeight.boneIndex[0];
				vb.boneNo[1] = vertexWeight.boneIndex[1];
				vb.SDEF_C = vertexWeight.SDEF_C;
				vb.SDEF_R0 = vertexWeight.SDEF_R0;
				vb.SDEF_R1 = vertexWeight.SDEF_R1;
			}
			break;
		default:
			break;
		}
		fread(&edgeScale, sizeof(edgeScale), 1, fp);
	}

	uint32_t faceNum;
	fread(&faceNum, sizeof(faceNum), 1, fp);
	if (byteData[2] == 1)
	{
		for (uint32_t f = 0; f < faceNum; ++f)
		{
			uint8_t temp;
			fread(&temp, sizeof(temp), 1, fp);
			tempvb->indices.push_back(static_cast<uint32_t>(temp));
		}
	}
	else if (byteData[2] == 2)
	{
		for (uint32_t f = 0; f < faceNum; ++f)
		{
			uint16_t temp;
			fread(&temp, sizeof(temp), 1, fp);
			tempvb->indices.push_back(static_cast<uint32_t>(temp));
		}
	}
	else
	{
		for (uint32_t f = 0; f < faceNum; ++f)
		{
			uint32_t temp;
			fread(&temp, sizeof(temp), 1, fp);
			tempvb->indices.push_back(static_cast<uint32_t>(temp));
		}
	}
	charaMesh->GetVertexBuffer() = tempvb;

	uint32_t texNum;
	fread(&texNum, sizeof(texNum), 1, fp);
	
	vector<int> texTable;
	auto& resManager = ResourceManager::Instance();
	vector<wstring> pathTable;
	for (uint32_t tex = 0; tex < texNum; ++tex)
	{
		if (byteData[0] == 0)
		{
			//2バイト文字
			TextBuff<wchar_t> modelInfo;
			fread(&modelInfo.textLen, sizeof(modelInfo.textLen), 1, fp);
			modelInfo.text.resize(modelInfo.textLen/2);
			fread(modelInfo.text.data(), sizeof(wchar_t) * modelInfo.text.size(), 1, fp);
			filesystem::path temp(wstring(modelInfo.text.data(), modelInfo.text.size()));
			texTable.push_back(resManager.LoadGraph(GetDirectoryFromPath(path) + temp.c_str()));
			pathTable.push_back(GetDirectoryFromPath(path) + temp.c_str());
		}
		else
		{
			//1バイト文字
			TextBuff<char> modelInfo;
			fread(&modelInfo.textLen, sizeof(modelInfo.textLen), 1, fp);
			modelInfo.text.resize(modelInfo.textLen);
			fread(modelInfo.text.data(), sizeof(char) * modelInfo.text.size(), 1, fp);
			filesystem::path temp(string(modelInfo.text.data(), modelInfo.text.size()));
			texTable.push_back(resManager.LoadGraph(GetDirectoryFromPath(path) + temp.c_str()));
			pathTable.push_back(GetDirectoryFromPath(path) + temp.c_str());
		}
	}


#pragma pack(1)
	struct PMXMaterial
	{
		DirectX::XMFLOAT4 diffuse;
		DirectX::XMFLOAT3 specular;
		float specularPow;
		DirectX::XMFLOAT3 ambient;
		char bitFlag;
		DirectX::XMFLOAT4 edgeColor;
		float edgeSize;
	};
#pragma pack()
	uint32_t matNum;
	fread(&matNum, sizeof(matNum), 1, fp);
	auto& matManager = charaMesh->GetMaterialManager();
	vector<Material> loadMat = {};
	vector<UINT> matIndexData = {};
	std::vector<int> faceVertNum{};//マテリアル対応頂点数

	for (uint32_t m = 0; m < matNum; ++m)
	{
		for (int i = 0; i < 2; ++i)
		{
			//マテリアル名と英マテリアル名読み込み
			if (byteData[0] == 0)
			{
				//2バイト文字
				TextBuff<wchar_t> matInfo;
				fread(&matInfo.textLen, sizeof(matInfo.textLen), 1, fp);
				matInfo.text.resize(matInfo.textLen / 2);
				fread(matInfo.text.data(), sizeof(wchar_t) * matInfo.text.size(), 1, fp);
			}
			else
			{
				//1バイト文字
				TextBuff<char> matInfo;
				fread(&matInfo.textLen, sizeof(matInfo.textLen), 1, fp);
				matInfo.text.resize(matInfo.textLen);
				fread(matInfo.text.data(), sizeof(char) * matInfo.text.size(), 1, fp);
			}
		}
		PMXMaterial pmxMaterial;
		fread(&pmxMaterial, sizeof(pmxMaterial), 1, fp);
		//テクスチャIndex読み込み
		if (byteData[3] == 1)
		{
			uint8_t tempIndex;
			fread(&tempIndex, sizeof(tempIndex), 1, fp);
			auto texHandle = texTable[tempIndex];
			loadMat.push_back(Material(pmxMaterial.diffuse, pmxMaterial.ambient, pmxMaterial.specular, pmxMaterial.specularPow, texHandle));
		}
		else if (byteData[3] == 2)
		{
			uint16_t tempIndex;
			fread(&tempIndex, sizeof(tempIndex), 1, fp);
			auto texHandle = texTable[tempIndex];
			loadMat.push_back(Material(pmxMaterial.diffuse, pmxMaterial.ambient, pmxMaterial.specular, pmxMaterial.specularPow, texHandle));
		}
		else
		{
			uint32_t tempIndex;
			fread(&tempIndex, sizeof(tempIndex), 1, fp);
			auto texHandle = texTable[tempIndex];
			loadMat.push_back(Material(pmxMaterial.diffuse, pmxMaterial.ambient, pmxMaterial.specular, pmxMaterial.specularPow, texHandle));
		}

		uint32_t sphereTex = {};

		//スフィアテクスチャ読み込み
		if (byteData[3] == 1)
		{
			uint8_t tempIndex;
			fread(&tempIndex, sizeof(tempIndex), 1, fp);
			if (tempIndex != static_cast<uint8_t>(-1))
			{
				sphereTex = texTable[tempIndex];
			}
		}
		else if (byteData[3] == 2)
		{
			uint16_t tempIndex;
			fread(&tempIndex, sizeof(tempIndex), 1, fp);
			if (tempIndex != static_cast<uint16_t>(-1))
			{
				sphereTex = texTable[tempIndex];
			}
		}
		else
		{
			uint32_t tempIndex;
			fread(&tempIndex, sizeof(tempIndex), 1, fp);
			if (tempIndex != static_cast<uint32_t>(-1))
			{
				sphereTex = texTable[tempIndex];
			}
		}

		uint32_t sphHandle = static_cast<int>(DummyTextureType::White);
		uint32_t spaHandle = static_cast<int>(DummyTextureType::Black);
		uint32_t toonHandle = static_cast<int>(DummyTextureType::Gradation);

		char sphereMode;
		fread(&sphereMode, sizeof(sphereMode), 1, fp);
		if (sphereMode == 1)
		{
			sphHandle = sphereTex;
		}
		else if (sphereMode == 2)
		{
			spaHandle = sphereTex;
		}
		bool commonToonFlag{};
		fread(&commonToonFlag, sizeof(commonToonFlag), 1, fp);
		
		vector<int> addTextureHandle;
		if (commonToonFlag)
		{
			//共有を使う時
			char toonNum;
			fread(&toonNum, sizeof(toonNum), 1, fp);
			toonNum += 1;
			wstringstream wss;
			wss << setw(2) << setfill(L'0') << (int)toonNum << ".bmp";
			//auto pathNum = wstring() + WstringFromString(string(&toonNum));
			filesystem::path tempPath(wstring(toonPathDir) + wstring(L"toon") + wss.str());
			toonHandle = resManager.LoadGraph(tempPath);
		}
		else
		{
			//ToonテクスチャIndex読み込み
			if (byteData[3] == 1)
			{
				uint8_t tempIndex;
				fread(&tempIndex, sizeof(tempIndex), 1, fp);
				if (tempIndex != static_cast<uint8_t>(-1))
				{
					toonHandle = texTable[tempIndex];
				}
			}
			else if (byteData[3] == 2)
			{
				uint16_t tempIndex;
				fread(&tempIndex, sizeof(tempIndex), 1, fp);
				if (tempIndex != static_cast<uint16_t>(-1))
				{
					toonHandle = texTable[tempIndex];
				}
			}
			else
			{
				uint32_t tempIndex;
				fread(&tempIndex, sizeof(tempIndex), 1, fp);
				if (tempIndex != static_cast<uint32_t>(-1))
				{
					toonHandle = texTable[tempIndex];
				}
			}
		}
		addTextureHandle.push_back(sphHandle);
		addTextureHandle.push_back(spaHandle);
		addTextureHandle.push_back(toonHandle);
		matManager.AddIntParameter(addTextureHandle);

		//マテリアルメモ読み込み
		if (byteData[0] == 0)
		{
			//2バイト文字
			TextBuff<wchar_t> matMemo;
			fread(&matMemo.textLen, sizeof(matMemo.textLen), 1, fp);
			matMemo.text.resize(matMemo.textLen / 2);
			fread(matMemo.text.data(), sizeof(wchar_t) * matMemo.text.size(), 1, fp);
		}
		else
		{
			//1バイト文字
			TextBuff<char> matMemo;
			fread(&matMemo.textLen, sizeof(matMemo.textLen), 1, fp);
			matMemo.text.resize(matMemo.textLen);
			fread(matMemo.text.data(), sizeof(char) * matMemo.text.size(), 1, fp);
		}
		uint32_t faceNum;
		fread(&faceNum, sizeof(faceNum), 1, fp);
		faceVertNum.push_back(faceNum);
	}

	int offset = 0;
	for (int i = 0; i < loadMat.size(); ++i)
	{
		for (int face = offset; face < offset + faceVertNum[i]; face+=3)
		{
			matIndexData.push_back(i);
		}

		offset += faceVertNum[i];
	}
	matManager.SetMaterials(loadMat);
	matManager.SetMatNums(matIndexData);

	//ボーン読み込み
	int boneNum = 0;
	fread(&boneNum, sizeof(boneNum), 1, fp);
	auto& bManager = charaMesh->GetBoneManager();
	auto& bNodeMap = bManager->GetBoneMap();
	auto& boneTree = bManager->GetBoneTree();
	auto& boneMatrix = bManager->GetBoneMatrices();
	struct TempBone
	{
		wstring boneName; //ボーン名
		wstring EngBoneName; //ボーン名
		uint32_t parentBoneIndex{}; //親ボーン番号
		uint32_t tailBoneIndex{}; //tailになるボーン番号
		DirectX::XMFLOAT3 headPos{}; //ボーンのヘッド座標
	};

	std::vector<TempBone> readNode(boneNum);
	boneTree.resize(boneNum);
	wstring readBoneName = {};
	wstring readEngBoneName = {};
	for (int i = 0; i < boneNum; ++i)
	{
		TempBone tempNode{};

		//ボーン名読み込み
		if (byteData[0] == 0)
		{
			//2バイト文字
			TextBuff<wchar_t> boneName;
			fread(&boneName.textLen, sizeof(boneName.textLen), 1, fp);
			boneName.text.resize(boneName.textLen / 2);
			fread(boneName.text.data(), sizeof(wchar_t) * boneName.text.size(), 1, fp);
			readBoneName = wstring(boneName.text.begin(), boneName.text.end());
			tempNode.boneName = readBoneName;
			bNodeMap[readBoneName] = {};
		}
		else
		{
			//1バイト文字
			TextBuff<char> boneName;
			fread(&boneName.textLen, sizeof(boneName.textLen), 1, fp);
			boneName.text.resize(boneName.textLen);
			fread(boneName.text.data(), sizeof(char) * boneName.text.size(), 1, fp);
			string tempName(boneName.text.begin(), boneName.text.end());
			readBoneName = WstringFromString(tempName);
			tempNode.boneName = readBoneName;
			bNodeMap[readBoneName] = {};
		}

		//ボーン名読み込み
		if (byteData[0] == 0)
		{
			//2バイト文字
			TextBuff<wchar_t> boneName;
			fread(&boneName.textLen, sizeof(boneName.textLen), 1, fp);
			boneName.text.resize(boneName.textLen / 2);
			fread(boneName.text.data(), sizeof(wchar_t) * boneName.text.size(), 1, fp);
			readEngBoneName = wstring(boneName.text.begin(), boneName.text.end());
			if (!readEngBoneName.empty())
			{
				//bNodeMap[readEngBoneName];
			}
			tempNode.EngBoneName = readEngBoneName;
		}
		else
		{
			//1バイト文字
			TextBuff<char> boneName;
			fread(&boneName.textLen, sizeof(boneName.textLen), 1, fp);
			boneName.text.resize(boneName.textLen);
			fread(boneName.text.data(), sizeof(char) * boneName.text.size(), 1, fp);
			string tempName(boneName.text.begin(), boneName.text.end());
			readEngBoneName = WstringFromString(tempName);
			if (!readEngBoneName.empty())
			{
				//bNodeMap[readEngBoneName];
			}
			tempNode.EngBoneName = readEngBoneName;
		}

		if (byteData[5] == 1)
		{
			PMXBone<uint8_t> pmxb;
			fread(&pmxb, sizeof(pmxb), 1, fp);
			tempNode.headPos = pmxb.headPos;
			tempNode.parentBoneIndex = pmxb.parentBoneIndex;
			if (pmxb.parentBoneIndex != 0xffff)
			{
				boneTree[pmxb.parentBoneIndex].push_back(i);
			}
			//接続先:0 の場合
			if ((pmxb.boneFlag & 0x0001) != 0x0001)
			{
				//ボーンに接続していない
				DirectX::XMFLOAT3 offsetFromBone; //テイルの座標
				fread(&offsetFromBone, sizeof(offsetFromBone), 1, fp);

				DirectX::XMFLOAT3 total(pmxb.headPos.x + offsetFromBone.x, pmxb.headPos.y + offsetFromBone.y, pmxb.headPos.z + offsetFromBone.z);
				bNodeMap[readBoneName].tailPos = total;
			}
			else
			{
				//ボーンに接続している
				decltype(pmxb.parentBoneIndex)  tailBoneIdx;
				fread(&tailBoneIdx, sizeof(tailBoneIdx), 1, fp);
				tempNode.tailBoneIndex = tailBoneIdx;
			}

			//回転付与:1 または 移動付与:1 の場合
			if (((pmxb.boneFlag & 0x0100) == 0x0100) || ((pmxb.boneFlag & 0x0200) == 0x0200))
			{
				decltype(pmxb.parentBoneIndex) parentAddBoneIdx;
				fread(&parentAddBoneIdx, sizeof(parentAddBoneIdx), 1, fp);
				float addPercent;
				fread(&addPercent, sizeof(addPercent), 1, fp);
			}

			//軸固定:1 の場合
			if ((pmxb.boneFlag & 0x0400) == 0x0400)
			{
				DirectX::XMFLOAT3 axisVec;
				fread(&axisVec, sizeof(axisVec), 1, fp);
			}

			//ローカル軸:1の場合
			if ((pmxb.boneFlag & 0x0800) == 0x0800)
			{
				std::array<DirectX::XMFLOAT3, 2> axisXZVec;
				fread(&axisXZVec, sizeof(axisXZVec), 1, fp);
			}

			//外部親変形:1の場合
			if ((pmxb.boneFlag & 0x2000) == 0x2000)
			{
				int keyValue;
				fread(&keyValue, sizeof(keyValue), 1, fp);
			}

			//IK:1の場合
			if ((pmxb.boneFlag & 0x0020) == 0x0020)
			{
				decltype(pmxb.parentBoneIndex) ikTargetBoneIdx;
				fread(&ikTargetBoneIdx, sizeof(ikTargetBoneIdx), 1, fp);
				int ikLoop;
				fread(&ikLoop, sizeof(ikLoop), 1, fp);
				float ikradLimit;
				fread(&ikradLimit, sizeof(ikradLimit), 1, fp);
				int ikLinkNum;
				fread(&ikLinkNum, sizeof(ikLinkNum), 1, fp);

				decltype(pmxb.parentBoneIndex) linkBoneIdx;
				for (int link = 0; link < ikLinkNum; ++link)
				{
					fread(&linkBoneIdx, sizeof(linkBoneIdx), 1, fp);
					bool radLimit;
					fread(&radLimit, sizeof(radLimit), 1, fp);
					if (radLimit)
					{
						array<DirectX::XMFLOAT3, 2> LimitRadData;
						fread(&LimitRadData, sizeof(LimitRadData), 1, fp);
					}
				}
			}
		}
		else if (byteData[5] == 2)
		{
			PMXBone<uint16_t> pmxb;
			fread(&pmxb, sizeof(pmxb), 1, fp);
			tempNode.headPos = pmxb.headPos;
			tempNode.parentBoneIndex = pmxb.parentBoneIndex;
			if (pmxb.parentBoneIndex != 0xffff)
			{
				boneTree[pmxb.parentBoneIndex].push_back(i);
			}
			//接続先:0 の場合
			if ((pmxb.boneFlag & 0x0001) != 0x0001)
			{
				//ボーンに接続していない
				DirectX::XMFLOAT3 offsetFromBone; //テイルの座標
				fread(&offsetFromBone, sizeof(offsetFromBone), 1, fp);

				DirectX::XMFLOAT3 total(pmxb.headPos.x + offsetFromBone.x, pmxb.headPos.y + offsetFromBone.y, pmxb.headPos.z + offsetFromBone.z);
				bNodeMap[readBoneName].tailPos = total;
			}
			else
			{
				//ボーンに接続している
				decltype(pmxb.parentBoneIndex)  tailBoneIdx;
				fread(&tailBoneIdx, sizeof(tailBoneIdx), 1, fp);
				tempNode.tailBoneIndex = tailBoneIdx;
			}

			//回転付与:1 または 移動付与:1 の場合
			if (((pmxb.boneFlag & 0x0100) == 0x0100) || ((pmxb.boneFlag & 0x0200) == 0x0200))
			{
				decltype(pmxb.parentBoneIndex) parentAddBoneIdx;
				fread(&parentAddBoneIdx, sizeof(parentAddBoneIdx), 1, fp);
				float addPercent;
				fread(&addPercent, sizeof(addPercent), 1, fp);
			}

			//軸固定:1 の場合
			if ((pmxb.boneFlag & 0x0400) == 0x0400)
			{
				DirectX::XMFLOAT3 axisVec;
				fread(&axisVec, sizeof(axisVec), 1, fp);
			}

			//ローカル軸:1の場合
			if ((pmxb.boneFlag & 0x0800) == 0x0800)
			{
				std::array<DirectX::XMFLOAT3, 2> axisXZVec;
				fread(&axisXZVec, sizeof(axisXZVec), 1, fp);
			}

			//外部親変形:1の場合
			if ((pmxb.boneFlag & 0x2000) == 0x2000)
			{
				int keyValue;
				fread(&keyValue, sizeof(keyValue), 1, fp);
			}

			//IK:1の場合
			if ((pmxb.boneFlag & 0x0020) == 0x0020)
			{
				decltype(pmxb.parentBoneIndex) ikTargetBoneIdx;
				fread(&ikTargetBoneIdx, sizeof(ikTargetBoneIdx), 1, fp);
				int ikLoop;
				fread(&ikLoop, sizeof(ikLoop), 1, fp);
				float ikradLimit;
				fread(&ikradLimit, sizeof(ikradLimit), 1, fp);
				int ikLinkNum;
				fread(&ikLinkNum, sizeof(ikLinkNum), 1, fp);

				decltype(pmxb.parentBoneIndex) linkBoneIdx;
				for (int link = 0; link < ikLinkNum; ++link)
				{
					fread(&linkBoneIdx, sizeof(linkBoneIdx), 1, fp);
					bool radLimit;
					fread(&radLimit, sizeof(radLimit), 1, fp);
					if (radLimit)
					{
						array<DirectX::XMFLOAT3, 2> LimitRadData;
						fread(&LimitRadData, sizeof(LimitRadData), 1, fp);
					}
				}
			}
		}
		else
		{
			PMXBone<uint32_t> pmxb;
			fread(&pmxb, sizeof(pmxb), 1, fp);
			tempNode.headPos = pmxb.headPos;
			tempNode.parentBoneIndex = pmxb.parentBoneIndex;
			if (pmxb.parentBoneIndex != 0xffff)
			{
				boneTree[pmxb.parentBoneIndex].push_back(i);
			}
			//接続先:0 の場合
			if ((pmxb.boneFlag & 0x0001) != 0x0001)
			{
				//ボーンに接続していない
				DirectX::XMFLOAT3 offsetFromBone; //テイルの座標
				fread(&offsetFromBone, sizeof(offsetFromBone), 1, fp);

				DirectX::XMFLOAT3 total(pmxb.headPos.x + offsetFromBone.x, pmxb.headPos.y + offsetFromBone.y, pmxb.headPos.z + offsetFromBone.z);
				bNodeMap[readBoneName].tailPos = total;
			}
			else
			{
				//ボーンに接続している
				decltype(pmxb.parentBoneIndex)  tailBoneIdx;
				fread(&tailBoneIdx, sizeof(tailBoneIdx), 1, fp);
				tempNode.tailBoneIndex = tailBoneIdx;
			}

			//回転付与:1 または 移動付与:1 の場合
			if (((pmxb.boneFlag & 0x0100) == 0x0100) || ((pmxb.boneFlag & 0x0200) == 0x0200))
			{
				decltype(pmxb.parentBoneIndex) parentAddBoneIdx;
				fread(&parentAddBoneIdx, sizeof(parentAddBoneIdx), 1, fp);
				float addPercent;
				fread(&addPercent, sizeof(addPercent), 1, fp);
			}

			//軸固定:1 の場合
			if ((pmxb.boneFlag & 0x0400) == 0x0400)
			{
				DirectX::XMFLOAT3 axisVec;
				fread(&axisVec, sizeof(axisVec), 1, fp);
			}

			//ローカル軸:1の場合
			if ((pmxb.boneFlag & 0x0800) == 0x0800)
			{
				std::array<DirectX::XMFLOAT3, 2> axisXZVec;
				fread(&axisXZVec, sizeof(axisXZVec), 1, fp);
			}

			//外部親変形:1の場合
			if ((pmxb.boneFlag & 0x2000) == 0x2000)
			{
				int keyValue;
				fread(&keyValue, sizeof(keyValue), 1, fp);
			}

			//IK:1の場合
			if ((pmxb.boneFlag & 0x0020) == 0x0020)
			{
				decltype(pmxb.parentBoneIndex) ikTargetBoneIdx;
				fread(&ikTargetBoneIdx, sizeof(ikTargetBoneIdx), 1, fp);
				int ikLoop;
				fread(&ikLoop, sizeof(ikLoop), 1, fp);
				float ikradLimit;
				fread(&ikradLimit, sizeof(ikradLimit), 1, fp);
				int ikLinkNum;
				fread(&ikLinkNum, sizeof(ikLinkNum), 1, fp);

				decltype(pmxb.parentBoneIndex) linkBoneIdx;
				for (int link = 0; link < ikLinkNum; ++link)
				{
					fread(&linkBoneIdx, sizeof(linkBoneIdx), 1, fp);
					bool radLimit;
					fread(&radLimit, sizeof(radLimit), 1, fp);
					if (radLimit)
					{
						array<DirectX::XMFLOAT3, 2> LimitRadData;
						fread(&LimitRadData, sizeof(LimitRadData), 1, fp);
					}
				}
			}
		}
		readNode[i] = tempNode;
		auto tes = 10;
	}

	boneMatrix.resize(bNodeMap.size());
	fill(boneMatrix.begin(), boneMatrix.end(), DirectX::XMMatrixIdentity());

	for (int idx = 0; idx < readNode.size(); ++idx)
	{
		auto& bName = readNode[idx].boneName;
		bNodeMap[bName].boneIndex = idx;
		bNodeMap[bName].headPos = readNode[idx].headPos;
		if (readNode[idx].tailBoneIndex != 0xffff)
		{
			bNodeMap[bName].tailPos = readNode[readNode[idx].tailBoneIndex].headPos;
		}

		if (readNode[idx].parentBoneIndex == 0xffff)
		{
			continue;
		}
		boneTree[readNode[idx].parentBoneIndex].push_back(idx);
	}
	
	for (auto& node : bNodeMap)
	{
		if (readNode[node.second.boneIndex].parentBoneIndex >= readNode.size())
		{
			continue;
		}
		auto parentIdx = readNode[node.second.boneIndex].parentBoneIndex;
		wstring pName(readNode[parentIdx].boneName);
		bNodeMap[pName].children.push_back(&node.second);
	}
	std::fclose(fp);
	return true;
}

void ModelManager::ReadModelVertexSetting(std::shared_ptr<CharacterMesh>& readMesh)
{
	auto& commons = dx12_.GetCommons();
	auto& vbuff = readMesh->GetVertexBuffer();
	auto type = vbuff->GetVertexBuffertType();

	if (type == VertexBufferType::Vertex3D)
	{
		auto& vb3d = dynamic_cast<VertexBuffer3D&>(*vbuff);
		commons.CreateVertexBuffer(vb3d);
		commons.CreateIndexBuffer(vb3d);
	}
	if (type == VertexBufferType::PMD)
	{
		auto& vbpmd = dynamic_cast<VertexBufferPMD&>(*vbuff);
		commons.CreateVertexBuffer(vbpmd);
		commons.CreateIndexBuffer(vbpmd);
	}
	if (type == VertexBufferType::PMX)
	{
		auto& vbpmd = dynamic_cast<VertexBufferPMX&>(*vbuff);
		commons.CreateVertexBuffer(vbpmd);
		commons.CreateIndexBuffer(vbpmd);
	}
	vbuff->resource->SetName(L"ModelVertex3DBuffer");
	vbuff->index->SetName(L"ModelIndicesBuffer");
}

void ModelManager::ReadModelMaterialSetting(std::shared_ptr<CharacterMesh>& readMesh)
{
	auto& commons = dx12_.GetCommons();
	auto& mManager = readMesh->GetMaterialManager();
	auto& materials = mManager.GetMaterials();

	if (materials.empty())
	{
		return;
	}
	auto& mDescriptor = mManager.GetDescriptor();
	auto& matBuff = mManager.GetMaterialBuffer();
	auto& mNumBuff = mManager.GetMaterialNumBuffer();
	auto& mAddBuff = mManager.GetAdditionalBuffer();

	commons.CreateStructureBufferAndPlaceView(matBuff, materials, mDescriptor, 3);
	matBuff->SetName(L"MaterialsBuffer");
	commons.CreateStructureBufferAndPlaceView(mNumBuff, mManager.GetMatNums(), mDescriptor, 1);
	mNumBuff->SetName(L"MaterialSettingNumBuffer");
	commons.CreateStructureBufferAndPlaceView(mAddBuff, mManager.GetIntParameter(), mDescriptor, 2);
	mAddBuff->SetName(L"MaterialAddIntParamBuffer");
}

void ModelManager::ReadModelBoneSetting(std::shared_ptr<CharacterMesh>& readMesh)
{
	auto& commons = dx12_.GetCommons();
	auto& bManager = readMesh->GetBoneManager();
	auto& bBuff = bManager->GetBuffer();
	const auto& bMatrices = bManager->GetBoneMatrices();
	commons.CreateConstantBuffer(bBuff, sizeof(bMatrices[0]) * static_cast<UINT>(bMatrices.size()));
	bBuff->SetName(L"BonesBuffer");
	auto& mappedptr = bManager->GetMappedPointer();
	commons.CreateCBVDescriptorAndPlaceView(bManager->GetDescriptorHeap(), bBuff, mappedptr);
	fill_n(mappedptr, bMatrices.size(), DirectX::XMMatrixIdentity());
}

void ModelManager::PrimitiveVertexSetting(std::shared_ptr<PrimitiveMesh>& mesh)
{
	auto& commons = dx12_.GetCommons();
	auto& vbuff = mesh->GetVertexBuffer();
	auto type = vbuff->GetVertexBuffertType();
	std::shared_ptr<VertexBuffer3D> tempVbuff = make_shared<VertexBuffer3D>();
	auto cast3d = dynamic_cast<VertexBuffer3D*>(vbuff.get());
	if (!cast3d)
	{
		assert(0);
		return;
	}

	tempVbuff->vertices = cast3d->vertices;
	tempVbuff->indices = cast3d->indices;
	if (type == VertexBufferType::Vertex3D)
	{
		commons.CreateVertexBuffer(*tempVbuff);
		commons.CreateIndexBuffer(*tempVbuff);
	}
	tempVbuff->resource->SetName(L"PrimitiveVertex3DBuffer");
	tempVbuff->index->SetName(L"PrimitiveIndicesBuffer");
	vbuff = tempVbuff;
}

void ModelManager::PrimitiveMaterialSetting(std::shared_ptr<PrimitiveMesh>& mesh)
{
	auto& commons = dx12_.GetCommons();
	auto& mManager = mesh->GetMaterialManager();
	auto& materials = mManager.GetMaterials();

	if (materials.empty())
	{
		return;
	}
	auto& mDescriptor = mManager.GetDescriptor();
	auto& matBuff = mManager.GetMaterialBuffer();
	auto& mNumBuff = mManager.GetMaterialNumBuffer();
	auto& mAddBuff = mManager.GetAdditionalBuffer();

	commons.CreateStructureBufferAndPlaceView(matBuff, materials, mDescriptor, 2);
	matBuff->SetName(L"MaterialsBuffer");
	commons.CreateStructureBufferAndPlaceView(mNumBuff, mManager.GetMatNums(), mDescriptor, 1);
	mNumBuff->SetName(L"MaterialSettingNumBuffer");
}
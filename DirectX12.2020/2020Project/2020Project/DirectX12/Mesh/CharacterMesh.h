#pragma once
#include "Mesh.h"
#include <memory>

class BoneManager;
class MorphingManager;
struct MotionData;

class CharacterMesh : public Mesh
{
public:
	CharacterMesh();
	CharacterMesh(CharacterMesh*) {};
	~CharacterMesh();
	void Update();
	/// <summary>
	/// アニメーションをセットする
	/// </summary>
	/// <param name="set">セットデータ</param>
	/// <param name="loop">ループ再生するかどうか</param>
	/// <param name="interval">ループ時の再生間隔</param>
	void SetAnimation(const MotionData& set, bool loop, uint32_t interval);
	std::shared_ptr<BoneManager>& GetBoneManager();
	std::shared_ptr<MorphingManager>& GetMorphingManager();
private:
	std::shared_ptr<BoneManager> boneManager_;
	std::shared_ptr<MorphingManager> morpManager_;
};
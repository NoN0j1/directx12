#include "Mesh.h"
#include "../Material/Material.h"
#include "../DirectX12Geometory.h"
#include "../Dx12Wrapper.h"

using namespace std;

Mesh::Mesh() :materialManager_()
{
	shaderParam_ = make_shared<Shader>();
	materialManager_ = MaterialManager();
}

std::shared_ptr<VertexBufferBase>& Mesh::GetVertexBuffer()
{
    return vertexBuffer_;
}

void Mesh::SetMaterial(const vector<Material>& inMat)
{
	materialManager_.SetMaterials(inMat);
}

void Mesh::SetMaterialParameter(const Material& mat, UINT matNum)
{
	if (matNum > materialManager_.GetMaterials().size())
	{
		OutputDebugString(L"Error::[SetMaterialParameter]指定のマテリアル番号が範囲外です\n");
		return;
	}
	auto& matParameter = materialManager_.GetMaterialParameter();
	if (!matParameter)
	{
		return;
	}
	auto tes = &matParameter[matNum];
	if (&matParameter[matNum])
	{
		matParameter[matNum] = mat;
	}
}

MaterialManager& Mesh::GetMaterialManager()
{
	return materialManager_;
}

const DirectX::XMFLOAT3& Mesh::GetOffsetCenterPosition()const
{
	return *centerOffset_;
}

bool Mesh::SettingMeshDrawCommands(ID3D12GraphicsCommandList5*& cmdList, int enumTableVal)
{
	switch (shaderParam_->tolopogyType)
	{
	case D3D12_PRIMITIVE_TOPOLOGY_TYPE_POINT:
		cmdList->IASetPrimitiveTopology(D3D_PRIMITIVE_TOPOLOGY_POINTLIST);
		break;
	case D3D12_PRIMITIVE_TOPOLOGY_TYPE_TRIANGLE:
		cmdList->IASetPrimitiveTopology(D3D_PRIMITIVE_TOPOLOGY_TRIANGLELIST);
		break;
	default:
		break;
	}
	if (shaderParam_->rootSignature == nullptr)
	{
		return false;
	}
	cmdList->SetGraphicsRootSignature(shaderParam_->rootSignature.Get());
	cmdList->SetPipelineState(shaderParam_->pipelineState.Get());
	if (materialManager_.GetMaterialBuffer() != nullptr)
	{
		auto temtem = materialManager_.GetMaterials();
		auto& descHeap = materialManager_.GetDescriptor();
		cmdList->SetDescriptorHeaps(1, &descHeap);
		cmdList->SetGraphicsRootDescriptorTable(enumTableVal, descHeap->GetGPUDescriptorHandleForHeapStart());
	}
	return true;
}

bool Mesh::SetCastShadowShader(ID3D12GraphicsCommandList5*& cmdList)
{
	switch (shaderParam_->tolopogyType)
	{
	case D3D12_PRIMITIVE_TOPOLOGY_TYPE_POINT:
		cmdList->IASetPrimitiveTopology(D3D_PRIMITIVE_TOPOLOGY_POINTLIST);
		break;
	case D3D12_PRIMITIVE_TOPOLOGY_TYPE_TRIANGLE:
		cmdList->IASetPrimitiveTopology(D3D_PRIMITIVE_TOPOLOGY_TRIANGLELIST);
		break;
	default:
		break;
	}
	if (shadowShader_ == nullptr)
	{
		return false;
	}

	cmdList->SetGraphicsRootSignature(shadowShader_->rootSignature.Get());
	cmdList->SetPipelineState(shadowShader_->pipelineState.Get());

	return true;
}

void Mesh::SetShader(const std::shared_ptr<Shader>& shader, const std::shared_ptr<Shader> shadow)
{
	shaderParam_ = shader;
	if (shadow == nullptr)
	{
		return;
	}

	shadowShader_ = shadow;
}

#include <d3dcompiler.h>
#include <wrl.h>
#include <array>
#include <cassert>
#include "Dx12Wrapper.h"
#include "Dx12Commons.h"
#include "ShaderManager.h"
#include "DirectX12Geometory.h"
#include "DirectX12ToolFunction.h"

#pragma comment(lib, "D3DCompiler.lib")

using namespace std;

ShaderManager::ShaderManager(Dx12Wrapper& dx12):wrapper_(dx12)
{
	InitBlendDesc();
	InitRasterizeDesc();
	InitDepthStencilDesc();
}

bool ShaderManager::LoadShader(ShaderParameter& loadParam)
{
	if (manageShader_.contains(loadParam.shaderName))
	{
		return false;
	}

	array<Microsoft::WRL::ComPtr<ID3D10Blob>, static_cast<int>(ShaderType::MAX)> blob = {};
	Microsoft::WRL::ComPtr<ID3D10Blob> errBlob = nullptr;

	HRESULT result = E_FAIL;
	for (int i = 0; i < static_cast<int>(ShaderType::MAX); ++i)
	{
		if (loadParam.shader[i] == nullptr)
		{
			blob[i] = nullptr;
			continue;
		}
		result = D3DCompileFromFile(
			loadParam.shader[i]->fileName,
			nullptr,
			D3D_COMPILE_STANDARD_FILE_INCLUDE,
			loadParam.shader[i]->entryPoint,
			loadParam.shader[i]->verTarget,
			0,
			0,
			&blob[i],
			&errBlob);
		if (!CheckResult(result, errBlob.Get()))
		{
			assert(false);
			continue;
		}
	}

	Microsoft::WRL::ComPtr<ID3DBlob> signature;
	result = D3DGetBlobPart(blob[static_cast<int>(ShaderType::VS)]->GetBufferPointer(), blob[static_cast<int>(ShaderType::VS)]->GetBufferSize(), D3D_BLOB_ROOT_SIGNATURE, 0, &signature);
	if (!CheckResult(result))
	{
		return false;
	}

	//実体追加
	manageShader_[loadParam.shaderName] = make_shared<Shader>();
	auto& addShader = manageShader_[loadParam.shaderName];
	result = wrapper_.GetDevice()->CreateRootSignature(
		0,
		signature->GetBufferPointer(),
		signature->GetBufferSize(),
		IID_PPV_ARGS(addShader->rootSignature.ReleaseAndGetAddressOf()));
	if (!CheckResult(result))
	{
		return false;
	}
	
	auto plDesc = loadParam.pdesc;
	if (loadParam.plDesc != nullptr)
	{
		plDesc = GetPipelineStateDesc(*loadParam.plDesc);
	}

	plDesc.pRootSignature = addShader->rootSignature.Get();
	if (blob[static_cast<int>(ShaderType::VS)] != nullptr)
	{
		plDesc.VS = CD3DX12_SHADER_BYTECODE(blob[static_cast<int>(ShaderType::VS)].Get());
	}
	if (blob[static_cast<int>(ShaderType::PS)] != nullptr)
	{
		plDesc.PS = CD3DX12_SHADER_BYTECODE(blob[static_cast<int>(ShaderType::PS)].Get());
	}
	if (blob[static_cast<int>(ShaderType::GS)] != nullptr)
	{
		plDesc.GS = CD3DX12_SHADER_BYTECODE(blob[static_cast<int>(ShaderType::GS)].Get());
	}
	if (blob[static_cast<int>(ShaderType::HS)] != nullptr)
	{
		plDesc.HS = CD3DX12_SHADER_BYTECODE(blob[static_cast<int>(ShaderType::HS)].Get());
	}
	if (blob[static_cast<int>(ShaderType::DS)] != nullptr)
	{
		plDesc.DS = CD3DX12_SHADER_BYTECODE(blob[static_cast<int>(ShaderType::DS)].Get());
	}
	addShader->tolopogyType = plDesc.PrimitiveTopologyType;

	wrapper_.GetCommons().AddPipeline(plDesc, loadParam.shaderName.c_str());
	wrapper_.GetCommons().LoadPipeline(*addShader->pipelineState.GetAddressOf(), plDesc, loadParam.shaderName.c_str());
	return true;
}

bool ShaderManager::LoadComputeShader(ShaderDetailed& shader, const std::wstring& shaderName)
{
	if (manageShader_.contains(shaderName))
	{
		return false;
	}
	Microsoft::WRL::ComPtr<ID3D10Blob> blob = nullptr;
	Microsoft::WRL::ComPtr<ID3D10Blob> errBlob = nullptr;
	HRESULT result = E_FAIL;
	result = D3DCompileFromFile(
		shader.fileName,
		nullptr,
		D3D_COMPILE_STANDARD_FILE_INCLUDE,
		shader.entryPoint,
		shader.verTarget,
		0,
		0,
		&blob,
		&errBlob);
	if (!CheckResult(result, errBlob.Get()))
	{
		assert(false);
	}
	Microsoft::WRL::ComPtr<ID3DBlob> signature;
	result = D3DGetBlobPart(blob->GetBufferPointer(), blob->GetBufferSize(), D3D_BLOB_ROOT_SIGNATURE, 0, &signature);
	if (!CheckResult(result))
	{
		return false;
	}

	//実体追加
	manageShader_[shaderName] = make_shared<Shader>();
	auto& addShader = manageShader_[shaderName];
	result = wrapper_.GetDevice()->CreateRootSignature(
		0,
		signature->GetBufferPointer(),
		signature->GetBufferSize(),
		IID_PPV_ARGS(addShader->rootSignature.ReleaseAndGetAddressOf()));
	if (!CheckResult(result))
	{
		return false;
	}

	D3D12_COMPUTE_PIPELINE_STATE_DESC plDesc = {};
	plDesc.pRootSignature = addShader->rootSignature.Get();
	plDesc.CS = CD3DX12_SHADER_BYTECODE(blob.Get());
	plDesc.NodeMask = 0;

	result = wrapper_.GetDevice()->CreateComputePipelineState(&plDesc, IID_PPV_ARGS(addShader->pipelineState.GetAddressOf()));
	if (!CheckResult(result))
	{
		return false;
	}
	return true;
}

std::shared_ptr<Shader> ShaderManager::GetShader(const std::wstring& name)
{
	if (manageShader_.contains(name))
	{
		return manageShader_.at(name);
	}
	return nullptr;
}

void ShaderManager::InitBlendDesc()
{
	D3D12_BLEND_DESC BlendState = CD3DX12_BLEND_DESC(D3D12_DEFAULT);
	//ブレンド無しを登録
	blendDescData_[RenderTargetBlend::None] = BlendState;
	
	//ブレンド有(アルファ無)を登録
	for (UINT i = 0; i < D3D12_SIMULTANEOUS_RENDER_TARGET_COUNT; ++i)
	{
		BlendState.RenderTarget[i].DestBlend = D3D12_BLEND_INV_SRC_ALPHA;
		BlendState.RenderTarget[i].BlendEnable = true;
	}
	blendDescData_[RenderTargetBlend::Blend] = BlendState;

	//アルファ有のブレンドを登録
	D3D12_RENDER_TARGET_BLEND_DESC blendDesc = {};
	blendDesc.BlendEnable = true;
	blendDesc.LogicOpEnable = false;
	blendDesc.SrcBlend = D3D12_BLEND_SRC_ALPHA;
	blendDesc.DestBlend = D3D12_BLEND_INV_SRC_ALPHA;
	blendDesc.BlendOp = D3D12_BLEND_OP_ADD;
	blendDesc.SrcBlendAlpha = D3D12_BLEND_ONE;
	blendDesc.DestBlendAlpha = D3D12_BLEND_ONE;
	blendDesc.BlendOpAlpha = D3D12_BLEND_OP_ADD;
	blendDesc.LogicOp = D3D12_LOGIC_OP_NOOP;
	blendDesc.RenderTargetWriteMask = D3D12_COLOR_WRITE_ENABLE_ALL;
	for (UINT i = 0; i < D3D12_SIMULTANEOUS_RENDER_TARGET_COUNT; ++i)
	{
		BlendState.RenderTarget[i] = blendDesc;
	}
	blendDescData_[RenderTargetBlend::AlphaBlend] = BlendState;
}

void ShaderManager::InitRasterizeDesc()
{
	//カリング:Back登録
	D3D12_RASTERIZER_DESC rastDesc = CD3DX12_RASTERIZER_DESC(D3D12_DEFAULT);
	rasterizeDescData_[RasterizerCullingState::Back] = rastDesc;

	//カリング:Front登録
	rastDesc.CullMode = D3D12_CULL_MODE_FRONT;
	rasterizeDescData_[RasterizerCullingState::Front] = rastDesc;
	//カリング:None登録
	rastDesc.CullMode = D3D12_CULL_MODE_NONE;
	rasterizeDescData_[RasterizerCullingState::None] = rastDesc;
}

void ShaderManager::InitDepthStencilDesc()
{
	D3D12_DEPTH_STENCIL_DESC depthDesc = CD3DX12_DEPTH_STENCIL_DESC(D3D12_DEFAULT);
	depthDesc.DepthEnable = false;
	depthStencilDescData_[false] = depthDesc;

	depthDesc.DepthEnable = true;
	depthDesc.DepthWriteMask = D3D12_DEPTH_WRITE_MASK_ALL;
	depthDesc.DepthFunc = D3D12_COMPARISON_FUNC_LESS;
	depthStencilDescData_[true] = depthDesc;
	//フォーマットは実際に生成する時にD32に指定
}

D3D12_GRAPHICS_PIPELINE_STATE_DESC ShaderManager::GetPipelineStateDesc(const PipelineDescSetting& setting)
{
	D3D12_GRAPHICS_PIPELINE_STATE_DESC retDesc = {};
	retDesc.BlendState = blendDescData_[setting.blend];
	retDesc.RasterizerState = rasterizeDescData_[setting.cullState];
	retDesc.DepthStencilState = depthStencilDescData_[setting.depthEnable];
	if (setting.depthEnable)
	{
		retDesc.DSVFormat = DXGI_FORMAT_D32_FLOAT;
	}
	retDesc.PrimitiveTopologyType = D3D12_PRIMITIVE_TOPOLOGY_TYPE_TRIANGLE;
	retDesc.InputLayout.pInputElementDescs = setting.inputElement.data();
	retDesc.InputLayout.NumElements = static_cast<UINT>(setting.inputElement.size());
	retDesc.NumRenderTargets = setting.targetNum;
	for (UINT i = 0; i < setting.targetNum; ++i)
	{
		retDesc.RTVFormats[i] = setting.format;
	}
	retDesc.Flags = D3D12_PIPELINE_STATE_FLAG_NONE;
	retDesc.SampleDesc.Count = 1;
	retDesc.SampleDesc.Quality = 0;
	retDesc.SampleMask = D3D12_DEFAULT_SAMPLE_MASK;
	retDesc.NodeMask = 0;
	return retDesc;
}

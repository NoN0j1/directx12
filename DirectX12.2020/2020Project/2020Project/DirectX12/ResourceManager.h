#pragma once
#include <unordered_map>
#include <memory>
#include <vector>
#include <string>
#include <d3d12.h>
#include <wrl.h>

class Dx12Wrapper;
class Texture;
class ModelManager;
class Mesh;
enum class DummyTextureType;
enum class PrimitiveMeshType;
struct VertexBuffer;
struct MotionData;
struct ShaderDetailed;
struct ShaderParameter;
struct Shader;
class ShaderManager;

class ResourceManager
{
public:
	static ResourceManager& Instance();

	void Init(Dx12Wrapper& dx12Wrapper);
	/// <summary>
	/// 画像をGPUに読み込む
	/// </summary>
	/// <param name="path">ファイルパス</param>
	/// <returns>画像ハンドル</returns>
	int LoadGraph(const std::wstring& path);

	/// <summary>
	/// レンダーターゲットを画像として読み込む
	/// </summary>
	/// <param name="rtv">レンダーターゲットバッファ</param>
	/// <returns>画像としてのハンドル</returns>
	int AddRenderGraph(Microsoft::WRL::ComPtr<ID3D12Resource>& rt);

	int AddShadowGraph(ID3D12Resource*& shadowBuff);

	/// <summary>
	/// モデル情報を読み込む
	/// </summary>
	/// <param name="fileName">ファイルパス</param>
	/// <param name="modelName">登録名</param>
	bool LoadModel(const std::wstring& fileName, const std::wstring& modelName);

	/// <summary>
	/// ロード済みモデルデータを取得する
	/// </summary>
	/// <param name="modelName">登録名</param>
	/// <returns>モデルメッシュデータ</returns>
	const std::shared_ptr<Mesh> GetModel(const std::wstring& modelName);

	/// <summary>
	/// プリミティブモデルを取得する
	/// </summary>
	/// <param name="type">プリミティブ種別</param>
	/// <returns>プリミティブメッシュデータ</returns>
	const std::shared_ptr<Mesh> GetModel(const PrimitiveMeshType& type);

	/// <summary>
	/// アニメーションを読み込む
	/// </summary>
	/// <param name="path">ファイルパス</param>
	/// <param name="name">登録アニメーション名</param>
	void LoadAnimation(const std::wstring& path, const std::wstring& name);

	/// <summary>
	/// ロード済みアニメーションを取得する
	/// </summary>
	/// <param name="name">登録名</param>
	/// <returns>存在する場合は登録モーションデータ,無い場合はダミーデータ</returns>
	const MotionData& GetAnimation(const std::wstring& name)const;


	/// <summary>
	/// シェーダを新規に読み込む
	/// </summary>
	/// <param name="param">読み込みに必要なパラメータ</param>
	/// <returns>true = 成功, false = 失敗</returns>
	bool LoadShader(ShaderParameter* param);

	/// <summary>
	/// 演算シェーダを新規に読み込む
	/// </summary>
	/// <param name="param">読み込みに必要なパラメータ</param>
	/// <param name="shaderName">登録名</param>
	/// <returns>true = 成功, false = 失敗</returns>
	bool LoadComputeShader(ShaderDetailed& param, const std::wstring& shaderName);

	/// <summary>
	/// ロード済み取得する
	/// </summary>
	/// <param name="shaderName">登録名</param>
	/// <returns>成功時=shaderのRootSignatureとPipeline, nullptr = 読み込み失敗</returns>
	const std::shared_ptr<Shader> GetShader(const std::wstring& shaderName);


	/// <summary>
	/// ロード済みテクスチャを取得する
	/// </summary>
	/// <param name="handle">画像ハンドル</param>
	/// <returns>不正なハンドルの場合、白テクスチャ、正常な場合はテクスチャデータ</returns>
	Texture& GetTexture(int handle);

	/// <summary>
	/// ダミー画像を取得する
	/// </summary>
	/// <param name="type">ダミー種別</param>
	/// <returns>ダミー画像</returns>
	Texture& GetDummyTexture(DummyTextureType type);

	/// <summary>
	/// テクスチャ配置用デスクリプタヒープを取得する
	/// </summary>
	/// <returns></returns>
	ID3D12DescriptorHeap*& GetTextureDescriptorHeap();

	/// <summary>
	/// テクスチャ配置用デスクリプタヒープを取得する
	/// </summary>
	/// <returns></returns>
	ID3D12DescriptorHeap*& GetRenderTextureDescriptorHeap();

	/// <summary>
	/// テクスチャ配置用デスクリプタヒープを取得する
	/// </summary>
	/// <returns></returns>
	ID3D12DescriptorHeap*& GetShadowDescriptorHeap();

	void Release();
	~ResourceManager();
private:
	ResourceManager();
	ResourceManager(const ResourceManager&) = delete;
	void operator=(const ResourceManager&) = delete;

	UINT GetOffset();

	/// <summary>
	/// ダミー白画像を作成する
	/// </summary>
	void CreateWhiteTexture();

	/// <summary>
	/// ダミー黒画像を作成する
	/// </summary>
	void CreateBlackTexture();

	/// <summary>
	/// ダミーグラデーション画像を作成する
	/// </summary>
	void CreateGradationTexture();

	/// <summary>
	/// filePathで多重読み込みチェック、loadTextureの配列位置を保持
	/// </summary>
	std::unordered_map<std::wstring, int> textureTable_;

	/// <summary>
	/// ロード済み画像配列
	/// </summary>
	std::vector<Texture> loadTextures_;

	/// <summary>
	/// ダミー画像用配列
	/// </summary>
	std::vector<Texture> dummyTextures_;

	/// <summary>
	/// 深度を書いた画像配列
	/// </summary>
	std::vector<ID3D12Resource*> shadowTextures_;

	/// <summary>
	/// レンダーターゲット用配列
	/// </summary>
	std::vector<Microsoft::WRL::ComPtr<ID3D12Resource>> renderTargetTextures_;

	/// <summary>
	/// テクスチャ用デスクリプタヒープ
	/// </summary>
	Microsoft::WRL::ComPtr<ID3D12DescriptorHeap> textureDescriptor_;

	/// <summary>
	/// テクスチャ用デスクリプタヒープ
	/// </summary>
	Microsoft::WRL::ComPtr<ID3D12DescriptorHeap> renderTextureDescriptor_;


	/// <summary>
	/// テクスチャ用デスクリプタヒープ
	/// </summary>
	Microsoft::WRL::ComPtr<ID3D12DescriptorHeap> shadowTextureDescriptor_;

	/// <summary>
	/// FilePathと登録名の管理
	/// </summary>
	std::unordered_map<std::wstring, std::wstring> MotionPathList_;
	
	/// <summary>
	/// 登録名でモーションの管理
	/// </summary>
	std::unordered_map<std::wstring, MotionData> animationList_;

	/// <summary>
	/// モデル管理をしているクラスを保持
	/// </summary>
	std::unique_ptr<ModelManager> modelManager_;

	/// <summary>
	/// シェーダ管理をしているクラスを保持
	/// </summary>
	std::unique_ptr<ShaderManager> shaderManager_;

	/// <summary>
	/// Wrapperのポインタ
	/// </summary>
	Dx12Wrapper* dx12_{};
};
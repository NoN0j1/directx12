#pragma once
#include <wrl.h>
#include <vector>
#include <memory>
#include <string>
#include "Material/Material.h"

struct ID3D12Resource;
struct ID3D12DescriptorHeap;

class MaterialManager
{
public:
	MaterialManager();
	~MaterialManager() = default;

	/// <summary>
	/// マテリアルバッファを取得する
	/// </summary>
	/// <returns>materialBuffer_*</returns>
	ID3D12Resource*& GetMaterialBuffer();

	/// <summary>
	/// マテリアル振り分けIndexバッファを取得する
	/// </summary>
	/// <returns>materialNumBuffer_*</returns>
	ID3D12Resource*& GetMaterialNumBuffer();

	/// <summary>
	/// マテリアルの追加パラメータ
	/// </summary>
	/// <returns></returns>
	ID3D12Resource*& GetAdditionalBuffer();

	/// <summary>
	/// マテリアル関連で使用するデスクリプタを取得する
	/// </summary>
	/// <returns>descriptor_*</returns>
	ID3D12DescriptorHeap*& GetDescriptor();

	/// <summary>
	/// 管理しているMaterialDataを取得する
	/// </summary>
	/// <returns>Materialパラメータ配列</returns>
	const std::vector<Material>& GetMaterials();

	/// <summary>
	/// マテリアルを一括で差し替える
	/// </summary>
	/// <param name="inMat">差し替えるMaterial配列</param>
	void SetMaterials(const std::vector<Material>& inMat);

	Material*& GetMaterialParameter();

	/// <summary>
	/// Materialの振り分けIndex配列を取得する
	/// </summary>
	/// <returns>Shaderでの振り分けIndex配列</returns>
	const std::vector<UINT>& GetMatNums();

	/// <summary>
	/// Materialの振り分けIndex配列を一括で差し替える
	/// </summary>
	/// <param name="inNums"></param>
	void SetMatNums(const std::vector<UINT>& inNums);

	/// <summary>
	/// パラメータ(Int)を追加する
	/// </summary>
	/// <param name="add">追加するパラメータ</param>
	void AddIntParameter(const std::vector<int>& add);

	const std::vector<int>& GetIntParameter();

private:
	/// <summary>
	/// マテリアルバッファ
	/// </summary>
	Microsoft::WRL::ComPtr<ID3D12Resource> materialBuffer_;

	/// <summary>
	/// マテリアルの振り分けIndex用バッファ
	/// </summary>
	Microsoft::WRL::ComPtr<ID3D12Resource> materialNumBuffer_;
	
	/// <summary>
	/// マテリアル関連のデスクリプタヒープ
	/// </summary>
	Microsoft::WRL::ComPtr<ID3D12DescriptorHeap> descriptor_;

	/// <summary>
	/// 追加パラメータ用バッファ
	/// </summary>
	Microsoft::WRL::ComPtr<ID3D12Resource> addParameterBuffers_;

	/// <summary>
	/// Material管理配列
	/// </summary>
	std::vector<Material> materials_;

	Material* mappedMaterials_;

	/// <summary>
	/// Material振り分けIndex配列
	/// </summary>
	std::vector<UINT> materialNums_;

	/// <summary>
	/// 追加Intパラメータ
	/// </summary>
	std::vector<int> addIntParam_;
};

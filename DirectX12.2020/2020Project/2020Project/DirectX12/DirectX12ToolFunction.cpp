#include <cassert>
#include <d3d12.h>
#include "DirectX12ToolFunction.h"

using namespace std;

bool CheckResult(HRESULT result)
{
	if (FAILED(result))
	{
		OutputDebugString(L"処理に失敗しました\n");
		return false;
	}
	return true;
}

bool CheckResult(HRESULT result, ID3DBlob* blob)
{
	if (FAILED(result))
	{
		if (blob)
		{
			wstring output;
			output.resize(blob->GetBufferSize());
			copy_n((char*)blob->GetBufferPointer(), blob->GetBufferSize(), output.begin());
			OutputDebugString(output.c_str());
		}
		return false;
	}
	return true;
}

UINT AlignmentedValue(UINT size, UINT alignment)
{
	if (size == alignment)
	{
		return size;
	}
	return(size + alignment - (size % alignment));
}

wstring GetDirectoryFromPath(wstring path)
{
	int pos1 = static_cast<int>(path.rfind('/'));
	int pos2 = static_cast<int>(path.rfind('\\'));
	if (pos1 == wstring::npos && pos2 == wstring::npos)
	{
		return L"";
	}
	auto curPos = max(pos1, pos2);
	return path.substr(0, static_cast<size_t>(curPos) + 1);
}

vector<string> SeparateString(string str, const char separator)
{
	vector<string> ret; //戻り値用
	//例えば文字列が"a.bmp*b.sph"だったとする
	//これをstringのfindとsubstrを使用して分離する
	size_t idx = 0;
	size_t offset = 0;
	do
	{
		idx = str.find(separator, offset);
		if (idx != string::npos)
		{
			ret.emplace_back(str.substr(offset, idx - offset));
			offset = idx + 1;
		}
		else
		{
			ret.emplace_back(str.substr(offset));
		}

	} while (idx != string::npos);
	return ret;
}


wstring WstringFromString(const std::string& str)
{
	wstring wstr;
	//wcharの文字列数を取得
	auto wcharLength = MultiByteToWideChar(CP_ACP, 0, str.c_str(), static_cast<int>(str.length()), nullptr, 0);
	wstr.resize(wcharLength);

	//文字を取得
	wcharLength = MultiByteToWideChar(CP_ACP, 0, str.c_str(), static_cast<int>(str.length()), &wstr[0], static_cast<int>(wstr.size()));
	return wstr;
}
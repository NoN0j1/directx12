#include "DirectionalLight.h"
#include "Camera.h"

constexpr float length = 100.0f;

using namespace std;
DirectionalLight::DirectionalLight(const DirectX::XMFLOAT3& rot) :direction_(rot)
{
	struct TEMP : Camera
	{
		TEMP(const Transform& tr) :Camera(tr) {}
	};
	auto posX = -sinf(rot.x) * length;
	auto posY = -sinf(rot.y) * length; //上下逆になるので -
	auto posZ = -sinf(rot.z) * length; //LHの場合前後が逆なので-

	auto temp = make_shared<TEMP>(Transform({ posX, posY, posZ }));
	lightCamera_ = move(temp);
	shadowScreen_ = make_shared<Screen>();
}

DirectionalLight::~DirectionalLight()
{
}

void DirectionalLight::Update()
{
	direction_ = mappedConstant_->direction;
	DirectX::XMFLOAT4 qua(addRotate_.x, addRotate_.y, addRotate_.z, 1);
	auto rotDirection = DirectX::XMVector3Rotate(DirectX::XMLoadFloat3(&direction_), DirectX::XMLoadFloat4(&qua));
	DirectX::XMStoreFloat3(&direction_, rotDirection);
	SetCameraPos();
	lightCamera_->Update();
	//DirectX::XMStoreFloat3(&direction_, temp);
}

void DirectionalLight::LightInit(bool perth)
{
	lightCamera_->CameraInit(perth, length * 3.0f);
	mappedConstant_->direction = direction_;
}

void DirectionalLight::AddRotate(float x, float y, float z)
{
	addRotate_.x += x;
	addRotate_.y += y;
	addRotate_.z += z;
}

std::shared_ptr<Camera>& DirectionalLight::GetShadowCamera()
{
	return lightCamera_;
}

std::shared_ptr<Screen>& DirectionalLight::GetShadowScreen()
{
	return shadowScreen_;
}

ID3D12DescriptorHeap*& DirectionalLight::GetDescriptorHeap()
{
	return *lightCBVDescriptor_.GetAddressOf();
}

ID3D12Resource*& DirectionalLight::GetBuffer()
{
	return *lightCBuffer_.GetAddressOf();
}

LightCommons*& DirectionalLight::GetMappingConstant()
{
	return mappedConstant_;
}

float DirectionalLight::GetSign(float checkNum)
{
	if (signbit(checkNum))
	{
		//マイナスの場合
		return -1.0f;
	}
	//プラスの場合
	return 1.0f;
}

void DirectionalLight::SetCameraPos()
{
	auto posX = -sinf(direction_.x) * length;
	auto posY = -sinf(direction_.y) * length; //上下逆になるので -
	auto posZ = -sinf(direction_.z) * length; //LHの場合前後が逆なので-
	if (direction_.x < -0.5f)
	{
		int tes = 00;
	}
	lightCamera_->SetPosition({ posX,posY,posZ });
}

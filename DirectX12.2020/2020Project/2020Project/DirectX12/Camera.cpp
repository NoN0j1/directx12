#include "../Application.h"
#include "Camera.h"
#include "DirectX12Geometory.h"
#include "Input.h"

constexpr float default_depthRange = 300.0f;

using namespace std;

Camera::Camera():Actor()
{
}

void Camera::SetLookAtTarget(std::shared_ptr<Actor>* t)
{
	lookAtTarget_ = t;
}

void Camera::Rotate(const DirectX::XMFLOAT3& dir)
{
	DirectX::XMMATRIX rot = DirectX::XMMatrixIdentity();
	rot *= DirectX::XMMatrixRotationX(dir.x);
	rot *= DirectX::XMMatrixRotationY(dir.y);
	rot *= DirectX::XMMatrixRotationZ(dir.z);
	auto vec = DirectX::XMLoadFloat3(&dir);
	vec = DirectX::XMVector3Transform(vec, rot);
	DirectX::XMStoreFloat3(&target_, vec);
}

void Camera::SetPosition(const DirectX::XMFLOAT3& pos, float time)
{
	auto& position = GetTransform().position;
	auto add = DirectX::XMFLOAT3(pos.x - position.x, pos.y - position.y, pos.z - position.z);
	MoveActor(add);
}

CameraCommons*& Camera::GetConstantCommonsParameter()
{
	return mappedCommons_;
}

Camera::Camera(const Transform& pos)
	: Actor(pos), target_{}, up_(0.0f, 1.0f, 0.0f), fov_(DirectX::XM_PIDIV4), depthRange(default_depthRange)
{
}

Camera::~Camera() = default;

void Camera::CameraInit(bool perspect)
{
	perspective_ = perspect;
	UpdateViewProjection();
}

void Camera::CameraInit(bool perspect, float shadowR)
{
	perspective_ = perspect;
	depthRange = shadowR;
	UpdateViewProjection();
}

const DirectX::XMFLOAT3& Camera::GetUpVector()const
{
	return up_;
}

const DirectX::XMFLOAT3& Camera::GetTarget()const
{
	return target_;
}

const DirectX::XMFLOAT3& Camera::GetEye()const
{
	return GetTransform().position;
}

const float Camera::GetFov()const
{
	return fov_;
}

const float Camera::GetCameraViewRange() const
{
	return depthRange;
}

void Camera::Update()
{
	UpdateTransform();
	UpdateViewProjection();
}

void Camera::UpdateViewProjection()
{
	auto& transform = GetTransform();
	auto& wsize = Application::Instance().GetWindowSize();

	auto eyePos = DirectX::XMLoadFloat3(&transform.position);
	auto directionVec = DirectX::XMLoadFloat3(&target_);
	auto upVec = DirectX::XMLoadFloat3(&up_);
	DirectX::XMMATRIX view,proj;
	view = DirectX::XMMatrixIdentity();
	proj = DirectX::XMMatrixIdentity();
	if (perspective_)
	{
		view = DirectX::XMMatrixLookAtLH(eyePos, directionVec, upVec);
		proj = DirectX::XMMatrixPerspectiveFovLH(fov_,
			static_cast<float>(wsize.cx) / static_cast<float>(wsize.cy),
			1.0f,
			depthRange);
	}
	else
	{
		view = DirectX::XMMatrixLookAtLH(eyePos, directionVec, upVec);
		proj = DirectX::XMMatrixOrthographicLH(60, 60, 1.0f, depthRange);
	}
	
	mappedCommons_->viewproj = view * proj;
	auto eyeToTargetVec = DirectX::XMVectorSubtract(eyePos, DirectX::XMLoadFloat3(&target_));
	auto normalizePos = DirectX::XMVector3Normalize(eyeToTargetVec);
	auto rot = transform.rotation;
	auto rotateVec = DirectX::XMVector3Rotate(normalizePos, DirectX::XMLoadFloat4(&DirectX::XMFLOAT4(rot.x,rot.y,rot.z,1)));
	DirectX::XMFLOAT3 rotateRay = {};
	DirectX::XMStoreFloat3(&rotateRay, rotateVec);
	mappedCommons_->eyeRay = rotateRay;
	DirectX::XMVECTOR det;
	mappedCommons_->invProj = DirectX::XMMatrixInverse(&det, proj);
	mappedCommons_->proj = proj;
}
#include <cassert>
#include <filesystem>
#include <algorithm>
#include "Dx12Commons.h"
#include "DirectX12ToolFunction.h"
#include "DirectX12Geometory.h"
#include "../Application.h"

using namespace std;
using namespace DirectX;

Dx12Commons::Dx12Commons(ID3D12Device6* dev):device_(dev)
{
}

Dx12Commons::~Dx12Commons()
{
}

bool Dx12Commons::CreateTextureBuffer(ID3D12Resource*& buff, ID3D12Resource*& internalBuff, TextureParameter& texParam, D3D12_TEXTURE_COPY_LOCATION& src, D3D12_TEXTURE_COPY_LOCATION& dst)
{
	HRESULT result = device_->CreateCommittedResource(
		&CD3DX12_HEAP_PROPERTIES(D3D12_HEAP_TYPE_DEFAULT),
		D3D12_HEAP_FLAG_NONE,
		&texParam.resourceDesc,
		D3D12_RESOURCE_STATE_COPY_DEST,
		nullptr,
		IID_PPV_ARGS(&buff));
	if (!CheckResult(result))
	{
		return false;
	}
	auto img = texParam.scratchImage.GetImage(0,0,0);
	buff->SetName(L"TextureBuffer");

	//転送の為の中間バッファ
	
	result = device_->CreateCommittedResource(
		&CD3DX12_HEAP_PROPERTIES(D3D12_HEAP_TYPE_UPLOAD),
		D3D12_HEAP_FLAG_NONE,
		&CD3DX12_RESOURCE_DESC::Buffer(AlignmentedValue(static_cast<UINT>(img->rowPitch), D3D12_TEXTURE_DATA_PITCH_ALIGNMENT) * img->height * texParam.metadata.mipLevels),
		D3D12_RESOURCE_STATE_GENERIC_READ,
		nullptr,
		IID_PPV_ARGS(&internalBuff));
	if (!CheckResult(result))
	{
		return false;
	}
	internalBuff->SetName(L"InternalTextureBuffer");

	uint8_t* mapForImage = nullptr;
	result = internalBuff->Map(0, nullptr, (void**)&mapForImage);
	if (!CheckResult(result))
	{
		return false;
	}

	auto address = img->pixels;
	for (int i = 0; i < img->height; ++i)
	{
		copy_n(address, img->rowPitch, mapForImage);
		address += img->rowPitch;
		mapForImage += AlignmentedValue(static_cast<UINT>(img->rowPitch), D3D12_TEXTURE_DATA_PITCH_ALIGNMENT);
	}
	internalBuff->Unmap(0, nullptr);

	src.Type = D3D12_TEXTURE_COPY_TYPE_PLACED_FOOTPRINT;
	src.pResource = internalBuff;
	src.PlacedFootprint.Offset = 0;
	src.PlacedFootprint.Footprint.Width = static_cast<UINT>(img->width);
	src.PlacedFootprint.Footprint.Height = static_cast<UINT>(img->height);
	src.PlacedFootprint.Footprint.RowPitch = AlignmentedValue(static_cast<UINT>(img->rowPitch), D3D12_TEXTURE_DATA_PITCH_ALIGNMENT);
	src.PlacedFootprint.Footprint.Depth = static_cast<UINT>(texParam.metadata.depth);
	src.PlacedFootprint.Footprint.Format = img->format;

	dst.Type = D3D12_TEXTURE_COPY_TYPE_SUBRESOURCE_INDEX;
	dst.SubresourceIndex = 0;
	dst.pResource = buff;

	return true;
}

bool Dx12Commons::CreateTextureBuffer(ID3D12Resource*& buff, DummyTextureType type, UINT64 width, UINT height, DXGI_FORMAT format)
{
	HRESULT result = device_->CreateCommittedResource(
		&CD3DX12_HEAP_PROPERTIES(D3D12_CPU_PAGE_PROPERTY_WRITE_BACK, D3D12_MEMORY_POOL_L0),
		D3D12_HEAP_FLAG_NONE,
		&CD3DX12_RESOURCE_DESC::Tex2D(format, width, height),
		D3D12_RESOURCE_STATE_PIXEL_SHADER_RESOURCE,
		nullptr,
		IID_PPV_ARGS(&buff));
	if (!CheckResult(result))
	{
		return false;
	}

	result = S_OK;
	
	struct Color
	{
		uint8_t r, g, b, a;
		Color():r(0),g(0),b(0),a(0) {};
		Color(uint8_t inr, uint8_t ing, uint8_t inb, uint8_t ina) :r(inr), g(ing), b(inb), a(ina) {};
	};
	std::vector<uint8_t> data(4 * width * height);
	vector<Color> cdata(width * height);
	auto it = cdata.begin();
	switch (type)
	{
	case DummyTextureType::White:
		
		std::fill(data.begin(), data.end(), 0xff);
		result = buff->WriteToSubresource(0, nullptr, data.data(), static_cast<UINT>(width) * height, static_cast<UINT>(data.size()));
		break;
	case DummyTextureType::Black:
		std::fill(data.begin(), data.end(), 0x00);
		result = buff->WriteToSubresource(0, nullptr, data.data(), static_cast<UINT>(width) * height, static_cast<UINT>(data.size()));
		break;
	case DummyTextureType::Gradation:
		for (uint8_t i = 255; i > 0; i--)
		{
			fill(it, it + 4, Color(i, i, i, 0xff));
			it += 4;
		}
		result = buff->WriteToSubresource(0, nullptr, cdata.data(), sizeof(Color), static_cast<UINT>(data.size()));
		break;
	default:
		break;
	}
	if (!CheckResult(result))
	{
		return false;
	}
	return true;
}

bool Dx12Commons::CreateSRVBuffer(ID3D12Resource*& buffer, UINT size)
{
	D3D12_HEAP_PROPERTIES heapProp = CD3DX12_HEAP_PROPERTIES(D3D12_HEAP_TYPE_UPLOAD);
	D3D12_RESOURCE_DESC resDesc = CD3DX12_RESOURCE_DESC::Buffer(size);
	HRESULT result = device_->CreateCommittedResource(
		&heapProp,
		D3D12_HEAP_FLAG_NONE,
		&resDesc,
		D3D12_RESOURCE_STATE_GENERIC_READ,
		nullptr,
		IID_PPV_ARGS(&buffer));
	if (!CheckResult(result))
	{
		return false;
	}
	return true;
}

bool Dx12Commons::CreateRenderTargetBuffer(ID3D12Resource*& buffer, D3D12_RESOURCE_DESC screenDesc, const float* clearColor)
{
	D3D12_HEAP_PROPERTIES heapProp = CD3DX12_HEAP_PROPERTIES(D3D12_HEAP_TYPE_DEFAULT);
	D3D12_CLEAR_VALUE clearVal = CD3DX12_CLEAR_VALUE(screenDesc.Format, clearColor);
	HRESULT result = device_->CreateCommittedResource(
		&heapProp,
		D3D12_HEAP_FLAG_NONE,
		&screenDesc,
		D3D12_RESOURCE_STATE_PIXEL_SHADER_RESOURCE,
		&clearVal,
		IID_PPV_ARGS(&buffer)
	);
	if (!CheckResult(result))
	{
		return false;
	}
	return true;
}

bool Dx12Commons::CreateRTV(ID3D12Resource* resource, D3D12_CPU_DESCRIPTOR_HANDLE handle, DXGI_FORMAT format)
{
	if(&resource == nullptr)
	{
		return false;
	}
	D3D12_RENDER_TARGET_VIEW_DESC rtvDesc = {};
	rtvDesc.ViewDimension = D3D12_RTV_DIMENSION_TEXTURE2D;
	rtvDesc.Format = format;
	device_->CreateRenderTargetView(resource, &rtvDesc, handle);

	return true;
}

bool Dx12Commons::CreateIndexBuffer(ID3D12Resource*& idxBuff, D3D12_INDEX_BUFFER_VIEW& view, vector<uint16_t> indices)
{
	auto totalSize = indices.size() * sizeof(indices[0]);
	HRESULT result = device_->CreateCommittedResource(
		&CD3DX12_HEAP_PROPERTIES(D3D12_HEAP_TYPE_UPLOAD),
		D3D12_HEAP_FLAG_NONE,
		&CD3DX12_RESOURCE_DESC::Buffer(totalSize),
		D3D12_RESOURCE_STATE_GENERIC_READ,
		nullptr,
		IID_PPV_ARGS(&idxBuff));
	if (!CheckResult(result))
	{
		return false;
	}

	uint16_t* mappedIndices = nullptr;
	result = idxBuff->Map(0, nullptr, (void**)&mappedIndices);
	if (!CheckResult(result))
	{
		assert(false);
		return false;
	}
	copy(begin(indices), end(indices), mappedIndices);
	idxBuff->Unmap(0, nullptr);

	view.BufferLocation = idxBuff->GetGPUVirtualAddress();
	view.Format = DXGI_FORMAT_R32_UINT;
	view.SizeInBytes = static_cast<UINT>(totalSize);
	return true;
}

bool Dx12Commons::CreateIndexBuffer(VertexBuffer& buffer)
{
	auto totalSize = buffer.indices.size() * sizeof(buffer.indices[0]);
	HRESULT result = device_->CreateCommittedResource(
		&CD3DX12_HEAP_PROPERTIES(D3D12_HEAP_TYPE_UPLOAD),
		D3D12_HEAP_FLAG_NONE,
		&CD3DX12_RESOURCE_DESC::Buffer(totalSize),
		D3D12_RESOURCE_STATE_GENERIC_READ,
		nullptr,
		IID_PPV_ARGS(buffer.index.ReleaseAndGetAddressOf()));
	if (!CheckResult(result))
	{
		return false;
	}

	uint32_t* mappedIndices = nullptr;
	result = buffer.index->Map(0, nullptr, (void**)&mappedIndices);
	if (!CheckResult(result))
	{
		assert(false);
		return false;
	}
	copy(begin(buffer.indices), end(buffer.indices), mappedIndices);
	buffer.index->Unmap(0, nullptr);

	buffer.indexView.BufferLocation = buffer.index->GetGPUVirtualAddress();
	buffer.indexView.Format = DXGI_FORMAT_R32_UINT;
	buffer.indexView.SizeInBytes = static_cast<UINT>(totalSize);
	return true;
}

bool Dx12Commons::CreateIndexBuffer(VertexBuffer3D& buffer)
{
	auto totalSize = buffer.indices.size() * sizeof(buffer.indices[0]);
	HRESULT result = device_->CreateCommittedResource(
		&CD3DX12_HEAP_PROPERTIES(D3D12_HEAP_TYPE_UPLOAD),
		D3D12_HEAP_FLAG_NONE,
		&CD3DX12_RESOURCE_DESC::Buffer(totalSize),
		D3D12_RESOURCE_STATE_GENERIC_READ,
		nullptr,
		IID_PPV_ARGS(buffer.index.ReleaseAndGetAddressOf()));
	if (!CheckResult(result))
	{
		return false;
	}

	uint32_t* mappedIndices = nullptr;
	result = buffer.index->Map(0, nullptr, (void**)&mappedIndices);
	if (!CheckResult(result))
	{
		assert(false);
		return false;
	}
	copy(begin(buffer.indices), end(buffer.indices), mappedIndices);
	buffer.index->Unmap(0, nullptr);

	buffer.indexView.BufferLocation = buffer.index->GetGPUVirtualAddress();
	buffer.indexView.Format = DXGI_FORMAT_R32_UINT;
	buffer.indexView.SizeInBytes = static_cast<UINT>(totalSize);
	return true;
}

bool Dx12Commons::CreateIndexBuffer(VertexBufferPMD& buffer)
{
	auto totalSize = buffer.indices.size() * sizeof(buffer.indices[0]);
	HRESULT result = device_->CreateCommittedResource(
		&CD3DX12_HEAP_PROPERTIES(D3D12_HEAP_TYPE_UPLOAD),
		D3D12_HEAP_FLAG_NONE,
		&CD3DX12_RESOURCE_DESC::Buffer(totalSize),
		D3D12_RESOURCE_STATE_GENERIC_READ,
		nullptr,
		IID_PPV_ARGS(buffer.index.ReleaseAndGetAddressOf()));
	if (!CheckResult(result))
	{
		return false;
	}

	uint32_t* mappedIndices = nullptr;
	result = buffer.index->Map(0, nullptr, (void**)&mappedIndices);
	if (!CheckResult(result))
	{
		assert(false);
		return false;
	}
	copy(begin(buffer.indices), end(buffer.indices), mappedIndices);
	buffer.index->Unmap(0, nullptr);

	buffer.indexView.BufferLocation = buffer.index->GetGPUVirtualAddress();
	buffer.indexView.Format = DXGI_FORMAT_R32_UINT;
	buffer.indexView.SizeInBytes = static_cast<UINT>(totalSize);
	return true;
}

bool Dx12Commons::CreateIndexBuffer(VertexBufferPMX& buffer)
{
	auto totalSize = buffer.indices.size() * sizeof(buffer.indices[0]);
	HRESULT result = device_->CreateCommittedResource(
		&CD3DX12_HEAP_PROPERTIES(D3D12_HEAP_TYPE_UPLOAD),
		D3D12_HEAP_FLAG_NONE,
		&CD3DX12_RESOURCE_DESC::Buffer(totalSize),
		D3D12_RESOURCE_STATE_GENERIC_READ,
		nullptr,
		IID_PPV_ARGS(buffer.index.ReleaseAndGetAddressOf()));
	if (!CheckResult(result))
	{
		return false;
	}

	uint32_t* mappedIndices = nullptr;
	result = buffer.index->Map(0, nullptr, (void**)&mappedIndices);
	if (!CheckResult(result))
	{
		assert(false);
		return false;
	}
	copy(begin(buffer.indices), end(buffer.indices), mappedIndices);
	buffer.index->Unmap(0, nullptr);

	buffer.indexView.BufferLocation = buffer.index->GetGPUVirtualAddress();
	buffer.indexView.Format = DXGI_FORMAT_R32_UINT;
	buffer.indexView.SizeInBytes = static_cast<UINT>(totalSize);
	return true;
}

bool Dx12Commons::CreateDepthBuffer(ID3D12Resource*& buffer, const SIZE& size)
{
	D3D12_RESOURCE_DESC resDesc{};
	if (size.cx == 0 && size.cy == 0)
	{
		auto& wsize = Application::Instance().GetWindowSize();
		resDesc = CD3DX12_RESOURCE_DESC::Tex2D(DXGI_FORMAT_R32_TYPELESS, wsize.cx, wsize.cy);
	}
	else
	{
		resDesc = CD3DX12_RESOURCE_DESC::Tex2D(DXGI_FORMAT_R32_TYPELESS, size.cx, size.cy);
	}
	resDesc.MipLevels = 1;
	resDesc.Flags = D3D12_RESOURCE_FLAG_ALLOW_DEPTH_STENCIL; //デプスはこれ

	HRESULT result = device_->CreateCommittedResource(
		&CD3DX12_HEAP_PROPERTIES(D3D12_HEAP_TYPE_DEFAULT),
		D3D12_HEAP_FLAG_NONE,
		&resDesc,
		D3D12_RESOURCE_STATE_PIXEL_SHADER_RESOURCE,
		&CD3DX12_CLEAR_VALUE(DXGI_FORMAT_D32_FLOAT, 1.0f, 0U),
		IID_PPV_ARGS(&buffer));
	if (!CheckResult(result))
	{
		return false;
	}
	return true;
}

bool Dx12Commons::CreateConstantBuffer(ID3D12Resource*& buffer, UINT bufferSize)
{
	D3D12_HEAP_PROPERTIES heapProp = CD3DX12_HEAP_PROPERTIES(D3D12_HEAP_TYPE_UPLOAD);
	UINT alignmentedSize = AlignmentedValue(bufferSize, D3D12_CONSTANT_BUFFER_DATA_PLACEMENT_ALIGNMENT);
	D3D12_RESOURCE_DESC resDesc = CD3DX12_RESOURCE_DESC::Buffer(alignmentedSize);
	HRESULT result = device_->CreateCommittedResource(
		&heapProp,
		D3D12_HEAP_FLAG_NONE,
		&resDesc,
		D3D12_RESOURCE_STATE_GENERIC_READ,
		nullptr,
		IID_PPV_ARGS(&buffer));
	if (!CheckResult(result))
	{
		return false;
	}
	return true;
}

bool Dx12Commons::CreateUnorderedAccessBuffer(ID3D12Resource*& buffer, UINT bufferSize, ID3D12Resource*& copyBuffer)
{
	D3D12_HEAP_PROPERTIES heapProp = CD3DX12_HEAP_PROPERTIES(D3D12_HEAP_TYPE_DEFAULT);
	D3D12_RESOURCE_DESC resDesc = CD3DX12_RESOURCE_DESC::Buffer(bufferSize, D3D12_RESOURCE_FLAG_ALLOW_UNORDERED_ACCESS);
	HRESULT result = device_->CreateCommittedResource(
		&heapProp,
		D3D12_HEAP_FLAG_NONE,
		&resDesc,
		D3D12_RESOURCE_STATE_UNORDERED_ACCESS,
		nullptr,
		IID_PPV_ARGS(&buffer));
	if (!CheckResult(result))
	{
		return false;
	}

	heapProp = CD3DX12_HEAP_PROPERTIES(D3D12_HEAP_TYPE_READBACK);
	resDesc.Flags = D3D12_RESOURCE_FLAG_NONE;

	result = device_->CreateCommittedResource(
		&heapProp,
		D3D12_HEAP_FLAG_NONE,
		&resDesc,
		D3D12_RESOURCE_STATE_COPY_DEST,
		nullptr,
		IID_PPV_ARGS(&copyBuffer));
	if (!CheckResult(result))
	{
		buffer = nullptr;
		return false;
	}

	return true;
}

bool Dx12Commons::CreateVertexBuffer(VertexBuffer& buffer)
{
	UINT onedataSize = buffer.vertices[0].Size();
	UINT totalSize = onedataSize * static_cast<UINT>(buffer.vertices.size());

	HRESULT result = device_->CreateCommittedResource(
		&CD3DX12_HEAP_PROPERTIES(D3D12_HEAP_TYPE_UPLOAD),
		D3D12_HEAP_FLAG_NONE,
		&CD3DX12_RESOURCE_DESC::Buffer(totalSize),
		D3D12_RESOURCE_STATE_GENERIC_READ,
		nullptr,
		IID_PPV_ARGS(buffer.resource.ReleaseAndGetAddressOf()));
	if (!CheckResult(result))
	{
		return false;
	}
	buffer.resource->SetName(L"TextureVertexBuffer");

	result = buffer.resource->Map(0, nullptr, (void**)&buffer.mappedPtr);
	if (!CheckResult(result))
	{
		assert(false);
		return false;
	}
	copy(begin(buffer.vertices), end(buffer.vertices), buffer.mappedPtr);

	buffer.view.BufferLocation = buffer.resource->GetGPUVirtualAddress();
	buffer.view.SizeInBytes = totalSize;
	buffer.view.StrideInBytes = onedataSize;

	return true;
}

bool Dx12Commons::CreateVertexBuffer(VertexBuffer3D& buffer)
{
	UINT onedataSize = buffer.vertices[0].Size();
	UINT totalSize = onedataSize * static_cast<UINT>(buffer.vertices.size());

	HRESULT result = device_->CreateCommittedResource(
		&CD3DX12_HEAP_PROPERTIES(D3D12_HEAP_TYPE_UPLOAD),
		D3D12_HEAP_FLAG_NONE,
		&CD3DX12_RESOURCE_DESC::Buffer(totalSize),
		D3D12_RESOURCE_STATE_GENERIC_READ,
		nullptr,
		IID_PPV_ARGS(buffer.resource.ReleaseAndGetAddressOf()));
	if (!CheckResult(result))
	{
		return false;
	}
	buffer.resource->SetName(L"3DVertexBuffer");

	result = buffer.resource->Map(0, nullptr, (void**)&buffer.mappedPtr);
	if (!CheckResult(result))
	{
		assert(false);
		return false;
	}
	copy(begin(buffer.vertices), end(buffer.vertices), buffer.mappedPtr);

	buffer.view.BufferLocation = buffer.resource->GetGPUVirtualAddress();
	buffer.view.SizeInBytes = totalSize;
	buffer.view.StrideInBytes = onedataSize;

	return true;
}

bool Dx12Commons::CreateVertexBuffer(VertexBufferPMD& buffer)
{
	UINT onedataSize = buffer.vertices[0].Size();
	UINT totalSize = onedataSize * static_cast<UINT>(buffer.vertices.size());

	HRESULT result = device_->CreateCommittedResource(
		&CD3DX12_HEAP_PROPERTIES(D3D12_HEAP_TYPE_UPLOAD),
		D3D12_HEAP_FLAG_NONE,
		&CD3DX12_RESOURCE_DESC::Buffer(totalSize),
		D3D12_RESOURCE_STATE_GENERIC_READ,
		nullptr,
		IID_PPV_ARGS(buffer.resource.ReleaseAndGetAddressOf()));
	if (!CheckResult(result))
	{
		return false;
	}
	buffer.resource->SetName(L"PMDVertexBuffer");

	result = buffer.resource->Map(0, nullptr, (void**)&buffer.mappedPtr);
	if (!CheckResult(result))
	{
		assert(false);
		return false;
	}
	copy(begin(buffer.vertices), end(buffer.vertices), buffer.mappedPtr);

	buffer.view.BufferLocation = buffer.resource->GetGPUVirtualAddress();
	buffer.view.SizeInBytes = totalSize;
	buffer.view.StrideInBytes = onedataSize;

	return true;
}

bool Dx12Commons::CreateVertexBuffer(VertexBufferPMX& buffer)
{
	UINT onedataSize = buffer.vertices[0].Size();
	UINT totalSize = onedataSize * static_cast<UINT>(buffer.vertices.size());

	HRESULT result = device_->CreateCommittedResource(
		&CD3DX12_HEAP_PROPERTIES(D3D12_HEAP_TYPE_UPLOAD),
		D3D12_HEAP_FLAG_NONE,
		&CD3DX12_RESOURCE_DESC::Buffer(totalSize),
		D3D12_RESOURCE_STATE_GENERIC_READ,
		nullptr,
		IID_PPV_ARGS(buffer.resource.ReleaseAndGetAddressOf()));
	if (!CheckResult(result))
	{
		return false;
	}
	buffer.resource->SetName(L"PMDVertexBuffer");

	result = buffer.resource->Map(0, nullptr, (void**)&buffer.mappedPtr);
	if (!CheckResult(result))
	{
		assert(false);
		return false;
	}
	copy(begin(buffer.vertices), end(buffer.vertices), buffer.mappedPtr);
	buffer.resource->Unmap(0, nullptr);

	buffer.view.BufferLocation = buffer.resource->GetGPUVirtualAddress();
	buffer.view.SizeInBytes = totalSize;
	buffer.view.StrideInBytes = onedataSize;

	return true;
}

bool Dx12Commons::CreateDescriptorHeap(ID3D12DescriptorHeap*& descriptor, UINT num, D3D12_DESCRIPTOR_HEAP_TYPE type)
{
	if (type == D3D12_DESCRIPTOR_HEAP_TYPE_RTV)
	{
		D3D12_DESCRIPTOR_HEAP_DESC heapDesc = {};
		heapDesc.Type = D3D12_DESCRIPTOR_HEAP_TYPE_RTV;
		heapDesc.NumDescriptors = num;
		heapDesc.Flags = D3D12_DESCRIPTOR_HEAP_FLAG_NONE;
		heapDesc.NodeMask = 0;
		HRESULT result = device_->CreateDescriptorHeap(&heapDesc, IID_PPV_ARGS(&descriptor));
		if (!CheckResult(result))
		{
			assert(false);
			return false;
		}
		return true;
	}

	if (type == D3D12_DESCRIPTOR_HEAP_TYPE_CBV_SRV_UAV)
	{
		D3D12_DESCRIPTOR_HEAP_DESC heapDesc = {};
		heapDesc.Type = D3D12_DESCRIPTOR_HEAP_TYPE_CBV_SRV_UAV;
		heapDesc.NumDescriptors = num;
		heapDesc.Flags = D3D12_DESCRIPTOR_HEAP_FLAG_SHADER_VISIBLE;
		heapDesc.NodeMask = 0;
		HRESULT result = device_->CreateDescriptorHeap(&heapDesc, IID_PPV_ARGS(&descriptor));
		if (!CheckResult(result))
		{
			assert(false);
			return false;
		}
		return true;
	}

	if (type == D3D12_DESCRIPTOR_HEAP_TYPE_DSV)
	{
		D3D12_DESCRIPTOR_HEAP_DESC heapDesc = {};
		heapDesc.Type = D3D12_DESCRIPTOR_HEAP_TYPE_DSV;
		heapDesc.NumDescriptors = num;
		heapDesc.Flags = D3D12_DESCRIPTOR_HEAP_FLAG_NONE;
		heapDesc.NodeMask = 0;
		HRESULT result = device_->CreateDescriptorHeap(&heapDesc, IID_PPV_ARGS(&descriptor));
		if (!CheckResult(result))
		{
			assert(false);
			return false;
		}
		return true;
	}
	return false;
}

bool Dx12Commons::CreateSRV(ID3D12DescriptorHeap& descriptor, ID3D12Resource& resource, UINT offset)
{
	if (&descriptor == nullptr)
	{
		return false;
	}
	D3D12_SHADER_RESOURCE_VIEW_DESC srvDesc = {};
	srvDesc.Shader4ComponentMapping = D3D12_DEFAULT_SHADER_4_COMPONENT_MAPPING;
	srvDesc.Texture2D.MipLevels = 1;
	srvDesc.ViewDimension = D3D12_SRV_DIMENSION_TEXTURE2D;
	auto format = resource.GetDesc().Format;
	if (format == DXGI_FORMAT_R32_TYPELESS)
	{
		srvDesc.Format = DXGI_FORMAT_R32_FLOAT;
	}
	else
	{
		srvDesc.Format = format;
	}

	auto handle = descriptor.GetCPUDescriptorHandleForHeapStart();
	handle.ptr += static_cast<size_t>(device_->GetDescriptorHandleIncrementSize(D3D12_DESCRIPTOR_HEAP_TYPE_CBV_SRV_UAV)) * offset;
	device_->CreateShaderResourceView(&resource, &srvDesc, handle);

	return true;
}

bool Dx12Commons::CreateRTVDescriptorHeap(ID3D12DescriptorHeap*& descriptor, UINT num)
{
	D3D12_DESCRIPTOR_HEAP_DESC heapDesc = {};
	heapDesc.Type = D3D12_DESCRIPTOR_HEAP_TYPE_RTV;
	heapDesc.NumDescriptors = num;
	heapDesc.Flags = D3D12_DESCRIPTOR_HEAP_FLAG_NONE;
	heapDesc.NodeMask = 0;
	HRESULT result = device_->CreateDescriptorHeap(&heapDesc, IID_PPV_ARGS(&descriptor));
	if (!CheckResult(result))
	{
		assert(false);
		return false;
	}
	return true;
}

bool Dx12Commons::CreateCB_SR_UAViewDescriptorHeap(ID3D12DescriptorHeap*& descriptor, UINT num)
{
	//デスクリプタから新規で作成
	D3D12_DESCRIPTOR_HEAP_DESC heapDesc = {};
	heapDesc.Type = D3D12_DESCRIPTOR_HEAP_TYPE_CBV_SRV_UAV;
	heapDesc.Flags = D3D12_DESCRIPTOR_HEAP_FLAG_SHADER_VISIBLE;
	heapDesc.NumDescriptors = num;
	heapDesc.NodeMask = 0;
	HRESULT result = device_->CreateDescriptorHeap(&heapDesc, IID_PPV_ARGS(&descriptor));
	if (!CheckResult(result))
	{
		assert(false);
		return false;
	}
	return true;
}

bool Dx12Commons::CreateSRVTexture2DView(ID3D12Resource* resource, D3D12_CPU_DESCRIPTOR_HANDLE handle)
{
	if (resource == nullptr)
	{
		OutputDebugString(L"ERROR::CreateTexture2DのResourceがnullです。\n");
		return false;
	}
	D3D12_SHADER_RESOURCE_VIEW_DESC srvDesc = {};
	srvDesc.Shader4ComponentMapping = D3D12_DEFAULT_SHADER_4_COMPONENT_MAPPING;
	srvDesc.Texture2D.MipLevels = 1;
	srvDesc.ViewDimension = D3D12_SRV_DIMENSION_TEXTURE2D;
	srvDesc.Format = resource->GetDesc().Format;
	device_->CreateShaderResourceView(resource, &srvDesc, handle);
	return true;
}

bool Dx12Commons::CreateSRVDescriptorAndPlaceView(ID3D12DescriptorHeap*& descriptor, ID3D12Resource* resource, UINT num)
{
	if (descriptor == nullptr)
	{
		if (!CreateCB_SR_UAViewDescriptorHeap(descriptor, num))
		{
			return false;
		}
		if (!CreateSRVTexture2DView(resource, descriptor->GetCPUDescriptorHandleForHeapStart()))
		{
			return false;
		}
	}
	else
	{
		//デスクリプタは作成済みで後ろに種類をくっ付けたい場合
		auto tempDesc = descriptor->GetDesc();
		if (tempDesc.NumDescriptors < 2)
		{
			//サイズが1個の時は何もしない
			return false;
		}
		if (descriptor->GetDesc().Type != D3D12_DESCRIPTOR_HEAP_TYPE_CBV_SRV_UAV)
		{
			//タイプが違う時は何もしない
			return false;
		}
		//既存のデスクリプタヒープの後ろにくっ付ける
		auto handle = descriptor->GetCPUDescriptorHandleForHeapStart();
		handle.ptr += static_cast<size_t>(device_->GetDescriptorHandleIncrementSize(D3D12_DESCRIPTOR_HEAP_TYPE_CBV_SRV_UAV)) * num;
		if (!CreateSRVTexture2DView(resource, handle))
		{
			return false;
		}
	}
	return true;
}

bool Dx12Commons::CreateDSVDescriptor(ID3D12DescriptorHeap*& descriptor, UINT num)
{
	//新規で作成
	D3D12_DESCRIPTOR_HEAP_DESC heapDesc = {};
	heapDesc.Type = D3D12_DESCRIPTOR_HEAP_TYPE_DSV;
	heapDesc.Flags = D3D12_DESCRIPTOR_HEAP_FLAG_NONE;
	heapDesc.NodeMask = 0;
	heapDesc.NumDescriptors = num;
	HRESULT result = device_->CreateDescriptorHeap(&heapDesc, IID_PPV_ARGS(&descriptor));
	if (!CheckResult(result))
	{
		assert(false);
		return false;
	}
	return true;
}

bool Dx12Commons::CreateDSV(ID3D12Resource* resource, D3D12_CPU_DESCRIPTOR_HANDLE handle)
{
	if (resource == nullptr)
	{
		OutputDebugString(L"ERROR::CreateDSViewのResourceがnullです。");
		return false;
	}
	D3D12_DEPTH_STENCIL_VIEW_DESC dsvDesc = {};
	dsvDesc.ViewDimension = D3D12_DSV_DIMENSION_TEXTURE2D;
	dsvDesc.Flags = D3D12_DSV_FLAG_NONE;
	dsvDesc.Format = DXGI_FORMAT_D32_FLOAT;
	device_->CreateDepthStencilView(resource, &dsvDesc, handle);
	return true;
}

bool Dx12Commons::CreateDSVDescriptorAndPlaceView(ID3D12DescriptorHeap*& descriptor, ID3D12Resource* resource, UINT num)
{
	if (descriptor == nullptr)
	{
		if (!CreateDSVDescriptor(descriptor, num))
		{
			return false;
		}
		if (!CreateDSV(resource, descriptor->GetCPUDescriptorHandleForHeapStart()))
		{
			return false;
		}
	}
	else
	{
		//既存のデスクリプタヒープの最後尾にくっ付ける
		auto tempDesc = descriptor->GetDesc();
		if (tempDesc.NumDescriptors < 2)
		{
			//1つ未満の場合は足せないのでNG
			return false;
		}
		if (tempDesc.Type != D3D12_DESCRIPTOR_HEAP_TYPE_DSV)
		{
			//種類が違う時はまとめて扱えないのでNG
			return false;
		}
		//既存のデスクリプタヒープの後ろにくっ付ける
		auto handle = descriptor->GetCPUDescriptorHandleForHeapStart();
		handle.ptr += static_cast<size_t>(device_->GetDescriptorHandleIncrementSize(D3D12_DESCRIPTOR_HEAP_TYPE_DSV)) * num;
		if (!CreateDSV(resource, handle))
		{
			return false;
		}
	}
	return true;
}

bool Dx12Commons::CreateCBV(ID3D12Resource* resource, D3D12_CPU_DESCRIPTOR_HANDLE handle)
{
	if (resource == nullptr)
	{
		return false;
	}
	auto resDesc = resource->GetDesc();
	D3D12_CONSTANT_BUFFER_VIEW_DESC cbvDesc = {};
	cbvDesc.BufferLocation = resource->GetGPUVirtualAddress();
	cbvDesc.SizeInBytes = static_cast<UINT>(resDesc.Width);
	device_->CreateConstantBufferView(&cbvDesc, handle);
	return true;
}

bool Dx12Commons::AddPipeline(D3D12_GRAPHICS_PIPELINE_STATE_DESC& plsDesc, const LPCWSTR pipelineName)
{
	Microsoft::WRL::ComPtr<ID3D12PipelineState> plState;
	HRESULT result = device_->CreateGraphicsPipelineState(&plsDesc, IID_PPV_ARGS(plState.ReleaseAndGetAddressOf()));
	if (!CheckResult(result))
	{
		OutputDebugString(L"ERROR::Pipelineの作成に失敗しました。\n");
		return false;
	}

	if (pipelineLibrary_ == nullptr)
	{
		CreatePipelineLibrary();
	}

	result = pipelineLibrary_->StorePipeline(pipelineName, plState.Get());
	if (!CheckResult(result))
	{
		OutputDebugString(L"ERROR::PipelineLibraryの登録に失敗しました。\n");
		return false;
	}

	size_t librarySize = pipelineLibrary_->GetSerializedSize();
	void* data = new BYTE[librarySize];
	result = pipelineLibrary_->Serialize(data, librarySize);
	if (!CheckResult(result))
	{
		OutputDebugString(L"ERROR::Libraryのシリアライズ登録に失敗しました。\n");
		return false;
	}
	OutputDebugString(L"Shader:");
	OutputDebugString(pipelineName);
	OutputDebugString(L" を作成しました。\n");
	return true;
}

bool Dx12Commons::LoadPipeline(ID3D12PipelineState*& pipeline, const D3D12_GRAPHICS_PIPELINE_STATE_DESC& loadplsDesc, LPCWSTR pName)
{
	if (pipelineLibrary_ == nullptr)
	{
		CreatePipelineLibrary();
	}
	HRESULT result = pipelineLibrary_->LoadGraphicsPipeline(pName, &loadplsDesc, IID_PPV_ARGS(&pipeline));
	if (!CheckResult(result))
	{
		OutputDebugString(L"ERROR::Pipelineの読み込みに失敗しました。\n");
		return false;
	}
	return true;
}

void Dx12Commons::CreatePipelineLibrary()
{
	HRESULT result = device_->CreatePipelineLibrary(nullptr, 0, IID_PPV_ARGS(pipelineLibrary_.ReleaseAndGetAddressOf()));
	if (!CheckResult(result))
	{
		OutputDebugString(L"PipelineLibraryの作成に失敗しました。\n");
	}
}

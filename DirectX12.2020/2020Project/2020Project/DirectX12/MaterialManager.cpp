#include <d3d12.h>
#include "MaterialManager.h"

MaterialManager::MaterialManager()
{
}

ID3D12Resource*& MaterialManager::GetMaterialBuffer()
{
	return *materialBuffer_.GetAddressOf();
}

ID3D12Resource*& MaterialManager::GetMaterialNumBuffer()
{
	return *materialNumBuffer_.GetAddressOf();
}

ID3D12Resource*& MaterialManager::GetAdditionalBuffer()
{
	return *addParameterBuffers_.GetAddressOf();
}

ID3D12DescriptorHeap*& MaterialManager::GetDescriptor()
{
	return *descriptor_.GetAddressOf();
}

const std::vector<Material>& MaterialManager::GetMaterials()
{
	return materials_;
}

void MaterialManager::SetMaterials(const std::vector<Material>& inMat)
{
	materials_.clear();
	materials_ = inMat;
}

Material*& MaterialManager::GetMaterialParameter()
{
	return mappedMaterials_;
}

const std::vector<UINT>& MaterialManager::GetMatNums()
{
	return materialNums_;
}

void MaterialManager::SetMatNums(const std::vector<UINT>& inNums)
{
	materialNums_.clear();
	materialNums_ = inNums;
}

void MaterialManager::AddIntParameter(const std::vector<int>& add)
{
	for (auto& a : add)
	{
		addIntParam_.push_back(a);
	}
}

const std::vector<int>& MaterialManager::GetIntParameter()
{
	return addIntParam_;
}

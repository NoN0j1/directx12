#include <filesystem>
#include <cassert>
#include <d3dcompiler.h>
#include <DirectXTex.h>
#include "ResourceManager.h"
#include "Dx12Wrapper.h"
#include "Dx12Commons.h"
#include "Mesh/ModelManager.h"
#include "Texture/Texture.h"
#include "DirectX12Geometory.h"
#include "DirectX12ToolFunction.h"
#include "ShaderManager.h"
#include "Mesh/VMDMotion.h"

#pragma comment(lib, "D3DCompiler.lib")
#pragma comment(lib, "DirectXTex.lib")

using namespace std;
using namespace DirectX;

constexpr UINT texture_manage_max = 512;
constexpr UINT shadow_manage_max = 16;

ID3D12DescriptorHeap*& ResourceManager::GetShadowDescriptorHeap()
{
	return *shadowTextureDescriptor_.GetAddressOf();
}

void ResourceManager::Release()
{
	dx12_->WaitWithFence();
	loadTextures_.clear();
	dummyTextures_.clear();
}

ResourceManager::~ResourceManager()
{
}

ResourceManager::ResourceManager()
{
}

UINT ResourceManager::GetOffset()
{
	return static_cast<UINT>(dummyTextures_.size() + loadTextures_.size() + renderTargetTextures_.size());
}

void ResourceManager::CreateWhiteTexture()
{
	auto tex = Texture(SIZE({ 4, 4}));
	auto& commons = dx12_->GetCommons();
	auto& buffer = tex.GetResource();
	commons.CreateTextureBuffer(buffer, DummyTextureType::White);
	buffer->SetName(L"WhiteTextureBuffer");
	commons.CreateSRV(*textureDescriptor_.Get(), *buffer, static_cast<UINT>(DummyTextureType::White));
	commons.CreateConstantBuffer(tex.GetConstantBuffer(), sizeof(*tex.GetConstantParameter()));
	commons.CreateCBVDescriptorAndPlaceView(tex.GetDescriptorHeap(), tex.GetConstantBuffer(), tex.GetConstantParameter());
	tex.InitConstantMatrix();
	dummyTextures_[static_cast<int>(DummyTextureType::White)] = tex;
}

void ResourceManager::CreateBlackTexture()
{
	auto tex = Texture(SIZE({ 4, 4 }));
	auto& commons = dx12_->GetCommons();
	auto& buffer = tex.GetResource();
	dx12_->GetCommons().CreateTextureBuffer(buffer, DummyTextureType::Black);
	buffer->SetName(L"BlackTextureBuffer");
	dx12_->GetCommons().CreateSRV(*textureDescriptor_.Get(), *buffer, static_cast<UINT>(DummyTextureType::Black));
	commons.CreateConstantBuffer(tex.GetConstantBuffer(), sizeof(*tex.GetConstantParameter()));
	commons.CreateCBVDescriptorAndPlaceView(tex.GetDescriptorHeap(), tex.GetConstantBuffer(), tex.GetConstantParameter());
	tex.InitConstantMatrix();
	dummyTextures_[static_cast<int>(DummyTextureType::Black)] = tex;
}

void ResourceManager::CreateGradationTexture()
{
	auto tex = Texture(SIZE({ 4, 256 }));
	auto& commons = dx12_->GetCommons();
	auto& buffer = tex.GetResource();
	dx12_->GetCommons().CreateTextureBuffer(buffer, DummyTextureType::Gradation, 4, 256);
	buffer->SetName(L"GradationTexture");
	dx12_->GetCommons().CreateSRV(*textureDescriptor_.Get(), *buffer, static_cast<UINT>(DummyTextureType::Gradation));
	commons.CreateConstantBuffer(tex.GetConstantBuffer(), sizeof(*tex.GetConstantParameter()));
	commons.CreateCBVDescriptorAndPlaceView(tex.GetDescriptorHeap(), tex.GetConstantBuffer(), tex.GetConstantParameter());
	tex.InitConstantMatrix();
	dummyTextures_[static_cast<int>(DummyTextureType::Gradation)] = tex;
}

ResourceManager& ResourceManager::Instance()
{
	static ResourceManager instance_;
	return instance_;
}

void ResourceManager::Init(Dx12Wrapper& dx12Wrapper)
{
	dx12_ = &dx12Wrapper;
	auto& commons = dx12_->GetCommons();
	if (!commons.CreateDescriptorHeap(*textureDescriptor_.GetAddressOf(), texture_manage_max, D3D12_DESCRIPTOR_HEAP_TYPE_CBV_SRV_UAV))
	{
		assert(0);
	}
	if (!commons.CreateDescriptorHeap(*renderTextureDescriptor_.GetAddressOf(), shadow_manage_max, D3D12_DESCRIPTOR_HEAP_TYPE_CBV_SRV_UAV))
	{
		assert(0);
	}
	if (!commons.CreateDescriptorHeap(*shadowTextureDescriptor_.GetAddressOf(), shadow_manage_max, D3D12_DESCRIPTOR_HEAP_TYPE_CBV_SRV_UAV))
	{
		assert(0);
	}
	dummyTextures_.resize(static_cast<int>(DummyTextureType::MAX));

	CreateWhiteTexture();
	CreateBlackTexture();
	CreateGradationTexture();

	animationList_[L"dummy"] = MotionData();
	modelManager_ = make_unique<ModelManager>(dx12Wrapper);
	shaderManager_ = make_unique<ShaderManager>(dx12Wrapper);
}

int ResourceManager::LoadGraph(const std::wstring& path)
{
	if (textureTable_.contains(path))
	{
		return textureTable_.at(path);
	}

	filesystem::path filePath(path);
	if (!filesystem::exists(filePath))
	{
		OutputDebugString(L"WARNING! ");
		OutputDebugString(path.c_str());
		OutputDebugString(L":TextureのFilePathが不正です。\n");
	}

	TextureParameter param = {};

	HRESULT result = E_FAIL;
	auto ext = filePath.extension();
	if (ext == ".tga")
	{
		result = LoadFromTGAFile(filePath.c_str(), &param.metadata, param.scratchImage);
	}
	else
	{
		result = LoadFromWICFile(filePath.c_str(), WIC_FLAGS_IGNORE_SRGB, &param.metadata, param.scratchImage);
	}

	if (!CheckResult(result))
	{
		OutputDebugString(L"Error: ");
		OutputDebugString(path.c_str());
		OutputDebugString(L" の読み込みに失敗しました。\n");
	}

	switch (param.metadata.dimension)
	{
	case TEX_DIMENSION_TEXTURE1D:
		param.resourceDesc = CD3DX12_RESOURCE_DESC::Tex1D(
			param.metadata.format,
			static_cast<UINT64>(param.metadata.width),
			static_cast<UINT16>(param.metadata.arraySize));
		break;
	case TEX_DIMENSION_TEXTURE2D:
		param.resourceDesc = CD3DX12_RESOURCE_DESC::Tex2D(
			param.metadata.format,
			static_cast<UINT64>(param.metadata.width),
			static_cast<UINT>(param.metadata.height),
			static_cast<UINT16>(param.metadata.arraySize),
			static_cast<UINT16>(param.metadata.mipLevels));
		break;
	case TEX_DIMENSION_TEXTURE3D:
		param.resourceDesc = CD3DX12_RESOURCE_DESC::Tex3D(
			param.metadata.format,
			static_cast<UINT64>(param.metadata.width),
			static_cast<UINT>(param.metadata.height),
			static_cast<UINT16>(param.metadata.depth),
			static_cast<UINT16>(param.metadata.mipLevels));
		break;
	default:
		OutputDebugString(L"matadataが無効です。\n");
		break;
	}
	Texture tex(SIZE({ static_cast<LONG>(param.resourceDesc.Width), static_cast<LONG>(param.resourceDesc.Height) }));
	D3D12_TEXTURE_COPY_LOCATION src = {};
	D3D12_TEXTURE_COPY_LOCATION dst = {};
	Microsoft::WRL::ComPtr<ID3D12Resource> internalBuffer = nullptr;
	auto& texBuff = tex.GetResource();
	auto& commons = dx12_->GetCommons();
	commons.CreateConstantBuffer(tex.GetConstantBuffer(), sizeof(*tex.GetConstantParameter()));
	commons.CreateCBVDescriptorAndPlaceView(tex.GetDescriptorHeap(), tex.GetConstantBuffer(), tex.GetConstantParameter());
	tex.InitConstantMatrix();
	commons.CreateVertexBuffer(tex.GetVertexBuffer());
	commons.CreateIndexBuffer(tex.GetVertexBuffer());

	commons.CreateTextureBuffer(texBuff, *internalBuffer.GetAddressOf(), param, src, dst);
	dx12_->CopyTexture(src, dst);
	commons.CreateSRV(*textureDescriptor_.Get(), *tex.GetResource(), GetOffset());
	textureTable_[path] = static_cast<int>(GetOffset());
	loadTextures_.push_back(tex);
	
	OutputDebugString(path.c_str());
	OutputDebugString(L" 読み込み完了\n");
	return textureTable_[path];
}

int ResourceManager::AddRenderGraph(Microsoft::WRL::ComPtr<ID3D12Resource>& rt)
{
	auto& commons = dx12_->GetCommons();
	auto offset = static_cast<int>(renderTargetTextures_.size());
	commons.CreateSRV(*renderTextureDescriptor_.Get(), *rt.Get(), offset);
	renderTargetTextures_.push_back(rt);
	return offset;
}

int ResourceManager::AddShadowGraph(ID3D12Resource*& shadowBuff)
{
	auto& commons = dx12_->GetCommons();
	auto offset = static_cast<int>(shadowTextures_.size());
	commons.CreateSRV(*shadowTextureDescriptor_.Get(), *shadowBuff, offset);
	shadowTextures_.push_back(shadowBuff);
	return offset;
}

bool ResourceManager::LoadModel(const std::wstring& fileName, const std::wstring& modelName)
{
	return modelManager_->LoadModel(fileName, modelName);
}

const std::shared_ptr<Mesh> ResourceManager::GetModel(const std::wstring& modelName)
{
	return modelManager_->GetMesh(modelName);
}

const std::shared_ptr<Mesh> ResourceManager::GetModel(const PrimitiveMeshType& type)
{
	return modelManager_->GetMesh(type);
}

void ResourceManager::LoadAnimation(const std::wstring& path, const std::wstring& name)
{
	if (MotionPathList_.contains(path))
	{
		//重複チェック
		return;
	}

	filesystem::path filePath(path);
	if (!filesystem::exists(filePath))
	{
		OutputDebugString(L"WARNING! ");
		OutputDebugString(path.c_str());
		OutputDebugString(L":AnimationのFilePathが不正です。\n");
	}
	FILE* fp = nullptr;;
	auto error = _wfopen_s(&fp, filePath.c_str(), L"rb");
	if (error != 0)
	{
		return;
	}
	MotionPathList_[path] = name;
	auto& anim = animationList_[name];
	anim.animationName = name;
	//最初50はイランので進める
	fseek(fp, 50, SEEK_SET);

	uint32_t keyframeNum;

	fread(&keyframeNum, sizeof(keyframeNum), 1, fp); //キーフレーム数読み込み

#pragma pack(1)
	struct VMDKeyFrame
	{
		char boneName[15];
		uint32_t frameNo;
		XMFLOAT3 location;
		XMFLOAT4 quaternion;
		uint8_t bezier[64];
	};
#pragma pack()
	vector<VMDKeyFrame> frameData(keyframeNum);
	fread(frameData.data(), sizeof(VMDKeyFrame), frameData.size(), fp);
	//各ボーンの回転に適用
	anim.duration = 0;
	uint32_t offset = 15;
	for (auto& keyframe : frameData)
	{
		string boneName = keyframe.boneName;
		wstring wboneName = WstringFromString(boneName);
		anim.motionData[wboneName].emplace_back(KeyFrame(
			keyframe.frameNo,
			keyframe.quaternion,
			keyframe.location,
			XMFLOAT2((float)keyframe.bezier[3 + offset] / 127.0f, (float)keyframe.bezier[7 + offset] / 127.0f),
			XMFLOAT2((float)keyframe.bezier[11 + offset] / 127.0f, (float)keyframe.bezier[15 + offset] / 127.0f)
		));
		anim.duration = max(keyframe.frameNo, anim.duration);
	}

	//MMDの罠で純粋にキーフレームを登録した順でデータが入っているので
	//ボーン内の各キーフレームのソートをキーフレーム順になるように行う
	//順序には実行フレーム数を用いる
	for (auto& boneData : anim.motionData)
	{
		auto& frame = boneData.second;
		sort(frame.begin(), frame.end(), [](KeyFrame& a, KeyFrame& b) {return a.frameNum < b.frameNum; });
	}
#pragma pack(1)
	//スキンデータ読み込み// 23 Bytes // 表情
	struct VMDSkinMotionData 
	{
		char skinName[15]; // 表情名
		DWORD frameNo; // フレーム番号
		float weight; // 表情の設定値(表情スライダーの値)
	};
#pragma pack()
	uint32_t skinMotionNum = 0;
	fread(&skinMotionNum, sizeof(skinMotionNum), 1, fp); //表情データ数取得
	vector<VMDSkinMotionData> skinMotions(skinMotionNum);
	fread(skinMotions.data(), sizeof(VMDSkinMotionData), skinMotions.size(), fp);
	
	sort(skinMotions.begin(), skinMotions.end(), [](const VMDSkinMotionData& lval, const VMDSkinMotionData& rval)->bool {return lval.frameNo < rval.frameNo; });
	for (auto& skin : skinMotions)
	{
		anim.skinData[skin.skinName].push_back(SkinMotionData(skin.frameNo, skin.weight));
	}

	fclose(fp);
}

const MotionData& ResourceManager::GetAnimation(const std::wstring& name) const
{
	if (animationList_.contains(name))
	{
		return animationList_.at(name);
	}
	OutputDebugString(name.c_str());
	OutputDebugString(L"を取得できませんでした\n");
	return animationList_.at(L"dummy");
}

bool ResourceManager::LoadShader(ShaderParameter* param)
{
	return shaderManager_->LoadShader(*param);
}

bool ResourceManager::LoadComputeShader(ShaderDetailed& param, const std::wstring& shaderName)
{
	return shaderManager_->LoadComputeShader(param, shaderName);
}

const std::shared_ptr<Shader> ResourceManager::GetShader(const std::wstring& shaderName)
{
	return shaderManager_->GetShader(shaderName);
}

Texture& ResourceManager::GetTexture(int handle)
{
	if (handle < 0 || handle > loadTextures_.size())
	{
		return dummyTextures_[0];
	}
	auto num = handle - static_cast<int>(DummyTextureType::MAX);
	if (num < 0)
	{
		return dummyTextures_[handle];
	}

	return loadTextures_[num];
}

Texture& ResourceManager::GetDummyTexture(DummyTextureType type)
{
	return dummyTextures_[static_cast<int>(type)];
}

ID3D12DescriptorHeap*& ResourceManager::GetTextureDescriptorHeap()
{
	return *textureDescriptor_.GetAddressOf();
}

ID3D12DescriptorHeap*& ResourceManager::GetRenderTextureDescriptorHeap()
{
	return *renderTextureDescriptor_.GetAddressOf();
}

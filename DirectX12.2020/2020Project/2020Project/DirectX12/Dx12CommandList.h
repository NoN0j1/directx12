#pragma once
#include <wrl.h>

class Id3D12Device6;
struct ID3D12CommandAllocator;
struct ID3D12GraphicsCommandList5;

class Dx12CommandList
{
public:
	Dx12CommandList(ID3D12Device6*& dev);
	~Dx12CommandList();
	void Reset();
	ID3D12GraphicsCommandList5* Get();
private:
	Microsoft::WRL::ComPtr<ID3D12CommandAllocator> cmdAlloc_;
	Microsoft::WRL::ComPtr<ID3D12GraphicsCommandList5> cmdList_;
	/// <summary>
	/// コマンドアロケーターとコマンドリストの生成
	/// </summary>
	/// <returns>true = 成功, false = 失敗</returns>
	bool CreateCommandAllocator();
	bool CreateCommandList();

	ID3D12Device6*& device_;
};


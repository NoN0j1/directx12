#pragma once
#include <map>
#include "DirectX12Geometory.h"

class BoneManager
{
public:
	void Update();
	void UpdateAnimation(const uint32_t frame);
	void SetAnimation(const MotionData& data, bool loop, uint32_t interval);

	std::vector<DirectX::XMMATRIX>& GetBoneMatrices();
	std::map<std::wstring, BoneNode>& GetBoneMap();
	std::vector<std::vector<int>>& GetBoneTree();
	ID3D12Resource*& GetBuffer();
	ID3D12DescriptorHeap*& GetDescriptorHeap();
	DirectX::XMMATRIX*& GetMappedPointer();
private:
	void BoneRotate(const std::wstring& name, const std::vector<KeyFrame>::reverse_iterator& it);
	void BoneLerpRotate(const std::wstring& name, const std::vector<KeyFrame>::reverse_iterator& itA, const std::vector<KeyFrame>::iterator& itB, float c);

	//再帰関数
	//@param idx 適用させるインデックス情報
	//@param mat 合成する行列
	void RecursiveBoneTransform(int idx, const DirectX::XMMATRIX& mat);

	/// <summary>
	/// X座標データからベジエのY座標を求める
	/// </summary>
	/// <param name="x">Y軸を取得するためのX座標</param>
	/// <param name="cp1">コントロールポイント1</param>
	/// <param name="cp2">コントロールポイント2</param>
	/// <param name="trycount">試行回数</param>
	/// <returns>Y座標</returns>
	float GetYFromXOnBezier(float x, const DirectX::XMFLOAT2& cp1, const DirectX::XMFLOAT2& cp2, uint32_t trycount = 12);

	uint32_t duration_{}; //アニメーションの総フレーム数
	std::wstring animName_{}; //現在保持しているアニメーション名
	bool isAnimation_{}; //アニメーションを再生中かどうか
	bool isLoop_{}; //ループ再生するかどうか
	uint32_t lastTickCount_{}; //ロード等の処理が終わってアニメーションを開始した時点でのTickCount
	std::unordered_map<std::wstring, std::vector<KeyFrame>> animationData_;

	std::vector<DirectX::XMMATRIX> boneMatrices_;
	std::map<std::wstring, BoneNode> boneMap_;
	std::vector<std::vector<int>> boneTree_;
	DirectX::XMMATRIX* mappedPtr_{};
	Microsoft::WRL::ComPtr<ID3D12Resource> boneBuffer_;
	Microsoft::WRL::ComPtr<ID3D12DescriptorHeap> descriptor_;
};
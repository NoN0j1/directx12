#pragma once
#include <unordered_map>
#include <memory>
#include <DirectXMath.h>
#include <string>
#include <vector>

struct VertexBufferBase;
struct MotionData;
struct SkinMotionData;

struct PMDSkin
{
	std::vector<std::pair<uint32_t, DirectX::XMFLOAT3>> skindata;
};

class MorphingManager
{
public:
	MorphingManager();
	~MorphingManager();
	void Update(std::shared_ptr<VertexBufferBase>& vbBase);
	void SetAnimation(const MotionData& data, bool loop, uint32_t interval);
	PMDSkin& GetPMDSkinData(std::string name);
private:
	void UpdateMorphing(const uint32_t currentFrame, std::shared_ptr<VertexBufferBase>& vbBase);
	void StopMorphing(std::shared_ptr<VertexBufferBase>& vbBase);
	std::unordered_map<std::string, PMDSkin> skinDatas_;
	uint32_t duration_{}; //アニメーションの総フレーム数
	std::wstring animName_{}; //現在保持しているアニメーション名
	bool isAnimation_{}; //アニメーションを再生中かどうか
	bool isLoop_{}; //ループ再生するかどうか
	uint32_t lastTickCount_{}; //ロード等の処理が終わってアニメーションを開始した時点でのTickCount
	std::unordered_map<std::string, std::vector<SkinMotionData>> skinAnimationData_;
};


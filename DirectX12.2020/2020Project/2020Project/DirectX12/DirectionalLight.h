#pragma once
#include <memory>
#include <DirectXMath.h>
#include <wrl.h>
#include "../Shader/MultiPurpose.hlsli"

struct ID3D12DescriptorHeap;
struct ID3D12Resource;
struct Screen;
class Camera;
class Input;

class DirectionalLight
{
public:
	DirectionalLight(const DirectX::XMFLOAT3& rot);
	~DirectionalLight();
	void Update();
	void LightInit(bool perth);
	void AddRotate(float x, float y, float z);
	std::shared_ptr<Camera>& GetShadowCamera();
	std::shared_ptr<Screen>& GetShadowScreen();
	ID3D12DescriptorHeap*& GetDescriptorHeap();
	ID3D12Resource*& GetBuffer();
	LightCommons*& GetMappingConstant();
private:
	Microsoft::WRL::ComPtr<ID3D12DescriptorHeap> lightCBVDescriptor_;
	Microsoft::WRL::ComPtr<ID3D12Resource> lightCBuffer_;
	std::shared_ptr<Camera> lightCamera_;
	DirectX::XMFLOAT3 direction_;
	std::shared_ptr<Screen> shadowScreen_;
	LightCommons* mappedConstant_;
	float GetSign(float checkNum);
	DirectX::XMFLOAT3 addRotate_{};
	void SetCameraPos();
};
#include "CameraActor.h"
#include "../Application.h"
#include "Input.h"

CameraActor::CameraActor()
{
}

void CameraActor::CameraInit(bool perce)
{
	UpdateViewProjection();
}

void CameraActor::Update()
{
	auto& input = Input::Instance();
	Move(input);
	UpdateTransform();
	UpdateViewProjection();
}

CameraActor::CameraActor(const Transform& tr):Camera(tr)
{
	auto& input = Input::Instance();
	input.AddKeyEvent("cameraLeft", KeyCode::A);
	input.AddKeyEvent("cameraRight", KeyCode::D);
	input.AddKeyEvent("cameraUp", KeyCode::W);
	input.AddKeyEvent("cameraDown", KeyCode::S);
	input.AddKeyEvent("cameraZoom", KeyCode::Space);
}

void CameraActor::Move(const Input& input)
{
	DirectX::XMFLOAT3 pos{};

	//左移動
	if (input.GetKey("cameraLeft"))
	{
		pos.x -= 0.1f;
		target_.x -= 0.1f;
	}
	//右移動
	if (input.GetKey("cameraRight"))
	{
		pos.x += 0.1f;
		target_.x += 0.1f;
	}
	//上移動
	if (input.GetKey("cameraUp"))
	{
		pos.y += 0.1f;
		target_.y += 0.1f;
	}
	//上回転
	if (input.GetKey("cameraUp", DoublePush::Ctr))
	{
		target_.y += 0.1f;
	}
	//下移動
	if (input.GetKey("cameraDown"))
	{
		pos.y -= 0.1f;
		target_.y -= 0.1f;
	}
	//下回転
	if (input.GetKey("cameraDown", DoublePush::Ctr))
	{
		target_.y -= 0.1f;
	}
	//奥移動
	if (input.GetKey("cameraZoom"))
	{
		pos.z += 0.1f;
		target_.z += 0.1f;
	}
	//手前移動
	if (input.GetKey("cameraZoom", DoublePush::Shift))
	{
		pos.z -= 0.1f;
		target_.z -= 0.1f;
	}

	MoveActor(pos);
}

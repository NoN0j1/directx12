#include "ParticalEffect.h"

ParticleEffect::ParticleEffect(const Effekseer::Handle& h):handle_(h)
{
}

void ParticleEffect::Update()
{
	++time;
	if (time == 120)
	{
		fin_ = true;
	}
}

const bool ParticleEffect::GetFinish() const
{
	return fin_;
}

const Effekseer::Handle& ParticleEffect::GetHandle()
{
	return handle_;
}

#pragma once
#include <array>
#include <vector>
#include <wrl.h>
#include <dinput.h>
#include <string>
#include <unordered_map>

/// <summary>
/// キーイベントに指定可能なキーデータ
/// </summary>
enum class KeyCode
{
	BackSpace = DIK_BACKSPACE,
	Return = DIK_RETURN,
	Escape = DIK_ESCAPE,
	Space = DIK_SPACE,
	Left = DIK_LEFT,
	Up = DIK_UP,
	Right = DIK_RIGHT,
	Down = DIK_DOWN,
	PrintScreen = DIK_SYSRQ,
	Delete = DIK_DELETE,
	KEY_0 = DIK_0,
	KEY_1 = DIK_1,
	KEY_2 = DIK_2,
	KEY_3 = DIK_3,
	KEY_4 = DIK_4,
	KEY_5 = DIK_5,
	KEY_6 = DIK_6,
	KEY_7 = DIK_7,
	KEY_8 = DIK_8,
	KEY_9 = DIK_9,
	A = DIK_A,
	B = DIK_B,
	C = DIK_C,
	D = DIK_D,
	E = DIK_E,
	F = DIK_F,
	G = DIK_G,
	H = DIK_H,
	I = DIK_I,
	J = DIK_J,
	K = DIK_K,
	L = DIK_L,
	M = DIK_M,
	N = DIK_N,
	O = DIK_O,
	P = DIK_P,
	Q = DIK_Q,
	R = DIK_R,
	S = DIK_S,
	T = DIK_T,
	U = DIK_U,
	V = DIK_V,
	W = DIK_W,
	X = DIK_X,
	Y = DIK_Y,
	Z = DIK_Z,
	LShift = DIK_LSHIFT,
	RShift = DIK_RSHIFT,
	LCtr = DIK_LCONTROL,
	RCtr = DIK_RCONTROL,
	LAlt = DIK_LALT,
	RAlt = DIK_RALT,
};

/// <summary>
/// 同時押しに対応しているキー
/// </summary>
enum class DoublePush
{
	Ctr,
	Shift,
	Alt,
};

struct KeyState
{
	/// <summary>
	/// 現在のキー情報
	/// </summary>
	std::array<BYTE, 256> keyState{};

	/// <summary>
	/// 更新前(1フレーム前)のキー情報
	/// </summary>
	std::array<BYTE, 256> oldState{};

	/// <summary>
	/// キー入力が有効かどうか true = 有効, false = 無効
	/// </summary>
	bool enable{};
};

class Input
{
public:
	~Input();
	static Input& Instance();

	bool Init(HWND& winH, HINSTANCE instnace);

	/// <summary>
	/// コントローラーを追加する
	/// </summary>
	void AddController();

	/// <summary>
	/// 全部の入力状態を更新する
	/// </summary>
	void UpdateKeyState();

	/// <summary>
	/// 指定したコントローラーの入力状態を更新する
	/// </summary>
	/// <param name="pNum">コントローラー番号</param>
	void UpdateKeyState(uint8_t pNum);

	/// <summary>
	/// 押した瞬間かどうかを取得する
	/// </summary>
	/// <param name="code">確認する入力キー</param>
	/// <param name="pNum">コントローラー番号</param>
	/// <returns>true = 押した瞬間、false = それ以外</returns>
	const bool GetKeyDown(KeyCode code, uint8_t pNum = 0)const;

	/// <summary>
	/// 指定キーを押しながら押した瞬間かどうかを取得する
	/// </summary>
	/// <param name="code">確認する入力キー</param>
	/// <param name="key">同時押しするキー</param>
	/// <param name="pNum">コントローラ−番号</param>
	/// <returns>true = 押した瞬間、false = それ以外</returns>
	const bool GetKeyDown(KeyCode code, DoublePush key, uint8_t pNum = 0)const;

	/// <summary>
	/// 指定キーイベントに含まれるキーが押されているかを取得する
	/// </summary>
	/// <param name="eventName">イベント名</param>
	/// <param name="pNum">コントローラ−番号</param>
	/// <returns>true = 押した瞬間、false = それ以外</returns>
	const bool GetKeyDown(std::string eventName, uint8_t pNum = 0)const;

	/// <summary>
	/// 指定キーを押しながら押した瞬間かどうかを取得する
	/// </summary>
	/// <param name="eventName">イベント名</param>
	/// <param name="key">同時押しするキー</param>
	/// <param name="pNum">コントローラ−番号</param>
	/// <returns>true = 押した瞬間、false = それ以外</returns>
	const bool GetKeyDown(std::string eventName, DoublePush key, uint8_t pNum = 0)const;

	/// <summary>
	/// キーを押しているかを取得する
	/// </summary>
	/// <param name="code">確認する入力キー</param>
	/// <param name="pNum">コントローラー番号</param>
	/// <returns>true = 押している, false = 押していない</returns>
	const bool GetKey(KeyCode code, uint8_t pNum = 0)const;

	/// <summary>
	/// 指定キーを押しながらキーを押しているか取得する
	/// </summary>
	/// <param name="code">確認する入力キー</param>
	/// <param name="key">同時押し押しするキー</param>
	/// <param name="pNum">コントローラ−番号</param>
	/// <returns>true = 押している, false = 押していない</returns>
	const bool GetKey(KeyCode code, DoublePush key, uint8_t pNum = 0)const;

	/// <summary>
	/// 指定キーイベントに含まれるキーが押されているかを取得する
	/// </summary>
	/// <param name="eventName">イベント名</param>
	/// <param name="pNum">コントローラ−番号</param>
	/// <returns>true = 押している, false = 押していない</returns>
	const bool GetKey(std::string eventName, uint8_t pNum = 0)const;

	/// <summary>
	/// 指定キーを押しながらキーを押しているか取得する
	/// </summary>
	/// <param name="eventName">イベント名</param>
	/// <param name="key">同時押し押しするキー</param>
	/// <param name="pNum">コントローラ−番号</param>
	/// <returns>true = 押している, false = 押していない</returns>
	const bool GetKey(std::string eventName, DoublePush key, uint8_t pNum = 0)const;

	/// <summary>
	/// キーイベントに対応キーを追加する(存在しない場合は新規追加)
	/// </summary>
	/// <param name="eventName">イベント名</param>
	/// <param name="code">キーコード</param>
	void AddKeyEvent(std::string eventName, KeyCode code);

	/// <summary>
	/// キーイベントの中身を入れ替える
	/// </summary>
	/// <param name="eventName">入れ替え先イベント名</param>
	/// <param name="code">入れ替えるキー配列</param>
	void SwapKeyEvent(std::string eventName, std::vector<KeyCode> code);

	/// <summary>
	/// コントローラーの入力受付状態を変更する
	/// </summary>
	/// <param name="pNum">コントローラー番号</param>
	/// <param name="enable">切り替える状態(ture = 有効)</param>
	/// <param name="inputReset">切り替えた際に前回更新時の情報を削除するか</param>
	void SetControllEnable(uint8_t pNum, bool enable, bool inputReset = false);
private:
	Input();
	Input(const Input&) = delete;
	const bool CheckDoubleInput(const DoublePush key, const KeyState& input)const;
	const bool IsDoubleInputKey(const KeyState& input, KeyCode nowcode)const;
	void operator=(const Input&) = delete;
	std::vector<KeyState> inputState_;
	LPDIRECTINPUT8 directInput_{};
	std::vector<LPDIRECTINPUTDEVICE8> inputDevices_;
	std::unordered_map<std::string, std::vector<KeyCode>> eventTable_;
};


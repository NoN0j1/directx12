#pragma once
#include <Windows.h>
#include <memory>
#include <vector>
#include <wrl.h>
#include <d3d12.h>
#include <dxgi1_6.h>
#include <string>
#include <DirectXMath.h>
#include <unordered_map>
#include "Object/ActorFactory.h"
#include "Dx12Commons.h"
#include "../Shader/MultiPurpose.hlsli"
#include "../Shader/ComputeShader.hlsli"

class Dx12Commons;
class Dx12CommandList;
struct VertexBuffer;
class ShaderManager;
class Camera;
class DirectionalLight;
class Actor;
class Ground;
class GroundFactory;
class EffectManager;
class MaterialManager;
struct Transform;
struct ShaderParameter;
struct Shader;
struct Screen;
struct PostEffectParameter;
class ParticleEffect;
class ResourceManager;
class DebugEditor;

enum class PostEffectTable
{
	LoadTextures,
	ShadowTextures,
	RenderTextures,
	SSAO,
	WindowShrink,
	Parameter,
	Debug,
	MAX
};

enum class PrimitiveTable
{
	LoadTextures,
	ShadowTextures,
	WorldConstant,
	CameraView,
	ShadowCameraView,
	Light,
	Materials,
	MAX
};

enum class PrimitiveShadowTable
{
	WorldConstant,
	CameraView,
	Light,
	MAX
};

enum class ActorTable
{
	LoadTextures,
	ShadowTextures,
	WorldConstant,
	CameraView,
	ShadowCameraView,
	Light,
	Materials,
	Bone,
	MAX
};

enum class ActorShadowTable
{
	WorldConstant,
	CameraView,
	Light,
	Bone,
	MAX
};

enum class GraphTable
{
	LoadTextures,
	TextureConstant,
	MAX
};

enum class SSAOTable
{
	RenderTexture,
	ShadowTexture,
	Camera,
	MAX
};

struct ShakeParameter
{
	ShakeParameter() = default;
	/// <summary>
	/// 画面振動を起こす
	/// </summary>
	/// <param name="setTime">振動時間</param>
	/// <param name="vec">振動方向(-1 〜 1)</param>
	/// <param name="setStrength">振動の強さ</param>
	/// <param name="setCycle">振動の周期</param>
	/// <param name="setInterval">Cos波の乗数</param>
	/// <returns></returns>
	ShakeParameter(float setTime, DirectX::XMINT2 vec, float setStrength = 1.0f, float setCycle = 1.0f, UINT setInterval = 1.0f)
		:shakeTime(setTime),shakeTimeMAX(setTime),vector(vec),strength(setStrength),cycle(setCycle), interval(setInterval) {}
	float shakeTime = {};
	float shakeTimeMAX = {};
	DirectX::XMINT2 vector = {};
	float strength = {};
	float cycle = {};
	UINT interval = {};
};

class Dx12Wrapper
{
public:
	Dx12Wrapper(HWND hwnd);
	~Dx12Wrapper();
	/// <summary>
	/// wrapperの初期化を行う
	/// </summary>
	/// <returns>true = 成功, false = 何らかの処理で失敗</returns>
	bool Init();

	/// <summary>
	/// スクリーンの裏表を入れ替える
	/// </summary>
	void ScreenFlip();

	/// <summary>
	/// フリップ用スクリーンをクリアする
	/// </summary>
	void SetCurrentScreen(bool depth = true);

	/// <summary>
	/// 描画先スクリーンをセットする
	/// </summary>
	/// <param name="scrIdx">追加スクリーン番号</param>
	/// <param name="depthIdx">深度スクリーン番号、不要の場合は-1</param>
	/// <param name="inRow">描画先スクリーン数</param>
	void SetDrawScreen(std::vector<std::wstring> scrName, std::vector<std::wstring> depscrName);

	/// <summary>
	/// 指定スクリーンを縮小バッファに描画する
	/// </summary>
	/// <param name="shrinkScrName">描画先縮小バッファ</param>
	void DrawToShrinkBuffer(std::vector<std::wstring> shrinkScrName);

	/// <summary>
	/// 指定深度スクリーンにSSAOを描画する
	/// </summary>
	void DrawSSAO();

	/// <summary>
	/// 描画先のスクリーンを作成する
	/// </summary>
	void MakeScreen(const std::wstring& name);

	/// <summary>
	/// 深度の描画先を作成する
	/// </summary>
	void MakeDepthScreen(const std::wstring& name, const SIZE& size = SIZE());

	/// <summary>
	/// 深度の描画先を作成する
	/// </summary>
	void MakeShrinkScreen(const std::wstring& name, const SIZE& size = SIZE());

	/// <summary>
	/// 深度の描画先を特定のバッファに作成する
	/// </summary>
	/// <param name="name">登録名</param>
	/// <param name="screen">初期化を行うバッファ</param>
	/// <param name="size">バッファの縦横サイズ</param>
	void MakeLightShadowScreen(const std::wstring& name, std::shared_ptr<Screen>& screen, const SIZE& size = SIZE());

	/// <summary>
	/// 深度の描画先を作成する
	/// </summary>
	/// <param name="name">登録名</param>
	/// <param name="buff">初期化を行うバッファ</param>
	/// <param name="size">バッファの縦横サイズ</param>
	/// <returns>true = 成功, false = 失敗</returns>
	bool CreateDepthScreenProcess(const std::wstring& name, std::shared_ptr<Screen>& buff, const SIZE& size);

	/// <summary>
	/// ビューサイズを指定する
	/// </summary>
	/// <param name="size">ビューサイズ</param>
	void SetViewSize(D3D12_VIEWPORT& view, const SIZE& size, float depthMax = 1.0f);

	/// <summary>
	/// 画像を指定座標(左上画像の指定)で描画する
	/// </summary>
	/// <param name="x">描画座標X</param>
	/// <param name="y">描画座標Y</param>
	/// <param name="handle">画像ハンドル</param>
	void DrawGraph(float x, float y, int handle);

	/// <summary>
	/// 演算シェーダを実行する
	/// </summary>
	void DispathComputeShader(const std::wstring& shaderName);

	/// <summary>
	/// 画面を指定のパラメータで揺らす
	/// </summary>
	/// <param name="param"></param>
	void ShakeScreen(const ShakeParameter& param);

	/// <summary>
	/// エフェクトを発生させる
	/// </summary>
	const std::shared_ptr<ParticleEffect>& PlayEffect(const std::wstring& name, const DirectX::XMFLOAT3& pos, float scale = 1.0f);

	/// <summary>
	/// エフェクトの読み込みを行う
	/// </summary>
	bool LoadEffect(const char16_t* path, const std::wstring& name);

	/// <summary>
	/// 発生させているエフェクトの描画を行う
	/// </summary>
	void DrawEffect();

	/// <summary>
	/// 存在する追加バッファをバックバッファに書き込む。バックバッファのクリアも行う
	/// </summary>
	void DrawAddScreenToBackBuffer();

	/// <summary>
	/// 描画で使用するカメラを切り替える
	/// </summary>
	/// <param name="num">カメラ番号</param>
	void SetCamera(UINT num);

	/// <summary>
	/// アクターを生成する
	/// </summary>
	/// <typeparam name="T">Actorを継承した生成したいクラス</typeparam>
	/// <param name="actorTemplate">生成したいクラス生成</param>
	/// <param name="transform">生成座標</param>
	/// <param name="controllNum">コントローラ−番号</param>
	/// <returns>生成したアクター</returns>
	template<typename T>
	std::shared_ptr<Actor> SpawnActor(const T& actorTemplate, const Transform& transform, const int controllNum = -1)
	{
		auto& act = actorFactory_->Spawn(actorTemplate, transform);
		SpawnActorInit(*act, controllNum);
		return act;
	}

	/// <summary>
	/// カメラアクターを生成する
	/// </summary>
	/// <param name="transform">座標ほか</param>
	/// <param name="controllNum">コントローラー番号</param>
	/// <returns>生成したカメラアクター</returns>
	std::shared_ptr<Camera> SpawnCameraActor(const Transform& transform, const int controllNum = -1);

	/// <summary>
	/// 地面オブジェクトを生成する
	/// </summary>
	/// <param name="transform">座標ほか</param>
	/// <returns>生成したオブジェクト</returns>
	std::shared_ptr<Ground> SpawnGround(const Transform& transform);

	/// <summary>
	/// DirectionalLightを生成する
	/// </summary>
	/// <param name="direction"></param>
	void SpawnDirectionalLight(const DirectX::XMFLOAT3& direction);

	/// <summary>
	/// 存在するアクターの更新をする
	/// </summary>
	void UpdateActor();

	/// <summary>
	/// 存在するアクターの影を描画する
	/// </summary>
	void DrawCastShadow();

	/// <summary>
	/// 存在する描画可能アクターを全て描画する
	/// </summary>
	void DrawAllActor();

	/// <summary>
	/// アクターを指定して描画する
	/// </summary>
	/// <param name="act">描画したいアクター</param>
	void DrawActor(Actor& act);

	/// <summary>
	/// 存在する地形オブジェクトを全て描画する
	/// </summary>
	void DrawGround();

	/// <summary>
	/// デバイスを取得
	/// </summary>
	/// <returns>デバイス情報ポインタ</returns>
	ID3D12Device6* GetDevice();

	/// <summary>
	/// バッファ等の作成を行うクラスを取得
	/// </summary>
	/// <returns>shared管理しているCommonsクラス参照</returns>
	Dx12Commons& GetCommons();

	/// <summary>
	/// 処理の終了待ちを行う
	/// </summary>
	void WaitWithFence();

	/// <summary>
	/// リソースのバリアを張る
	/// </summary>
	/// <param name="p">バリアを張るバッファ</param>
	/// <param name="befor">変更前のステート</param>
	/// <param name="aftor">変更後のステート</param>
	/// <param name="cmdlist">実行コマンドリスト</param>
	void Barrier(ID3D12Resource* p, D3D12_RESOURCE_STATES befor, D3D12_RESOURCE_STATES aftor, ID3D12GraphicsCommandList5* cmdlist);

	/// <summary>
	/// コマンドの実行
	/// </summary>
	void ExcuteCommand();

	/// <summary>
	/// コマンドの実行(コマンドリスト指定)
	/// </summary>
	/// <param name="idx">コマンドリスト番号</param>
	void ExcuteCommand(char idx);

	/// <summary>
	/// テクスチャをGPUに転送する
	/// </summary>
	/// <param name="source">転送元</param>
	/// <param name="dest">転送先</param>
	void CopyTexture(const D3D12_TEXTURE_COPY_LOCATION& source, D3D12_TEXTURE_COPY_LOCATION& dest);
	
	/// <summary>
	/// マテリアルの値を変更可能にする
	/// </summary>
	/// <param name="matManager"></param>
	/// <returns></returns>
	bool CreateInstanceMaterial(MaterialManager& matManager);

private:
	void DrawEditor();

	/// <summary>
	/// アプリケーションウィンドウハンドルを保持
	/// </summary>
	HWND hwnd_;

	/// <summary>
	/// デバイスを保持する
	/// </summary>
	Microsoft::WRL::ComPtr<ID3D12Device6> device_;

	/// <summary>
	/// デバイスを作成する
	/// </summary>
	/// <returns>true = 成功, false = 失敗</returns>
	bool CreateDevice();

	/// <summary>
	/// バッファの作成等を行うクラス
	/// </summary>
	std::unique_ptr<Dx12Commons> commons_;

	/// <summary>
	/// dxgiFactoryを保持する
	/// </summary>
	Microsoft::WRL::ComPtr<IDXGIFactory5> dxgiFactory_;

	/// <summary>
	/// DXGIFactoryの生成
	/// </summary>
	/// <returns>true = 成功, false = 失敗</returns>
	bool CreateDxgiFactory();

	/// <summary>
	/// コマンドリストの配列
	/// </summary>
	std::vector<Dx12CommandList> commandLists_;

	/// <summary>
	/// コマンドを積むキュー
	/// </summary>
	Microsoft::WRL::ComPtr<ID3D12CommandQueue> cmdQueue_;
	/// <summary>
	/// コマンドキューの生成
	/// </summary>
	/// <returns>true = 成功, false = 失敗</returns>
	bool CreateCommandQueue();

	/// <summary>
	/// スワップチェインを保持する
	/// </summary>
	Microsoft::WRL::ComPtr<IDXGISwapChain4> swapChain_;

	/// <summary>
	/// SwapChainを作成する
	/// </summary>
	/// <returns>true = 成功, false = 失敗</returns>
	bool CreateSwapChain();

	/// <summary>
	/// バックバッファ用デスクリプタヒープ
	/// </summary>
	Microsoft::WRL::ComPtr<ID3D12DescriptorHeap> rtvDescHeap_;
	
	/// <summary>
	/// 基本フリップ用スクリーンバッファ(2枚)
	/// </summary>
	std::array<std::shared_ptr<Screen>, 2> drawScreen_;

	/// <summary>
	/// 追加したスクリーンバッファ
	/// </summary>
	std::unordered_map<std::wstring, std::shared_ptr<Screen>> addScreen_;

	/// <summary>
	/// 縮小バッファ
	/// </summary>
	std::unordered_map<std::wstring, std::shared_ptr<Screen>> shrinkScreen_;

	/// <summary>
	/// SSAOバッファ
	/// </summary>
	std::shared_ptr<Screen> ssaoScreen_;

	/// <summary>
	/// スクリーン用デスクリプタヒープ、16枚確保済み
	/// </summary>
	Microsoft::WRL::ComPtr<ID3D12DescriptorHeap> addScreenDescriptor_;

	/// <summary>
	/// 縮小バッファ用デスクリプタヒープ、16枚確保済み
	/// </summary>
	Microsoft::WRL::ComPtr<ID3D12DescriptorHeap> shrinkScreenDescriptor_;

	/// <summary>
	/// SSAOバッファ用デスクリプタヒープ
	/// </summary>
	Microsoft::WRL::ComPtr<ID3D12DescriptorHeap> ssaoRTVDescriptor_;
	Microsoft::WRL::ComPtr<ID3D12DescriptorHeap> ssaoSRVDescriptor_;

	void CreateSSAOBuffer();

	/// <summary>
	/// 深度バッファ用スクリーンバッファ
	/// </summary>
	std::unordered_map<std::wstring ,std::shared_ptr<Screen>> depthScreen_;

	/// <summary>
	/// 深度バッファ用デスクリプタヒープ
	/// </summary>
	Microsoft::WRL::ComPtr<ID3D12DescriptorHeap> dsvDescriptor_;

	/// <summary>
	/// 演算用バッファ
	/// </summary>
	Microsoft::WRL::ComPtr<ID3D12Resource> computeBuffer_;

	/// <summary>
	/// 演算後のコピー用バッファ
	/// </summary>
	Microsoft::WRL::ComPtr<ID3D12Resource> computeCopyBuffer_;

	/// <summary>
	/// 演算用デスクリプタ
	/// </summary>
	Microsoft::WRL::ComPtr<ID3D12DescriptorHeap> computeDescriptor_;

	std::vector<Buffer_t> computeData_;

	void ComputeShadeingBufferInit();

	/// <summary>
	/// 画面サイズをshader上で-1〜1に変換する定数バッファ
	/// </summary>
	Microsoft::WRL::ComPtr<ID3D12Resource> postEffectBuffer_;

	/// <summary>
	/// 画面サイズをshader上で-1〜1に戻すための定数バッファを保持するデスクリプタヒープ
	/// </summary>
	Microsoft::WRL::ComPtr<ID3D12DescriptorHeap> postEffectDescriptor_;

	/// <summary>
	/// ポストエフェクトで加工に使うパラメータ
	/// </summary>
	PostEffectParameter* mappedPostEffect_{};

	/// <summary>
	/// 画面揺れ用タイマー
	/// </summary>
	ShakeParameter shakeParam_;

	/// <summary>
	/// ポストプロセス用の画像を読み込む
	/// </summary>
	void PostProcessInit();

	/// <summary>
	/// Screen用バッファとビューを紐づける
	/// </summary>
	/// <returns>true = 成功, false = 失敗</returns>
	bool LinkScreenAndView();

	/// <summary>
	/// フェンス用
	/// </summary>
	Microsoft::WRL::ComPtr<ID3D12Fence1> fence_;
	
	/// <summary>
	/// フェンス待ちが完了した際に変更される変数
	/// </summary>
	UINT64 fenceVal_ = 0;
	
	/// <summary>
	/// フェンスを作成する
	/// </summary>
	/// <returns></returns>
	bool CreateFence();

	/// <summary>
	/// 画面サイズの頂点データバッファを作成する
	/// </summary>
	void CreateBaseVertexBuffer();
	
	/// <summary>
	/// デバッグ用Errorログレイヤーを有効にする
	/// </summary>
	void EnableDirectXDebugLayer();
	
	/// <summary>
	/// 最終描画用レンダーターゲットと深度バッファを作成する
	/// </summary>
	void CreateBaseRenderTarget();

	/// <summary>
	/// カメラバッファの初期化
	/// </summary>
	void CameraBufferInit(Camera& camera);

	/// <summary>
	/// マネージャー及びファクトリー系とカメラを生成する
	/// </summary>
	void ManagerAndFactoryInit();

	/// <summary>
	/// エディタから値を操作できるように関連付けする
	/// </summary>
	void DebugEditorInit();

	/// <summary>
	/// スポーンしたActorの初期設定
	/// </summary>
	/// <param name="act"></param>
	/// <param name="controllerNum"></param>
	void SpawnActorInit(Actor& act, const int controllerNum);

	/// <summary>
	///  ロード済みテクスチャバッファのデスクリプタヒープをセットする
	/// </summary>
	/// <param name="cmdList">実行コマンドリスト</param>
	/// <param name="enumVal">DescriptorTable上の位置</param>
	void SetTextureBuffer(ID3D12GraphicsCommandList5* cmdList, UINT enumVal);

	/// <summary>
	/// 追加作成したレンダーターゲット画像のデスクリプタヒープをセットする
	/// </summary>
	/// <param name="cmdList">実行コマンドリスト</param>
	/// <param name="enumVal">DescriptorTable上の位置</param>
	void SetRenderTextureBuffer(ID3D12GraphicsCommandList5* cmdList, UINT enumVal);

	/// <summary>
	/// 追加作成した深度バッファ画像のデスクリプタヒープをセットする
	/// </summary>
	/// <param name="cmdList">実行コマンドリスト</param>
	/// <param name="enumVal">DescriptorTable上の位置</param>
	void SetShadowTextureBuffer(ID3D12GraphicsCommandList5* cmdList, UINT enumVal);

	/// <summary>
	/// アクター系の描画プロセスを行う
	/// </summary>
	/// <param name="act">描画アクター</param>
	/// <param name="cmdList">実行コマンドリスト</param>
	void DrawingActorProcess(Actor& act, ID3D12GraphicsCommandList5* cmdList, ID3D12DescriptorHeap* camera);

	/// <summary>
	/// モデルの描画コマンドをセットする
	/// </summary>
	/// <param name="act">描画対象Actor</param>
	/// <param name="cmdList">コマンドリスト</param>
	void SetActorDrawCommands(Actor& act, ID3D12GraphicsCommandList5* cmdList, ID3D12DescriptorHeap* camera);
	
	/// <summary>
	/// モデルの影描画のコマンドをセットする
	/// </summary>
	/// <param name="act">描画対象Actor</param>
	/// <param name="cmdList">コマンドリスト</param>
	/// <param name="camera">ライトカメラ</param>
	void SetCastShadowActorDrawCommands(Actor& act, ID3D12GraphicsCommandList5* cmdList, ID3D12DescriptorHeap* camera);

	/// <summary>
	/// 地面代わりのオブジェクトの影描画コマンドをセットする
	/// </summary>
	/// <param name="ground">描画対象の地面</param>
	/// <param name="cmdList">コマンドリスト</param>
	/// <param name="camera">ライトカメラ</param>
	void SetCastShadowGroundDrawCommands(Ground& ground, ID3D12GraphicsCommandList5* cmdList, ID3D12DescriptorHeap* camera);

	/// <summary>
	/// 地面の描画コマンドをセットする
	/// </summary>
	/// <param name="act">描画対象Ground</param>
	/// <param name="cmdList">コマンドリスト</param>
	void SetGroundDrawCommands(Ground& ground, ID3D12GraphicsCommandList5* cmdList, ID3D12DescriptorHeap* camera);

	/// <summary>
	/// 画面サイズ頂点バッファを保持
	/// </summary>
	std::shared_ptr<VertexBuffer> wsizeVertexBuffer_;

	/// <summary>
	/// -1〜1サイズの頂点バッファを保持
	/// </summary>
	std::shared_ptr<VertexBuffer> baseVertexBuffer_;

	/// <summary>
	/// 画面サイズをshader上で-1〜1に変換する定数バッファ
	/// </summary>
	Microsoft::WRL::ComPtr<ID3D12Resource> wsizeShrinkConstantBuffer_;

	/// <summary>
	/// 画面サイズをshader上で-1〜1に戻すための定数バッファを保持するデスクリプタヒープ
	/// </summary>
	Microsoft::WRL::ComPtr<ID3D12DescriptorHeap> wsizeShrinkDescriptor_;

	/// <summary>
	/// 画面サイズをshader上で-1〜1に戻すための行列
	/// </summary>
	DirectX::XMMATRIX* mappedShrinkParam_{};

	//マネージャー系
	std::unique_ptr<ActorFactory> actorFactory_;
	std::unique_ptr<GroundFactory> groundFactory_;

	/// <summary>
	/// カメラActor、3Dの描画に必ず1つは必要
	/// </summary>
	std::vector<std::shared_ptr<Camera>> cameraActors_;
	std::shared_ptr<DirectionalLight> light_;

	/// <summary>
	/// スクリーンビューポート
	/// </summary>
	D3D12_VIEWPORT view_;

	/// <summary>
	/// 描画で使用するカメラ番号
	/// </summary>
	UINT usingCameraNum_;

	/// <summary>
	/// 使用中のカメラ
	/// </summary>
	std::shared_ptr<Camera> usingCamera_;

	/// <summary>
	/// Effekseerを実行するクラス
	/// </summary>
	std::unique_ptr<EffectManager> effectManager_;

	/// <summary>
	/// ImGuiを保持するクラス
	/// </summary>
	std::unique_ptr<DebugEditor> debugEditor_;
};
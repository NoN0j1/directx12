#pragma once
#include <Windows.h>
#include <DirectXMath.h>
#include "../DirectX12Geometory.h"

struct Material
{
	Material(const DirectX::XMFLOAT4& dif, const DirectX::XMFLOAT3& amb, const DirectX::XMFLOAT3& spc, const float spcP, const UINT texH = static_cast<UINT>(DummyTextureType::White))
		:diffuse_(dif), ambient_(amb.x, amb.y, amb.z, 1.0f), spcular_(spc.x, spc.y, spc.z, 1.0f), spcularPower_(spcP), texHandle(texH){}
	DirectX::XMFLOAT4 diffuse_;
	DirectX::XMFLOAT4 ambient_;
	DirectX::XMFLOAT4 spcular_;
	float spcularPower_;
	UINT texHandle; //使用するテクスチャハンドル
};
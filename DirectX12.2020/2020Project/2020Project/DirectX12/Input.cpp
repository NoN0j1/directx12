#include "Input.h"
#include <Windows.h>

#pragma comment(lib, "dinput8.lib")
#pragma comment(lib, "dxguid.lib")

using namespace std;

Input::Input()
{
	inputState_.push_back(KeyState());
	inputState_[0].enable = true;
}

const bool Input::CheckDoubleInput(const DoublePush key, const KeyState& input)const
{
	switch (key)
	{
	case DoublePush::Ctr:
		if (((input.keyState[static_cast<UINT>(KeyCode::LCtr)] & 0x80) |
			(input.keyState[static_cast<UINT>(KeyCode::RCtr)] & 0x80)) == 0)
		{
			return false;
		}
		break;
	case DoublePush::Shift:
		if (((input.keyState[static_cast<UINT>(KeyCode::LShift)] & 0x80) |
			(input.keyState[static_cast<UINT>(KeyCode::RShift)] & 0x80)) == 0)
		{
			return false;
		}
		break;
	case DoublePush::Alt:
		if (((input.keyState[static_cast<UINT>(KeyCode::LAlt)] & 0x80) |
			(input.keyState[static_cast<UINT>(KeyCode::RAlt)] & 0x80)) == 0)
		{
			return false;
		}
		break;
	default:
		break;
	}
	return true;
}

const bool Input::IsDoubleInputKey(const KeyState& input, KeyCode nowcode) const
{
	//false で抜けると取得しようとするキーの入力チェック
	//trueだと入力していない事にする
	bool ret = false;
	ret = ((input.keyState[static_cast<UINT>(KeyCode::LCtr)] & 0x80) |
		(input.keyState[static_cast<UINT>(KeyCode::RCtr)] & 0x80)) != 0;
	ret = (((input.keyState[static_cast<UINT>(KeyCode::LAlt)] & 0x80) ||
		(input.keyState[static_cast<UINT>(KeyCode::RAlt)] & 0x80)) != 0) | ret;
	ret = (((input.keyState[static_cast<UINT>(KeyCode::LShift)] & 0x80) ||
		(input.keyState[static_cast<UINT>(KeyCode::RShift)] & 0x80)) != 0) | ret;
	if (ret)
	{
		if (nowcode == KeyCode::LCtr || nowcode == KeyCode::RCtr ||
			nowcode == KeyCode::LShift || nowcode == KeyCode::RShift ||
			nowcode == KeyCode::LAlt || nowcode == KeyCode::RAlt)
		{
			ret = false;
		}
	}
	
	return ret;
}

Input::~Input()
{
	for (auto& d : inputDevices_)
	{
		d->Release();
	}
	directInput_->Release();
}

Input& Input::Instance()
{
	static Input instance_;
	return instance_;
}

bool Input::Init(HWND& winH, HINSTANCE hInstance)
{
	HRESULT result = DirectInput8Create(hInstance, DIRECTINPUT_VERSION, IID_IDirectInput8,(LPVOID*)&directInput_, NULL);
	if (FAILED(result))
	{
		OutputDebugString(L"DirectInput8の作成に失敗\n");
		return false;
	}
	inputDevices_.push_back(LPDIRECTINPUTDEVICE8());
	auto& dev = inputDevices_.back();
	result = directInput_->CreateDevice(GUID_SysKeyboard, &dev, NULL);
	if (FAILED(result))
	{
		OutputDebugString(L"デバイスの作成に失敗\n");
		return false;
	}
	result = dev->SetDataFormat(&c_dfDIKeyboard);
	if (FAILED(result))
	{
		OutputDebugString(L"入力データ形式のセット失敗\n");
		return false;
	}
	result = dev->SetCooperativeLevel(winH, DISCL_FOREGROUND | DISCL_NONEXCLUSIVE | DISCL_NONEXCLUSIVE | DISCL_NOWINKEY);
	if (FAILED(result))
	{
		OutputDebugString(L"排他制御のセット失敗\n");
		return false;
	}
	dev->Acquire();

	return true;
}

void Input::AddController()
{
	inputState_.push_back(KeyState());
}

void Input::UpdateKeyState()
{
	HRESULT result = S_OK;
	for (auto& dev : inputDevices_)
	{
		for (auto& input : inputState_)
		{
			if (!input.enable)
			{
				continue;
			}
			DWORD devSize = static_cast<DWORD>(input.keyState.size()) * sizeof(input.keyState[0]);
			copy_n(input.keyState.begin(), devSize, input.oldState.data());
			input.keyState = {};
			result = dev->GetDeviceState(devSize, &input.keyState);
			if (FAILED(result))
			{
				dev->Acquire();
				result = dev->GetDeviceState(devSize, &input.keyState);
			}
			else
			{
				result = S_OK;
			}
		}
	}
	POINT p = {};
	GetCursorPos(&p);
	//ScreenToClient()
}

void Input::UpdateKeyState(uint8_t pNum)
{
	if (pNum > static_cast<uint8_t>(inputState_.size()))
	{
		return;
	}

	auto& input = inputState_[pNum];
	copy_n(input.keyState.begin(), input.keyState.size() * sizeof(input.keyState[0]), input.oldState.data());
	GetKeyboardState(input.keyState.data());
}

const bool Input::GetKeyDown(KeyCode code, uint8_t pNum)const
{
	if (pNum > static_cast<uint8_t>(inputState_.size()))
	{
		return false;
	}
	return ( (inputState_[pNum].keyState[static_cast<UINT>(code)] & 0x80) && 
		!(inputState_[pNum].oldState[static_cast<int>(code)] & 0x80) );
}

const bool Input::GetKeyDown(KeyCode code, DoublePush key, uint8_t pNum) const
{
	if (pNum > static_cast<uint8_t>(inputState_.size()))
	{
		return false;
	}
	if (!CheckDoubleInput(key, inputState_[pNum]))
	{
		return false;
	}
	return ((inputState_[pNum].keyState[static_cast<UINT>(code)] & 0x80) &&
		!(inputState_[pNum].oldState[static_cast<int>(code)] & 0x80));
}

const bool Input::GetKeyDown(string eventName, uint8_t pNum)const
{
	if (pNum > static_cast<uint8_t>(inputState_.size()))
	{
		return false;
	}
	if (!eventTable_.contains(eventName))
	{
		OutputDebugStringA(eventName.c_str());
		OutputDebugString(L" は存在しないキーイベントです。\n");
		return false;
	}
	auto& tbl = eventTable_.at(eventName);
	bool ret = false;
	for (auto& c : tbl)
	{	
		if (IsDoubleInputKey(inputState_[pNum], c))
		{
			return false;
		}
		ret = inputState_[pNum].keyState[static_cast<UINT>(c)] & 0x80 &&
			!(inputState_[pNum].oldState[static_cast<UINT>(c)] & 0x80)
			|| ret;
	}

	return ret;
}

const bool Input::GetKeyDown(std::string eventName, DoublePush key, uint8_t pNum) const
{
	if (pNum > static_cast<uint8_t>(inputState_.size()))
	{
		return false;
	}
	if (!CheckDoubleInput(key, inputState_[pNum]))
	{
		return false;
	}
	if (!eventTable_.contains(eventName))
	{
		OutputDebugStringA(eventName.c_str());
		OutputDebugString(L" は存在しないキーイベントです。\n");
		return false;
	}
	auto& tbl = eventTable_.at(eventName);
	bool ret = false;
	for (auto& c : tbl)
	{
		ret = inputState_[pNum].keyState[static_cast<UINT>(c)] & 0x80 &&
			!(inputState_[pNum].oldState[static_cast<UINT>(c)] & 0x80)
			|| ret;
	}
	return ret;
}

const bool Input::GetKey(KeyCode code, uint8_t pNum)const
{
	if (pNum > static_cast<uint8_t>(inputState_.size()))
	{
		return false;
	}
	if (IsDoubleInputKey(inputState_[pNum], code))
	{
		return false;
	}

	return inputState_[pNum].keyState[static_cast<UINT>(code)] & 0x80;
}

const bool Input::GetKey(KeyCode code, DoublePush key, uint8_t pNum) const
{
	if (pNum > static_cast<uint8_t>(inputState_.size()))
	{
		return false;
	}
	if (!CheckDoubleInput(key, inputState_[pNum]))
	{
		return false;
	}
	return inputState_[pNum].keyState[static_cast<UINT>(code)] & 0x80;
}

const bool Input::GetKey(std::string eventName, uint8_t pNum) const
{
	if (pNum > static_cast<uint8_t>(inputState_.size()))
	{
		return false;
	}
	if (!eventTable_.contains(eventName))
	{
		OutputDebugStringA(eventName.c_str());
		OutputDebugString(L" は存在しないキーイベントです。\n");
		return false;
	}
	auto& tbl = eventTable_.at(eventName);
	bool ret = false;
	for (auto& c : tbl)
	{	
		if (IsDoubleInputKey(inputState_[pNum], c))
		{
			return false;
		}
		ret = inputState_[pNum].keyState[static_cast<UINT>(c)] & 0x80 || ret;
	}

	return ret;
}

const bool Input::GetKey(std::string eventName, DoublePush key, uint8_t pNum) const
{
	if (pNum > static_cast<uint8_t>(inputState_.size()))
	{
		return false;
	}
	if (!CheckDoubleInput(key, inputState_[pNum]))
	{
		return false;
	}
	if (!eventTable_.contains(eventName))
	{
		OutputDebugStringA(eventName.c_str());
		OutputDebugString(L" は存在しないキーイベントです。\n");
		return false;
	}
	auto& tbl = eventTable_.at(eventName);
	bool ret = false;
	for (auto& c : tbl)
	{
		ret = inputState_[pNum].keyState[static_cast<UINT>(c)] & 0x80 || ret;
	}
	return ret;
}

void Input::AddKeyEvent(std::string eventName, KeyCode code)
{
	if (!eventTable_.contains(eventName))
	{
		OutputDebugString(L"新規キーイベント:");
		OutputDebugStringA(eventName.c_str());
		OutputDebugStringA("\n");
	}
	auto it = find_if(eventTable_[eventName].begin(), eventTable_[eventName].end(), [code](const KeyCode& c) { return c == code; });
	if (it == eventTable_[eventName].end())
	{
		//重複していない場合だけ追加
		eventTable_[eventName].push_back(code);
		OutputDebugStringA(eventName.c_str());
		OutputDebugString(L" に対応キーを追加しました\n");
	}
}

void Input::SwapKeyEvent(std::string eventName, std::vector<KeyCode> code)
{
	if (!eventTable_.contains(eventName))
	{
		OutputDebugStringA(eventName.c_str());
		OutputDebugString(L"eventの内容を交換出来ませんでした。\n");
		return;
	}
	eventTable_[eventName].clear();
	eventTable_[eventName].resize(code.size());
	copy(code.begin(), code.end(), eventTable_[eventName].data());
}

void Input::SetControllEnable(uint8_t pNum, bool enable, bool inputReset)
{
	if (pNum > inputState_.size())
	{
		return;
	}
	inputState_[pNum].enable = enable;
	if (inputReset)
	{
		inputState_[pNum].oldState = {};
	}
}

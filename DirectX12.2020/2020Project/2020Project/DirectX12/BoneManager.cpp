#include "BoneManager.h"

using namespace std;

void BoneManager::Update()
{
	//モーションをセットし始めてから何ミリ秒経過したか
	//MMDは30fpsの速度で動く。1秒間(1000ミリ秒)あたりに30フレーム動くので1000/30 = 33.3333...ミリ秒
	//ということは 経過フレーム数 = 経過ミリ秒/33.3333...となる
	uint32_t frameToMILLISec = 1000 / 30; //1フレームに必要なミリ秒
	uint32_t frame = (GetTickCount() - lastTickCount_) / frameToMILLISec; //経過フレーム数 (GetTickCount() - lastTickCount) == 経過ミリ秒

	if (frame > duration_ && isLoop_)
	{
		lastTickCount_ = GetTickCount();
	}

	if (isAnimation_)
	{
		UpdateAnimation(frame);
	}
}

void BoneManager::UpdateAnimation(const uint32_t frame)
{
	if (boneMatrices_.empty())
	{
		return;
	}
	//動きを一旦初期化
	fill(boneMatrices_.begin(), boneMatrices_.end(), DirectX::XMMatrixIdentity());

	for (auto& boneData : animationData_)
	{
		auto& keyframes = boneData.second;
		auto& boneName = boneData.first;
		//無いデータの場合は飛ばす
		if (!boneMap_.contains(boneName))
		{
			continue;
		}

		//rbeginとrendで後ろから探すことで一番大きいframe、つまり今のフレーム数の進行に合わせて動く
		auto nowit = find_if(keyframes.rbegin(), keyframes.rend(), [frame](KeyFrame& f) { return f.frameNum <= frame; });

		if (nowit == keyframes.rend())
		{
			continue;
		}
		auto nextit = nowit.base();
		if (nextit == keyframes.end())
		{
			//次は無い
			BoneRotate(boneName, nowit);
			continue;
		}
		//次があるのでそれと補間をする
		float t = (static_cast<float>(frame) - nowit->frameNum) / static_cast<float>(nextit->frameNum - nowit->frameNum);	//補間係数
		t = GetYFromXOnBezier(t, nextit->cpnt[0], nextit->cpnt[1]);
		BoneLerpRotate(boneName, nowit, nextit, t);
	}
	RecursiveBoneTransform(boneMap_[L"センター"].boneIndex, DirectX::XMMatrixIdentity());
	copy(boneMatrices_.begin(), boneMatrices_.end(), mappedPtr_);
}

void BoneManager::SetAnimation(const MotionData& data, bool loop, uint32_t interval)
{
	if (animName_ == data.animationName)
	{
		//同じモーションの場合は更新しない
		return;
	}
	animationData_ = data.motionData;
	duration_ = data.duration + interval;
	lastTickCount_ = GetTickCount();
	animName_ = data.animationName;
	isLoop_ = loop;
	isAnimation_ = true;
}

void BoneManager::BoneRotate(const std::wstring& name, const std::vector<KeyFrame>::reverse_iterator& it)
{
	//補間無し(MMDのアニメーションキーで◆があるところ)
	DirectX::XMVECTOR q = DirectX::XMLoadFloat4(&it->quaternion);
	DirectX::XMVECTOR offset = DirectX::XMLoadFloat3(&it->offset);

	auto rot = DirectX::XMMatrixIdentity();
	auto& pos = boneMap_[name].headPos;
	rot *= DirectX::XMMatrixTranslation(-pos.x, -pos.y, -pos.z);
	rot *= DirectX::XMMatrixRotationQuaternion(q);
	rot *= DirectX::XMMatrixTranslation(pos.x, pos.y, pos.z);
	rot *= DirectX::XMMatrixTranslationFromVector(offset);

	boneMatrices_[boneMap_[name].boneIndex] = rot;
}

void BoneManager::BoneLerpRotate(const std::wstring& name, const std::vector<KeyFrame>::reverse_iterator& itA, const std::vector<KeyFrame>::iterator& itB, float c)
{
	//補間あり
	DirectX::XMVECTOR qA = DirectX::XMLoadFloat4(&itA->quaternion);
	DirectX::XMVECTOR qB = DirectX::XMLoadFloat4(&itB->quaternion);
	DirectX::XMVECTOR offsetA = DirectX::XMLoadFloat3(&itA->offset);
	DirectX::XMVECTOR offsetB = DirectX::XMLoadFloat3(&itB->offset);

	//線形だと長さが一定では無くなるので球面線形補間をする
	qA = DirectX::XMQuaternionSlerp(qA, qB, c);

	//qA = ((1 - c) * qA) + (c*qB); //線形補間version
	auto rot = DirectX::XMMatrixIdentity();

	auto& pos = boneMap_[name].headPos;
	rot *= DirectX::XMMatrixTranslation(-pos.x, -pos.y, -pos.z);
	rot *= DirectX::XMMatrixRotationQuaternion(qA);
	rot *= DirectX::XMMatrixTranslation(pos.x, pos.y, pos.z);
	rot *= DirectX::XMMatrixTranslationFromVector(DirectX::XMVectorLerp(offsetA, offsetB,c));

	boneMatrices_[boneMap_[name].boneIndex] = rot;
}

std::vector<DirectX::XMMATRIX>& BoneManager::GetBoneMatrices()
{
    return boneMatrices_;
}

std::map<std::wstring, BoneNode>& BoneManager::GetBoneMap()
{
    return boneMap_;
}

std::vector<std::vector<int>>& BoneManager::GetBoneTree()
{
	return boneTree_;
}

ID3D12Resource*& BoneManager::GetBuffer()
{
    return *boneBuffer_.GetAddressOf();
}

ID3D12DescriptorHeap*& BoneManager::GetDescriptorHeap()
{
    return *descriptor_.GetAddressOf();
}

DirectX::XMMATRIX*& BoneManager::GetMappedPointer()
{
    return mappedPtr_;
}

void BoneManager::RecursiveBoneTransform(int idx, const DirectX::XMMATRIX& mat)
{
	boneMatrices_[idx] *= mat;
	for (auto child : boneTree_[idx])
	{
		RecursiveBoneTransform(child, boneMatrices_[idx]);
	}
}

float BoneManager::GetYFromXOnBezier(float x, const DirectX::XMFLOAT2& cp1, const DirectX::XMFLOAT2& cp2, uint32_t trycnt)
{
	if (cp1.x == cp1.y && cp2.x == cp2.y)
	{
		//この場合は曲線ではなく直線なので計算しない
		return x;
	}
	//x座標から近似を用いてtを求め、得られたtを元にベジエの式によりyを返す
	//(x+y)^3 =  x^3 + 3x^2y + 3xy^2 + y^3
	//↑に係数を付けて三次ベジエの式にすると(x = (1 - t)、y = tとする。)
	//P0* (1-t)^3 + P1* 3*(1-t)^2*t + P2* 3*(1-t)*t^2 + P3*t^3
	//P0 = (0,0) P3 = (1,1)
	constexpr float epsilon = 0.00005f; //許容誤差
	float t = x; //最初に適当に取る点は y = xの直線式でいいよね。(結局これが一番近いし)
	float r = 1.0f - t;
	for (uint32_t i = 0; i < trycnt; i++)
	{
		float fx = (cp1.x * 3 * (r * r) * t) + (cp2.x * 3 * r * (t * t)) + (t * t * t) - x;
		if (abs(fx) <= epsilon)
		{
			//誤差の範囲なら終了
			break;
		}
		t -= fx / 2.0f; //半分刻む
		r = 1.0f - t;
	}
	float y = (cp1.y * 3 * (r * r) * t) + (cp2.y * 3 * r * (t * t)) + (t * t * t);
	return y;
}
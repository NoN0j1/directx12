#pragma once
#include "Actor.h"

class CharacterMesh;

class Player : public Actor
{
public:
	Player();
	/// <summary>
	/// 毎フレームの更新
	/// </summary>
	void Update()override final;
protected:
	Player(const Transform& tr);
private:
	/// <summary>
	/// 座標の移動
	/// </summary>
	/// <param name="input">入力情報</param>
	void Move(const Input& input);

	/// <summary>
	/// 入力情報によって回転させる
	/// </summary>
	/// <param name="input">入力情報</param>
	void Rotation(const Input& input);

	/// <summary>
	/// アニメーションを変更する
	/// </summary>
	/// <param name="input">入力情報</param>
	void SetAnimation(const Input& input);
};


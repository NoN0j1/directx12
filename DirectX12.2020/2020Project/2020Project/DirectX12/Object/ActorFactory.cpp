#include "ActorFactory.h"
#include "Actor.h"
#include "../CameraActor.h"
#include "../Dx12Wrapper.h"
#include "../Dx12Commons.h"
#include "../DirectX12ToolFunction.h"

using namespace std;

ActorFactory::ActorFactory(Dx12Wrapper& dx12):wrapper_(dx12)
{
}

ActorFactory::~ActorFactory()
{
	Release();
}

const std::vector<std::shared_ptr<Actor>>& ActorFactory::GetManageActors()
{
	sort(manageActors_.begin(), manageActors_.end(), 
		[](const std::shared_ptr<Actor>& lact, const std::shared_ptr<Actor>& ract)
		{
			return (lact->GetTransform().position.z > ract->GetTransform().position.z);
		});
	return manageActors_;
}

const std::vector<std::shared_ptr<Camera>>& ActorFactory::GetManageCameras()
{
	return manageCameras_;
}

std::shared_ptr<Camera>& ActorFactory::SpawnCamera(const Transform& transform)
{
	struct temp : CameraActor
	{
		temp(const Transform& tr) :CameraActor(tr) {}
	};
	auto p = std::make_shared<temp>(transform);
	manageCameras_.push_back(std::move(p));
	return manageCameras_.back();
}

void ActorFactory::Release()
{
	wrapper_.WaitWithFence();
	manageActors_.clear();
}

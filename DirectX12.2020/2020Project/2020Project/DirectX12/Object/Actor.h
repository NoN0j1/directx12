#pragma once
#include "Object.h"

class Mesh;
class Input;
class CharacterMesh;

/// <summary>
/// プレイヤー等のように操作が可能で描画機能を持つオブジェクトの基底クラス
/// </summary>
class Actor : Object
{
public:
	~Actor() = default;
	Actor();
	/// <summary>
	/// 毎フレームの更新
	/// </summary>
	virtual void Update()override;

	/// <summary>
	/// コントローラーをセットする
	/// </summary>
	/// <param name="num">コントローラ−番号</param>
	void SetController(int num);

	/// <summary>
	/// 使用中のコントローラー番号を取得する
	/// </summary>
	/// <returns>コントローラ−番号</returns>
	const int GetControllerNum()const;

	/// <summary>
	/// キャラクターメッシュを保持している場合に再生するモーションをセットする
	/// </summary>
	/// <param name="motion">モーションデータ</param>
	/// <param name="loop">ループ再生するかどうか</param>
	/// <param name="interval">ループ再生時の間隔</param>
	void SetSkinAnimation(const MotionData& motion, bool loop = true, uint32_t interval = 0);

	/// <summary>
	/// 描画で使用するメッシュデータとシェーダをセットする
	/// </summary>
	/// <param name="set">描画メッシュ</param>
	/// <param name="shader">描画時のシェーダ</param>
	/// <param name="shadowShader">シャドウマップ用シェーダ</param>
	void SetMesh(const std::shared_ptr<Mesh>& set, const std::shared_ptr<Shader>& shader, const std::shared_ptr<Shader>& shadowShader = nullptr);
	
	/// <summary>
	/// 描画で使用するメッシュデータをセットする
	/// </summary>
	/// <param name="set"></param>
	void SetMesh(const std::shared_ptr<Mesh>& set);

	/// <summary>
	/// 使用中のメッシュが保持しているデータを取得する
	/// </summary>
	/// <returns></returns>
	const std::shared_ptr<Mesh> GetMesh();

	/// <summary>
	/// 使用中のメッシュが保持しているデータを取得する
	/// </summary>
	/// <returns></returns>
	const std::shared_ptr<CharacterMesh> GetCharacterMesh();

	/// <summary>
	/// 定数パラメータの初期化
	/// </summary>
	void InitConstantParameter();

	/// <summary>
	/// 定数バッファを取得する
	/// </summary>
	/// <returns>定数バッファ</returns>
	ID3D12Resource*& GetConstantBuffer();

	/// <summary>
	/// 定数Matrixパラメータを取得する
	/// </summary>
	/// <returns>MATRIXパラメータ</returns>
	DirectX::XMMATRIX*& GetConstantParameter();

	/// <summary>
	/// バッファを配置するデスクリプタヒープを取得する
	/// </summary>
	/// <returns>デスクリプタヒープ</returns>
	ID3D12DescriptorHeap*& GetDescriptorHeap();

	/// <summary>
	/// 座標などの情報を取得する
	/// </summary>
	/// <returns>Transform</returns>
	const Transform& GetTransform()const override final;
protected:
	Actor(const Transform & transform);
	Actor(const Transform & transform, const int controller);

	/// <summary>
	/// Actorを仮移動させる
	/// </summary>
	/// <param name="addPos">移動量</param>
	void MoveActor(DirectX::XMFLOAT3& addPos);

	/// <summary>
	/// Actorを仮回転させる
	/// </summary>
	/// <param name="addRotate">回転量(度)</param>
	void RotationActor(DirectX::XMFLOAT3& addRotate);

	/// <summary>
	/// 最終的な移動状態を適用する
	/// </summary>
	void UpdateTransform();

	/// <summary>
	/// 保持しているメッシュ
	/// </summary>
	std::shared_ptr<Mesh> mesh_;

	/// <summary>
	/// キャラクターメッシュを保持している場合は中身がある。無い場合はnullptr
	/// </summary>
	std::shared_ptr<CharacterMesh> charaMesh_;

	/// <summary>
	/// 使用しているコントローラ−番号
	/// </summary>
	int controllerNum = -1;
private:
	DirectX::XMFLOAT3 addPos_{};
	DirectX::XMFLOAT3 addRotate_{};
};


#include "Actor.h"
#include "../Mesh/Mesh.h"
#include "../Input.h"
#include "../Mesh/CharacterMesh.h"

using namespace std;

void Actor::Update()
{
}

void Actor::SetController(int num)
{
    controllerNum = num;
}

const int Actor::GetControllerNum() const
{
    return controllerNum;
}

void Actor::SetSkinAnimation(const MotionData& motion, bool loop, uint32_t interval)
{
    if (charaMesh_ == nullptr)
    {
        return;
    }
    charaMesh_->SetAnimation(motion, loop, interval);
}

void Actor::SetMesh(const std::shared_ptr<Mesh>& set, const std::shared_ptr<Shader>& shader, const std::shared_ptr<Shader>& shadowShader)
{
    mesh_ = set;
    if (shader == nullptr)
    {
        return;
    }
    mesh_->SetShader(shader, shadowShader);

    auto temp = dynamic_cast<CharacterMesh*>(&*mesh_);
    if (temp != nullptr)
    {
        charaMesh_ = make_shared<CharacterMesh>(*temp);
    }
}

void Actor::SetMesh(const std::shared_ptr<Mesh>& set)
{
    mesh_ = set;
}

const std::shared_ptr<Mesh> Actor::GetMesh()
{
    return mesh_;
}

const Transform& Actor::GetTransform() const
{
    return transform_;
}

Actor::Actor() :Object()
{
}

Actor::Actor(const Transform& transform) :Object(transform)
{
}

Actor::Actor(const Transform& transform, const int controller) : Object(transform),controllerNum(controller)
{
}

void Actor::MoveActor(DirectX::XMFLOAT3& addPos)
{
    addPos_.x += addPos.x;
    addPos_.y += addPos.y;
    addPos_.z += addPos.z;
}

void Actor::RotationActor(DirectX::XMFLOAT3& addRotate)
{
    addRotate_.x += addRotate.x/ DirectX::XM_2PI;
    addRotate_.y += addRotate.y/ DirectX::XM_2PI;
    addRotate_.z += addRotate.z/ DirectX::XM_2PI;
}

void Actor::UpdateTransform()
{
    auto movedPos = DirectX::XMVectorAdd(DirectX::XMLoadFloat3(&transform_.position), DirectX::XMLoadFloat3(&addPos_));
    DirectX::XMStoreFloat3(&transform_.position, movedPos);
    addPos_ = {};
    auto rotatedPos = DirectX::XMVectorAdd(DirectX::XMLoadFloat3(&transform_.rotation), DirectX::XMLoadFloat3(&addRotate_));
    DirectX::XMStoreFloat3(&transform_.rotation, rotatedPos);
    addRotate_ = {};

    *param_CB = DirectX::XMMatrixIdentity();
    *param_CB *= DirectX::XMMatrixRotationRollPitchYaw(transform_.rotation.x, transform_.rotation.y, transform_.rotation.z);
    *param_CB *= DirectX::XMMatrixTranslation(transform_.position.x, transform_.position.y, transform_.position.z);
    *param_CB *= DirectX::XMMatrixScaling(transform_.scale.x, transform_.scale.y, transform_.scale.z);
}

const std::shared_ptr<CharacterMesh> Actor::GetCharacterMesh()
{
    return charaMesh_;
}

void Actor::InitConstantParameter()
{
    *param_CB = DirectX::XMMatrixIdentity();
    *param_CB = DirectX::XMMatrixRotationX(0.0f);
    *param_CB *= DirectX::XMMatrixRotationY(0.0f);
    *param_CB *= DirectX::XMMatrixRotationZ(0.0f);
    *param_CB *= DirectX::XMMatrixTranslation(transform_.position.x, transform_.position.y, transform_.position.z);
}

ID3D12Resource*& Actor::GetConstantBuffer()
{
    return *constantBuffer_.GetAddressOf();
}

DirectX::XMMATRIX*& Actor::GetConstantParameter()
{
    return param_CB;
}

ID3D12DescriptorHeap*& Actor::GetDescriptorHeap()
{
    return *descriptorHeap_.GetAddressOf();
}
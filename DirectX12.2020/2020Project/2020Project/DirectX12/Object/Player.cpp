#include "Player.h"
#include "../Input.h"
#include "../Mesh/CharacterMesh.h"
#include "../ResourceManager.h"

void Player::Update()
{
	auto& input = Input::Instance();
	Move(input);
	Rotation(input);
	SetAnimation(input);
	UpdateTransform();
	if (charaMesh_ != nullptr)
	{
		charaMesh_->Update();
	}
}

Player::Player() :Actor()
{
}

Player::Player(const Transform& tr):Actor(tr)
{
	auto& input = Input::Instance();
	input.AddKeyEvent("left", KeyCode::Left);
	input.AddKeyEvent("right", KeyCode::Right);
	input.AddKeyEvent("up", KeyCode::Up);
	input.AddKeyEvent("down", KeyCode::Down);
	input.AddKeyEvent("rotR", KeyCode::Q);
	input.AddKeyEvent("rotL", KeyCode::E);
	input.AddKeyEvent("select1", KeyCode::KEY_1);
	input.AddKeyEvent("select2", KeyCode::KEY_2);
	input.AddKeyEvent("select3", KeyCode::KEY_3);
	input.AddKeyEvent("select4", KeyCode::KEY_4);
	input.AddKeyEvent("select5", KeyCode::KEY_5);
	input.AddKeyEvent("select6", KeyCode::KEY_6);
	input.AddKeyEvent("select7", KeyCode::KEY_7);
}

void Player::Move(const Input& input)
{
	auto vel = DirectX::XMFLOAT3(0, 0, 0);
	if (input.GetKey("left", controllerNum))
	{
		vel.x -= 0.1f;
	}
	if (input.GetKey("right", controllerNum))
	{
		vel.x += 0.1f;
	}
	if (input.GetKey("up", DoublePush::Shift, controllerNum))
	{
		vel.y += 0.1f;
	}
	if (input.GetKey("down", DoublePush::Shift, controllerNum))
	{
		vel.y -= 0.1f;
	}
	if (input.GetKey("up", controllerNum))
	{
		vel.z += 0.1f;
	}
	if (input.GetKey("down", controllerNum))
	{
		vel.z -= 0.1f;
	}
	
	MoveActor(vel);
}

void Player::Rotation(const Input& input)
{
	auto rot = DirectX::XMFLOAT3(0, 0, 0);
	if (input.GetKey("rotR", controllerNum))
	{
		rot.y -= 0.3f;
	}
	if (input.GetKey("rotL", controllerNum))
	{
		rot.y += 0.3f;
	}
	RotationActor(rot);
}

void Player::SetAnimation(const Input& input)
{
	auto& res = ResourceManager::Instance();
	if (input.GetKey("select1", controllerNum))
	{
		SetSkinAnimation(res.GetAnimation(L"ポーズ1"));
	}
	if (input.GetKey("select2", controllerNum))
	{
		SetSkinAnimation(res.GetAnimation(L"ポーズ2"));
	}
	if (input.GetKey("select3", controllerNum))
	{
		SetSkinAnimation(res.GetAnimation(L"ポーズ3"));
	}
	if (input.GetKey("select4", controllerNum))
	{
		SetSkinAnimation(res.GetAnimation(L"ポーズ4"));
	}
	if (input.GetKey("select5", controllerNum))
	{
		SetSkinAnimation(res.GetAnimation(L"ポーズ5"));
	}
	if (input.GetKey("select6", controllerNum))
	{
		SetSkinAnimation(res.GetAnimation(L"ポーズ6"));
	}
	if (input.GetKey("select7", controllerNum))
	{
		SetSkinAnimation(res.GetAnimation(L""));
	}
}

#include "Object.h"
#include "../DirectX12Geometory.h"

Object::~Object()
{
}

Object::Object() :transform_(Transform()), param_CB(new DirectX::XMMATRIX())
{
}

Object::Object(const Transform& transform):transform_(transform), param_CB(new DirectX::XMMATRIX())
{
}
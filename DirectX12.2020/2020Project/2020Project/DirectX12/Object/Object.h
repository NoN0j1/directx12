#pragma once
#include "../DirectX12Geometory.h"

/// <summary>
/// <para>全てのオブジェクトの基底クラス</para>
/// <para>座標と座標を送信用の定数バッファだけを保持している</para>
/// </summary>
class Object
{
public:
	virtual ~Object();
	virtual void Update() = 0;
	/// <summary>
	/// 座標などの情報を取得する
	/// </summary>
	/// <returns>Transform</returns>
	virtual const Transform& GetTransform()const = 0;
protected:
	Object();
	Object(const Transform& transform);
	/// <summary>
	/// 座標、回転、スケール情報
	/// </summary>
	Transform transform_;

	/// <summary>
	/// マップ用定数Matrix
	/// </summary>
	DirectX::XMMATRIX* param_CB;

	/// <summary>
	/// 主に定数バッファに使用するデスクリプタヒープ
	/// </summary>
	Microsoft::WRL::ComPtr<ID3D12DescriptorHeap> descriptorHeap_;

	/// <summary>
	/// 定数バッファ
	/// </summary>
	Microsoft::WRL::ComPtr<ID3D12Resource> constantBuffer_;
};


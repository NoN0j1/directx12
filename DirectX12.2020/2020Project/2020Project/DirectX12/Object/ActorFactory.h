#pragma once
#include <memory>
#include <vector>
#include <wrl.h>
#include <array>
#include <d3d12.h>

class Dx12Wrapper;
class Actor;
class Camera;
struct Transform;

class ActorFactory
{
public:
	ActorFactory(Dx12Wrapper& dx12);
	~ActorFactory();
	const std::vector<std::shared_ptr<Actor>>& GetManageActors();
	const std::vector<std::shared_ptr<Camera>>& GetManageCameras();
	template<typename T>
	std::shared_ptr<Actor>& Spawn(const T& type, const Transform& transform)
	{
		struct temp : T
		{
			temp(const Transform& tr) :T(tr) {}
		};
		auto p = std::make_shared<temp>(transform);
		manageActors_.push_back(std::move(p));
		return manageActors_.back();
	}

	std::shared_ptr<Camera>& SpawnCamera(const Transform& transform);

	void Release();
private:
	Dx12Wrapper& wrapper_;
	std::vector<std::shared_ptr<Actor>> manageActors_{};
	std::vector<std::shared_ptr<Camera>> manageCameras_{};
	D3D12_GRAPHICS_PIPELINE_STATE_DESC pDesc_{};
};


#pragma once
#include <memory>
#include <vector>

struct Transform;
class Ground;

class GroundFactory
{
public:
	GroundFactory();
	~GroundFactory() = default;
	std::shared_ptr<Ground>& spawnGround(const Transform& tr);
	const std::vector<std::shared_ptr<Ground>>& GetManageGrounds();
private:
	std::vector<std::shared_ptr<Ground>> manageGrounds_;
};


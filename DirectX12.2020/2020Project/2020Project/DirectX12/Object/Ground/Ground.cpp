#include "Ground.h"
#include "../../Mesh/PrimitiveMesh.h"
#include "../../Mesh/Mesh.h"

void Ground::Update()
{
	*param_CB = transform_.GetMatrix();
}

const Transform& Ground::GetTransform() const
{
	return transform_;
}

void Ground::InitConstantParameter()
{
	*param_CB = transform_.GetMatrix();
}

ID3D12Resource*& Ground::GetConstantBuffer()
{
	return *constantBuffer_.GetAddressOf();
}

DirectX::XMMATRIX*& Ground::GetConstantParameter()
{
	return param_CB;
}

ID3D12DescriptorHeap*& Ground::GetDescriptorHeap()
{
	return *descriptorHeap_.GetAddressOf();
}

void Ground::SetMesh(const std::shared_ptr<Mesh> set, const std::shared_ptr<Shader>& shader, const std::shared_ptr<Shader>& shadowShader)
{
	/*auto m = dynamic_cast<PrimitiveMesh*>(set.get());
	if (m == nullptr)
	{
		return;
	}
	mesh_ = std::make_shared<PrimitiveMesh>(*m);
	if (shader == nullptr)
	{
		return;
	}*/
	mesh_ = set;
	centerOffset_ = mesh_->GetOffsetCenterPosition();

	mesh_->SetShader(shader, shadowShader);
}

const std::shared_ptr<Mesh> Ground::GetMesh()
{
	return mesh_;
}

Ground::Ground() = default;

Ground::Ground(const Transform& tr):Object(tr)
{
}

#include "GroundFactory.h"
#include "Ground.h"
#include "../../DirectX12Geometory.h"
using namespace std;

GroundFactory::GroundFactory()
{
}

std::shared_ptr<Ground>& GroundFactory::spawnGround(const Transform& tr)
{
	struct TEMP : Ground
	{
	public:
		TEMP(const Transform& tr) :Ground(tr) {}
	};
	auto temp = make_shared<TEMP>(tr);
	manageGrounds_.push_back(move(temp));
	return manageGrounds_.back();
}

const std::vector<std::shared_ptr<Ground>>& GroundFactory::GetManageGrounds()
{
	return manageGrounds_;
}

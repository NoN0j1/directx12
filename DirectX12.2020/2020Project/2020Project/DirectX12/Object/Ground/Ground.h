#pragma once
#include "../Object.h"

class PrimitiveMesh;
class Mesh;

class Ground :public Object
{
public:
	~Ground() = default;
	void Update();
	/// <summary>
	/// 座標などの情報を取得する
	/// </summary>
	/// <returns>Transform</returns>
	const Transform& GetTransform()const override final;

	/// <summary>
	/// 定数パラメータの初期化
	/// </summary>
	void InitConstantParameter();

	/// <summary>
	/// 定数バッファを取得する
	/// </summary>
	/// <returns>定数バッファ</returns>
	ID3D12Resource*& GetConstantBuffer();

	/// <summary>
	/// 定数Matrixパラメータを取得する
	/// </summary>
	/// <returns>MATRIXパラメータ</returns>
	DirectX::XMMATRIX*& GetConstantParameter();

	/// <summary>
	/// バッファを配置するデスクリプタヒープを取得する
	/// </summary>
	/// <returns>デスクリプタヒープ</returns>
	ID3D12DescriptorHeap*& GetDescriptorHeap();

	/// <summary>
	/// 描画で使用するメッシュデータとシェーダをセットする
	/// </summary>
	/// <param name="set">描画メッシュ</param>
	/// <param name="shader">描画時のシェーダ</param>
	void SetMesh(const std::shared_ptr<Mesh> set, const std::shared_ptr<Shader>& shader, const std::shared_ptr<Shader>& shadowShader);

	/// <summary>
	/// 使用中のメッシュが保持しているデータを取得する
	/// </summary>
	/// <returns></returns>
	const std::shared_ptr<Mesh> GetMesh();
protected:
	Ground();
	Ground(const Transform& tr);
private:
	/// <summary>
	/// 保持しているメッシュ
	/// </summary>
	std::shared_ptr<Mesh> mesh_;
	DirectX::XMFLOAT3 centerOffset_{};
};


#pragma once
#include <wrl.h>
#include <vector>
#include <DirectXMath.h>
#include "DirectXTex.h"
#include <d3dx12.h>
#include <array>
#include <string>

enum class ShaderType
{
	VS, PS, HS, GS, DS,
	MAX
};

enum class DummyTextureType
{
	White, Black, Gradation,
	MAX
};


struct Transform
{
	Transform():position(0,0,0), scale(1,1,1), rotation(0,0,0){}
	/// <summary>
	/// 座標等の変形情報
	/// </summary>
	/// <param name="p">座標</param>
	/// <param name="r">回転</param>
	/// <param name="s">スケーリング</param>
	Transform(DirectX::XMFLOAT3 p,
		DirectX::XMFLOAT3 r = DirectX::XMFLOAT3({ 0, 0, 0 }),
		DirectX::XMFLOAT3 s = DirectX::XMFLOAT3({ 1, 1, 1 }))
		: position(p), scale(s), rotation(r) {}
	DirectX::XMFLOAT3 position;
	DirectX::XMFLOAT3 scale;
	DirectX::XMFLOAT3 rotation;
	DirectX::XMMATRIX GetMatrix();
};

struct Vertex
{
	Vertex():vertex(DirectX::XMFLOAT4()), uv(DirectX::XMFLOAT2()) {}
	Vertex(DirectX::XMFLOAT3 v, DirectX::XMFLOAT2 u) :vertex(DirectX::XMFLOAT4(v.x, v.y, v.z, 1.0f)), uv(u) {}
	DirectX::XMFLOAT4 vertex;
	DirectX::XMFLOAT2 uv;
	const UINT Size()const;
};

struct Vertex3D : Vertex
{
	Vertex3D() :Vertex(),normal(DirectX::XMFLOAT3()) {}
	Vertex3D(DirectX::XMFLOAT3 v, DirectX::XMFLOAT2 u, DirectX::XMFLOAT3 n) :Vertex(v,u), normal(DirectX::XMFLOAT3(n.x, n.y, n.z)) {}
	Vertex3D(float* v, float* u, float* n)
		:Vertex({ v[0],v[1],v[2] }, { u[0],u[1] }), normal(DirectX::XMFLOAT3(n[0], n[1], n[2])){}
	DirectX::XMFLOAT3 normal;
	const UINT Size()const;
};

struct VertexPMD : Vertex3D
{
	VertexPMD() :Vertex3D(), boneNo({}), weight(0){}
	VertexPMD(DirectX::XMFLOAT3 v, DirectX::XMFLOAT2 u, DirectX::XMFLOAT3 n, uint16_t no[2], float w):Vertex3D(v,u,n), boneNo({ no[0], no[1] }),weight(w){}
	std::array<uint16_t, 2> boneNo;
	float weight;
	const UINT Size()const;
};

struct VertexPMX : Vertex3D
{
	VertexPMX() :Vertex3D(){}
	VertexPMX(DirectX::XMFLOAT3 v, DirectX::XMFLOAT2 u, DirectX::XMFLOAT3 n, uint8_t t) :Vertex3D(v, u, n), type(static_cast<uint32_t>(t)){}
	std::array<uint32_t, 4> boneNo = {};
	std::array<float, 4> boneWeight = {};
	DirectX::XMFLOAT3 SDEF_C{};
	DirectX::XMFLOAT3 SDEF_R0{};
	DirectX::XMFLOAT3 SDEF_R1{};
	UINT type;
	const UINT Size()const;
};

enum class VertexBufferType
{
	Normal,
	Vertex3D,
	PMD,
	PMX,
	MAX
};

struct VertexBufferBase
{
	VertexBufferBase(VertexBufferType t):type(t){}
	const VertexBufferType type;
	virtual VertexBufferType GetVertexBuffertType()
	{
		return type;
	}
	Microsoft::WRL::ComPtr<ID3D12Resource1> resource{};
	Microsoft::WRL::ComPtr<ID3D12Resource1> index{};
	D3D12_VERTEX_BUFFER_VIEW view{};
	D3D12_INDEX_BUFFER_VIEW indexView{};
	std::vector<uint32_t> indices = { 0,1,3,0,3,2 };
};


struct VertexBuffer : VertexBufferBase
{
	VertexBuffer():VertexBufferBase(VertexBufferType::Normal){};
	std::vector<Vertex> vertices{};
	Vertex* mappedPtr{};
};

struct VertexBuffer3D : VertexBufferBase
{
	VertexBuffer3D() :VertexBufferBase(VertexBufferType::Vertex3D) {};
	std::vector<Vertex3D> vertices;
	Vertex3D* mappedPtr{};
};

struct VertexBufferPMD : VertexBufferBase
{
	VertexBufferPMD():VertexBufferBase(VertexBufferType::PMD) {};
	std::vector<VertexPMD> vertices;
	VertexPMD* mappedPtr{};
};

struct VertexBufferPMX : VertexBufferBase
{
	VertexBufferPMX() :VertexBufferBase(VertexBufferType::PMX) {};
	std::vector<VertexPMX> vertices;
	VertexPMX* mappedPtr{};
};

enum class RenderTargetBlend
{
	None,
	Blend,
	AlphaBlend,
	MAX
};

enum class RasterizerCullingState
{
	Back,
	Front,
	None,
	MAX
};

struct PipelineDescSetting
{
	PipelineDescSetting() :blend(RenderTargetBlend()), cullState(RasterizerCullingState()), depthEnable(false), inputElement({}), targetNum(0), format(DXGI_FORMAT_R8G8B8A8_UNORM) {};
	/// <summary>
	/// パイプラインの設定
	/// </summary>
	/// <param name="rtb">ブレンドの種類</param>
	/// <param name="cs">カリングの種類</param>
	/// <param name="isDepth">深度を使うか</param>
	/// <param name="elems">Shaderに送るデータ</param>
	/// <param name="renderTargetNum">レンダーターゲット数</param>
	/// <param name="f">レンダーターゲットフォーマット</param>
	/// <returns></returns>
	PipelineDescSetting(
		RenderTargetBlend rtb,
		RasterizerCullingState cs,
		bool isDepth,
		const std::vector<D3D12_INPUT_ELEMENT_DESC>& elems,
		UINT renderTargetNum = 8,
		DXGI_FORMAT f = DXGI_FORMAT_R8G8B8A8_UNORM)
		:blend(rtb), cullState(cs), depthEnable(isDepth), inputElement(elems), targetNum(renderTargetNum),format(f) {}
	RenderTargetBlend blend{};
	RasterizerCullingState cullState{};
	bool depthEnable{};
	std::vector<D3D12_INPUT_ELEMENT_DESC> inputElement{};
	UINT targetNum{};
	DXGI_FORMAT format;
};


struct ShaderDetailed
{
	ShaderDetailed() :fileName(nullptr), entryPoint(nullptr), verTarget(nullptr) {}
	ShaderDetailed(const LPCWSTR name, const LPCSTR entry, const LPCSTR ver):fileName(name), entryPoint(entry), verTarget(ver){};
	const LPCWSTR fileName;
	const LPCSTR entryPoint;
	const LPCSTR verTarget;
};

struct ShaderParameter
{
	ShaderParameter(std::wstring name, PipelineDescSetting* state, ShaderDetailed* vs, ShaderDetailed* ps = nullptr, ShaderDetailed* hs = nullptr, ShaderDetailed* gs = nullptr, ShaderDetailed* ds = nullptr)
		:shaderName(name), plDesc(state), pdesc({}), shader(std::array<ShaderDetailed*, static_cast<int>(ShaderType::MAX)>({ vs, ps, hs, gs, ds })) {}
	ShaderParameter(std::wstring name, D3D12_GRAPHICS_PIPELINE_STATE_DESC* state, ShaderDetailed* vs, ShaderDetailed* ps = nullptr, ShaderDetailed* hs = nullptr, ShaderDetailed* gs = nullptr, ShaderDetailed* ds = nullptr)
		:shaderName(name), pdesc(*state), shader(std::array<ShaderDetailed*, static_cast<int>(ShaderType::MAX)>({ vs, ps, hs, gs, ds })), plDesc(nullptr){}
	std::wstring shaderName;
	PipelineDescSetting* plDesc;
	D3D12_GRAPHICS_PIPELINE_STATE_DESC pdesc;
	std::array<ShaderDetailed*, static_cast<int>(ShaderType::MAX)> shader;
};

struct Shader
{
	Microsoft::WRL::ComPtr<ID3D12RootSignature> rootSignature;
	Microsoft::WRL::ComPtr<ID3D12PipelineState> pipelineState;
	D3D12_PRIMITIVE_TOPOLOGY_TYPE tolopogyType = {};
};

struct TextureParameter
{
	DirectX::TexMetadata metadata;
	DirectX::ScratchImage scratchImage;
	D3D12_RESOURCE_DESC resourceDesc;
};

struct BoneNode
{
	BoneNode() {};
	int boneIndex{}; //接続先
	DirectX::XMFLOAT3 headPos{}; //自分の座標
	DirectX::XMFLOAT3 tailPos{}; //ボーンの接続先の座標
	std::vector<BoneNode*> children{}; //自分に接続しているボーン
};

//キーフレーム毎のデータ
struct KeyFrame
{
	uint32_t frameNum{}; //キーフレーム◆がある経過フレーム数
	DirectX::XMFLOAT4 quaternion{}; //その時にどれくらい回転させるか
	DirectX::XMFLOAT3 offset{}; //元の位置からのオフセット(location)
	DirectX::XMFLOAT2 cpnt[2]; //コントロールポイント
	KeyFrame() :quaternion({}), offset({}), cpnt(), frameNum(0) {};
	KeyFrame(uint32_t fno, DirectX::XMFLOAT4 q, DirectX::XMFLOAT3 o, DirectX::XMFLOAT2 cp1, DirectX::XMFLOAT2 cp2)
		:frameNum(fno), quaternion(q), offset(o)
	{
		cpnt[0].x = cp1.x;
		cpnt[0].y = cp1.y;
		cpnt[1].x = cp2.x;
		cpnt[1].y = cp2.y;
	};
};

struct SkinMotionData
{
	SkinMotionData() :frameNum(0),  weight(0.0f){};
	SkinMotionData(uint32_t fnum, float weight) :frameNum(fnum), weight(weight){};
	uint32_t frameNum{};
	float weight;
};

struct MotionData
{
	std::wstring animationName;
	std::unordered_map<std::wstring, std::vector<KeyFrame>> motionData; //ボーン毎に格納されたフレーム毎データ
	std::unordered_map<std::string, std::vector<SkinMotionData>> skinData;
	uint32_t duration = {}; //アニメーションの最終フレーム
};

/// <summary>
/// スクリーンバッファ用
/// </summary>
struct Screen
{
	Screen() :buffer(nullptr), handle({}),resourceState(D3D12_RESOURCE_STATE_COMMON) {}
	Screen(Microsoft::WRL::ComPtr<ID3D12Resource> b, D3D12_CPU_DESCRIPTOR_HANDLE h, D3D12_RESOURCE_STATES rs)
		:buffer(b), handle(h), resourceState(rs) {}
	Microsoft::WRL::ComPtr<ID3D12Resource> buffer;
	D3D12_CPU_DESCRIPTOR_HANDLE handle;
	D3D12_RESOURCE_STATES resourceState;

	/// <summary>
	/// リソースのバリアを張る
	/// </summary>
	/// <param name="aftor">変更後のステート</param>
	/// <param name="cmdlist">実行コマンドリスト</param>
	void Barrier(D3D12_RESOURCE_STATES aftor, ID3D12GraphicsCommandList5* cmdlist);
};

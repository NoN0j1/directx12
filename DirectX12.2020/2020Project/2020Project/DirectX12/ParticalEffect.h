#pragma once
#include <memory>
#include "Effekseer/Effekseer.h"

class ParticleEffect
{
public:
	ParticleEffect(const Effekseer::Handle& h);
	void Update();
	const bool GetFinish()const;
	const Effekseer::Handle& GetHandle();
private:
	int32_t time = 0;
	const Effekseer::Handle& handle_;
	bool fin_ = false;
};


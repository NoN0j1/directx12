#include <cassert>
#include <d3d12.h>
#include "Dx12CommandList.h"
#include "DirectX12ToolFunction.h"

Dx12CommandList::Dx12CommandList(ID3D12Device6*& dev):device_(dev)
{
	CreateCommandAllocator();
	CreateCommandList();
}

Dx12CommandList::~Dx12CommandList()
{
}

void Dx12CommandList::Reset()
{
	cmdAlloc_->Reset();
	cmdList_->Reset(cmdAlloc_.Get(), nullptr);
}

ID3D12GraphicsCommandList5* Dx12CommandList::Get()
{
	return cmdList_.Get();
}

bool Dx12CommandList::CreateCommandAllocator()
{
	HRESULT result = device_->CreateCommandAllocator(D3D12_COMMAND_LIST_TYPE_DIRECT, IID_PPV_ARGS(cmdAlloc_.ReleaseAndGetAddressOf()));
	if (!CheckResult(result))
	{
		assert(false);
		return false;
	}
	return true;
}

bool Dx12CommandList::CreateCommandList()
{
	HRESULT result = device_->CreateCommandList(0, D3D12_COMMAND_LIST_TYPE_DIRECT, cmdAlloc_.Get(), nullptr, IID_PPV_ARGS(cmdList_.ReleaseAndGetAddressOf()));
	if (!CheckResult(result))
	{
		assert(false);
		return false;
	}
	return true;
}
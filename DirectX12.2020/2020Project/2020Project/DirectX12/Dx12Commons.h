#pragma once
#include <d3d12.h>
#include <dxgi.h>
#include <wrl.h>
#include <DirectXMath.h>
#include "DirectXTex.h"
#include "DirectX12ToolFunction.h"

enum class DummyTextureType;
struct ConstantBuffer;
struct VertexBuffer;
struct VertexBuffer3D;
struct VertexBufferPMD;
struct VertexBufferPMX;
struct RootSignatureParameter;
struct TextureParameter;
class Image;

class Dx12Commons
{
public:
	Dx12Commons(ID3D12Device6* dev);
	~Dx12Commons();

	/// <summary>
	/// バッファにデータの永続的なマップを行う(Structure用)
	/// </summary>
	/// <typeparam name="T"></typeparam>
	/// <param name="buff">マップするバッファ</param>
	/// <param name="elements">マップする内容</param>
	/// <param name="mappedPtr">マップ先</param>
	/// <returns>true = 成功, false = 失敗</returns>
	template<typename T>
	bool PermanentMap(ID3D12Resource*& buff, const std::vector<T>& elements, T*& mappedPtr)
	{
		HRESULT result = buff->Map(0, nullptr, (void**)&mappedPtr);
		if (!CheckResult(result))
		{
			return false;
		}
		std::copy(elements.begin(), elements.end(), mappedPtr);
		return true;
	}

	/// <summary>
	/// バッファにデータの永続的なマップを行う
	/// </summary>
	/// <typeparam name="T"></typeparam>
	/// <param name="buff"></param>
	/// <param name="mappedPtr"></param>
	/// <returns></returns>
	template<typename T>
	bool PermanentMap(ID3D12Resource*& buff, T*& mappedPtr)
	{
		D3D12_RANGE readRange;
		readRange.Begin = 0;
		readRange.End = 0;
		HRESULT result = buff->Map(0, &readRange, (void**)&mappedPtr);
		if (!CheckResult(result))
		{
			return false;
		}
		return true;
	}

	/// <summary>
	/// テクスチャの中間バッファを作成し、GPUに転送したのちメインBufferに格納する
	/// </summary>
	/// <param name="buff">作成したものを格納するBuffer</param>
	/// <param name="internalBuff">転送中間バッファ</param>
	/// <param name="texParam">作成に必要な情報を入れたTextureParameter</param>
	/// <param name="src">転送用のソースデータ</param>
	/// <param name="dst">転送の受け取り手データ</param>
	/// <returns></returns>
	bool CreateTextureBuffer(ID3D12Resource*& buff, ID3D12Resource*& internalBuff, TextureParameter& texParam, D3D12_TEXTURE_COPY_LOCATION& src, D3D12_TEXTURE_COPY_LOCATION& dst);
	
	/// <summary>
	/// 基本的にダミーテクスチャを作成するためのもの
	/// </summary>
	/// <param name="format">画像フォーマット</param>
	/// <param name="width">画像の横幅(最小4)</param>
	/// <param name="height">画像の縦幅(最小4)</param>
	/// <returns>true = 成功、false = 失敗</returns>
	bool CreateTextureBuffer(ID3D12Resource*& buff, DummyTextureType type, UINT64 width = 4, UINT height = 4, DXGI_FORMAT format = DXGI_FORMAT_R8G8B8A8_UNORM);
	
	/// <summary>
	/// StructuredBufferを作成してViewを配置する
	/// </summary>
	/// <typeparam name="T">Shaderに転送したい型</typeparam>
	/// <param name="buff">作成したものを格納するBuffer</param>
	/// <param name="elements">Shaderに転送する型のvector配列</param>
	/// <param name="descriptor">Viewを配置するDescriptor</param>
	/// <param name="num">Descriptorのサイズ(作成も同時に行う場合に必要)、既に作ってある場合は何番目に配置するか</param>
	/// <returns>true = 成功、false = 失敗</returns>
	template<typename T>
	bool CreateStructureBufferAndPlaceView(ID3D12Resource*& buff, const std::vector<T>& elements, ID3D12DescriptorHeap*& descriptor, UINT num = 1)
	{
		CreateSRVBuffer(buff, sizeof(T) * static_cast<UINT>(elements.size()));
		T* mappedPtr;
		HRESULT result = buff->Map(0, nullptr, (void**)&mappedPtr);
		if (!CheckResult(result))
		{
			return false;
		}
		std::copy(elements.begin(), elements.end(), mappedPtr);
		buff->Unmap(0, nullptr);

		if (descriptor == nullptr)
		{
			CreateCB_SR_UAViewDescriptorHeap(descriptor, num);
			CreateSRVBufferView(buff, elements, descriptor->GetCPUDescriptorHandleForHeapStart());
		}
		else
		{
			auto tempDesc = descriptor->GetDesc();
			if (tempDesc.Type != D3D12_DESCRIPTOR_HEAP_TYPE_CBV_SRV_UAV)
			{
				return false;
			}
			if (tempDesc.NumDescriptors < 2)
			{
				return false;
			}
			auto handle = descriptor->GetCPUDescriptorHandleForHeapStart();
			handle.ptr += static_cast<size_t>(device_->GetDescriptorHandleIncrementSize(D3D12_DESCRIPTOR_HEAP_TYPE_CBV_SRV_UAV)) * num;
			CreateSRVBufferView(buff, elements, handle);
		}
		return true;
	}

	/// <summary>
	/// StructuredBufferを作成してViewを配置する。後で配列の中身が変わる場合に使用
	/// </summary>
	/// <typeparam name="T">Shaderに転送したい型</typeparam>
	/// <param name="buff">作成したものを格納するBuffer</param>
	/// <param name="elements">Shaderに転送する型のvector配列</param>
	/// <param name="mappedPtr">mapしたポインタを保持する変数</param>
	/// <param name="descriptor">Viewを配置するDescriptor</param>
	/// <param name="num">Descriptorのサイズ(作成も同時に行う場合に必要)、既に作ってある場合は何番目に配置するか</param>
	/// <returns>true = 成功、false = 失敗</returns>
	template<typename T>
	bool CreateStructureBufferToMapAndPlaceView(ID3D12Resource*& buff, const std::vector<T>& elements, T*& mappedPtr, ID3D12DescriptorHeap*& descriptor, UINT num = 1)
	{
		CreateSRVBuffer(buff, sizeof(T) * static_cast<UINT>(elements.size()));
		HRESULT result = buff->Map(0, nullptr, (void**)&mappedPtr);
		if (!CheckResult(result))
		{
			return false;
		}
		std::copy(elements.begin(), elements.end(), mappedPtr);

		if (descriptor == nullptr)
		{
			CreateCB_SR_UAViewDescriptorHeap(descriptor, num);
			CreateSRVBufferView(buff, elements, descriptor->GetCPUDescriptorHandleForHeapStart());
		}
		else
		{
			auto tempDesc = descriptor->GetDesc();
			if (tempDesc.Type != D3D12_DESCRIPTOR_HEAP_TYPE_CBV_SRV_UAV)
			{
				return false;
			}
			if (tempDesc.NumDescriptors < 2)
			{
				return false;
			}
			auto handle = descriptor->GetCPUDescriptorHandleForHeapStart();
			handle.ptr += static_cast<size_t>(device_->GetDescriptorHandleIncrementSize(D3D12_DESCRIPTOR_HEAP_TYPE_CBV_SRV_UAV)) * (num <= 1 ? descriptor->GetDesc().NumDescriptors - 1 : num - 1);
			CreateSRVBufferView(buff, elements, handle);
		}
		return true;
	}

	/// <summary>
	/// SRVをバッファとして作成する
	/// </summary>
	/// <param name="buffer">作成したものを格納するBuffer</param>
	/// <param name="size">Bufferサイズ</param>
	/// <returns>true = 成功、false = 失敗</returns>
	bool CreateSRVBuffer(ID3D12Resource*& buffer, UINT size);

	/// <summary>
	/// レンダーターゲット用バッファを作成する
	/// </summary>
	/// <param name="buffer">作成したものを格納するBuffer</param>
	/// <param name="screenDesc">BackBufferのDesc</param>
	/// <returns>true = 成功、false = 失敗</returns>
	bool CreateRenderTargetBuffer(ID3D12Resource*& buffer, D3D12_RESOURCE_DESC screenDesc, const float* clearColor);

	/// <summary>
	/// レンダーターゲットビューを作成する
	/// </summary>
	/// <param name="resource">Viewを作成するリソースバッファ></param>
	/// <param name="handle">Viewを配置するCPU_Descriptor_Handle</param>
	/// <returns>true = 成功、false = resourceがnull</returns>
	bool CreateRTV(ID3D12Resource* resource, D3D12_CPU_DESCRIPTOR_HANDLE handle, DXGI_FORMAT format = DXGI_FORMAT_R8G8B8A8_UNORM);

	/// <summary>
	/// 頂点IndexBufferを作成する
	/// </summary>
	/// <param name="idxBuff">作成したものを格納するBuffer</param>
	/// <param name="view">IndexBufferを指すView</param>
	/// <param name="indices">頂点のvector配列</param>
	/// <returns>true = 成功、false = 失敗</returns>
	bool CreateIndexBuffer(ID3D12Resource*& idxBuff, D3D12_INDEX_BUFFER_VIEW& view, std::vector<unsigned short> indices);
	
	/// <summary>
	/// 頂点IndexBufferを作成する
	/// </summary>
	/// <param name="buffer">頂点データを格納済みの2D用VertexBuffer構造体</param>
	/// <returns>true = 成功、false = 失敗</returns>
	bool CreateIndexBuffer(VertexBuffer& buffer);

	/// <summary>
	/// 頂点IndexBufferを作成する
	/// </summary>
	/// <param name="buffer">頂点データを格納済みの2D用VertexBuffer構造体</param>
	/// <returns>true = 成功、false = 失敗</returns>
	bool CreateIndexBuffer(VertexBuffer3D& buffer);

	/// <summary>
	/// 頂点IndexBufferを作成する
	/// </summary>
	/// <param name="buffer">頂点データを格納済みの2D用VertexBuffer構造体</param>
	/// <returns>true = 成功、false = 失敗</returns>
	bool CreateIndexBuffer(VertexBufferPMD& buffer);

	/// <summary>
	/// 頂点IndexBufferを作成する
	/// </summary>
	/// <param name="buffer">頂点データを格納済みの2D用VertexBuffer構造体</param>
	/// <returns>true = 成功、false = 失敗</returns>
	bool CreateIndexBuffer(VertexBufferPMX& buffer);

	/// <summary>
	/// 深度バッファを画面サイズで作成する
	/// </summary>
	/// <param name="buffer">作成したものを格納するBuffer</param>
	/// <returns>true = 成功、false = 失敗</returns>
	bool CreateDepthBuffer(ID3D12Resource*& buffer, const SIZE& size = SIZE());

	/// <summary>
	/// 定数バッファを作成する
	/// </summary>
	/// <param name="buffer">作成したものを格納するBuffer</param>
	/// <param name="bufferSize">定数バッファに格納する物のデータサイズ</param>
	/// <returns>true = 成功、false = 失敗</returns>
	bool CreateConstantBuffer(ID3D12Resource*& buffer, UINT bufferSize);

	/// <summary>
	/// 演算バッファを作成する
	/// </summary>
	/// <param name="buffer">計算リソースを格納するバッファ</param>
	/// <param name="bufferSize">総データサイズ</param>
	/// <param name="copyBuffer">CPU側から読めるようにするコピー先バッファ</param>
	/// <returns></returns>
	bool CreateUnorderedAccessBuffer(ID3D12Resource*& buffer, UINT bufferSize, ID3D12Resource*& copyBuffer);

	/// <summary>
	/// 頂点バッファを作成する
	/// </summary>
	/// <param name="buffer">作成したものを格納するBuffer</param>
	/// <returns>true = 成功、false = 失敗</returns>
	bool CreateVertexBuffer(VertexBuffer& buffer);

	/// <summary>
	/// 頂点バッファを作成する
	/// </summary>
	/// <param name="buffer">作成したものを格納するBuffer</param>
	/// <returns>true = 成功、false = 失敗</returns>
	bool CreateVertexBuffer(VertexBuffer3D& buffer);

	/// <summary>
	/// 頂点バッファを作成する
	/// </summary>
	/// <param name="buffer">作成したものを格納するBuffer</param>
	/// <returns>true = 成功、false = 失敗</returns>
	bool CreateVertexBuffer(VertexBufferPMD& buffer);

	/// <summary>
	/// 頂点バッファを作成する
	/// </summary>
	/// <param name="buffer">作成したものを格納するBuffer</param>
	/// <returns>true = 成功、false = 失敗</returns>
	bool CreateVertexBuffer(VertexBufferPMX& buffer);

	/// <summary>
	/// デスクリプタヒープを作成する
	/// </summary>
	/// <param name="descriptor">作成したものを格納するDescriptorHeap</param>
	/// <param name="num">DescriptorHeapに格納するViewの数</param>
	/// <param name="type">使用予定のHeap種別</param>
	/// <returns>true = 成功、false = 失敗</returns>
	bool CreateDescriptorHeap(ID3D12DescriptorHeap*& descriptor, UINT num, D3D12_DESCRIPTOR_HEAP_TYPE type);
	
	/// <summary>
	/// 2DTextureとして使用するSRViewを作成する
	/// </summary>
	/// <param name="descriptor">ViewをセットするDescriptorHeap</param>
	/// <param name="resource">2DTextureのリソースバッファ</param>
	/// <param name="offset">DescriptorHeapにViewを配置する際のオフセット</param>
	/// <returns>true = 成功、false = DescriptorHeapがnull</returns>
	bool CreateSRV(ID3D12DescriptorHeap& descriptor, ID3D12Resource& resource, UINT offset);

	/// <summary>
	/// レンダーターゲット用デスクリプタヒープを作成する
	/// </summary>
	/// <param name="descriptor">作成したものを格納するDescriptorHeap</param>
	/// <param name="num">使用予定のRTViewの数</param>
	/// <returns>true = 成功、false = 失敗</returns>
	bool CreateRTVDescriptorHeap(ID3D12DescriptorHeap*& descriptor, UINT num);

	/// <summary>
	/// シェーダリソース用デスクリプタヒープを作成する
	/// </summary>
	/// <param name="descriptor">作成したものを格納するDescriptorHeap</param>
	/// <param name="num">使用予定のSRViewの数</param>
	/// <returns>true = 成功、false = 失敗</returns>
	bool CreateCB_SR_UAViewDescriptorHeap(ID3D12DescriptorHeap*& descriptor, UINT num);

	/// <summary>
	/// 2Dテクスチャ用のSRViewを作成する
	/// </summary>
	/// <param name="resource">Viewを作成するリソースバッファ</param>
	/// <param name="handle">Viewを配置するCPU_Descriptor_Handle</param>
	/// <returns>true = 成功、false = リソースバッファがnullptr</returns>
	bool CreateSRVTexture2DView(ID3D12Resource* resource, D3D12_CPU_DESCRIPTOR_HANDLE handle);
	
	/// <summary>
	/// バッファ用のSRViewを作成する
	/// </summary>
	/// <typeparam name="T">格納するバッファの型</typeparam>
	/// <param name="resource">Viewを作成するリソースバッファ</param>
	/// <param name="elements">SRVバッファに格納する型のvector配列</param>
	/// <param name="handle">Viewを配置するCPU_Descriptor_Handle</param>
	/// <returns>true = 成功、false = リソースバッファがnullptr</returns>
	template<typename T>
	bool CreateSRVBufferView(ID3D12Resource* resource, const std::vector<T>& elements, D3D12_CPU_DESCRIPTOR_HANDLE handle)
	{
		if (resource == nullptr)
		{
			OutputDebugString(L"ERROR::CreateSRVBufferのResourceがnullです。\n");
			return false;
		}
		D3D12_SHADER_RESOURCE_VIEW_DESC srvDesc = {};
		srvDesc.Shader4ComponentMapping = D3D12_DEFAULT_SHADER_4_COMPONENT_MAPPING;
		srvDesc.ViewDimension = D3D12_SRV_DIMENSION_BUFFER;
		srvDesc.Format = DXGI_FORMAT_UNKNOWN;
		srvDesc.Buffer.NumElements = static_cast<UINT>(elements.size());
		srvDesc.Buffer.StructureByteStride = sizeof(T);
		srvDesc.Buffer.FirstElement = 0;
		srvDesc.Buffer.Flags = D3D12_BUFFER_SRV_FLAG_NONE;
		device_->CreateShaderResourceView(resource, &srvDesc, handle);
		return true;
	}

	/// <summary>
	/// SRVデスクリプタヒープの作成とViewの配置を同時に行う。
	/// </summary>
	/// <param name="descriptor">作成したものを格納するDescriptorHeap</param>
	/// <param name="resouce">Viewを作成するリソースバッファ</param>
	/// <param name="num"></param>
	/// <returns>true = 成功、false = 何らかの処理で失敗</returns>
	bool CreateSRVDescriptorAndPlaceView(ID3D12DescriptorHeap*& descriptor, ID3D12Resource* resouce, UINT num = 1);
	
	/// <summary>
	/// 深度ステンシル用のデスクリプタヒープを作成する
	/// </summary>
	/// <param name="descriptor">作成したものを格納するDescriptorHeap</param>
	/// <param name="num">使用予定のDSViewの数</param>
	/// <returns>true = 成功、false = 失敗</returns>
	bool CreateDSVDescriptor(ID3D12DescriptorHeap*& descriptor, UINT num);

	/// <summary>
	/// 深度ステンシルViewを作成する
	/// </summary>
	/// <param name="resource">Viewを作成するリソースバッファ></param>
	/// <param name="handle">Viewを配置するCPU_Descriptor_Handle</param>
	/// <returns>true = 成功、false = resourceがnull</returns>
	bool CreateDSV(ID3D12Resource* resource, D3D12_CPU_DESCRIPTOR_HANDLE handle);

	/// <summary>
	/// DSVデスクリプタヒープの作成とViewの配置を同時に行う
	/// </summary>
	/// <param name="descriptor">作成したものを格納するDescriptorHeap</param>
	/// <param name="resource">Viewを作成するリソースバッファ</param>
	/// <param name="num">descriptorHeapがnullの時は使用予定のDSView数、そうでない時はviewの配置位置</param>
	/// <returns>true = 成功、false = 何らかの処理で失敗</returns>
	bool CreateDSVDescriptorAndPlaceView(ID3D12DescriptorHeap*& descriptor, ID3D12Resource* resource, UINT num = 1);
	
	/// <summary>
	/// 定数バッファビューを作成する
	/// </summary>
	/// <param name="resource">Viewを作成するリソースバッファ</param>
	/// <param name="handle">Viewを配置するCPU_Descriptor_Handle</param>
	/// <returns>true = 成功、false = resourceがnull</returns>
	bool CreateCBV(ID3D12Resource* resource, D3D12_CPU_DESCRIPTOR_HANDLE handle);

	/// <summary>
	/// descriptorが無い場合、定数バッファビューを作成してでもViewを作成する
	/// </summary>
	/// <typeparam name="T">マップ時の型</typeparam>
	/// <param name="descriptor">配置予定Descriptor</param>
	/// <param name="resource">Viewを作成するリソース</param>
	/// <param name="map">マップして欲しいポインタ</param>
	/// <param name="num">Descriptor作成時の数、作成済みの場合はDescriptor上の配置位置(1スタート)</param>
	/// <returns>true = 成功、false =何かしらで失敗</returns>
	template<typename T>
	bool CreateCBVDescriptorAndPlaceView(ID3D12DescriptorHeap*& descriptor, ID3D12Resource* resource, T*& map, UINT num = 1)
	{
		if (descriptor == nullptr)
		{
			if (!CreateCB_SR_UAViewDescriptorHeap(descriptor, num))
			{
				return false;
			}
			if (!CreateCBV(resource, descriptor->GetCPUDescriptorHandleForHeapStart()))
			{
				return false;
			}
			HRESULT result = resource->Map(0, nullptr, (void**)&map);
			if (!CheckResult(result))
			{
				return false;
			}
			//resource->Unmap(0, nullptr);
		}
		else
		{
			//既存のデスクリプタヒープの最後尾にくっ付ける
			auto tempDesc = descriptor->GetDesc();
			if (tempDesc.Type != D3D12_DESCRIPTOR_HEAP_TYPE_CBV_SRV_UAV || tempDesc.NumDescriptors < 2)
			{
				//1つ未満の場合は足せないのでNG
				//種類がCBV or SRV or UAV 以外の時はまとめて扱えないのでNG
				return false;
			}
			auto handle = descriptor->GetCPUDescriptorHandleForHeapStart();
			handle.ptr += static_cast<size_t>(device_->GetDescriptorHandleIncrementSize(D3D12_DESCRIPTOR_HEAP_TYPE_CBV_SRV_UAV)) * (num <= 1 ? descriptor->GetDesc().NumDescriptors - 1 : num - 1);
			if (!CreateCBV(resource, handle))
			{
				return false;
			}
			HRESULT result = resource->Map(0, nullptr, (void**)&map);
			if (!CheckResult(result))
			{
				return false;
			}
			//resource->Unmap(0, nullptr);
		}
		return true;
	};

	template<typename T>
	bool CreateUAV(ID3D12Resource* resource, D3D12_CPU_DESCRIPTOR_HANDLE handle, std::vector<T> elements)
	{
		if (resource == nullptr)
		{
			return false;
		}
		D3D12_UNORDERED_ACCESS_VIEW_DESC uavDesc = {};
		uavDesc.Format = DXGI_FORMAT_UNKNOWN;
		uavDesc.ViewDimension = D3D12_UAV_DIMENSION_BUFFER;
		uavDesc.Buffer.NumElements = static_cast<UINT>(elements.size());
		uavDesc.Buffer.StructureByteStride = sizeof(T);
		uavDesc.Buffer.FirstElement = 0;
		uavDesc.Buffer.Flags = D3D12_BUFFER_UAV_FLAG_NONE;
		device_->CreateUnorderedAccessView(resource, nullptr, &uavDesc, handle);
		return true;
	}

	/// <summary>
	/// パイプラインを作成してライブラリに登録する
	/// </summary>
	/// <param name="plsDesc">パイプラインを作成する際のソースPLStateDesc</param>
	/// <param name="pName">登録名</param>
	/// <returns>true = 成功、false = 何らかの処理で失敗</returns>
	bool AddPipeline(D3D12_GRAPHICS_PIPELINE_STATE_DESC& plsDesc, const LPCWSTR pName);

	/// <summary>
	/// パイプラインライブラリから読み込む
	/// </summary>
	/// <param name="pipeline">読み込んだパイプラインを格納するPipelineState</param>
	/// <param name="loadplsDesc">作成した際のPLStateDesc(整合性チェック)</param>
	/// <param name="pipelineName">読み込む登録名</param>
	/// <returns>true = 成功、false = 何らかの処理で失敗</returns>
	bool LoadPipeline(ID3D12PipelineState*& pipeline, const D3D12_GRAPHICS_PIPELINE_STATE_DESC& loadplsDesc, LPCWSTR pipelineName);
private:
	void CreatePipelineLibrary();
	ID3D12Device6* device_;
	Microsoft::WRL::ComPtr<ID3D12PipelineLibrary1> pipelineLibrary_;
};
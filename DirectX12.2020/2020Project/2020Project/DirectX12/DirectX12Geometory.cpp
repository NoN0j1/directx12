#include "DirectX12Geometory.h"

const UINT Vertex::Size() const
{
    return sizeof(vertex) + sizeof(uv);
}

const UINT Vertex3D::Size() const
{
    return sizeof(vertex) + sizeof(uv) + sizeof(normal);
}

const UINT VertexPMD::Size() const
{
    return sizeof(vertex) + sizeof(uv) + sizeof(normal) + sizeof(weight) + sizeof(boneNo[0]) * static_cast<UINT>(boneNo.size());
}

const UINT VertexPMX::Size() const
{
    return sizeof(vertex) + sizeof(uv) + sizeof(normal) + sizeof(boneNo) + sizeof(boneWeight) + sizeof(SDEF_C) + sizeof(SDEF_R0) + sizeof(SDEF_R1) + sizeof(type);
}

void Screen::Barrier(D3D12_RESOURCE_STATES after, ID3D12GraphicsCommandList5* cmdlist)
{
	if (resourceState == after)
	{
		return;
	}
	D3D12_RESOURCE_BARRIER barrierDesc = {};
	barrierDesc.Type = D3D12_RESOURCE_BARRIER_TYPE_TRANSITION;
	barrierDesc.Flags = D3D12_RESOURCE_BARRIER_FLAG_NONE;
	barrierDesc.Transition.pResource = buffer.Get();
	barrierDesc.Transition.Subresource = D3D12_RESOURCE_BARRIER_ALL_SUBRESOURCES;
	barrierDesc.Transition.StateBefore = resourceState;
	barrierDesc.Transition.StateAfter = after;
	cmdlist->ResourceBarrier(1, &barrierDesc);

	resourceState = after;
}

DirectX::XMMATRIX Transform::GetMatrix()
{
	DirectX::XMMATRIX mat;
	mat = DirectX::XMMatrixIdentity();
	mat *= DirectX::XMMatrixScaling(scale.x, scale.y, scale.z);
	mat *= DirectX::XMMatrixRotationX(rotation.x * DirectX::XM_PI/180.0f);
	mat *= DirectX::XMMatrixRotationY(rotation.y * DirectX::XM_PI / 180.0f);
	mat *= DirectX::XMMatrixRotationZ(rotation.z * DirectX::XM_PI / 180.0f);
	mat *= DirectX::XMMatrixTranslation(position.x, position.y, position.z);
	return mat;
}

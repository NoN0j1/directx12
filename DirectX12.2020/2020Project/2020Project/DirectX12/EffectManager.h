#pragma once
#include <memory>
#include <wrl.h>
#include <string>
#include <DirectXMath.h>
#include "Effekseer/Effekseer.h"
#include "EffekseerRendererDX12/EffekseerRendererDX12.h"

class Camera;
class Dx12Wrapper;
class ParticleEffect;
struct ID3D12Device6;
struct ID3D12CommandQueue;

class EffectManager
{
public:
	bool Init(Microsoft::WRL::ComPtr<ID3D12Device6> device, Microsoft::WRL::ComPtr<ID3D12CommandQueue> cmdQueue, std::shared_ptr<Camera> usingCamera);
	void Update();
	void Draw(ID3D12GraphicsCommandList5* cmdList);
	bool LoadEffect(const char16_t* path, const std::wstring& name);
	const std::shared_ptr<ParticleEffect>& PlayEffect(const std::wstring& name, const DirectX::XMFLOAT3& pos, float scalse);
	EffectManager() = default;
	~EffectManager();
private:
	std::vector<Effekseer::Effect*> _effects;
	std::vector<std::pair<Effekseer::Handle, bool>> _playEffectHandle;

	Microsoft::WRL::ComPtr<EffekseerRenderer::Renderer> renderer_;

	/// <summary>
	/// Effekseerが使用するコマンドリスト
	/// </summary>
	Microsoft::WRL::ComPtr<EffekseerRenderer::CommandList> efkCmdList_;

	/// <summary>
	/// Effekseerがメモリ管理をしているクラス
	/// </summary>
	Microsoft::WRL::ComPtr<EffekseerRenderer::SingleFrameMemoryPool> memoryPool_;
	Microsoft::WRL::ComPtr<Effekseer::Manager> effekseerManager_;

	/// <summary>
	/// エフェクトの読み込んだパスを管理
	/// </summary>
	std::unordered_map<const char16_t*, std::wstring> loadEffectPath_;

	/// <summary>
	/// 読み込んだエフェクトと名前を管理
	/// </summary>
	std::unordered_map<std::wstring, std::shared_ptr<Effekseer::Effect*>> loadEffects_;

	/// <summary>
	/// DirectXコマンドキュー
	/// </summary>
	Microsoft::WRL::ComPtr<ID3D12CommandQueue> cmdQueue_;

	/// <summary>
	/// 描画に使用しているカメラ
	/// </summary>
	std::shared_ptr<Camera> camera_;

	std::vector<std::shared_ptr<ParticleEffect>> playingEffects_;
};


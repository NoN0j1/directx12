#pragma once
#include <memory>
#include <unordered_map>
#include <string>

struct ShaderParameter;
struct ShaderDetailed;
struct Shader;
struct PipelineDescSetting;
class Dx12Wrapper;
enum class RenderTargetBlend;
enum class RasterizerCullingState;

struct D3D12_RASTERIZER_DESC;
struct D3D12_BLEND_DESC;
struct D3D12_DEPTH_STENCIL_DESC;

class ShaderManager
{
public:
	ShaderManager(Dx12Wrapper& dx12);
	~ShaderManager() = default;
	bool LoadShader(ShaderParameter& loadParam);
	bool LoadComputeShader(ShaderDetailed& shader, const std::wstring& shaderName);
	std::shared_ptr<Shader> GetShader(const std::wstring& name);
private:
	void InitBlendDesc();
	void InitRasterizeDesc();
	void InitDepthStencilDesc();
	D3D12_GRAPHICS_PIPELINE_STATE_DESC GetPipelineStateDesc(const PipelineDescSetting& setting);

	Dx12Wrapper& wrapper_;
	std::unordered_map<std::wstring, std::shared_ptr<Shader>> manageShader_;
	std::unordered_map<RenderTargetBlend, D3D12_BLEND_DESC> blendDescData_;
	std::unordered_map<RasterizerCullingState, D3D12_RASTERIZER_DESC> rasterizeDescData_;
	std::array<D3D12_DEPTH_STENCIL_DESC, 2> depthStencilDescData_;
};


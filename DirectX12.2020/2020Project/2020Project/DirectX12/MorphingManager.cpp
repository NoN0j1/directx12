#include "MorphingManager.h"
#include "DirectX12Geometory.h"

MorphingManager::MorphingManager()
{
}

MorphingManager::~MorphingManager()
{
}

void MorphingManager::Update(std::shared_ptr<VertexBufferBase>& vbBase)
{
//ここボーンと同じだからリファクタリングでどうにかできたら。。。

	//モーションをセットし始めてから何ミリ秒経過したか
	//MMDは30fpsの速度で動く。1秒間(1000ミリ秒)あたりに30フレーム動くので1000/30 = 33.3333...ミリ秒
	//ということは 経過フレーム数 = 経過ミリ秒/33.3333...となる
	uint32_t frameToMILLISec = 1000 / 30; //1フレームに必要なミリ秒
	uint32_t frame = (GetTickCount() - lastTickCount_) / frameToMILLISec; //経過フレーム数 (GetTickCount() - lastTickCount) == 経過ミリ秒

	if (frame > duration_ && isLoop_)
	{
		lastTickCount_ = GetTickCount();
	}
	UpdateMorphing(frame, vbBase);
}

void MorphingManager::SetAnimation(const MotionData& data, bool loop, uint32_t interval)
{
	if (animName_ == data.animationName)
	{
		//同じモーションの場合は更新しない
		return;
	}
	skinAnimationData_ = data.skinData;
	duration_ = data.duration + interval;
	lastTickCount_ = GetTickCount();
	animName_ = data.animationName;
	isLoop_ = loop;
	isAnimation_ = true;
}

PMDSkin& MorphingManager::GetPMDSkinData(std::string name)
{
	return skinDatas_[name];
}

void MorphingManager::UpdateMorphing(const uint32_t currentFrame, std::shared_ptr<VertexBufferBase>& vbBase)
{
	auto pmd = static_cast<VertexBufferPMD*>(vbBase.get());
	if (pmd == nullptr)
	{
		return;
	}
	const auto& skinMasterVertex = skinDatas_["base"];
	auto skinFunc = [&skinMasterVertex, pmd](PMDSkin& skinraw, float t)
	{
		for (auto& skinning : skinraw.skindata)
		{
			auto& select = skinMasterVertex.skindata[skinning.first];
			const auto& idx = select.first;
			const auto& baseVertpos = select.second;
			pmd->mappedPtr[idx].vertex.x = baseVertpos.x + skinning.second.x * t;
			pmd->mappedPtr[idx].vertex.y = baseVertpos.y + skinning.second.y * t;
			pmd->mappedPtr[idx].vertex.z = baseVertpos.z + skinning.second.z * t;
		}
	};

	int isRun = 0;
	for (auto& keyframe : skinAnimationData_)
	{
		if (keyframe.first == "base")
		{
			continue;
		}
		auto revit = std::find_if(keyframe.second.rbegin(), keyframe.second.rend(), 
			[currentFrame](const SkinMotionData& skin) {return skin.frameNum <= currentFrame; });
		if (revit == keyframe.second.rend())
		{
			//現在のフレームより小さいアニメーションキー情報無し(キー未登録)
			continue;
		}
		auto it = revit.base(); //baseで向きを戻すことで見つかったやつの次を指す
		float weight = revit->weight; //今のキーのWeight
		if (it == keyframe.second.end())
		{
			if (weight != 0.0f)
			{
				skinFunc(skinDatas_[keyframe.first], weight); //モーフィング
				isRun++;
			}
			continue;
		}
		float nextWeight = it->weight;
		//lerpの時間を求める
		float t = static_cast<float>(currentFrame - revit->frameNum) / static_cast<float>(it->frameNum - revit->frameNum);
		skinFunc(skinDatas_[keyframe.first], std::lerp(weight,nextWeight,t)); //モーフィング
		isRun++;//実行回数+1
	}
	if (isRun <= 0)
	{
		//1回も実行されなかったらbaseの値にする
		StopMorphing(vbBase);
	}
}

void MorphingManager::StopMorphing(std::shared_ptr<VertexBufferBase>& vbBase)
{
	auto pmd = static_cast<VertexBufferPMD*>(vbBase.get());
	if (pmd == nullptr)
	{
		return;
	}
	const auto& skinMasterVertex = skinDatas_["base"];
	for (auto& skindata : skinMasterVertex.skindata)
	{
		auto idx = skindata.first;
		pmd->mappedPtr[idx].vertex.x = skindata.second.x;
		pmd->mappedPtr[idx].vertex.y = skindata.second.y;
		pmd->mappedPtr[idx].vertex.z = skindata.second.z;
	}
}

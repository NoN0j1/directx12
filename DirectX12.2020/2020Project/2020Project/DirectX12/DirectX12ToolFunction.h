#pragma once
#include <vector>
#include <string>

typedef long HRESULT;
struct ID3D10Blob typedef ID3DBlob;

/// <summary>
/// HRESULTの判定を行う
/// </summary>
/// <param name="result"> HRESULTの値</param>
/// <returns>true = 成功, false = 何らかの失敗</returns>
bool CheckResult(HRESULT result);

/// <summary>
/// パイプラインのHRESULTの判定を行い、失敗の場合はログに出力する
/// </summary>
/// <param name="result">HRESULTの値</param>
/// <param name="blob">ErrorBlobのポインタ</param>
/// <returns>true = 成功, false = 何らかの失敗</returns>
bool CheckResult(HRESULT result, ID3DBlob* blob);

/// <summary>
/// 指定したAlignment値の倍数に切り上げた値を返す
/// </summary>
/// <param name="size">元のサイズ</param>
/// <param name="alignment">Alignmentしたい値</param>
/// <returns></returns>
UINT AlignmentedValue(UINT size, UINT alignment);

///フォルダパスの取得を行う
///@param path パスの取得対象string
///@return パスの存在するフォルダパス
std::wstring GetDirectoryFromPath(std::wstring path);

///文字列を分離する
///@param str 分離対象文字列
///@param separator 分離文字(ここで分離する目印)
///@return 分離後の文字列ベクタ配列
std::vector<std::string> SeparateString(std::string str, const char separator = '*');

///1バイトstringをワイド文字wstringに変換する
///@param str 変換対象string文字列
///@return 変換後ワイド文字列
std::wstring WstringFromString(const std::string& str);
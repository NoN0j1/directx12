#pragma once
#include <DirectXMath.h>
#include <wrl.h>
#include "Object/Actor.h"
#include "d3dx12.h"
#include "../Shader/MultiPurpose.hlsli"

class Dx12Wrapper;

/// <summary>
/// カメラとしてViewProjection行列を作るクラス
/// </summary>
class Camera : public Actor
{
public:
	Camera();
	~Camera();

	/// <summary>
	/// 初期化
	/// </summary>
	virtual void CameraInit(bool perspect);

	void CameraInit(bool perspect, float shadowR);

	/// <summary>
	/// 上方向ベクトルを取得
	/// </summary>
	/// <returns>アップベクトル</returns>
	const DirectX::XMFLOAT3& GetUpVector()const;

	/// <summary>
	/// 注視点を取得
	/// </summary>
	/// <returns>注視点</returns>
	const DirectX::XMFLOAT3& GetTarget()const;

	/// <summary>
	/// 視点位置を取得
	/// </summary>
	/// <returns>視点</returns>
	const DirectX::XMFLOAT3& GetEye()const;

	/// <summary>
	/// 視野角を取得
	/// </summary>
	/// <returns>視野角</returns>
	const float GetFov()const;

	/// <summary>
	/// 最大描画距離を取得
	/// </summary>
	/// <returns>描画距離</returns>
	const float GetCameraViewRange()const;

	/// <summary>
	/// 移動等の更新情報
	/// </summary>
	virtual void Update()override;

	/// <summary>
	/// 注視対象を追加する
	/// </summary>
	/// <param name="t">注視対象</param>
	void SetLookAtTarget(std::shared_ptr<Actor>* t);

	/// <summary>
	/// カメラを回転させる
	/// </summary>
	void Rotate(const DirectX::XMFLOAT3& dir);

	/// <summary>
	/// カメラを移動させる
	/// </summary>
	/// <param name="pos">移動先座標</param>
	/// <param name="time">移動にかかる時間</param>
	void SetPosition(const DirectX::XMFLOAT3& pos, float time = 0.0f);

	/// <summary>
	/// マップしてGPUと連動するポインタを取得する
	/// </summary>
	/// <returns>マップ用ポインタ</returns>
	CameraCommons*& GetConstantCommonsParameter();
protected:
	Camera(const Transform& pos);

	void UpdateViewProjection();

	/// <summary>
	/// 注視点
	/// </summary>
	DirectX::XMFLOAT3 target_{};

	/// <summary>
	/// 上ベクトル
	/// </summary>
	DirectX::XMFLOAT3 up_{};

	/// <summary>
	/// 視野角
	/// </summary>
	float fov_{};

	/// <summary>
	/// 投影方法。true = 透視投影, false = 平行投影
	/// </summary>
	bool perspective_ = true;

	/// <summary>
	/// Shaderのマップ先
	/// </summary>
	CameraCommons* mappedCommons_ = {};

	std::shared_ptr<Actor>* lookAtTarget_;

	DirectX::XMFLOAT3 direction_{};
private:
	float depthRange;
};
#include <d3dcompiler.h>
#include <string>
#include "../../Application.h"
#include "Texture2D.h"
#include "../DirectX12Geometory.h"
#include "../DirectX12ToolFunction.h"

#pragma comment(lib, "D3DCompiler.lib")

using namespace std;

Texture2D::Texture2D(const SIZE& size) : Texture(size)
{
}

Texture2D::~Texture2D()
{
}
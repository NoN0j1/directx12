#include <filesystem>
#include <d3dcompiler.h>
#include <DirectXTex.h>
#include "TextureFactory.h"
#include "Texture.h"
#include "Texture2D.h"
#include "../Dx12Wrapper.h"
#include "../Dx12Commons.h"
#include "../Dx12CommandList.h"
#include "../DirectX12Geometory.h"
#include "../DirectX12ToolFunction.h"

#pragma comment(lib, "D3DCompiler.lib")
#pragma comment(lib, "DirectXTex.lib")

using namespace std;
using namespace DirectX;

TextureFactory::~TextureFactory()
{
	Release();
}

TextureFactory::TextureFactory(Dx12Wrapper& dx12) :wrapper_(dx12)
{
	Shader pixelShader(L"Shader/PS_Texture.hlsl", "main", "ps_5_1");
	Shader vertexShader(L"Shader/VS_Texture.hlsl", "main", "vs_5_1");
	Microsoft::WRL::ComPtr<ID3D10Blob> vsBlob_ = nullptr;
	Microsoft::WRL::ComPtr<ID3D10Blob> psBlob_ = nullptr;
	Microsoft::WRL::ComPtr<ID3D10Blob> errBlob = nullptr;
	HRESULT result = D3DCompileFromFile(
		pixelShader.fileName,
		nullptr,
		D3D_COMPILE_STANDARD_FILE_INCLUDE,
		pixelShader.entryPoint,
		pixelShader.verTarget,
		0,
		0,
		&psBlob_,
		&errBlob);
	if (!CheckResult(result, errBlob.Get()))
	{
		return;
	}

	result = D3DCompileFromFile(
		vertexShader.fileName,
		nullptr,
		D3D_COMPILE_STANDARD_FILE_INCLUDE,
		vertexShader.entryPoint,
		vertexShader.verTarget,
		0,
		0,
		&vsBlob_,
		&errBlob);
	if (!CheckResult(result, errBlob.Get()))
	{
		return;
	}
	Microsoft::WRL::ComPtr<ID3DBlob> signature;
	result = D3DGetBlobPart(vsBlob_->GetBufferPointer(), vsBlob_->GetBufferSize(), D3D_BLOB_ROOT_SIGNATURE, 0, &signature);
	if (!CheckResult(result))
	{
		return;
	}
	result = dx12.GetDevice()->CreateRootSignature(0, signature->GetBufferPointer(), signature->GetBufferSize(), IID_PPV_ARGS(rootSignature_.ReleaseAndGetAddressOf()));
	if (!CheckResult(result))
	{
		return;
	}

	D3D12_GRAPHICS_PIPELINE_STATE_DESC plsDesc = {};
	plsDesc.pRootSignature = rootSignature_.Get();
	plsDesc.VS = CD3DX12_SHADER_BYTECODE(vsBlob_.Get());
	plsDesc.PS = CD3DX12_SHADER_BYTECODE(psBlob_.Get());
	plsDesc.RasterizerState = CD3DX12_RASTERIZER_DESC(D3D12_DEFAULT);
	plsDesc.BlendState = CD3DX12_BLEND_DESC(D3D12_DEFAULT);
	plsDesc.DepthStencilState.DepthEnable = false;
	plsDesc.DepthStencilState.StencilEnable = false;
	plsDesc.PrimitiveTopologyType = D3D12_PRIMITIVE_TOPOLOGY_TYPE_TRIANGLE;
	plsDesc.NumRenderTargets = 2;
	plsDesc.RTVFormats[0] = DXGI_FORMAT_R8G8B8A8_UNORM;
	plsDesc.RTVFormats[1] = DXGI_FORMAT_R8G8B8A8_UNORM;
	plsDesc.SampleDesc.Count = 1;
	plsDesc.SampleMask = D3D12_DEFAULT_SAMPLE_MASK;

	inputDesc_[0] = D3D12_INPUT_ELEMENT_DESC({ "POSITION", 0, DXGI_FORMAT_R32G32B32A32_FLOAT, 0, D3D12_APPEND_ALIGNED_ELEMENT,D3D12_INPUT_CLASSIFICATION_PER_VERTEX_DATA, 0 });
	inputDesc_[1] = D3D12_INPUT_ELEMENT_DESC({ "TEXCOORD", 0, DXGI_FORMAT_R32G32_FLOAT, 0, D3D12_APPEND_ALIGNED_ELEMENT,D3D12_INPUT_CLASSIFICATION_PER_VERTEX_DATA, 0 });
	plsDesc.InputLayout.NumElements = 2;
	plsDesc.InputLayout.pInputElementDescs = inputDesc_.data();
	pDesc_ = plsDesc;

	wrapper_.GetCommons().AddPipeline(pDesc_, L"2DTexturePipeline");
	wrapper_.GetCommons().LoadPipeline(*plState_.GetAddressOf(), pDesc_, L"2DTexturePipeline");
}

TextureParameter TextureFactory::LoadTextureFromFile(const std::wstring& fileName)
{
	filesystem::path filePath(fileName);
	if (!filesystem::exists(filePath))
	{
		OutputDebugString(L"テクスチャのパスが不正です。");
		assert(false);
	}
	TextureParameter ret = {};

	HRESULT result = E_FAIL;
	auto ext = filePath.extension();
	if (ext == ".dds")
	{
		result = LoadFromDDSFile(fileName.c_str(), DDS_FLAGS_FORCE_RGB, &ret.metadata, ret.scratchImage);
	}
	else if (ext == ".tga")
	{
		result = LoadFromTGAFile(fileName.c_str(), &ret.metadata, ret.scratchImage);
	}
	else
	{
		result = LoadFromWICFile(fileName.c_str(), WIC_FLAGS_IGNORE_SRGB, &ret.metadata, ret.scratchImage);
	}

	if (!CheckResult(result))
	{
		OutputDebugString(L"テクスチャの読み込みに失敗しました。");
		assert(false);
	}

	switch (ret.metadata.dimension)
	{
	case TEX_DIMENSION_TEXTURE1D:
		ret.resourceDesc = CD3DX12_RESOURCE_DESC::Tex1D(
			ret.metadata.format,
			static_cast<UINT64>(ret.metadata.width),
			static_cast<UINT16>(ret.metadata.arraySize));
		break;
	case TEX_DIMENSION_TEXTURE2D:
		ret.resourceDesc = CD3DX12_RESOURCE_DESC::Tex2D(
			ret.metadata.format,
			static_cast<UINT64>(ret.metadata.width),
			static_cast<UINT>(ret.metadata.height),
			static_cast<UINT16>(ret.metadata.arraySize),
			static_cast<UINT16>(ret.metadata.mipLevels));
		break;
	case TEX_DIMENSION_TEXTURE3D:
		ret.resourceDesc = CD3DX12_RESOURCE_DESC::Tex3D(
			ret.metadata.format,
			static_cast<UINT64>(ret.metadata.width),
			static_cast<UINT>(ret.metadata.height),
			static_cast<UINT16>(ret.metadata.depth),
			static_cast<UINT16>(ret.metadata.mipLevels));
		break;
	default:
		OutputDebugString(L"matadataが無効です。");
		break;
	}

	return ret;
}


void TextureFactory::SetDrawTextureCommands(ID3D12GraphicsCommandList5*& cmdList)
{
	cmdList->IASetPrimitiveTopology(D3D_PRIMITIVE_TOPOLOGY_TRIANGLELIST);
	cmdList->SetGraphicsRootSignature(rootSignature_.Get());
	cmdList->SetPipelineState(plState_.Get());
}

const D3D12_GRAPHICS_PIPELINE_STATE_DESC& TextureFactory::GetPipelineDesc()
{
	return pDesc_;
}

shared_ptr<Texture2D>& TextureFactory::GetTexture(const std::wstring& path, const SIZE& size)
{
	if (textureTable_.contains(path))
	{
		return textureTable_.at(path);
	}
	textureTable_[path] = make_shared<Texture2D>(SIZE(size));
	return textureTable_[path];
}

void TextureFactory::Release()
{
	wrapper_.WaitWithFence();
	textureTable_.clear();
}

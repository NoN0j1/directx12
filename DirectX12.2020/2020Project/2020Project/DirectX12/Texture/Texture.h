#pragma once
#include <memory>
#include <DirectXMath.h>
#include "../DirectX12Geometory.h"

struct TextureConstantParameter
{
	DirectX::XMMATRIX scaleMatrix;
	DirectX::XMMATRIX positionMatrix;
	int texIndex;
};

class Texture
{
public:
	~Texture() = default;
	Texture();
	Texture(const SIZE& size);
	virtual VertexBuffer& GetVertexBuffer();
	virtual ID3D12Resource*& GetConstantBuffer();
	virtual ID3D12DescriptorHeap*& GetDescriptorHeap();
	virtual ID3D12Resource*& GetResource();
	TextureConstantParameter*& GetConstantParameter();
	virtual void InitConstantMatrix();
	void SetDrawPosition(float x, float y);
	void SetReadTextureIndex(int handle);
protected:

	std::shared_ptr<VertexBuffer> vertexBuffer_;
	Microsoft::WRL::ComPtr<ID3D12Resource> resource_;
	Microsoft::WRL::ComPtr<ID3D12DescriptorHeap> descriptorHeap_;
	Microsoft::WRL::ComPtr<ID3D12Resource> constantBuffer_;
	TextureConstantParameter* constantParameter_;
};


#pragma once
#include <wrl.h>
#include <d3d12.h>
#include <array>
#include <memory>
#include "Texture.h"
#include "../DirectX12Geometory.h"

struct ShaderDetailed;
struct VertexBuffer;

class TextureFactory;

class Texture2D : public Texture
{
	friend TextureFactory;
public:
	~Texture2D();
	Texture2D(const SIZE& size);
};


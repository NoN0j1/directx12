#include "Texture.h"
#include "../../Application.h"

using namespace std;


VertexBuffer& Texture::GetVertexBuffer()
{
	return *vertexBuffer_;
}

ID3D12Resource*& Texture::GetConstantBuffer()
{
	return *constantBuffer_.GetAddressOf();
}

ID3D12DescriptorHeap*& Texture::GetDescriptorHeap()
{
	return *descriptorHeap_.GetAddressOf();
}

ID3D12Resource*& Texture::GetResource()
{
	return *resource_.GetAddressOf();
}

TextureConstantParameter*& Texture::GetConstantParameter()
{
	return constantParameter_;
}

void Texture::InitConstantMatrix()
{
	constantParameter_->scaleMatrix = DirectX::XMMatrixIdentity();
	auto& wsize = Application::Instance().GetWindowSize();
	// w = wsize.w/2, h = wsize.h/2;
	// 1.0f/w , 0, 0, 0
	// 0, -1.0f/h, 0, 0
	// 0, 0, 1, 0,
	// -1, 1, 0, 1
	constantParameter_->scaleMatrix.r[0].m128_f32[0] = 1.0f / (wsize.cx/2);
	constantParameter_->scaleMatrix.r[1].m128_f32[1] = -1.0f / (wsize.cy/2);
	constantParameter_->scaleMatrix.r[3].m128_f32[0] = -1.0f;
	constantParameter_->scaleMatrix.r[3].m128_f32[1] = 1.0f;

	constantParameter_->positionMatrix = DirectX::XMMatrixIdentity();
}

void Texture::SetDrawPosition(float x, float y)
{
	constantParameter_->positionMatrix.r[3].m128_f32[0] = x;
	constantParameter_->positionMatrix.r[3].m128_f32[1] = y;
}

void Texture::SetReadTextureIndex(int handle)
{
	constantParameter_->texIndex = handle;
}

Texture::Texture()
{
}

Texture::Texture(const SIZE& size)
{
	vertexBuffer_ = make_shared<VertexBuffer>();
	constantParameter_ = new TextureConstantParameter();
	auto& wsize = Application::Instance().GetWindowSize();
	vertexBuffer_->vertices.push_back(Vertex({ 0.0f, 0.0f, 0.0f }, { 0.0f, 0.0f }));
	vertexBuffer_->vertices.push_back(Vertex({ static_cast<float>(size.cx), 0.0f, 0.0f }, { 1.0f, 0.0f }));
	vertexBuffer_->vertices.push_back(Vertex({ 0.0f, static_cast<float>(size.cy), 0.0f }, { 0.0f, 1.0f }));
	vertexBuffer_->vertices.push_back(Vertex({ static_cast<float>(size.cx), static_cast<float>(size.cy), 0.0f }, { 1.0f, 1.0f }));
}

#pragma once
#include <wrl.h>
#include <d3d12.h>
#include <array>
#include <vector>
#include <string>
#include <DirectXMath.h>

class Dx12Wrapper;
class Texture2D;
struct TextureParameter;

class TextureFactory
{
public:
	~TextureFactory();
	TextureFactory(Dx12Wrapper& dx12);
	TextureParameter LoadTextureFromFile(const std::wstring& fileName);
	void SetDrawTextureCommands(ID3D12GraphicsCommandList5*& cmdList);
	const D3D12_GRAPHICS_PIPELINE_STATE_DESC& GetPipelineDesc();
	std::shared_ptr<Texture2D>& GetTexture(const std::wstring& path, const SIZE& size);
	void Release();
private:
	Dx12Wrapper& wrapper_;
	std::unordered_map<std::wstring, std::shared_ptr<Texture2D>> textureTable_;
	D3D12_GRAPHICS_PIPELINE_STATE_DESC pDesc_;
	std::array<D3D12_INPUT_ELEMENT_DESC, 3> inputDesc_;
	Microsoft::WRL::ComPtr<ID3D12RootSignature> rootSignature_;
	Microsoft::WRL::ComPtr<ID3D12PipelineState> plState_;
};


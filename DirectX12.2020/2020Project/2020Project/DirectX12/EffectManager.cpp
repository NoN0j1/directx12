#include "EffectManager.h"
#include "ParticalEffect.h"
#include "../Application.h"
#include "Camera.h"
#include "Effekseer/Effekseer.h"

#ifdef _DEBUG
#pragma comment(lib, "Effekseerd.lib")
#pragma comment(lib, "EffekseerRendererDX12d.lib")
#pragma comment(lib, "LLGId.lib")
#else
#pragma comment(lib, "Effekseer.lib")
#pragma comment(lib, "EffekseerRendererDX12.lib")
#pragma comment(lib, "LLGI.lib")
#endif

bool EffectManager::Init(Microsoft::WRL::ComPtr<ID3D12Device6> device, Microsoft::WRL::ComPtr<ID3D12CommandQueue> cmdQueue, std::shared_ptr<Camera> usingCamera)
{
	cmdQueue_ = cmdQueue;
	camera_ = usingCamera;
	// Create a renderer of effects
	// エフェクトのレンダラーの作成
	auto format = DXGI_FORMAT_R8G8B8A8_UNORM;
	renderer_ = ::EffekseerRendererDX12::Create(
		device.Get(),		//デバイス
		cmdQueue_.Get(),	//コマンドキュー
		3,					//スワップチェイン数
		&format,			//レンダーターゲットフォーマット
		1,					//レンダーターゲット数
		DXGI_FORMAT_UNKNOWN,//深度フォーマット
		false,				//深度を逆転させるかどうか
		8000				//パーティクルの最大数
	);

	// Create a memory pool
	// メモリプールの作成
	memoryPool_ = EffekseerRendererDX12::CreateSingleFrameMemoryPool(renderer_.Get());

	// Create a command list
	// コマンドリストの作成
	efkCmdList_ = EffekseerRendererDX12::CreateCommandList(renderer_.Get(), memoryPool_.Get());

	// Create a manager of effects
	// エフェクトのマネージャーの作成
	effekseerManager_ = ::Effekseer::Manager::Create(8000);

	// Sprcify rendering modules
	// 描画モジュールの設定
	effekseerManager_->SetSpriteRenderer(renderer_->CreateSpriteRenderer());
	effekseerManager_->SetRibbonRenderer(renderer_->CreateRibbonRenderer());
	effekseerManager_->SetRingRenderer(renderer_->CreateRingRenderer());
	effekseerManager_->SetTrackRenderer(renderer_->CreateTrackRenderer());
	effekseerManager_->SetModelRenderer(renderer_->CreateModelRenderer());

	// テクスチャ、モデル、マテリアルローダーの設定する。
	// ユーザーが独自で拡張できる。現在はファイルから読み込んでいる。
	effekseerManager_->SetTextureLoader(renderer_->CreateTextureLoader());
	effekseerManager_->SetModelLoader(renderer_->CreateModelLoader());
	effekseerManager_->SetMaterialLoader(renderer_->CreateMaterialLoader());
    return true;
}

void EffectManager::Update()
{
	if (playingEffects_.empty())
	{
		return;
	}
	std::erase_if(playingEffects_, [this](const std::shared_ptr<ParticleEffect>& ef)
		{
			bool fin = ef->GetFinish();
			if (fin)
			{
				effekseerManager_->StopEffect(ef->GetHandle());
			}
			return fin; 
		});

	for (auto& e : playingEffects_)
	{
		e->Update();
	}

	auto& wsize = Application::Instance().GetWindowSize();

	// 投影行列を設定
	renderer_->SetProjectionMatrix(::Effekseer::Matrix44().PerspectiveFovLH(
		camera_->GetFov(),
		(float)wsize.cx / (float)wsize.cy,
		1.0f,
		camera_->GetCameraViewRange()));

	// カメラ行列を設定
	auto& cameraEye = camera_->GetEye();
	auto& cameraUp = camera_->GetUpVector();
	auto& cameraTarget = camera_->GetTarget();
	Effekseer::Vector3D eye(cameraEye.x, cameraEye.y, cameraEye.z);
	Effekseer::Vector3D up(cameraUp.x, cameraUp.y, cameraUp.z);
	Effekseer::Vector3D target(cameraTarget.x, cameraTarget.y, cameraTarget.z);
	renderer_->SetCameraMatrix(::Effekseer::Matrix44().LookAtLH(eye, target, up));
	effekseerManager_->Update();
}

void EffectManager::Draw(ID3D12GraphicsCommandList5* cmdList)
{
	if (playingEffects_.empty())
	{
		return;
	}
	memoryPool_->NewFrame();
	EffekseerRendererDX12::BeginCommandList(efkCmdList_.Get(), cmdList);
	renderer_->SetCommandList(efkCmdList_.Get());

	//エフェクトの描画開始処理を行う。
	renderer_->BeginRendering();
	//エフェクトの描画を行う。
	effekseerManager_->Draw();
	//エフェクトの描画終了処理を行う。
	renderer_->EndRendering();
	//コマンドリストを終了する。
	renderer_->SetCommandList(nullptr);
	EffekseerRendererDX12::EndCommandList(efkCmdList_.Get());
}

bool EffectManager::LoadEffect(const char16_t* path, const std::wstring& name)
{
	if (loadEffectPath_.contains(path))
	{
		//読み込み済み
		return false;
	}

	// エフェクトの読込
	auto effect = Effekseer::Effect::Create(effekseerManager_.Get(), path, 3.0f);
	loadEffects_[name] = std::make_shared<Effekseer::Effect*>(effect);
	return true;
}

EffectManager::~EffectManager()
{
	// Release effects
	// エフェクトの解放
	for (auto& e : loadEffects_)
	{
		ES_SAFE_RELEASE(*e.second);
	}

	// Dispose the manager
	// マネージャーの破棄
	ES_SAFE_RELEASE(*effekseerManager_.GetAddressOf());


	ES_SAFE_RELEASE(*memoryPool_.GetAddressOf());
	ES_SAFE_RELEASE(*efkCmdList_.GetAddressOf());

	// Dispose the renderer
	// レンダラーの破棄
	ES_SAFE_RELEASE(*renderer_.GetAddressOf());
}

const std::shared_ptr<ParticleEffect>& EffectManager::PlayEffect(const std::wstring& name, const DirectX::XMFLOAT3& pos, float scale)
{
	if (loadEffects_.contains(name) == false)
	{
		return nullptr;
	}
	auto& effect = loadEffects_[name];
	auto handle = effekseerManager_->Play(*effect, pos.x, pos.y, pos.z);
	playingEffects_.push_back(std::make_shared<ParticleEffect>(handle));
	effekseerManager_->SetScale(handle, scale, scale, scale);
	return playingEffects_.back();
}

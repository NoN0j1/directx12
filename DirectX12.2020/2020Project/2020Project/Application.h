#pragma once
#include <Windows.h>
#include <memory>
#include <vector>

class Dx12Wrapper;
class Texture2D;
class Actor;

/// <summary>
/// <para>シングルトン</para>
/// <para>アプリケーションの本体クラス</para>
/// </summary>
class Application
{
public:
	/// <summary>
	/// シングルトンのインスタンスを返す
	/// </summary>
	/// <returns>インスタンスオブジェクト</returns>
	static Application& Instance();

	/// <summary>
	/// 画面の初期化等を行う
	/// </summary>
	/// <returns>true = 成功、false = 失敗</returns>
	bool Init();

	/// <summary>
	/// アプリケーション本体を実行している関数
	/// </summary>
	void Run();

	/// <summary>
	/// 終了時の後処理を行う
	/// </summary>
	void Terminate();
	~Application();

	/// <summary>
	/// アプリケーションの画面サイズを取得する
	/// </summary>
	/// <returns>画面サイズ</returns>
	const SIZE GetWindowSize()const;
	
private:
	Application();
	Application(const Application&) = delete;
	void operator=(const Application&) = delete;

	/// <summary>
	/// プリミティブ用ベースシェーダを登録する
	/// </summary>
	void LoadPrimitiveShader();

	/// <summary>
	/// PMD用ベースシェーダを登録する
	/// </summary>
	void LoadPMDShader();

	/// <summary>
	/// PMX用ベースシェーダを登録する
	/// </summary>
	void LoadPMXShader();

	/// <summary>
	/// 2Dテクスチャ用ベースシェーダを登録する
	/// </summary>
	void LoadTextureShader();

	/// <summary>
	/// ポストプロセス用シェーダを登録する
	/// </summary>
	void LoadPostProcessShader();

	/// <summary>
	/// 縮小バッファ用シェーダを登録する
	/// </summary>
	void LoadShrinkShader();

	/// <summary>
	/// SSAO用シェーダを登録する
	/// </summary>
	void LoadSSAOShader();

	/// <summary>
	/// 演算シェーダを登録する
	/// </summary>
	void LoadComputeShader();

	/// <summary>
	/// モデルを切り替える
	/// </summary>
	void ChangeModel();

	/// <summary>
	/// 画面サイズを格納
	/// </summary>
	SIZE windowSize_ = {};

	/// <summary>
	/// windowsから使用許可を貰っている事を格納
	/// </summary>
	WNDCLASSEX wclass_;

	/// <summary>
	/// windowsアプリの許可ハンドル
	/// </summary>
	HWND windowHundle_;

	/// <summary>
	/// DirectX12の本体
	/// </summary>
	std::unique_ptr<Dx12Wrapper> dx12wrapper_;

	/// <summary>
	/// windowsからの要求命令を受ける関数
	/// </summary>
	/// <param name="msg">要求内容</param>
	/// <returns>1 = 特に何もなし、-1 = アプリを終了させる</returns>
	int WindowsOrder(MSG& msg);
};
#pragma once
#include <windows.h>
#include <wrl.h>
#include <d3d12.h>
#include <DirectXMath.h>

class Dx12Wrapper;

struct DebugEditorParameter
{
	DirectX::XMFLOAT3* CameraEye;
	DirectX::XMFLOAT3* CameraTarget;
	DirectX::XMFLOAT3* CameraRotate;
	DirectX::XMFLOAT3* LightDirection;
	DirectX::XMFLOAT3* LightAttenuation;
	DirectX::XMFLOAT3* LightColor;
};

class DebugEditor
{
public:
	DebugEditor(HWND hwnd, Microsoft::WRL::ComPtr<ID3D12Device6> dev);

	void InitBuffer(Dx12Wrapper& dx12Wrapper);
	/// <summary>
	/// ImGuiEditorを描画する
	/// </summary>
	/// <param name="cmdList"></param>
	/// <param name="tableNum">デスクリプタテーブル番号</param>
	void Draw(ID3D12GraphicsCommandList5*& cmdList);

	void SetDebugDescriptorTable(ID3D12GraphicsCommandList5*& cmdList, UINT tableNum);

	void Update();

	/// <summary>
	/// エディタとして値を変更可能なパラメータを取得する
	/// </summary>
	/// <returns></returns>
	DebugEditorParameter*& GetEditorParameter();
private:
	Microsoft::WRL::ComPtr<ID3D12DescriptorHeap> guiHeap_;
	Microsoft::WRL::ComPtr<ID3D12DescriptorHeap> editorParamDescriptor_;
	Microsoft::WRL::ComPtr<ID3D12Resource> editorParamBuffer_;
	DebugEditorParameter* mappedEditorParamter_ = nullptr;
};

